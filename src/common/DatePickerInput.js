import React from 'react'
import { Item, Text } from 'native-base'
import { COLOR, ICON } from '../config/constant'
import DatePicker from 'react-native-datepicker'
import moment from 'moment'
import { View } from 'react-native'

const currentDate = new Date()
const minDate = moment(currentDate).format('YYYY-MM-DD')

const DatePickerInput = ({ input, label, icon, type, secureTextEntry, autoFocus, autoCorrect, autoCapitalize, keyboardType, placeholder, disabled, meta: { touched, error, warning } }) => {
  return (
    <View style={styles.inputWrapper}>
      <Item error={error && touched} style={styles.inputItem}>
        <DatePicker
          style={styles.datePicker}
          date={input.value === '0000-00-00' ? null : input.value}
          mode="date"
          placeholder={placeholder}
          format="YYYY-MM-DD"
          minDate={minDate}
          showIcon={false}
          confirmBtnText="Confirm"
          cancelBtnText="Cancel"
          customStyles={{
            dateInput: {
              borderColor: 'transparent',
              alignItems: 'flex-start'
            },
            placeholderText: {
              color: '#ABB4C5',
              fontSize: 17,
              fontFamily: 'Cabin-Regular'
            },
            dateText: {
              fontSize: 17,
              fontFamily: 'Cabin-Regular',
              color: COLOR.textGray
            }
          }}
          disabled={disabled}
          onDateChange={input.onChange}
        />
      </Item>
    </View>
  )
}

const styles = {
  inputWrapper: {
    marginBottom: 7
  },
  inputItem: {
    marginBottom: 0,
    marginLeft: 0
  },
  footer: {
    backgroundColor: '#FFFFFF'
  },
  datePicker: {
    paddingTop: 4,
    paddingBottom: 4,
    paddingLeft: 2,
    flex: 1,
    marginBottom: 0,
    alignItems: 'flex-start'
  },
  errorText: {
    color: COLOR.primary
  }
}

export { DatePickerInput }
