import React, { Component } from 'react'
import { Input, Icon, Item, Card, CardItem, Body, Text } from 'native-base'
import { DIMENS, COLOR } from '../config/constant'
import { Image, View, StyleSheet, TouchableOpacity } from 'react-native'
import ImagePicker from 'react-native-image-crop-picker'
// import FastImage from 'react-native-fast-image'

class ImageInput extends Component {
	openPicker = () => {
		ImagePicker.openCamera({
			width: parseInt(width),
			height: parseInt(height),
			cropping: true,
		}).then(res => {
			this.props.input.onChange(res)
		});
	}
	render() {
		const { input, label, icon, type, secureTextEntry, autoFocus, autoCorrect, autoCapitalize, keyboardType, placeholder, disabled, meta: { touched, error, warning } } = this.props
		return (
			<TouchableOpacity onPress={() => this.openPicker()} >
				<View style={styles.cardItem}>
					{
						input.value ? 
							<View style={styles.cardImageWrapper}><Image resizeMode='cover' source={{uri: input.value.path || input.value}} style={styles.cardImage}/></View>
						: <Text style={styles.placeholder}>{placeholder}</Text>

					}
				</View>
			</TouchableOpacity>
		);
	}
}
const diameter = 100

const styles = StyleSheet.create({
  cardWrapper: {
    flex: 1,
		borderRadius: 7,
		// borderColor: '#BFC7D5',
		marginBottom: 20
	},
	cardItem: {
		// flex: 1,
    height: diameter,
		width: diameter,
		borderRadius: diameter / 2,
    // flexDirection: "column"
	},
	placeholder: {
		fontFamily: 'Cabin-Bold'
	},
  cardText: {
    fontSize: 20
	},
	cardImageWrapper: {
		borderRadius: diameter / 2,
		height: diameter,
		width: diameter,
		overflow: 'hidden'
	},
	cardImage: {
		height: diameter,
    width: diameter,
		flex: 1,
		borderRadius: 2,
		// zIndex: 999,
    // position: "absolute",
    // top: 0,
    // left: 0,
    // bottom: 0,
    // right: 0,
  },
});

export { ImageInput }
