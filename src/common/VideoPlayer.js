import React, { Component } from "react";
import { Icon, Button } from "native-base";
import { View } from "react-native";
import Video from "react-native-video";
import { DIMENS, COLOR } from '../config/constant'

class VideoPlayer extends Component {
	constructor(props) {
		super(props)
		this.state = {
			paused: true,
			media: props.media
		}
		this.onVideoLoad = this.onVideoLoad.bind(this)
		this.onVideoEnd = this.onVideoEnd.bind(this)
	}
	onVideoLoad = () => {
		this.player.seek(0)
	}
	onVideoEnd = () => {
		this.player.seek(0)
		this.setState({
			paused: true
		})
	}
	render() {
		const { paused, media } = this.state
		console.log(media)
		return (
			<View style={styles.videoContainer}>
				<Video 
					ref={(ref) => {
						this.player = ref
					}}
					paused={paused}
					onLoad={this.onVideoLoad}
					onEnd={this.onVideoEnd}
					resizeMode="cover"
					source={{uri: media}}
					style={{height: DIMENS.screenWidth, width: DIMENS.screenWidth}}	
				/>
				{ paused ? <Button transparent style={styles.playButton} onPress={() => this.setState({ paused: !paused })} ><Icon name='play' style={styles.playIcon}/></Button> : null }
			</View>
		)
	}
	
}

const styles = {
	videoContainer: {
		alignItems: 'center',
		justifyContent: 'center'
	},
	playButton: {
		zIndex: 999,
		position: 'absolute',
		marginLeft: (DIMENS.screenWidth / 2) - 20,
		width: 50,
		height: 50,
		alignItems: 'center',
		justifyContent: 'center',
		backgroundColor: '#FFFFFF',
		opacity: .2
	},
	playIcon: {
		color: '#FFFFFF'
	}
};


export { VideoPlayer }
