import React from 'react'
import { Input, Icon, Item } from 'native-base'

const TextInput = ({ input, label, icon, type, secureTextEntry, autoFocus, disabled, autoCorrect, autoCapitalize, keyboardType, placeholder, meta: { touched, error, warning } }) => {
	return (
		<Item error={error && touched}>
			<Icon active name={icon} />
			<Input
				placeholder={placeholder}
				secureTextEntry={secureTextEntry}
				autoFocus={autoFocus}
				autoCorrect={autoCorrect}
				autoCapitalize={autoCapitalize}
				keyboardType={keyboardType}
				disabled={disabled}
				{...input}
			/>
		</Item>
	);
}

export { TextInput }