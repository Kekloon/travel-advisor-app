import React from "react";
import { Card, CardItem, Text } from "native-base";
import { Image } from "react-native";
import { DIMENS, COLOR } from "../config/constant";
// import { cardBackgroundColor, getImageURL, cardTextColor, getCardHeight } from "../config/helper";
import _ from "lodash";

const MemberCard = ({ card, onPress }) => {
	return (
		<Card style={styles.cardWrapper} onPress={onPress}>
			<CardItem cardBody button style={{
				backgroundColor: cardBackgroundColor(card),
				flex: 1,
				borderRadius: 8,
				alignItems: "center",
				justifyContent: "center",
				flexDirection: "column",
				position: "relative",
			}} >
				{
					card.merchant ?
						card.merchant.merchantLogo ?
							<Image resizeMode="contain" style={styles.cardImage} source={{uri: getImageURL(card.merchant.merchantLogo)}}/>
						:
							<Text style={_.merge({color: cardTextColor(cardBackgroundColor(card))}, styles.cardText)}>{card.merchant.merchantName}</Text>
					:
						<Text style={_.merge({color: cardTextColor(cardBackgroundColor(card))}, styles.cardText)}>{card.brand}</Text>
				}
			</CardItem>
		</Card>
	);
}

const cardWidth = DIMENS.screenWidth / 2 - 30;
const cardHeight = getCardHeight(cardWidth);

const styles = {
  cardWrapper: {
    marginLeft: 10,
    marginRight: 10,
    shadowColor: "#000000",
    flex: 0,
    height: cardHeight,
    width: cardWidth,
    borderRadius: 10,
    borderColor: COLOR.grayLight
	},
	cardItem: {
    backgroundColor: COLOR.primary,
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    flexDirection: "column"
  },
  cardText: {
    fontSize: 20
	},
	cardImage: {
    position: "absolute",
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
  },
};
export { MemberCard };
