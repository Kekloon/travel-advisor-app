import React from 'react'
import { Picker, Item, Text, Icon } from 'native-base'
import { Platform, View } from 'react-native'
import { COLOR } from '../config/constant'

const PickerInput = ({ input, label, icon, type, prompt, placeholder, children, iosHeader, meta: { touched, error, warning } }) => {
  const onValueChange = (value) => {
    input.onChange(value)
  }
  return (
    <View style={styles.inputWrapper}>
      <Item error={error && touched} style={styles.inputItem}>
				<Icon active name={icon} />
        {
          Platform.OS === 'android'
            ? !input.value
              ? <Text style={styles.placeholder}>{placeholder}</Text>
              : <Text style={styles.pickerText}>{input.value}</Text>
            : null
        }
        <Picker
          style={Platform.OS === 'ios' ? styles.iosPicker : styles.picker}
          itemStyle={styles.pickerItem}
          itemTextStyle={styles.pickerItemText}
          iosHeader={iosHeader}
          prompt={prompt}
          mode='dialog'
          placeholder={placeholder}
          selectedValue={input.value}
          onValueChange={onValueChange}
        >
          {children}
        </Picker>
      </Item>
      { error && touched ? <Text style={styles.errorText}>{error}</Text> : null }
    </View>
  )
}

const styles = {
  pickerContainer: {
    flex: 1
  },
  inputWrapper: {
    marginBottom: 7
  },
  inputItem: {
    marginBottom: 10,
    marginLeft: 0
  },
  placeholder: {
    color: COLOR.placeholderGray,
    position: 'absolute',
    top: 11,
    fontSize: 17,
    left: 34
  },
  picker: {
    width: 200,
    paddingTop: 0,
    paddingBottom: 0,
    color: 'transparent',
    paddingLeft: 0,
    backgroundColor: 'transparent',
    paddingRight: 0
  },
  pickerText: {
    color: COLOR.textGray,
    fontFamily: 'Cabin-Regular',
    position: 'absolute',
    top: 11,
    fontSize: 17,
    left: 34
  },
  pickerItemText: {
    color: COLOR.textGray,
    fontFamily: 'Cabin-Regular',
    fontWeight: '300',
    paddingLeft: 0,
    paddingRight: 0
  },
  iosPicker: {
    width: 200,
    paddingTop: 0,
    paddingBottom: 0,
    // paddingLeft: 7,
    backgroundColor: 'transparent',
    paddingLeft: 0,
    paddingRight: 0
  },
  errorIcon: {
    right: 0,
    position: 'absolute'
  },
  errorText: {
    color: COLOR.primary
  }
}

export { PickerInput }
