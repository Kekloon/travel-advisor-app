import React, { Component } from "react";
import { View } from 'react-native'
import { Container, Content, List, ListItem, Text, Input, Form, Item, Icon } from 'native-base'
import { COLOR } from '../config/constant'
import _ from "lodash";
import axios from 'axios'
import RNGooglePlaces from 'react-native-google-places'

class GooglePlacesAutoComplete extends Component {
	constructor(props) {
		super(props)
		this.state = {
			places: [],
			defaultPlaces: [],
			filteredPlaces: [],
			position: null
		}
		this.onPress = this.onPress.bind(this)
	}
	componentDidMount () {
		let places

		navigator.geolocation.getCurrentPosition( async (position) => {
			this.setState({ position }, async () => {
				const { position } = this.state
				try {
					places = await axios.get(`https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=${_.toString(position.coords.latitude)},${_.toString(position.coords.longitude)}&radius=500&type=restaurant&key=AIzaSyBKMRtTy120nM63lBN0SccqGSVsEJakoTc`)
				} catch(e) {
					console.log(e)
				}
				this.setState({
					places: _filterPlacesArray(places.data.results),
					defaultPlaces: _filterPlacesArray(places.data.results)
				})
			})
		})
	}

	onChange = async (text) => {
		if(text || text !== '') {
			let places
			navigator.geolocation.getCurrentPosition( async (position) => {
				this.setState({ position }, async () => {
					const { position } = this.state
					try {
						places = await RNGooglePlaces.getAutocompletePredictions(text, {
							latitude: position.coords.latitude,
							longitude: position.coords.longitude,
							radius: 500
						})
					} catch(e) {
			
					}
					this.setState({
						places: _filterPlacesSearchArray(places)
					})
				})
			})
		} else {
			this.setState({
				places: this.state.defaultPlaces
			})
		}
		
		
	}

	onPress = async (placeId) => {
		let place
		try {
			place = await RNGooglePlaces.lookUpPlaceByID(placeId)
		} catch(e) {

		}
		this.props.onPressPlaceItem(place)
	}

	
	render() {
		const { places } = this.state
		return (
			<Container style={styles.container}>
				<Form>
					<Item>
						<Icon active name='search' />
						<Input onChangeText={this.onChange.bind(this)} placeholder='Find places...'></Input>
					</Item>
				</Form>
				<Content>
					<List
						dataArray={places}
						renderRow={(item) =>
							<ListItem button onPress={() => this.onPress(item.placeId)}>
								<Text>{item.name}</Text>
							</ListItem>
						}
					>
					</List>
				</Content>
			</Container>
		);
	}
}

const _renderPlaceItem = (item) => {
	return (
		<ListItem>
			<Text>{item.name}</Text>
		</ListItem>
	)
}

const _filterPlacesArray = (places) => {
	return _.map(places, (obj) => ({
		name: obj.name,
		placeId: obj.place_id
	}))
}

const _filterPlacesSearchArray = (places) => {
	return _.map(places, (obj) => ({
		name: obj.primaryText,
		placeId: obj.placeID
	}))
}

const styles = {
  container: {
		backgroundColor: '#FFFFFF',
		flex: 1
	},
	// cardItem: {
  //   backgroundColor: COLOR.primary,
  //   flex: 1,
  //   alignItems: "center",
  //   justifyContent: "center",
  //   flexDirection: "column"
  // },
  // cardText: {
  //   fontSize: 20
	// },
	// cardImage: {
  //   position: "absolute",
  //   top: 0,
  //   left: 0,
  //   bottom: 0,
  //   right: 0,
  // },
};
export { GooglePlacesAutoComplete };
