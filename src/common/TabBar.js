import React, { Component } from 'react';
import { Icon } from 'native-base'
import { Animated, StyleSheet, TouchableNativeFeedback, TouchableWithoutFeedback, Platform, Text, View, StatusBar } from 'react-native';
import { COLOR, DIMENS, ICON } from "../config/constant"
import Color from 'color'
import _ from 'lodash'

const Button = Platform.OS === 'ios' ? TouchableWithoutFeedback : TouchableNativeFeedback

const styles = StyleSheet.create({
  tabbarWrapper: {
    height: 66,
    justifyContent: 'flex-end',
    alignItems: 'center',
    backgroundColor: "transparent",
    zIndex: 1,
    paddingBottom: 0,
    bottom: 0,
    width: DIMENS.screenWidth,
    position: 'absolute'
  },
  tabbar: {
    height: 49,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: "#FFFFFF",
    zIndex: 1,
    position: 'relative'
  },
  tab: {
    alignSelf: 'stretch',
    flex: 1,
    alignItems: 'center',
    // borderRadius: 30,
    justifyContent: 'center'
	},
	icon: {
    flexGrow: 1,
  },
});

class TabBar extends Component {

  state = {
    statusBarColor: Color('#FF7D7D').darken(0.05)
  }

  render() {
    const {
      navigation,
      renderIcon,
      activeTintColor,
      inactiveTintColor,
      jumpToIndex
    } = this.props;

    const {
      routes
    } = navigation.state;

    const onClickTabItem = (index) => {
      let statusBarColor
      jumpToIndex(index)
      if(index === 0 ) {
        statusBarColor = Color('#FF7D7D').darken(0.1)
      } else if(index === 1 ) {
        statusBarColor = Color('#a5a4fa').darken(0.05)
      } else if(index === 2 ) {
        statusBarColor = Color('#FF8D79').darken(0.05)
      } else if(index === 3 ) {
        statusBarColor = Color('#76CEDE').darken(0.05)
      }

      this.setState({
        statusBarColor
      })

    }

    const renderButton = (route, index) => {

      const focused = index === navigation.state.index;
      const tintColor = focused ? activeTintColor : inactiveTintColor;
      return (
        <Button
          key={route.key}
          style={styles.tab}
          onPress={() => onClickTabItem(index)}
          rippleColor={Platform.OS === 'ios' ? null : COLOR.primary}
          rippleCentered
        >
          <Animated.View style={styles.tab}>
            {renderIcon({
              route,
              index,
              focused,
              tintColor
            })}
          </Animated.View>
        </Button>
      )
    }

    return (
      <Animated.View style={styles.tabbarWrapper}>
        {/* <StatusBar backgroundColor={this.state.statusBarColor}/> */}
        <Animated.View style={styles.tabbar}>
          {
            routes && routes.map((route, index) => {
              if(index === 0) {
                return renderButton(route, index)
              }
            })
          }
          <View style={_.merge(styles.tab, {marginLeft: 45, marginRight: 45})}></View>
          {
            routes && routes.map((route, index) => {
              if(index === 2 || index === 3) {
                return renderButton(route, index)
              }
            })
          }
        </Animated.View>
        <Animated.View 
          style={{
            backgroundColor: '#ffffff',
            bottom: 0,
            zIndex: 999,
            padding: 8,
            borderRadius: 33,
            overflow: 'visible',
            position: 'absolute',
          }}
        >
          <Button
            style={{
              
            }}
            onPress={() => navigation.navigate('Publish')}
            // rippleColor={Platform.OS === 'ios' ? null : '#FFFFFF'}
            // rippleCentered
          >
            <Animated.View 
              style={{
                backgroundColor: COLOR.primary,
                alignSelf: 'stretch',
                width:50,
                height: 50,
                alignItems: 'center',
                borderRadius:25,
                justifyContent: 'center',
                
              }}>
              <Icon name='add' style={{color: '#FFF'}}/>
            </Animated.View>
          </Button>
        </Animated.View>
      </Animated.View>
    );
  }
};

export { TabBar };