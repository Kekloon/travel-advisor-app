import React, { Component } from 'react'
import { Image, Dimensions } from 'react-native'
import PropTypes from 'prop-types'
import { View } from 'native-base'
import AutoHeightImage from 'react-native-auto-height-image'

const { width, height } = Dimensions.get('window')

class ActualImageComponent extends Component {
    constructor(props) {
        super(props)
        this.state = {
            width: 0,
            height: 0,
        }
        this.getImageActualSize = this.getImageActualSize.bind(this)
    }

    componentDidMount() {
        this.getImageActualSize(this.props.imageUrl)
    }

    getImageActualSize(imageUrl) {
        Image.getSize(imageUrl, (w, h) => this.setState({ width: w, height: h }))
    }

    render() {
        return (
            this.state.width > 0 && this.state.height > 0 ?
                <AutoHeightImage
                    source={{ uri: this.props.imageUrl }}
                    resizeMode='contain'
                    width={this.state.width > width ? width - 35 : this.state.width - 35}
                />
                : null
        )
    }
}

ActualImageComponent.propTypes = {
    imageUrl: PropTypes.string
}

export default ActualImageComponent