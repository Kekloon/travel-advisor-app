import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { TouchableHighlight, StyleSheet } from 'react-native'
import { Header, View, Icon, Text } from 'native-base'

class HeaderComponent extends Component {
    render() {
        return (
            <Header style={styles.header}>
                <View style={styles.buttonContainer}>
                    <TouchableHighlight style={styles.touch}
                        underlayColor='#D5D8DC'
                        onPress={this.props.onPress}
                    >
                        <Icon name='ios-arrow-dropleft' />
                    </TouchableHighlight>
                </View>
                <View style={{ flex: 1 }}>
                    <Text style={styles.title}>{this.props.title ? this.props.title : ''}</Text>
                </View>
            </Header>
        )
    }
}

HeaderComponent.propTypes = {
    title: PropTypes.string,
    onPress: PropTypes.func
}

const styles = StyleSheet.create({
    header: {
        backgroundColor: '#EEEEEE',
        alignItems: 'center'
    },
    buttonContainer: {
        position: 'absolute',
        left: 1
    },
    touch: {
        marginLeft: 15,
        borderRadius: 50 / 2
    },
    title: {
        alignSelf: 'center'
    }
})

export default HeaderComponent