import React from "react";
import { Input, Icon, Item } from "native-base";
import StarRating from "react-native-star-rating";
import _ from 'lodash'

const StarRatingInput = ({ input, label, icon, type, secureTextEntry, autoFocus, disabled, autoCorrect, autoCapitalize, keyboardType, placeholder, meta: { touched, error, warning } }) => {
	return (
		<Item error={error && touched}>
			<StarRating
				disabled={disabled}
				maxStars={5}
				rating={_.toInteger(input.value)}
				starSize={20} 
				starColor={'yellow'}
				selectedStar={input.onChange}
			/>
		</Item>
	);
}

export { StarRatingInput }