import React from "react";
import { Animated, Easing } from "react-native";
import { StackNavigator, DrawerNavigator, TabNavigator } from "react-navigation";
import WallPosts from "./container/WallPostsContainer";
import CreatePost from "./container/CreatePostContainer";
import EditPost from './container/EditPostContainer'
import CameraModal from "./container/CameraModalContainer";
import ConfirmSnapshot from "./container/ConfirmSnapshotContainer";
import ChatRoom from './container/ChatRoomContainer'
import SearchPosts from './container/SearchPostsContainer'
import { Icon } from "native-base";

export const CreatePostNavigator = StackNavigator({
  CameraModal: {
		screen: CameraModal,
	},
	ConfirmSnapshot: {
		screen: ConfirmSnapshot
	},
	CreatePost: {
		screen: CreatePost
	},
},
{
	initialRouteName: "CameraModal",
	headerMode: "none",
	mode: "modal",
	transitionConfig : () => ({
		transitionSpec: {
			duration: 0,
			timing: Animated.timing,
			easing: Easing.step0,
		},
	}),
});

export const PostsNavigator = StackNavigator({
  WallPosts: {
		screen: WallPosts,
	},
	SearchPosts: {
		screen: SearchPosts
	},
	ChatRoom: {
		screen: ChatRoom
	},
},
{
	initialRouteName: "WallPosts",
	headerMode: "none",
	navigationOptions: {
		title: '',
		tabBarIcon: ({ tintColor }) => (
			<Icon name="home" style={{ color: tintColor }}/>
		),
	}
});