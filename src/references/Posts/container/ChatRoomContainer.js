import { connect } from "react-redux";
import ChatRoom from "../component/ChatRoom";
import { createTripsRequest } from "../../Trips/TripsActions";

const mapStateToProps = (state) => ({
})

const mapDispatchToProps = (dispatch, props) => {
	const post = props.navigation.state.params.post
	return {
		buyPostPackage: (body) => {
			dispatch(createTripsRequest(body, post))
		}
	}
}

const ChatRoomContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(ChatRoom);

export default ChatRoomContainer;
