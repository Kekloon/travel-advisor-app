import { connect } from "react-redux";
import WallPosts from "../component/WallPosts";
import { getPostsRequest } from "../PostsActions";

const mapStateToProps = (state) => ({
  posts: state.posts.posts.payload,
  isFilter: state.posts.posts.isFilter,
  searchFor: state.posts.posts.searchFor
})

const mapDispatchToProps = (dispatch, props) => ({
  getWallPosts: () => {
    navigator.geolocation.getCurrentPosition((position) => {
      dispatch(getPostsRequest(position))
    })
  }
});

const WallPostsContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(WallPosts);

export default WallPostsContainer;
