import { connect } from "react-redux";
import CreatePost from "../component/CreatePost";
import { createPostRequest } from "../PostsActions";
import { change, formValueSelector } from 'redux-form'

const selector = formValueSelector('PostForm')

const mapStateToProps = (state) => ({
  mediaArr: state.posts.newPost.media,
  place: selector(state, 'place')
})

const mapDispatchToProps = (dispatch, props) => {
  const navigation = props.navigation
  return {
    createPost: (body) => {
      dispatch(createPostRequest(body, navigation));
    },
    changeValue: (field, value) => {
      dispatch(change('PostForm', field, value));
    }
  }
};

const CreatePostContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(CreatePost);

export default CreatePostContainer;
