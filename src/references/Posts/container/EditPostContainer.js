import { connect } from "react-redux";
import EditPost from "../component/EditPost";
import { updatePostRequest } from "../PostsActions";

const mapStateToProps = (state, props) => ({
  initialValues: props.navigation.state.params.post,
  post: props.navigation.state.params.post
})

const mapDispatchToProps = (dispatch, props) => {
  const navigation = props.navigation
  const post = navigation.state.params.post
  return {
    updatePost: (body) => {
      dispatch(updatePostRequest(body, post.postId, navigation));
    }
  }
};

const EditPostContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(EditPost);

export default EditPostContainer;
