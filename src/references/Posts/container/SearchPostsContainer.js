import { connect } from "react-redux";
import SearchPosts from "../component/SearchPosts";
import { searchPostsRequest } from "../PostsActions";

const mapStateToProps = (state, props) => ({
})

const mapDispatchToProps = (dispatch, props) => {
  const navigation = props.navigation
  return {
    searchPosts: (keyword) => {
      dispatch(searchPostsRequest(keyword, navigation));
    }
  }
};

const SearchPostsContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(SearchPosts);

export default SearchPostsContainer;
