import { connect } from "react-redux";
import CameraModal from "../component/CameraModal";
import { requestAddPostImages } from '../PostsActions'

const mapStateToProps = (state) => ({
  mediaArr: state.posts.newPost.media
})

const mapDispatchToProps = (dispatch, props) => ({
  addPostImages: (media) => {
    dispatch(requestAddPostImages(media))
  }
});

const CameraModalContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(CameraModal);

export default CameraModalContainer;
