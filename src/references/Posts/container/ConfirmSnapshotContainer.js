import { connect } from "react-redux";
import ConfirmSnapshot from "../component/ConfirmSnapshot";

const mapStateToProps = (state) => ({
  mediaArr: state.posts.newPost.media
})

const mapDispatchToProps = (dispatch, props) => ({
  
});

const ConfirmSnapshotContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(ConfirmSnapshot);

export default ConfirmSnapshotContainer;
