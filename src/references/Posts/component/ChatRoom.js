import React, { Component } from "react";
import { Container, Content, Icon, Button, Text, Thumbnail, Left, Body, Form } from "native-base";
import { Field, reduxForm } from "redux-form";
import { Image, Platform, View, TouchableOpacity } from "react-native";
import { COLOR, DIMENS, GOOGLE_PLACES } from "../../../config/constant";
import { TextInput, DatePickerInput, GooglePlacesAutoComplete, TimePickerInput } from "../../../common";
import axios from 'axios'
import _ from "lodash";
import Camera from "react-native-camera";
import Video from "react-native-video";
import Carousel from 'react-native-snap-carousel'
import Modal from 'react-native-modal'
import { alphaNumeric, maxLength, email, required } from '../../../config/validations'



class ChatRoom extends Component {
	static navigationOptions ={
		tabBarVisible: false,
	};
  buyPackage (values) {
		this.props.buyPostPackage(values)
	}
  render () {
		const { handleSubmit, isSubmitting } = this.props
    return (
      <Container style={styles.container}>
        <Form style={styles.formFields}>
          <Field
            name='date'
            placeholder='When?'
            component={DatePickerInput}
            validate={[required]} />
          <Field
            name='time'
            placeholder='Starting at?'
            component={TimePickerInput}
            validate={[required]} />
          <Field
            name='duration'
            keyboardType='numeric'
            maxLength={2}
            placeholder='Duration?'
            component={TextInput}
            validate={[required]} />
          <Button primary onPress={handleSubmit(this.buyPackage.bind(this))}>
            <Text>Buy</Text>
          </Button>
        </Form>
      </Container>
    )
  }
}

const styles = {
  container: {
    flex: 1,
    backgroundColor: "#ffffff",
  }
};


ChatRoom = reduxForm({
	form: 'PackagePurchaseForm',
})(ChatRoom)

export default ChatRoom;
