import React, { Component } from "react";
import { Card, CardItem, Left, Body, Right, Thumbnail, Text, Icon, Button } from "native-base";
import { Image, View } from "react-native";
import StarRating from "react-native-star-rating";
import Carousel from 'react-native-snap-carousel'
import { VideoPlayer } from '../../../common'
import { DIMENS, COLOR } from '../../../config/constant'
import moment from 'moment'

class PostCard extends Component {
	constructor(props) {
		super(props)
	}
  
  _renderImages ({item, index}) {
		return (
			<View style={styles.slide}>
				<Image source={{uri: item}} style={{height: DIMENS.screenWidth, width: DIMENS.screenWidth}}/>
			</View>
		);
	}
	render() {
		const { post, onPressChat } = this.props
		return (
			<Card style={styles.card}>
				<CardItem>
					<Left>
						<Thumbnail source={{uri: post.user.profilePicture}} />
						<Body>
							<Text>{post.user.firstName}</Text>
							<Text note>{post.place}</Text>
						</Body>
					</Left>
				</CardItem>
				<CardItem cardBody>
					{
						post.mediaType === 'still'
							? <Carousel
									ref={(c) => { this._carousel = c; }}
									data={post.media}
									renderItem={this._renderImages}
									sliderWidth={DIMENS.screenWidth - 30}
									itemWidth={DIMENS.screenWidth}
								/>
							: 
								<VideoPlayer media={post.media} />
					}
				</CardItem>
				<CardItem>
					<StarRating disabled={false} maxStars={5} rating={post.rating} starSize={20} starColor={'yellow'}/>
				</CardItem>
				<CardItem>
					<Body>
						<Button block onPress={onPressChat}>
							<Text>Chat</Text>
						</Button>
					</Body>
				</CardItem>
				<CardItem>
					<Left>
						<Text style={styles.timeAgo}>{moment(post.createdAt).fromNow()}</Text>
					</Left>
				</CardItem>
			</Card>
		)
	}
	
}

const styles = {
  container: {
    flex: 1,
    backgroundColor: "#ffffff",
	},
	card: {
		marginLeft: 0,
		marginRight: 0,
		marginTop: 0,
		marginBottom: 0
	},
	videoContainer: {
		alignItems: 'center',
		justifyContent: 'center'
	},
	playButton: {
		zIndex: 999,
		position: 'absolute',
		marginLeft: (DIMENS.screenWidth / 2) - 20,
		width: 50,
		height: 50,
		alignItems: 'center',
		justifyContent: 'center',
		backgroundColor: '#FFFFFF',
		opacity: .2
	},
	playIcon: {
		color: '#FFFFFF'
	},
	timeAgo: {
		fontSize: 13,
		color: COLOR.gray
	}
};


export default PostCard
