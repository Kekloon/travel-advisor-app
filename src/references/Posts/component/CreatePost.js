import React, { Component } from "react";
import { Container, Content, Icon, Button, Text, Thumbnail, Left, Body, Form, List, ListItem, Item } from "native-base";
import { Field, reduxForm } from "redux-form";
import { Image, Platform, View, TouchableOpacity } from "react-native";
import { COLOR, DIMENS, GOOGLE_PLACES } from "../../../config/constant";
import { TextInput, StarRatingInput, GooglePlacesAutoComplete, VideoPlayer } from "../../../common";
import axios from 'axios'
import _ from "lodash";
import Camera from "react-native-camera";
import Video from "react-native-video";
import Carousel from 'react-native-snap-carousel'
import Modal from 'react-native-modal'

class CreatePost extends Component {
	static navigationOptions = ({navigation}) => {
		const { params = {} } = navigation.state
		return {
			headerTitle: "Create Post",
			tabBarIcon: ({ tintColor }) => (
				<Icon name="add" style={{ color: tintColor }}/>
			),
			tabBarVisible: false,
			headerLeft: (
				<Button transparent onPress={() => navigation.goBack()}>
					<Icon name="arrow-back"></Icon>
				</Button>
			)
		}
	};
	
	state = {
		media: {
			path: null
		},
		mediaArr: [],
		cameraMode: null,
		paused: true,
		isModalVisible: false
	}
  componentDidMount() {
		const { media, cameraMode } = this.props.navigation.state.params
		this.setState ({
			media,
			cameraMode,
			mediaArr: this.props.mediaArr
		}, () => {
			if(this.state.cameraMode === 'video') {
				this.props.changeValue('media', this.state.media)
			} else {
				this.props.changeValue('media', this.state.mediaArr)
			}
			this.props.changeValue('mediaType', this.state.cameraMode)
		})
	}
	
	handleCreatePost = (values) => {
		this.props.createPost(values);
	}

	_renderImages ({item, index}) {
		return (
			<View style={styles.slide}>
				<Image source={{uri: item.path}} style={{height: DIMENS.screenWidth, width: DIMENS.screenWidth}}/>
			</View>
		);
	}

	onPressPlaceItem = (place) => {
		const { name, latitude, longitude } = place
		this.props.changeValue('place', name)
		this.props.changeValue('location', { latitude, longitude })
		this.setState({ isModalVisible: false })
	}

  render () {
		const { media, cameraMode, mediaArr, paused, isModalVisible } = this.state
		const { handleSubmit, place } = this.props;
    return (
			<Container style={styles.container}>
				<Form>
					<Item>
						<Field
							name="status"
							placeholder="Write something..."
							autoFocus={true}
							autoCapitalize="none"
							component={TextInput} 
						/>
					</Item>
					<ListItem button onPress={() => this.setState({isModalVisible: true})}>
						<Text>{place ? place : 'Choose a place'}</Text>
					</ListItem>
				</Form>
				{
					cameraMode === "video" ?
						<VideoPlayer media={media.path} />
					: 
						<Carousel
              ref={(c) => { this._carousel = c; }}
              data={this.state.mediaArr}
              renderItem={this._renderImages}
              sliderWidth={DIMENS.screenWidth}
              itemWidth={DIMENS.screenWidth}
            />
				}
				<Button onPress={handleSubmit(this.handleCreatePost.bind(this))}><Text>Publish</Text></Button>
				<Modal 
					useNativeDriver={true}
					isVisible={isModalVisible}
					onBackButtonPress={() => this.setState({isModalVisible: false})}
					style={{flex: 1, marginLeft: 0, marginRight: 0, marginTop: 0, marginBottom: 0}}
				>
					<GooglePlacesAutoComplete onPressPlaceItem={this.onPressPlaceItem.bind(this)}/>
				</Modal>
      </Container>
    )
  }
}

const styles = {
  container: {
    flex: 1,
    backgroundColor: "#ffffff",
	},
	modal: {
		flex: 1,
		marginLeft: 0,
		marginRight: 0,
		marginTop: 0,
		marginBottom: 0
	}
};

CreatePost = reduxForm({
	form: "PostForm",
})(CreatePost);

export default CreatePost;
