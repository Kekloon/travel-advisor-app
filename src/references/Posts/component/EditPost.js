import React, { Component } from "react";
import { Container, Content, Icon, Button, Text, Thumbnail, Left, Body, Form } from "native-base";
import { Field, reduxForm } from "redux-form";
import { Image, Platform, View, TouchableOpacity } from "react-native";
import { COLOR, DIMENS, GOOGLE_PLACES } from "../../../config/constant";
import { TextInput, StarRatingInput, GooglePlacesAutoComplete } from "../../../common";
import axios from 'axios'
import _ from "lodash";
import Camera from "react-native-camera";
import Video from "react-native-video";
import Carousel from 'react-native-snap-carousel'
import Modal from 'react-native-modal'

class EditPost extends Component {
	static navigationOptions = ({navigation}) => {
		const { params = {} } = navigation.state
		return {
			headerTitle: "Edit Post",
			tabBarIcon: ({ tintColor }) => (
				<Icon name="add" style={{ color: tintColor }}/>
			),
			tabBarVisible: false,
			headerLeft: (
				<Button transparent onPress={() => navigation.goBack()}>
					<Icon name="arrow-back"></Icon>
				</Button>
			)
		}
	};
	constructor(props) {
		super(props)
		this.state = {
			media: {
				path: null
			},
			isMounted: null,
			post: props.post,
			paused: true,
			isModalVisible: false
		}
	}
	componentDidUpdate(prevProps, prevState) {
		if (prevState.isMounted !== this.state.isMounted) {
			setTimeout(() => {
				this.setState({
					// isOpenCamera: true,
					// mediaArr: this.props.mediaArr
				})
			}, 100);

		}
	}
	componentDidMount() {
		this.setState({
			isMounted: true
		})
	}
	handleUpdatePost = (values) => {
		this.props.updatePost(values);
	}

	_renderImages ({item, index}) {
		return (
			<View style={styles.slide}>
				<Image source={{uri: item}} style={{height: DIMENS.screenWidth, width: DIMENS.screenWidth}}/>
			</View>
		);
	}

	onPressPlaceItem = (place) => {
		this.props.changeValue('place', place)
		this.setState({ isModalVisible: false })
	}

  render () {
		const { paused, isModalVisible, isMounted, post: { media, mediaType} } = this.state
		const { handleSubmit, place } = this.props;
    return (
			<Container style={styles.container}>
				<Form>
					<Field
						name="status"
						placeholder="Write something..."
						autoFocus={true}
						autoCapitalize="none"
						component={TextInput} 
					/>
				</Form>
				{
					isMounted ?
					mediaType === "video" ?
							<TouchableOpacity onPress={() => this.setState({ paused: !this.state.paused })} >
								<Video 
									ref={(ref) => {
										this.player = ref
									}}
									paused={paused}
									onLoad={()=>{this.player.seek(0)}}
									resizeMode="contain"
									source={{uri: media.path}}
									style={{height: DIMENS.screenWidth, width: DIMENS.screenWidth}}	
								/>
							</TouchableOpacity>
						: 
							<Carousel
								ref={(c) => { this._carousel = c; }}
								data={media}
								renderItem={this._renderImages}
								sliderWidth={DIMENS.screenWidth -30}
								itemWidth={DIMENS.screenWidth}
							/>
						: null
				}
				<Button onPress={handleSubmit(this.handleUpdatePost.bind(this))}><Text>Publish</Text></Button>
      </Container>
    )
  }
}

const styles = {
  container: {
    flex: 1,
    backgroundColor: "#ffffff",
  }
};

EditPost = reduxForm({
	form: "EditPostForm",
})(EditPost);

export default EditPost;
