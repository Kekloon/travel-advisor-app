import React, { Component } from "react";
import { Container, Content, Button, Text, List, Icon } from "native-base";
import { Image, Platform, View } from "react-native";
import { COLOR, DIMENS } from "../../../config/constant";
import _ from "lodash";
import firebase from 'react-native-firebase'

import PostCard from './PostCard'

class WallPosts extends Component {
  static navigationOptions = ({ navigation }) => {
    return {
      headerTitle: "Hopfy",
      headerRight: (
        <Button transparent onPress={() => navigation.navigate('SearchPosts')}><Icon name='search'/></Button>
      ),
    }
  }
  state = {
    posts: [],
    searchFor: null,
    isFilter: false
  }

  componentDidMount() {
    this.props.getWallPosts();
  }
  componentWillReceiveProps(nextProps) {
    this.setState({
      posts: nextProps.posts,
      isFilter: nextProps.isFilter,
      searchFor: nextProps.searchFor
    })
  }

  render () {
    const { posts, isFilter, searchFor } = this.state
    const { navigation } = this.props
    return (
      <Container style={styles.container}>
        {
          isFilter
            ? <View>
                <Text>Search for: {searchFor}</Text>
                <Button block onPress={() => this.props.getWallPosts()}><Text>Reset</Text></Button>
              </View>
            : null
        }
        <Content>
          <List
            dataArray={posts}
            renderRow={(post) => <PostCard post={post} onPressChat={() => navigation.navigate('ChatRoom', { post })}/> }
          >
          </List>
        </Content>
      </Container>
    )
  }
}

const styles = {
  container: {
    flex: 1,
    backgroundColor: "#ffffff",
  }

};

export default WallPosts;
