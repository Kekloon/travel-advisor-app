import React, { Component } from "react";
import { Container, Content, Icon, Button, Text } from "native-base";
import { Image, Platform, TouchableOpacity, View } from "react-native";
import { COLOR, DIMENS } from "../../../config/constant";
import { VideoPlayer } from '../../../common'
import _ from "lodash";
import Camera from 'react-native-camera'
import Video from "react-native-video";
import Carousel from 'react-native-snap-carousel'


class ConfirmSnapshot extends Component {
	static navigationOptions = ({navigation}) => {
		const { params = {} } = navigation.state
		return {
			headerTitle: "Preview",
			tabBarIcon: ({ tintColor }) => (
				<Icon name="add" style={{ color: tintColor }}/>
			),
			tabBarVisible: false,
			// headerRight: (
			// 	<Button transparent onPress={() => navigation.navigate('CreatePost')}>
			// 		<Text>Next</Text>
			// 	</Button>
			// ),
			headerLeft: (
				<Button transparent onPress={() => navigation.goBack()}>
					<Icon name="arrow-back"></Icon>
				</Button>
			)
		}
	};
	state = {
		media: {
			path: null
		},
		mediaArr: [],
	}

  componentDidMount() {
		this.setState ({
			media: this.props.navigation.state.params.media,
			cameraMode: this.props.navigation.state.params.cameraMode,
			mediaArr: this.props.mediaArr
		})
	}
	// componentWillReceiveProps (nextProps) {
	// 	console.log(nextProps)
	// 	this.setState({
	// 		mediaArr: nextProps.mediaArr
	// 	})
	// }

	_renderImages ({item, index}) {
		return (
			<View style={styles.slide}>
				<Image source={{uri: item.path}} style={{height: DIMENS.screenWidth, width: DIMENS.screenWidth}}/>
			</View>
		);
	}

  render () {
		const { media, cameraMode, mediaArr } = this.state
		const { navigation } = this.props
    return (
			<Container style={styles.container}>
				{
					cameraMode === "video" ?
						<VideoPlayer media={media.path}/>
					: 
						<Carousel
              ref={(c) => { this._carousel = c; }}
              data={this.state.mediaArr}
              renderItem={this._renderImages}
              sliderWidth={DIMENS.screenWidth}
              itemWidth={DIMENS.screenWidth}
            />
				}
				<Button onPress={() => navigation.navigate('CreatePost',  { media, cameraMode })}>
					<Text>Next</Text>
				</Button>
      </Container>
    )
  }
}

const styles = {
  container: {
    flex: 1,
    backgroundColor: "#ffffff",
  }

};

export default ConfirmSnapshot;
