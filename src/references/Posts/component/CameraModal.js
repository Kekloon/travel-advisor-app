import React, { Component } from "react";
import { Container, Content, Icon, Button, Text, Toast, Spinner } from "native-base";
import { Image, Platform, View, TouchableOpacity } from "react-native";
import { COLOR, DIMENS } from "../../../config/constant";
import { toTimeElapse } from '../../../config/helper'
import _ from "lodash";
import Camera from 'react-native-camera'
import timer from 'react-native-timer'

class CameraModal extends Component {
	static navigationOptions = ({ navigation }) => {
		const { params = {} } = navigation.state
		return {
			headerTitle: "Capture",
			tabBarVisible: false,
			headerLeft: (
				<Button transparent onPress={() => navigation.goBack(null)}>
					<Icon name="close"></Icon>
				</Button>
			)
		}
	};

	constructor(props) {
		super(props);
		this.state = {
			isMounted: false,
			isOpenCamera: false,
			cameraType: "back",
			cameraMode: "still",
			isRecording: false,
			hasCaptured: false,
			maxVideoCapture: 15000,
			elapse: 0,
			mediaArr: [],
			capturedPhoto: 0,
			maxPhotoCapture: 5,
			photoLimitedToast: {
				msg: "Photo taken have reached the maximum of 5",
				duration: 3000,
				position: 'bottom'
			}
		}
		this.switchCameraType = this.switchCameraType.bind(this);
		this.switchCameraMode = this.switchCameraMode.bind(this);
		this.startVideoRecording = this.startVideoRecording.bind(this);
		this.pauseVideoRecording = this.pauseVideoRecording.bind(this);
		this.isPhotoTakenLimited = this.isPhotoTakenLimited.bind(this);
		this.startElapse = this.startElapse.bind(this)
		// this.handleCapture = this.handleCapture.bind(this);
	}

	// mixins = [TimerMixin]
	componentDidMount() {
		this.setState({
			isMounted: true
		})
	}
	componentDidUpdate(prevProps, prevState) {
		if (prevState.isMounted !== this.state.isMounted) {
			setTimeout(() => {
				this.setState({
					isOpenCamera: true,
					mediaArr: this.props.mediaArr
				})
			}, 100);

		}
	}
	componentWillUnmount() {
		this.setState({
			isOpenCamera: false
		});
	}

	switchCameraType() {
		return this.setState({
			cameraType: this.state.cameraType == "back" ? "front" : "back"
		});
	}

	switchCameraMode() {
		this.setState({
			cameraMode: this.state.cameraMode == "still" ? "video" : "still"
		});
	}

	// retake() {
	//   this.setState({
	//     hasCaptured: false,
	//     mediaUri: null
	//   });
	// }

	_resetCapture() {
		this.setState({ 
			elapse: 0,
			capturedPhoto: 0
		 })
	}

	isPhotoTakenLimited() {
		return this.state.mediaArr.length < this.state.maxPhotoCapture ? false : true
	}

	_captureMedia() {
		const { cameraMode } = this.state
		if (this.isPhotoTakenLimited() == false) {
			this.camera.capture()
			.then((media) => {
				this.props.addPostImages({ media, mediaType: cameraMode })
				this.props.navigation.navigate("ConfirmSnapshot", { media, cameraMode })
				this.setState({capturedPhoto: this.state.mediaArr.length})
			})
			.catch(err => console.error(err));
		} else {
			const media = this.camera.media;
			this.props.navigation.navigate("ConfirmSnapshot", { media, cameraMode })
			Toast.show({
				text: this.state.photoLimitedToast.msg,
				duration: this.state.photoLimitedToast.duration,
				position: this.state.photoLimitedToast.position
			})
		}
	}

	startVideoRecording() {
		const { maxVideoCapture } = this.state
		this.camera.stopCapture();
		this.setState({ isRecording: true }, () => {
			this._captureMedia();
			timer.setInterval(this, 'cameraElapse', this.startElapse, 1000);
			timer.setTimeout(this, 'cameraElapse', () => {
				timer.clearInterval(this);
				this.pauseVideoRecording();
			}, maxVideoCapture);
		});
	}
	startElapse() {
		const { elapse } = this.state
		this.setState({ elapse: elapse + 1 })
	}
	
	pauseVideoRecording() {
		if (this.state.isRecording) {
			this.setState({ isRecording: false }, () => {
				this.camera.stopCapture();
				this.setState({ elapse: 0 })
				timer.clearInterval(this);
			});
		}
	}

	takePhoto() {
		this.camera.stopCapture();
		this._captureMedia();
	}

	

	renderCamera() {
		const { cameraMode, cameraType, isRecording } = this.state;
		return (
			<View style={styles.cameraContainer}>
				<Camera
					ref={(cam) => {
						this.camera = cam;
					}}
					style={{ width: DIMENS.screenWidth, height: DIMENS.screenWidth }}
					aspect={Camera.constants.Aspect.fill}
					captureTarget={Camera.constants.CaptureTarget.disk}
					captureMode={cameraMode === "video" ? Camera.constants.CaptureMode.video : Camera.constants.CaptureMode.still}
					type={cameraType === "front" ? Camera.constants.Type.front : Camera.constants.Type.back}
					captureQuality="medium"
				>

				</Camera>
			</View>
		);
	}

	render() {
		const { isOpenCamera, cameraMode, cameraType, isRecording, elapse, capturedPhoto } = this.state
		return (
			<Container style={styles.container}>
				{
					isOpenCamera ?
						this.renderCamera()
						: <View style={styles.cameraContainerPlaceholder} ><Spinner color='#FFF' size='small'/></View>
				}
			
				<View style={styles.cameraInfo}>
					{
						cameraMode === 'video' ?  isRecording ? <View style={styles.recordingIndicator}><View style={styles.recordingDot}/><Text>{toTimeElapse(elapse)}s</Text></View> : null : <Text>{capturedPhoto}</Text>
					}
				</View>
				<View style={styles.buttonWrapper}>
					<View style={styles.rightCol}>
						<Button transparent onPress={() => this.switchCameraMode()}>
							<Icon
								name={cameraMode === "video" ? "ios-camera" : "md-videocam"}
								size={25}
								color="white"
								style={styles.videoCameraButton}
							/>
						</Button>
					</View>

					<View style={styles.middleCol}>
						{cameraMode === "video" ?
							<TouchableOpacity
								onPressIn={() => this.startVideoRecording()}
								onPressOut={() => this.pauseVideoRecording()}
							>
								<View style={styles.cameraTrigger}></View>
							</TouchableOpacity>
							:
							<TouchableOpacity onPress={() => this.takePhoto()}>
								<View style={styles.cameraTrigger}></View>
							</TouchableOpacity>
						}
					</View>

					<View style={styles.rightCol}>
						<Button transparent onPress={() => this.switchCameraType()}>
							<Icon
								name={cameraType === "back" ? "ios-reverse-camera-outline" : "ios-reverse-camera"}
								size={30}
								color={"white"}
								style={styles.closeButton}
							/>
						</Button>
					</View>

				</View>
			</Container>
		)
	}
}

const triggerButtonLength = 70

const styles = {
	container: {
		flex: 1,
		backgroundColor: "#ffffff",
	},
	cameraContainer: {
		width: DIMENS.screenWidth,
		height: DIMENS.screenWidth,
	},
	cameraContainerPlaceholder: {
		backgroundColor: '#000',
		width: DIMENS.screenWidth,
		height: DIMENS.screenWidth,
		alignItems: 'center',
		justifyContent: 'center',
	},
	buttonWrapper: {
		flexDirection: 'row',
		alignItems: 'center',
		justifyContent: 'center',
		flex: 1,
		marginTop: -30
	},
	cameraTrigger: {
		backgroundColor: COLOR.gray,
		width: triggerButtonLength,
		height: triggerButtonLength,
		borderRadius: triggerButtonLength / 2,
	},
	middleCol: {
		// flexDirection: 'row',
		paddingLeft: 60,
		paddingRight: 60
	},
	recordingIndicator: {
		flexDirection: 'row',
		justifyContent: 'center',
		alignItems: 'center'
	},
	recordingDot: {
		width: 10,
		height: 10,
		borderRadius: 5,
		backgroundColor: 'red',
		marginRight: 10
	},
	cameraInfo: {
		height: 30,
		width: DIMENS.screenWidth,
		// position: 'absolute',
		alignItems: 'center',
		justifyContent: 'center',
	}
};

export default CameraModal;
