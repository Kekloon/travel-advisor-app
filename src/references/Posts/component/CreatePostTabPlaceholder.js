import React, { Component } from "react";
import { Container, Icon } from "native-base";


class CreatePostTabPlaceholder extends Component {
	static navigationOptions = {
		tabBarIcon: ({ tintColor }) => (
			<Icon name="add" style={{ color: tintColor }}/>
		),
  };

  render () {
    return (
			<Container>
      </Container>
    )
  }
}

export default CreatePostTabPlaceholder;
