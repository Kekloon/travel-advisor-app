import React, { Component } from "react";
import { Container, Content, Button, Text, List } from "native-base";
import { Image, Platform, View } from "react-native";
import { COLOR, DIMENS } from "../../../config/constant";
import { GooglePlacesAutoComplete } from '../../../common'
import _ from "lodash";


class SearchPosts extends Component {
	static navigationOptions = {
    headerTitle: "Search",
    tabBarVisible: false,
  };
  constructor (props) {
    super(props)
    this.state = {
      posts: []
    }
    this.searchPosts = this.searchPosts.bind(this)
  }
  searchPosts (place) {
    this.props.navigation.goBack()
    this.props.searchPosts(place.name)
  } 
  render () {
    const { posts } = this.state
    const { navigation } = this.props
    return (
      <Container style={styles.container}>
        <GooglePlacesAutoComplete onPressPlaceItem={(place) => this.searchPosts(place)}/>
      </Container>
    )
  }
}

const styles = {
  container: {
    flex: 1,
    backgroundColor: "#ffffff",
  }

};

export default SearchPosts;
