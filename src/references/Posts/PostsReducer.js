const initialState = {
  newPost: {
    media: [],
    mediaType: null
  },
  posts: {
    isFetching: false,
    payload: [],
    isFilter: false,
    searchFor: null,
    err: null
  },
  post: {
    isSubmitting: false,
    post: {},
    err: null
  }
}

function cards (state = initialState, action) {
  switch (action.type) {
    case 'REQUEST_GET_POSTS':
      return Object.assign({}, state, {
        posts: {
          isFetching: true,
          payload: state.posts.payload,
          isFilter: action.isFilter
        }
      })
    case 'SUCCESS_GET_POSTS':
      return Object.assign({}, state, {
        posts: {
          isFetching: false,
          payload: action.payload,
          isFilter: action.isFilter,
          searchFor: action.searchFor
        }
      })
    case 'REQUEST_FAIL_POSTS':
      return Object.assign({}, state, {
        posts: {
          isFetching: true,
          payload: state.posts.payload,
          isFilter: action.isFilter,
          searchFor: null,
          err: action.err
        }
      })
    case 'REQUEST_ADD_POST_IMAGES': 
      state.newPost.media.push(action.postMedia.media)
      return Object.assign({}, state, {
        newPost: {
          media: state.newPost.media,
          mediaType: action.postMedia.mediaType
        }
      })
    case "REQUEST_CREATE_POST":
      return Object.assign({}, state, {
        post: {
          isSubmitting: true
        }
      })

    case "REQUEST_CREATE_POST":
      return Object.assign({}, state, {
        post: {
          isSubmitting: false
        }
      })
    case "SUCCESS_CREATE_POST":
      return Object.assign({}, state, {
        post: {
          isSubmitting: false
        }
      })
    case "REQUEST_UPDATE_POST":
      return Object.assign({}, state, {
        post: {
          payload: state.posts.payload,
          isSubmitting: true
        }
      })
    case "SUCCESS_UPDATE_POST":
      return Object.assign({}, state, {
        post: {
          payload: state.posts.payload,
          isSubmitting: false
        }
      })
    case "FAIL_UPDATE_POST":
      return Object.assign({}, state, {
        post: {
          payload: state.posts.payload,
          isSubmitting: false
        }
      })
    default:
      return state
  }
}

export default cards
