import { getURL, uploadFile } from "../../config/helper";
import axios from "axios";
import { AsyncStorage } from "react-native";
import { Toast } from "native-base";
import firebase from 'react-native-firebase';
import GeoFire from "geofire"
import { NavigationActions } from 'react-navigation'
import Geohash from 'latlon-geohash'

const db = firebase.database();
const firestoreDb = firebase.firestore();
const storageRef = firebase.storage().ref();


export const requestAddPostImages = (postMedia) => ({
  type: "REQUEST_ADD_POST_IMAGES",
  postMedia

})

export const requestClearPostImage = () => ({
  type: "REQUEST_CLEAR_POST_IMAGE",
  
})

export const getPostsRequest = (position) => async (dispatch) => {
  dispatch(requestGetPosts());
  const geoFire = new GeoFire(db.ref().child("post_location"));

  const { coords } = position
  const geoQuery = geoFire.query({
    // center: [3.136639, 101.541305],
    center: [coords.latitude, coords.longitude],
    radius: 20
  });
  let posts = [], postIds = []
  geoQuery.on("key_entered", function(key, location, distance) {
    postIds.push(key)
  });
  geoQuery.on("ready", async () => {
    let snapshot
    try {
      await Promise.all((postIds.map(async (key) => {
        postSnapshot = await firestoreDb.collection('posts').doc(key).get()
        const postObj = postSnapshot.data()
        const userObj = await postObj.user.get()
        postObj.user = userObj.data()

        posts.push(postObj)
      })))
    } catch(e) {

    }
    dispatch(successGetPosts(posts))
    // db.ref().child(`posts/${key}`).once("value", (snapshot) => {
    //   const snap = snapshot.val()
    //   posts.push(snap)
    //   dispatch(successGetPosts(posts))
    // })
  });
}

export const requestGetPosts = () => ({
  type: "REQUEST_GET_POSTS"
})

export const successGetPosts = (payload, isFilter = false, searchFor = null) => ({
  type: "SUCCESS_GET_POSTS",
  isFilter,
  searchFor,
  payload
})

export const failGetPosts = (err, isFilter = false) => ({
  type: "FAIL_GET_POSTS",
  isFilter,
})

export const createPostRequest = (body, navigation) => async (dispatch) => {
  dispatch(requestCreatePost());
  const userId = await AsyncStorage.getItem('uid')
  const { latitude, longitude } = body.location
  let fileUrl, fileUrls = []
  try {
    if(body.mediaType === 'video') {
      fileUrl = await uploadFile(body.media.path)
    } else {
      await Promise.all((body.media.map(async (obj) => {
        fileUrl = await uploadFile(obj.path)
        fileUrls.push(fileUrl)
      })))
    }
  } catch (e) {
    dispatch(failCreatePost('Something went wrong, please try again'));
  }
  const media = body.mediaType === 'video' ? fileUrl : fileUrls
  
  if(media) {

    const ref = db.ref().push();
    const key = ref.key;
    try {

      // store firestore
      postId = await firestoreDb.collection('posts').doc().id
      userRef = firestoreDb.collection('users').doc(userId)
      const geoPoint = new firebase.firestore.GeoPoint(latitude, longitude)
      const geohash = Geohash.encode(latitude, longitude)
      await firestoreDb.collection('posts').doc(postId).set({
        status: body.status,
        place: body.place,
        location: geoPoint,
        geohash,
        rating : body.rating,
        postId,
        ownerId: userId,
        user: userRef,
        createdAt: new Date(),
        mediaType: body.mediaType,
        media
      })
    } catch (e) {
      dispatch(failCreatePost(e))
    }

    const geoFire = new GeoFire(db.ref().child("post_location"))
    geoFire.set(postId, [latitude, longitude]);
    dispatch(successCreatePost())

    const resetAction = NavigationActions.reset({
      index: 0,
      key: null,
      actions: [
        NavigationActions.navigate({
          routeName: 'Main'
        })
      ]
    })
    navigation.dispatch(resetAction)
  }
  
}

export const requestCreatePost = () => ({
  type: "REQUEST_CREATE_POST"
})

export const successCreatePost = () => ({
  type: "SUCCESS_CREATE_POST"
})

export const failCreatePost = (err) => ({
  type: "FAIL_CREATE_POST",
  err
})

export const getUserPostsRequest = (userId) => async (dispatch) => {
  dispatch(requestGetUserPosts());
  // const userId = await AsyncStorage.getItem('uid')
  let snapshot, posts = []

  try {
    snapshot = await firestoreDb.collection('posts').where('ownerId', '==', userId).orderBy('createdAt', 'desc').get()

    snapshot.forEach((doc) => {
      posts.push(doc.data())
    })
  } catch(e) {
    dispatch(failGetUserPosts(e.message))
  }
  dispatch(successGetUserPosts(posts))
}

export const requestGetUserPosts = () => ({
  type: "REQUEST_GET_USER_POSTS"
})

export const successGetUserPosts = (payload) => ({
  type: "SUCCESS_GET_USER_POSTS",
  payload
})

export const failGetUserPosts = (err) => ({
  type: "FAIL_GET_USER_POSTS",
  err
})

export const updatePostRequest = (body, postId, navigation) => async (dispatch) => {
  dispatch(requestUpdatePost());
  const formData = {
    status: body.status
  }
  try {
    await firestoreDb.collection('posts').doc(postId).update(formData)
  } catch(e) {
    dispatch(failUpdatePost());
  }
  navigation.goBack()
  dispatch(successUpdatePost())
  
}

export const requestUpdatePost = () => ({
  type: "REQUEST_UPDATE_POST"
})

export const successUpdatePost = () => ({
  type: "SUCCESS_UPDATE_POST"
})

export const failUpdatePost = (err) => ({
  type: "FAIL_UPDATE_POST",
  err
})


export const searchPostsRequest = (keyword, navigation) => async (dispatch) => {
	dispatch(requestGetPosts())
  const userId = await AsyncStorage.getItem('uid')
  let posts = [], postsRef, snapshot
  try {
    postsRef = firestoreDb.collection('posts')
    postSnapshot = await postsRef.where('place', '==', keyword).get()

    postSnapshot.forEach(async (doc) => {
      const postObj = doc.data()
      posts.push(postObj)
    })

    await Promise.all(posts.map(async (obj, i) => {
      const user = await obj.user.get()
      posts[i].user = user.data()
    }))
  } catch (e) {
		dispatch(failGetPosts(e.message, true))
  }
  navigation.goBack()
  dispatch(successGetPosts(posts, true, keyword))
}

const _getUserSnapshot = (ownerId) => {
  return new Promise((resolve, reject) => {
    db.ref().child(`users/${ownerId}`).once('value')
    .then((snapshot) => (
      resolve(snapshot.val())
    ))
    .catch(e => (
      reject(e)
    ))
  })
}
