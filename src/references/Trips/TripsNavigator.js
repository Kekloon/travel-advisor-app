import React from "react";
import { StackNavigator } from "react-navigation";
import MyTrips from "./container/MyTripsContainer";
import UpcomingTripDetails from './container/UpcomingTripDetailsContainer'
// import CompletedTripDetails from './container/CompletedTripDetailsContainer'
import { Icon } from "native-base";

export const TripsNavigator = StackNavigator({
	MyTrips: { screen: MyTrips },
	UpcomingTripDetails: { screen: UpcomingTripDetails },
	// CompletedTripDetails: { screen: CompletedTripDetails }
},
{
	initialRouteName: "MyTrips",
	headerMode: "none",
	navigationOptions: {
		title: '',
		tabBarIcon: ({ tintColor }) => (
			<Icon name="person" style={{ color: tintColor }}/>
		),
	}
});