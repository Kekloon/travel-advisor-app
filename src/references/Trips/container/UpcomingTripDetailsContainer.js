import { connect } from 'react-redux'
import UpcomingTripDetails from '../component/UpcomingTripDetails'

const mapStateToProps = (state = {}, props) => {
  const trip = props.navigation.state.params.trip
  return {
    trip
  }
}

const mapDispatchToProps = (dispatch, props) => ({
  // getUserProfile: () => {
  //   dispatch(getUserProfileRequest())
  // },
})

const UpcomingTripDetailsContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(UpcomingTripDetails)

export default UpcomingTripDetailsContainer
