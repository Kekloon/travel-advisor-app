import { connect } from 'react-redux'
import CompletedTripDetails from '../component/CompletedTripDetails'

const mapStateToProps = (state = {}) => ({

})

const mapDispatchToProps = (dispatch, props) => ({
  // getUserProfile: () => {
  //   dispatch(getUserProfileRequest())
  // },
})

const CompletedTripDetailsContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(CompletedTripDetails)

export default CompletedTripDetailsContainer
