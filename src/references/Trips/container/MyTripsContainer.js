import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Container, Button, Icon } from 'native-base'
import { StyleSheet } from 'react-native'
import { COLOR, DIMENS } from '../../../config/constant'
import UpcomingTrips from '../component/UpcomingTrips'
import CompletedTrips from '../component/CompletedTrips'
import { getMyTripsRequest } from '../TripsActions'
import ScrollableTabView from 'react-native-scrollable-tab-view'

class MyTripsContainer extends Component {
  static navigationOptions = ({ navigation }) => {
    return {
      headerTitle: 'My Trips',
			tabBarVisible: false,
      headerLeft: (
        <Button transparent onPress={() => navigation.goBack()}><Icon name='arrow-back'/></Button>
      ),
    }
  }

  render () {
    const { navigation } = this.props
    return (
      <Container style={styles.container}>
        <ScrollableTabView
          tabBarBackgroundColor='#FFF'
        >
					<UpcomingTrips tabLabel='Upcoming' {...this.props} />
					<CompletedTrips tabLabel='Completed' {...this.props} />
        </ScrollableTabView>
      </Container>
    )
  }
}

const mapStateToProps = (state = {}) => ({
  // upcomingTrips: state.trips.upcomingTrips.payload,
  // completedTrips: state.trips.completedTrips.payload
})

const mapDispatchToProps = (dispatch, props) => ({
  getMyTrips: (status) => {
    dispatch(getMyTripsRequest(status))
  },
})

const styles = StyleSheet.create({
  container: {
		backgroundColor: COLOR.grayLight,
		flex: 1
  },
})

MyTripsContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(MyTripsContainer)

export default MyTripsContainer
