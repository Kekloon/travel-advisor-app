
import { getURL, uploadFile } from "../../config/helper";
import { AsyncStorage } from "react-native";
import { Toast } from "native-base";
import firebase from 'react-native-firebase';
import _ from 'lodash'

const db = firebase.database();
const firestoreDb = firebase.firestore();
const storageRef = firebase.storage().ref();

export const createTripsRequest = (body, post) => async (dispatch) => {
	dispatch(requestCreateTrip())
  let ref, tripId
  const { user } = post

  const userId = await AsyncStorage.getItem('uid')
  const price = _.toInteger(user.price)
  const duration = _.toInteger(body.duration)
  const serviceCharge = 5
  const total = price * duration
  const subtotal = total + serviceCharge
  try {
    tripId = await firestoreDb.collection('trips').doc().id
    tripRef = await firestoreDb.collection('trips').doc(tripId).set({
      createdAt: new Date(),
      status: 'Upcoming',
      price,
      serviceCharge,
      total,
      post,
      tripId,
      ownerId: post.user.userId,
      userId,
      subtotal,
      duration,
      startDateTime: new Date(`${body.date} ${body.time}`)
    })
  } catch (e) {
		dispatch(failCreateTrip(e.message))
	}
	dispatch(successCreateTrip())
}

export const requestCreateTrip = () => ({
  type: "REQUEST_CREATE_TRIP"
})

export const successCreateTrip = () => ({
  type: "SUCCESS_CREATE_TRIP"
})

export const failCreateTrip = (err) => ({
  type: "FAIL_CREATE_TRIP",
  err
})

export const getMyTripsRequest = (status) => async (dispatch) => {
	dispatch(requestGetMyTrips(status))
  const userId = await AsyncStorage.getItem('uid')
  let myTrips = [], tripRef, snapshot
  try {
    tripRef = firestoreDb.collection('trips')
    snapshot = await tripRef.where('status', '==', _.capitalize(status)).where('userId', '==', userId).get()
  } catch (e) {
		dispatch(failGetMyTrips(status, e.message))
	}
	
	snapshot.forEach(function(doc) {
		myTrips.push(doc.data())
	})
	dispatch(successGetMyTrips(status, myTrips))
}

export const requestGetMyTrips = (status) => ({
  type: `REQUEST_GET_${_.toUpper(status)}_TRIPS`
})

export const successGetMyTrips = (status, payload) => ({
	type: `SUCCESS_GET_${_.toUpper(status)}_TRIPS`,
	payload
})

export const failGetMyTrips = (status, err) => ({
  type: `FAIL_GET_${_.toUpper(status)}_TRIPS`,
  err
})

export const changeTripStatusRequest = (tripId, status) => async (dispatch) => {
	dispatch(requesthangeTripStatus())
  let snapshot
  try {
    tripRef = firestoreDb.collection('trips')
    snapshot = await tripRef.doc(tripId).update({ status })
  } catch (e) {
		dispatch(failChangeTripStatus(e.message))
	}

	dispatch(successChangeTripStatus())
}

export const requesthangeTripStatus = () => ({
  type: 'REQUEST_CHANGE_TRIP_STATUS'
})

export const successChangeTripStatus = () => ({
	type: 'SUCCESS_CHANGE_TRIP_STATUS'
})

export const failChangeTripStatus = (err) => ({
  type: 'FAIL_CHANGE_TRIP_STATUS',
  err
})