import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Container, Content, List, ListItem, Body, Text, Right } from 'native-base'
import { Platform, RefreshControl, StyleSheet, Alert, StatusBar } from 'react-native'
import { COLOR, DIMENS } from '../../../config/constant'
import _ from 'lodash'
import moment from 'moment'

class UpcomingTrips extends Component {
  static navigationOptions = {
		tabBarVisible: false,
  }
  constructor(props) {
    super(props)
    this.state = {
      upcomingTrips: []
    }
  }

  componentDidMount () {
    this.props.getMyTrips('Upcoming')
  }

  componentWillReceiveProps (nextProps) {
    this.setState({
      upcomingTrips: nextProps.upcomingTrips
    })
  }

  render () {
    const { navigation } = this.props
    const { upcomingTrips } = this.state
    return (
      <Container style={styles.container}>
        <Content>
          <List dataArray={upcomingTrips} 
						renderRow={(trip) => <TripItem trip={trip} onPress={() => navigation.navigate('UpcomingTripDetails', { trip })} /> }
					>
					</List>
        </Content>
      </Container>
    )
  }
}

const TripItem = ({ trip, onPress }) => (
	<ListItem icon onPress={onPress}>
		<Body style={styles.listBody}>
			<Text style={styles.menuLabel}>{trip.post.place}</Text>
			<Text note>{moment(trip.startDateTime).calendar()}</Text>
    </Body>
	</ListItem>
)

const mapStateToProps = (state = {}) => ({
  upcomingTrips: state.trips.upcomingTrips.payload,
  // completedTrips: state.trips.completedTrips.payload
})

const styles = StyleSheet.create({
  container: {
		backgroundColor: '#FFFFFF',
		flex: 1
  },
})

UpcomingTrips = connect(
  mapStateToProps
)(UpcomingTrips)

export default UpcomingTrips
