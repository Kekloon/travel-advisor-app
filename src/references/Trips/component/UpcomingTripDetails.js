import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Container, Content, List, ListItem, Body, Text, Right } from 'native-base'
import { Platform, RefreshControl, StyleSheet, Alert, StatusBar, View } from 'react-native'
import { COLOR, DIMENS } from '../../../config/constant'
import _ from 'lodash'
import moment from 'moment'
import QRCode from 'react-native-qrcode'

class UpcomingTripDetails extends Component {
  static navigationOptions = {
		tabBarVisible: false,
  }
  constructor(props) {
    super(props)
    this.state = {
      trip: []
    }
  }

  componentDidMount () {
    this.setState({
      trip: this.props.trip
    })
  }

  render () {
    const { navigation } = this.props
    const { trip } = this.state
    return (
      <Container style={styles.container}>
        <View
          style={{marginLeft: 100, marginTop: 100}}
        >
          <QRCode
            value={trip.tripId}
            size={150}
            bgColor='#000'
            fgColor='white'
          />
        </View>
      </Container>
    )
  }
}

const styles = StyleSheet.create({
  container: {
		backgroundColor: COLOR.grayLight,
		flex: 1
  },
})

export default UpcomingTripDetails
