import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Container, Content, List, ListItem, Body, Text } from 'native-base'
import { Platform, RefreshControl, StyleSheet, Alert, StatusBar } from 'react-native'
import { COLOR, DIMENS } from '../../../config/constant'
import _ from 'lodash'
import moment from 'moment'

class CompletedTrips extends Component {
  static navigationOptions = {
		tabBarVisible: false,
  }
  constructor(props) {
    super(props)
    this.state = {
			completedTrips: []
		}
  }

  componentDidMount () {
    this.props.getMyTrips('Completed')
  }

  componentWillReceiveProps (nextProps) {
    this.setState({
      completedTrips: nextProps.completedTrips
    })
  }

  render () {
    const { navigation } = this.props
    const { completedTrips } = this.state
    return (
      <Container style={styles.container}>
				<Content>
					<List dataArray={completedTrips} 
						renderRow={(trip) => <TripItem trip={trip} onPress={() => navigation.navigate('CompletedTripDetails', { trip })} /> }
					>
					</List>
        </Content>
      </Container>
    )
  }
}

const TripItem = ({ trip, onPress }) => (
	<ListItem icon onPress={onPress}>
		<Body style={styles.listBody}>
			<Text style={styles.menuLabel}>{trip.post.place}</Text>
			<Text note>{moment(trip.startDateTime).calendar()}</Text>
		</Body>
	</ListItem>
)

const mapStateToProps = (state = {}) => ({
  completedTrips: state.trips.completedTrips.payload,
  // completedTrips: state.trips.completedTrips.payload
})

const styles = StyleSheet.create({
  container: {
		backgroundColor: '#FFFFFF',
		flex: 1
  },
})

CompletedTrips = connect(
  mapStateToProps
)(CompletedTrips)

export default CompletedTrips
