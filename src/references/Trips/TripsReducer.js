const initialState = {
  trips: {
    isSubmitting: false,
    err: null
  },
  upcomingTrips: {
    isFetching: false,
		payload: [],
		err: null
	},
	completedTrips: {
    isFetching: false,
		payload: [],
		err: null
  },
}

function users (state = initialState, action) {
  switch (action.type) {
    case 'REQUEST_CREATE_TRIP':
      return Object.assign({}, state, {
        trips: {
          isSubmitting: true,
        }
      })
    case 'SUCCESS_CREATE_TRIP':
      return Object.assign({}, state, {
        trips: {
          isSubmitting: false,
        }
      })
    case 'FAIL_CREATE_TRIP':
      return Object.assign({}, state, {
        trips: {
          isSubmitting: false,
          err: action.err
        }
			})
    case 'REQUEST_GET_UPCOMING_TRIPS':
      return Object.assign({}, state, {
        upcomingTrips: {
          isFetching: true,
          payload: state.upcomingTrips.payload,
        }
      })
    case 'SUCCESS_GET_UPCOMING_TRIPS':
      return Object.assign({}, state, {
        upcomingTrips: {
          isFetching: false,
          payload: action.payload
        }
      })
    case 'FAIL_GET_UPCOMING_TRIPS':
      return Object.assign({}, state, {
        upcomingTrips: {
          isFetching: false,
          payload: state.upcomingTrips.payload,
          err: action.err
        }
			})
		case 'REQUEST_GET_COMPLETED_TRIPS':
      return Object.assign({}, state, {
        completedTrips: {
          isFetching: true,
          payload: state.completedTrips.payload,
        }
      })
    case 'SUCCESS_GET_COMPLETED_TRIPS':
      return Object.assign({}, state, {
        completedTrips: {
          isFetching: false,
          payload: action.payload
        }
      })
    case 'FAIL_GET_COMPLETED_TRIPS':
      return Object.assign({}, state, {
        completedTrips: {
          isFetching: false,
          payload: state.completedTrips.payload,
          err: action.err
        }
      })
    default:
      return state
  }
}

export default users
