import React, { Component } from "react"
import { Footer, FooterTab, Icon, Text, Button } from "native-base";
// import { COLOR } from "../config/constant"
import { PixelRatio } from "react-native";

class FooterSection extends Component {
  render () {
    return (
      <Footer style={styles.footer}>
        <FooterTab>
          <Button>
						<Text>Apps</Text>
					</Button>
					<Button>
						<Text>Camera</Text>
					</Button>
					<Button active>
						<Text>Navigate</Text>
					</Button>
					<Button>
						<Text>Contact</Text>
					</Button>
        </FooterTab>
      </Footer>
    );
  }
}

const styles = {
  footer: {
    position:"absolute",
    backgroundColor: "#FFFFFF",
    elevation: 8,
    bottom: 0,
    borderTopWidth: PixelRatio.get() > 2 ? 1 : 1.5,
    // borderTopColor: COLOR.grayLighter
  }
};
export default FooterSection;