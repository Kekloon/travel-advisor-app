import { AsyncStorage } from "react-native";
import { getURL } from "../../config/helper";
import axios from "axios";
import { Toast } from "native-base";
import { AccessToken, LoginManager } from 'react-native-fbsdk';
import firebase from 'react-native-firebase'

const db = firebase.database();
const firestoreDb = firebase.firestore();

export const facebookLoginRequest = () => (dispatch) => {
  dispatch(_requestLogin())
  return LoginManager
    .logInWithReadPermissions(['public_profile', 'email'])
    .then(async (result) => {
      if (!result.isCancelled) {
        const data = await AccessToken.getCurrentAccessToken()
        if (data) {
          const credential = firebase.auth.FacebookAuthProvider.credential(data.accessToken)
          firebase.auth().signInWithCredential(credential)
          .then(res => {
            const userData = res._user
            firestoreDb.collection('users').doc(userData.uid).get()
            .then(async (snapshot) => {
              const firestoreUserData = snapshot.data()
              if (_.isEmpty(firestoreUserData)) {
                firestoreDb.collection('users').doc(userData.uid).set({
                  createdAt: new Date(),
                  ..._filterUserData(userData)    
                })
              }
              await AsyncStorage.setItem('uid', userData.uid)
              dispatch(_successLogin(userData))
            })
          })
          .catch(err => {
            dispatch(_failedLogin(error))
          })
        }
      }
    })
}

const _filterUserData = (user) => ({
  userId: user.uid,
  firstName: user.displayName,
  email: user.email,
  profilePicture: user.photoURL
})

export const checkAuthenticationStatusRequest = () => (dispatch) => {
  firebase.auth().onAuthStateChanged(async (payload) => {
    const userData = payload && payload._user
    if (payload) {
      await AsyncStorage.setItem('uid', userData.uid)
      dispatch(_successLogin(userData))
    } else {
      dispatch(_failedLogin())
    }
  })
}

const _requestLogin = () => ({
  type: 'REQUEST_LOGIN'
})

const _successLogin = (payload) => ({
  type: 'SUCCESS_LOGIN',
  payload
})

const _failedLogin = (errorMessage = null) => ({
  type: 'FAILED_LOGIN',
  errorMessage
})

const _successLogout = () => ({
  type: 'SUCCESS_LOGOUT'
})

const _failedLogout = () => ({
  type: 'FAILED_LOGOUT'
})
