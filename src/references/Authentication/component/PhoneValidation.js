import React, { Component } from 'react'
import { StyleSheet } from 'react-native'
import { Container, View, Text, Button, Item, Input } from 'native-base'

class PhoneValidation extends Component {
    constructor(props){
        super(props)
        this.state = {
            num: null
        }

        this.onChanged = this.onChanged.bind(this)
    }

	onChanged(text) {
		this.setState({
			num: text.replace(/[^0-9]/g, '')
		})
    }
    
    render() {
        return (
            <Container>
                <Text> Verication Code </Text>
                <View>
                    <Item regular>
                        <Input
                            placeholder="Verication"
                            onChangeText={(text) => this.onChanged(text)}
                            value={this.state.num}
                            keyboardType="numeric"
                            maxLength={6}
                        />
                    </Item>
                </View>
                <View>
                    <Button>
                        <Text>Confirm</Text>
                    </Button>
                </View>

            </Container>
        )
    }
}

export default PhoneValidation