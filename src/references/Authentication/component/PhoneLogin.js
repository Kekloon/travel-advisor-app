
import React, { Component } from "react";
import { Item, Input, Toast, Form, Container, Content, Header, Body, Title, Button, Text, View, Icon, Footer, ListItem } from "native-base";
import { Field, reduxForm } from "redux-form";
import { Image, Platform, StyleSheet } from "react-native";
import { alphaNumeric, maxLength, minLength, email, required } from "../../../config/validations";
import { TextInput } from "../../../common";

class PhoneLogin extends Component {
	constructor(props) {
		super(props)
		this.state = {
			phoneNumber: null
		}
		this.onChanged = this.onChanged.bind(this)
	}

	onChanged(text) {
		this.setState({
			phoneNumber: text.replace(/[^0-9]/g, '')
		})
	}

	render() {
		const { handleSubmit } = this.props;
		const { navigation } = this.props
		return (
			<Container>
				{/* <Image style={styles.logo} source={{ uri: 'https://facebook.github.io/react/logo-og.png' }} /> */}
				<Text style={styles.title}>Enter Your Phone Number</Text>
				<Content>
					<View style={styles.flowLayout}>
						<Item rounded style={styles.phoneCode}>
							{/* <Input value="+60" editable={false} /> */}
							<Text style={styles.textCenter}> +60 </Text>
						</Item>
						<Item rounded style={styles.phoneInput}>
							<Input 
								placeholder="Phone number" 
								onChangeText={(value) => this.onChanged(value)} 
								value={this.state.phoneNumber} 
								keyboardType="numeric" 
								maxLength={10} 
								/>
						</Item>
					</View>
					<View padder>
						<Button block onPress={() => navigation.navigate('PhoneValidation')}>
							<Text>Sign in</Text>
						</Button>
					</View>
				</Content>
			</Container>
		);
	}
}
PhoneLogin = reduxForm({
	form: "login",
})(PhoneLogin);

const styles = StyleSheet.create({
	phoneCode: {
		width: 60
	},
	title: {
		textAlign: 'center',
		margin: 30
	},
	logo: {
		width: 100,
		height: 100,
		marginBottom: 20
	},
	phoneInput:{
		flex: 1.0,
	},
	flowLayout:{
		flexDirection: "row" 
	}
})
export default PhoneLogin;
