import React, { Component } from 'react'
import { Container, Button, Text } from 'native-base'

class Login extends Component {

    render() {
        const { navigation } = this.props
        return (
            <Container>
                <Button onPress={() => navigation.navigate('PhoneLogin')}>
                    <Text> Log In </Text>
                </Button>
            </Container>
        )
    }
}

export default Login