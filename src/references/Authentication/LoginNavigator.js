import React from 'react'
import { StackNavigator } from "react-navigation";
import { Icon } from "native-base";
//Path
import Login from './component/Login'
import PhoneLogin from './container/PhoneLoginContainer'
import PhoneValidation from './container/PhoneValidationContainer'
import Register from '../User/component/Register'

export const LoginNavigator = StackNavigator({
    Login: {screen: Login},
    PhoneLogin: {screen: PhoneLogin},
	PhoneValidation: { screen: PhoneValidation},
	Register: { screen: Register}
}, {
    initialRouteName: 'Register',
    headerMode: "none",
	navigationOptions: {
		title: '',
		tabBarIcon: ({ tintColor }) => (
			<Icon name="person" style={{ color: tintColor }}/>
		),
	}
})