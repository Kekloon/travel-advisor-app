const initialState = {
  isAuthenticating: true,
  isAuthenticated: false,
  authData: {},
  errorMessage: null
}

export default function(state = initialState, action) {
	switch (action.type) {
    case 'REQEUST_LOGIN':
      return Object.assign({}, state, {
        isAuthenticating: true
      })
    case 'SUCCESS_LOGIN':
      return Object.assign({}, state, {
        authData: action.payload,
        isAuthenticating: false,
        isAuthenticated: true
      })
    case 'FAILED_LOGIN':
      return Object.assign({}, state, {
        isAuthenticating: false,
        isAuthenticated: false,
        errorMessage: action.errorMessage
      })
    case 'SUCCESS_LOGOUT':
      return Object.assign({}, state, {
        isAuthenticating: false,
        isAuthenticated: false
      })
    case 'FAIL_LOGOUT':
      return Object.assign({}, state, {
        isAuthenticating: false,
        errorMessage: action.errorMessage
      })
		default:
			return state;
	}	
}
