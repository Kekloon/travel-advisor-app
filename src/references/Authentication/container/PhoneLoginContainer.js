import React, { Component } from "react";
import { connect } from "react-redux";
import PhoneLogin from "../component/PhoneLogin";
import { checkAuthenticationStatusRequest, facebookLoginRequest } from "../AuthenticationActions";

const mapDispatchToProps = (dispatch) => ({
  authenticationStatusListener: () => {
    dispatch(checkAuthenticationStatusRequest());
  },
  facebookLogin: () => {
    dispatch(facebookLoginRequest())
  }
});

const LoginContainer = connect(null, mapDispatchToProps)(PhoneLogin);

export default LoginContainer;
