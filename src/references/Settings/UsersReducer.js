const initialState = {
  userInfo: {
    isSubmitting: false,
    isFetching: false,
    payload: {},
    err: null,
    isCalculating: false,
    earnings: 0
  },
  localGuideApplication: {
    isFetching: false,
    isSubmitting: false,
    payload: {
      status: 'Not Applied'
    }
  },
  localGuideSchedule: {
    isFetching: false,
    isSubmitting: false,
    payload: []
  },
  posts: {
    isFetching: false,
    payload: []
  }
}

function users (state = initialState, action) {
  switch (action.type) {
    case 'REQUEST_GET_USER_PROFILE':
      return Object.assign({}, state, {
        userInfo: {
          payload: state.userInfo.payload,
          isFetching: true
        }
      })
    case 'SUCCESS_GET_USER_PROFILE':
      return Object.assign({}, state, {
        userInfo: {
          payload: action.userInfo,
          isFetching: false
        }
      })
    case 'FAIL_GET_USER_PROFILE':
      return Object.assign({}, state, {
        userInfo: {
          err: action.err,
          payload: state.userInfo.payload,
          isFetching: false
        }
      })
    case 'REQUEST_UPDATE_USER_PROFILE':
      return Object.assign({}, state, {
        userInfo: {
          isSubmitting: true,
          payload: state.userInfo.payload,
        }
      })
    case 'SUCCESS_UPDATE_USER_PROFILE':
      return Object.assign({}, state, {
        userInfo: {
          payload: state.userInfo.payload,
          isSubmitting: false
        }
      })
    case 'FAIL_UPDATE_USER_PROFILE':
      return Object.assign({}, state, {
        userInfo: {
          payload: state.userInfo.payload,
          isSubmitting: false,
          err: action.err
        }
      })
    case 'REQUEST_GET_LOCAL_GUIDE_DATA':
      return Object.assign({}, state, {
        localGuideApplication: {
          isFetching: true,
          payload: state.localGuideApplication.payload
        }
      })
    case 'SUCCESS_GET_LOCAL_GUIDE_DATA':
      return Object.assign({}, state, {
        localGuideApplication: {
          isFetching: false,
          payload: action.applyForm || state.localGuideApplication.payload
        }
      })
    case 'FAIL_GET_LOCAL_GUIDE_DATA':
      return Object.assign({}, state, {
        localGuideApplication: {
          isFetching: false,
          payload: state.localGuideApplication.payload,
          err: action.err
        }
      })
    case 'REQUEST_APPLY_LOCAL_GUIDE':
      return Object.assign({}, state, {
        localGuideApplication: {
          isSubmitting: true,
          payload: state.localGuideApplication.payload
        }
      })
    case 'SUCCESS_APPLY_LOCAL_GUIDE':
      return Object.assign({}, state, {
        localGuideApplication: {
          isSubmitting: false,
          payload: action.applyForm
        }
      })
    case 'FAIL_APPLY_LOCAL_GUIDE':
      return Object.assign({}, state, {
        localGuideApplication: {
          isSubmitting: false,
          payload: state.localGuideApplication.payload,
          err: action.err
        }
      })
    case 'REQUEST_GET_USER_POSTS':
      return Object.assign({}, state, {
        posts: {
          isFetching: true,
          payload: state.posts.payload
        }
      })
    case 'SUCCESS_GET_USER_POSTS':
      return Object.assign({}, state, {
        posts: {
          isFetching: false,
          payload: action.payload
        }
      })
    case 'FAIL_GET_USER_POSTS':
      return Object.assign({}, state, {
        posts: {
          isFetching: false,
          payload: state.posts.payload,
          err: action.err
        }
      })
    case 'REQUEST_GET_EARNINGS':
      return Object.assign({}, state, {
        userInfo: {
          isCalculating: true,
          payload: state.userInfo.payload,
          earnings: state.userInfo.earnings
        }
      })
    case 'SUCCESS_GET_EARNINGS':
      return Object.assign({}, state,{
        userInfo: {
          isCalculating: false,
          payload: state.userInfo.payload,
          earnings: action.earnings
        }
      })
    case 'FAIL_GET_EARNINGS':
      return Object.assign({}, state, {
        userInfo: {
          isCalculating:false,
          payload: state.userInfo.payload,
          earnings: state.userInfo.earnings
        }
      })
    case 'REQUEST_GET_SCHEDULE':
      return Object.assign({}, state, {
        localGuideSchedule: {
          isFetching: true,
          payload: state.localGuideSchedule.payload
        }
      })
    case 'SUCCESS_GET_SCHEDULE':
      return Object.assign({}, state, {
        localGuideSchedule: {
          isFetching: false,
          payload: action.payloadedit
        }
      })
    case 'FAIL_GET_SCHEDULE':
      return Object.assign({}, state, {
        localGuideSchedule: {
          isFetching: false,
          payload: state.localGuideSchedule.payload,
          err: action.err
        }
      })
    default:
      return state
  }
}

export default users
