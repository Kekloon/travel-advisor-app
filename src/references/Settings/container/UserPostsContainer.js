import { connect } from 'react-redux'
import UserPosts from '../component/UserPosts'
import { getUserPostsRequest } from '../../Posts/PostsActions'

const mapStateToProps = (state = {}) => ({
  user: state.users.userInfo.payload,
  localGuideApplication: state.users.localGuideApplication.payload,
  posts: state.users.posts.payload
})

const mapDispatchToProps = (dispatch, props) => ({
  getUserProfile: () => {
    dispatch(getUserProfileRequest())
  },
  getLocalGuideApplication: () => {
    dispatch(getLocalGuideApplicationRequest())
  },
  getPosts: (userId) => {
    dispatch(getUserPostsRequest(userId))
  }
})

const UserPostsContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(UserPosts)

export default UserPostsContainer
