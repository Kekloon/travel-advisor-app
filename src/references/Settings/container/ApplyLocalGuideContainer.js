import { connect } from 'react-redux'
import ApplyLocalGuide from '../component/ApplyLocalGuide'
import { applyLocalGuideRequest } from '../UsersActions'

const mapStateToProps = (state = {}) => ({
  isSubmitting: state.users.localGuideApplication.isSubmitting
})

const mapDispatchToProps = (dispatch, props) => ({
  applyLocalGuide: (body, navigation) => {
    dispatch(applyLocalGuideRequest(body, navigation))
  }
})

const ApplyLocalGuideContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(ApplyLocalGuide)

export default ApplyLocalGuideContainer
