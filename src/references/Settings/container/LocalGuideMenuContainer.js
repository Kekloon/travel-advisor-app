import { connect } from 'react-redux'
import LocalGuideMenu from '../component/LocalGuideMenu'

import { getEarningsRequest } from '../UsersActions'

const mapStateToProps = (state = {}) => ({
  earnings: state.users.userInfo.earnings
})

const mapDispatchToProps = (dispatch) => ({
  getEarnings: () => {
    dispatch(getEarningsRequest())
  }
})

const LocalGuideMenuContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(LocalGuideMenu)

export default LocalGuideMenuContainer
