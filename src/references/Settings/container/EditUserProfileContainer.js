import { connect } from 'react-redux'
import EditUserProfile from '../component/EditUserProfile'
import { updateUserProfileRequest, getUserProfileRequest } from '../UsersActions'

const mapStateToProps = (state = {}) => ({
  initialValues: state.users.userInfo.payload,
  isSubmitting: state.users.userInfo.isSubmitting
})

const mapDispatchToProps = (dispatch, props) => ({
  getUserProfile: () => {
    dispatch(getUserProfileRequest())
  },
  updateUserProfile: (body, navigation) => {
    dispatch(updateUserProfileRequest(body, navigation))
  }
})

const EditUserProfileContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(EditUserProfile)

export default EditUserProfileContainer
