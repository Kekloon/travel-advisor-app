import { connect } from 'react-redux'
import UserProfile from '../component/UserProfile'
import { getUserProfileRequest, getLocalGuideApplicationRequest } from '../UsersActions'
import { getMyPostsRequest } from '../../Posts/PostsActions'

const mapStateToProps = (state = {}) => ({
  user: state.users.userInfo.payload,
  localGuideApplication: state.users.localGuideApplication.payload,
  myPosts: state.users.posts.payload
})

const mapDispatchToProps = (dispatch, props) => ({
  getUserProfile: () => {
    dispatch(getUserProfileRequest())
  },
  getLocalGuideApplication: () => {
    dispatch(getLocalGuideApplicationRequest())
  },
  getMyPost: () => {
    dispatch(getMyPostsRequest())
  }
})

const UserProfileContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(UserProfile)

export default UserProfileContainer
