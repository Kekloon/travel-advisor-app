import { connect } from 'react-redux'
import LocalGuideScanner from '../component/LocalGuideScanner'
import { changeTripStatusRequest } from '../../Trips/TripsActions'

const mapStateToProps = (state = {}) => ({
})

const mapDispatchToProps = (dispatch, props) => ({
  completeTrip: (tripId) => {
    dispatch(changeTripStatusRequest(tripId, 'Completed'))
  }
})

const LocalGuideScannerContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(LocalGuideScanner)

export default LocalGuideScannerContainer
