import { connect } from 'react-redux'
import LocalGuideSchedule from '../component/LocalGuideSchedule'
import { getLocalGuideScheduleRequest } from '../../Settings/UsersActions'

const mapStateToProps = (state = {}) => ({
  trips: state.users.localGuideSchedule.payload
})

const mapDispatchToProps = (dispatch, props) => ({
  getLocalGuideSchedule: () => {
    dispatch(getLocalGuideScheduleRequest())
  }
})

const LocalGuideScheduleContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(LocalGuideSchedule)

export default LocalGuideScheduleContainer
