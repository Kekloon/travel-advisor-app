import { connect } from 'react-redux'
import Settings from '../component/Settings'
import { getUserProfileRequest } from '../UsersActions'

const mapStateToProps = (state = {}) => ({
  user: state.users.userInfo.payload
})

const mapDispatchToProps = (dispatch, props) => ({
  getUserProfile: () => {
    dispatch(getUserProfileRequest())
  },
})

const SettingsPage = connect(
  mapStateToProps,
  mapDispatchToProps
)(Settings)

export default SettingsPage
