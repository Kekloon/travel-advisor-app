import React from "react";
import { StackNavigator } from "react-navigation";
import Settings from "./container/SettingsContainer";
import EditUserProfile from "./container/EditUserProfileContainer";
import UserProfile from './container/UserProfileContainer'
import EditPost from '../Posts/container/EditPostContainer'
import ApplyLocalGuide from './container/ApplyLocalGuideContainer'
import LocalGuideMenu from './container/LocalGuideMenuContainer'
import LocalGuideSchedule from './container/LocalGuideScheduleContainer'
import LocalGuideScanner from './container/LocalGuideScannerContainer'
import { TripsNavigator } from '../Trips/TripsNavigator'
import { Icon } from "native-base";

export const SettingsNavigator = StackNavigator({
	MyUserProfile: {
		screen: UserProfile,
	},
	EditPost: {
		screen: EditPost
	},
  Settings: {
		screen: Settings,
	},
	EditUserProfile: {
		screen: EditUserProfile,
	},
	ApplyLocalGuide: {
		screen: ApplyLocalGuide
	},
	LocalGuideMenu: {
		screen: LocalGuideMenu
	},
	LocalGuideSchedule: {
		screen: LocalGuideSchedule
	},
	LocalGuideScanner: {
		screen: LocalGuideScanner
	},
	UsersTrips: {
		screen: TripsNavigator
	}
},
{
	initialRouteName: "MyUserProfile",
	headerMode: "none",
	navigationOptions: {
		title: '',
		tabBarIcon: ({ tintColor }) => (
			<Icon name="person" style={{ color: tintColor }}/>
		),
	}
});