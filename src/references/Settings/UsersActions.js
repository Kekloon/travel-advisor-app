import { AsyncStorage } from 'react-native'
import { uploadFile } from "../../config/helper";
import axios from 'axios'
import _ from 'lodash'
import firebase from 'react-native-firebase'

const db = firebase.database();
const firestoreDb = firebase.firestore();

export const getMyUserProfileRequest = () => async (dispatch) => {
  dispatch(requestGetMyUserProfile())
  const userId = await AsyncStorage.getItem('uid')
  firestoreDb.collection('users').doc(userId).onSnapshot((doc) => {
    const user = doc.data()
    dispatch(successGetMyUserProfile(user))
  }, (e) => {
    dispatch(failGetMyUserProfile(e.message))
  })

}

export const requestGetMyUserProfile = () => ({
  type: 'REQUEST_GET_MY_USER_PROFILE'
})

export const successGetMyUserProfile = (userInfo) => ({
  type: 'SUCCESS_GET_MY_USER_PROFILE',
  userInfo
})

export const failGetMyUserProfile = (err) => ({
  type: 'FAIL_GET_MY_USER_PROFILE',
  err
})

export const updateUserProfileRequest = (body, navigation = null) => async (dispatch) => {
  dispatch(requestUpdateUserProfile())
  const userId = await AsyncStorage.getItem('uid')
  let formData = body
  if(body.profilePicture.path) {

    let fileUrl
    try {
      fileUrl = await uploadFile(body.profilePicture.path)
    } catch (e) {
  
    }
    formData = _.merge(body, { profilePicture: fileUrl })

  }
  try {
    await firestoreDb.collection('users').doc(userId).update(formData)
  } catch(e) {
    dispatch(failUpdateUserProfile(e.message))
  }
  if (navigation) {
    navigation.goBack()
  }
  dispatch(successUpdateUserProfile())
}

export const requestUpdateUserProfile = () => ({
  type: 'REQUEST_UPDATE_USER_PROFILE'
})

export const successUpdateUserProfile = (userInfo) => ({
  type: 'SUCCESS_UPDATE_USER_PROFILE',
  userInfo
})

export const failUpdateUserProfile = (err) => ({
  type: 'FAIL_UPDATE_USER_PROFILE',
  err
})

export const getLocalGuideApplicationRequest = () => async (dispatch) => {
  dispatch(requestGetLocalGuideData())
  const userId = await AsyncStorage.getItem('uid')

  firestoreDb.collection('local_guide_form').doc(userId).onSnapshot((doc) => {
    const application = doc.data()
    dispatch(successGetLocalGuideData(application))
  })
}

export const requestGetLocalGuideData = () => ({
  type: 'REQUEST_GET_LOCAL_GUIDE_DATA'
})

export const successGetLocalGuideData = (applyForm) => ({
  type: 'SUCCESS_GET_LOCAL_GUIDE_DATA',
  applyForm
})

export const failGetLocalGuideData = (err) => ({
  type: 'FAIL_GET_LOCAL_GUIDE_DATA',
  err
})

export const applyLocalGuideRequest = (body, navigation = null) => async (dispatch) => {
  dispatch(requestApplyLocalGuide())
  const userId = await AsyncStorage.getItem('uid')
  try {
    await firestoreDb.collection('local_guide_form').doc(userId).set(_.merge(body, { status: 'Pending', createdAt: new Date()}))
  } catch(e) {
    dispatch(failApplyLocalGuide(e.message))
  }
  if (navigation) {
    navigation.goBack()
  }
  dispatch(successApplyLocalGuide())
}

export const requestApplyLocalGuide = () => ({
  type: 'REQUEST_APPLY_LOCAL_GUIDE'
})

export const successApplyLocalGuide = (userInfo) => ({
  type: 'SUCCESS_APPLY_LOCAL_GUIDE',
})

export const failApplyLocalGuide = (err) => ({
  type: 'FAIL_APPLY_LOCAL_GUIDE',
  err
})

export const getEarningsRequest = () => async (dispatch) => {
  dispatch(requestGetEarnings())
  const userId = await AsyncStorage.getItem('uid')
  try{
    tripRef = firestoreDb.collection('trips')
    snapshot = await tripRef.where('status', '==' ,'Completed').where('ownerId', '==', userId).get()
  }catch(e){
    dispatch(failGetEarnings())
  }
  let total = 0;
  snapshot.forEach(doc => {
    total += doc.data().total
  })
  dispatch(successGetEarnings(total.toFixed(2)))
}

export const requestGetEarnings = () => ({
  type: 'REQUEST_GET_EARNINGS'
})

export const successGetEarnings = (earnings) => ({
  type: 'SUCCESS_GET_EARNINGS',
  earnings
})


export const failGetEarnings = () => ({
  type: 'FAIL_GET_EARNINGS',
  err
})

export const getLocalGuideScheduleRequest = () => async (dispatch) => {
	dispatch(requestGetScheduleTrips())
  const userId = await AsyncStorage.getItem('uid')
  let schedules = [], tripRef, snapshot
  try {
    tripRef = firestoreDb.collection('trips')
    snapshot = await tripRef.where('ownerId', '==', userId).where('status', '==', 'Upcoming').orderBy('startDateTime', 'desc').get()
  } catch (e) {
		dispatch(failGetScheduleTrips(e.message))
  }
  snapshot.forEach(function(doc) {
		schedules.push(doc.data())
  })
	dispatch(successGetScheduleTrips(schedules))
}

export const requestGetScheduleTrips = () => ({
  type: 'REQUEST_GET_SCHEDULE'
})

export const successGetScheduleTrips = (payload) => ({
  type: 'SUCCESS_GET_SCHEDULE',
  payload
})

export const failGetScheduleTrips = (err) => ({
  type: 'FAIL_GET_SCHEDULE',
  err
})

