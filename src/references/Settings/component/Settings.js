import React, { Component } from 'react'
import { Container, Header, Content, List, ListItem, Text, Icon, Left, Body, Right, Button, Toast, Thumbnail } from 'native-base'
import { Platform, RefreshControl, StyleSheet, Alert, StatusBar } from 'react-native'
import { COLOR, DIMENS } from '../../../config/constant'
import firebase from 'react-native-firebase'
import _ from 'lodash'
import { connect } from 'react-redux'

class Settings extends Component {
  static navigationOptions = ({ navigation }) => {
    return {
      headerTitle: 'Settings',
			tabBarVisible: false,
      headerLeft: (
        <Button transparent onPress={() => navigation.goBack()}><Icon name='arrow-back'/></Button>
      ),
    }
  }
  constructor(props) {
    super(props)
    this.state = {
      user: {}
    }
  }

  componentDidMount () {
    this.setState({
      user: this.props.user
    })
  }

  signOut = () => {
    Alert.alert(
      'Logout',
      'Are you sure you want to logout?',
      [
        {text: 'Maybe later', style: 'cancel'},
        {text: 'Logout', onPress: () => firebase.auth().signOut()}
      ],
      { cancelable: false }
    )
  }

  render () {
    const { navigation } = this.props
    const { user } = this.state
    return (
      <Container style={styles.container}>
        <Content>
          <MenuItem onPress={() => this.signOut()} label='Logout'/>
          <MenuItem onPress={() => navigation.navigate('EditUserProfile')} label='Edit Profile'/>
          {
            user.isLocalGuide
              ? <MenuItem onPress={() => navigation.navigate('LocalGuideMenu')} label='Local Guide'/>
              : null
          }
        </Content>
      </Container>
    )
  }
}

const MenuItem = ({ onPress, hasArrow, label }) => (
  <List style={styles.list}>
    <ListItem icon onPress={onPress} style={styles.listItem}>
      <Body style={styles.listBody}>
        <Text style={styles.menuLabel}>{label}</Text>
      </Body>
      {/* <Right style={styles.listRight}>
        { hasArrow ? <SvgUri width={22} height={22} svgXmlData={ICON.right}/> : null }
      </Right> */}
    </ListItem>
  </List>
)

const styles = StyleSheet.create({
  container: {
    backgroundColor: COLOR.grayLight,
  },
  list: {
    backgroundColor: '#FAFBFB',
  },
  listItem: {
    marginLeft: 0,
    // paddingLeft: 35,
    // paddingTop: 18,
    // paddingBottom: 18,

    height: 65,
  },
  listBody: {
    paddingLeft: 35,
    paddingTop: 20,
    paddingBottom: 20,
    height: 65,
    borderBottomWidth: 1,
    borderBottomColor: '#D3DCE1'
  },
  listRight: {
    height: 65,
    borderBottomWidth: 1,
    borderBottomColor: '#D3DCE1'
  },
  menuLabel: {
    fontSize: 22
  }
})

export default Settings
