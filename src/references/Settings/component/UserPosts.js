import React, { Component } from 'react'
import { Container, Header, Content, List, ListItem, Text, Icon, Left, Body, Right, Button, Toast, Thumbnail } from 'native-base'
import { Platform, RefreshControl, StyleSheet, Alert, StatusBar, Image, TouchableOpacity, View } from 'react-native'
import { COLOR, DIMENS } from '../../../config/constant'
import _ from 'lodash'
import { connect } from 'react-redux'
import Video from "react-native-video";

class UserPosts extends Component {
  constructor(props) {
    super(props)
    this.state = {
			posts: [],
			userId: props.userId || null
    }
    this.renderPost = this.renderPost.bind(this)
  }

  async componentDidMount () {
		let { userId } = this.state
		if(!userId) {
  		userId = await AsyncStorage.getItem('uid')
		}
		this.props.getUserPosts(userId)
	}
	
	componentWillReceiveProps (nextProps) {
    
    this.setState({
      posts: nextProps.posts
    })
  }
  renderPost = (post) => {
		const { navigation } = this.props
    const postLength = DIMENS.screenWidth / 3
    return (
      <TouchableOpacity onPress={() => navigation.navigate('EditPost', { post })}>
        {
          post.mediaType === 'still'
            ? <Image style={{width: postLength, height: postLength}} source={{uri: post.media[0]}}/>
            : <VideoGrid media={post.media} />
        }
      </TouchableOpacity>
    )
  }
  render () {
		const { navigation } = this.props
		const { posts } = this.state
    return (
			<Content>
				<List
					contentContainerStyle={styles.contentContainer}
					style={styles.list}
					dataArray={posts}
					renderRow={this.renderPost}
				>
				</List>
			</Content>
    )
  }
}

class VideoGrid extends Component {
	render() {
    const postLength = DIMENS.screenWidth / 3
		return (
			<Video 
        ref={(ref) => {
          this.player = ref
        }}
        paused={true}
        onLoad={()=>{this.player.seek(0)}}
        resizeMode="cover"
        source={{uri: this.props.media}}
        style={{height: postLength, width: postLength}}	
      />
		)
	}
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#FFF',
    justifyContent: 'center'
  },
  topSection: {
    paddingTop: 20,
    paddingBottom: 20,
    borderBottomWidth: 1,
    borderBottomColor: COLOR.grayLight,   
    justifyContent: 'center',
    alignItems: "center",
  },
  starRatingWrapper: {
    flexDirection: 'row'
  },
  starRatingText: {
    marginRight: 10
  },
  localGuideStatus: {
    marginTop: 20,
    justifyContent: 'center',
    alignItems: "center",
  },
  profilePicture: {
    marginBottom: 5,
  },
  userName: {
    marginBottom: 10
  },
  contentContainer: {
    flexDirection: 'row',
    // justifyContent: 'center',
    flexWrap: 'wrap'
  },
  list: {
  },
  listItem: {
    marginLeft: 0,
    // paddingLeft: 35,
    // paddingTop: 18,
    // paddingBottom: 18,

    height: 65,
  },
  listBody: {
    paddingLeft: 35,
    paddingTop: 20,
    paddingBottom: 20,
    height: 65,
    borderBottomWidth: 1,
    borderBottomColor: '#D3DCE1'
  },
  listRight: {
    height: 65,
    borderBottomWidth: 1,
    borderBottomColor: '#D3DCE1'
  },
  menuLabel: {
    fontSize: 22
  }
})

export default UserPosts
