import React, { Component } from 'react'
import { Container, Header, Content, List, ListItem, Text, Icon, Left, Body, Right, Button, Toast, Thumbnail, View } from 'native-base'
import { Platform, RefreshControl, StyleSheet, Alert, StatusBar } from 'react-native'
import { COLOR, DIMENS } from '../../../config/constant'
import firebase from 'react-native-firebase'
import _ from 'lodash'
import { connect } from 'react-redux'

class LocalGuideMenu extends Component {
  static navigationOptions = {
    tabBarVisible: false,
  }

  constructor(props) {
    super(props)

  }
  
  componentDidMount() {
    this.props.getEarnings()
  }

  render() {
    const { navigation } = this.props
    const { earnings } = this.props
    return (
			<Container style={styles.container}>
        <Content>
          <Body style={styles.earningsBody}>
            <Text style={styles.earningsTitle} > Earnings </Text>
            <Text style={styles.earningsValue}> RM {earnings}</Text>
          </Body>
          <MenuItem onPress={() => navigation.navigate('LocalGuideScanner')} label='End Tour'/>
          <MenuItem onPress={() => navigation.navigate('LocalGuideSchedule')} label='Tour Schedule'/>
        </Content>
      </Container>
    )
  }
}

const MenuItem = ({ onPress, hasArrow, label }) => (
  <List style={styles.list}>
    <ListItem icon onPress={onPress} style={styles.listItem}>
      <Body style={styles.listBody}>
        <Text style={styles.menuLabel}>{label}</Text>
      </Body>
    </ListItem>
  </List>
)

const styles = StyleSheet.create({
  container: {
    backgroundColor: COLOR.grayLight,
  },
  list: {
    backgroundColor: '#FAFBFB',
  },
  listItem: {
    marginLeft: 0,
    // paddingLeft: 35,
    // paddingTop: 18,
    // paddingBottom: 18,

    height: 65,
  },
  listBody: {
    paddingLeft: 35,
    paddingTop: 20,
    paddingBottom: 20,
    height: 65,
    borderBottomWidth: 1,
    borderBottomColor: '#D3DCE1'
  },
  listRight: {
    height: 65,
    borderBottomWidth: 1,
    borderBottomColor: '#D3DCE1'
  },
  menuLabel: {
    fontSize: 22
  },
  earningsBody: {
    backgroundColor: '#FFF',
    paddingBottom: 60,
    paddingTop: 60,
    marginTop: 30,
    marginBottom: 30,
    alignSelf: 'stretch'
  },
  earningsTitle:{
    textAlign: 'center',
    fontSize: 22,
  },
  earningsValue:{
    textAlign: 'center',
    fontSize: 44
  }
})

export default LocalGuideMenu
