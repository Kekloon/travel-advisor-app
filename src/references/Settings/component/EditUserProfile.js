import React, { Component } from 'react'
import { Container, Content, Text, Form, Button, Item, Left, Right, Icon, Body, Title, Toast, Spinner } from 'native-base'
import { View, Platform } from 'react-native'
import { alphaNumeric, maxLength, email, required } from '../../../config/validations'
import { COLOR } from '../../../config/constant'
import { TextInput, ImageInput } from '../../../common'
import { Field, reduxForm } from 'redux-form';

const maxLength20 = maxLength(20);

class EditUserProfile extends Component {
	static navigationOptions = ({ navigation }) => {
    return {
      headerTitle: 'Edit Account',
			tabBarVisible: false,
      headerLeft: (
        <Button transparent onPress={() => navigation.goBack()}><Icon name='arrow-back'/></Button>
      ),
    }
  }

	updateUserProfile (values) {
		this.props.updateUserProfile(values, this.props.navigation)
	}
	render() {
		const { handleSubmit, isSubmitting } = this.props
		return (
			<Container style={styles.container}>
				<Content style={styles.content}>
					<Form style={styles.formFields}>
						<View style={styles.profilePictureField}>
							<Field name='profilePicture' component={ImageInput} />
						</View>
						<Field
							name='price'
							placeholder='Price per hour'
							component={TextInput}
							validate={[required]} />
						<Field
							name='firstName'
							icon='person'
							placeholder='First Name'
							autoFocus={true}
							autoCorrect={true}
							autoCapitalize='words'
							component={TextInput}
							validate={[required, maxLength20]} />
						<Field
							name='lastName'
							icon='person'
							placeholder='Last Name'
							autoFocus={true}
							autoCorrect={true}
							autoCapitalize='words'
							component={TextInput}
							validate={[maxLength20]} />
						<Button primary onPress={handleSubmit(this.updateUserProfile.bind(this))}>
							<Text>Save</Text>
						</Button>
					</Form>
				</Content>
			</Container>
			
		)
	}
}

const styles = {
  container: {
		flex: 1,
		backgroundColor: '#FFFFFF',
		// paddingTop: 80,

	},
	profilePictureField: {
		  
    justifyContent: 'center',
    alignItems: "center",
	},
	content: {
		paddingTop: 30,
		// paddingLeft: 30,
		// paddingRight: 30,
		// paddingBottom: 30
	},
  formFields: {
		margin: 0,
		marginBottom: 25,
	},
}

EditUserProfile = reduxForm({
	form: 'EditUserProfileForm',
})(EditUserProfile)

export default EditUserProfile
