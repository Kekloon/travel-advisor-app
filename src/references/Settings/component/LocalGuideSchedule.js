import React, { Component } from 'react'
import { Container, Card, CardItem, Body, Left, Right, Thumbnail, Text, Button} from 'native-base'
import { Platform, RefreshControl, StyleSheet, Alert, StatusBar, View } from 'react-native'
import { COLOR, DIMENS } from '../../../config/constant'
import firebase from 'react-native-firebase'
import _ from 'lodash'
import { connect } from 'react-redux'
import { Agenda } from 'react-native-calendars'
import moment from 'moment'

class LocalGuideSchedule extends Component {
  static navigationOptions = {
		tabBarVisible: false,
  }
  constructor(props) {
    super(props)
    this.state = {
      trips: {}
    }
  }

  componentDidMount () {
    this.props.getLocalGuideSchedule()
  }
  componentWillReceiveProps(nextProps) {
    this.setState({
      trips: _.groupBy(nextProps.trips, (obj) => { return moment(obj.startDateTime).format('YYYY-MM-DD')})
    })
  }

  render () {
    const { navigation } = this.props
    const { trips } = this.state
    console.log(trips)
    return (
			<Container style={styles.container}>
        <Agenda 
          items={trips}
          minDate={_parseAgendaDateFormat(new Date())}
          renderEmptyData = {() => {return (<View />);}}
          renderItem={agendaItem}
          renderEmptyDate={() => {return (<View />);}}
          rowHasChanged={(r1, r2) => {return r1.text !== r2.text}}
        />
      </Container>
    )
  }
}

const agendaItem = (item, firstItemInDay) => {
  return (
    <Card style={{flex: 0}}>
      <CardItem>

        <Body>
          <Text>{item.post.place}</Text>
        </Body>
        <Right>
          <Thumbnail source={{uri: item.post.user.profilePicture}} />
        </Right>
      </CardItem>
    </Card>
  )
}

const _parseAgendaDateFormat = (date) => {
  return moment(date).format('YYYY-MM-DD')
}

const MenuItem = ({ onPress, hasArrow, label }) => (
  <List style={styles.list}>
    <ListItem icon onPress={onPress} style={styles.listItem}>
      <Body style={styles.listBody}>
        <Text style={styles.menuLabel}>{label}</Text>
      </Body>
    </ListItem>
  </List>
)

const styles = StyleSheet.create({
  container: {
    backgroundColor: COLOR.grayLight,
  },
  list: {
    backgroundColor: '#FAFBFB',
  },
  listItem: {
    marginLeft: 0,
    // paddingLeft: 35,
    // paddingTop: 18,
    // paddingBottom: 18,

    height: 65,
  },
  listBody: {
    paddingLeft: 35,
    paddingTop: 20,
    paddingBottom: 20,
    height: 65,
    borderBottomWidth: 1,
    borderBottomColor: '#D3DCE1'
  },
  listRight: {
    height: 65,
    borderBottomWidth: 1,
    borderBottomColor: '#D3DCE1'
  },
  menuLabel: {
    fontSize: 22
  }
})

export default LocalGuideSchedule
