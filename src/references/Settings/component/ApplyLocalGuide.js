import React, { Component } from 'react'
import { Container, Content, Text, Form, Button, Item, Left, Right, Icon, Body, Title, Toast, Spinner } from 'native-base'
import { View, Platform } from 'react-native'
import { alphaNumeric, maxLength, email, required } from '../../../config/validations'
import { COLOR, LANG, COUNTRY } from '../../../config/constant'
import { TextInput, PickerInput } from '../../../common'
import { Field, reduxForm } from 'redux-form';

const maxLength20 = maxLength(20);

class ApplyLocalGuide extends Component {
	static navigationOptions = {
		tabBarVisible: false,
  }

	applyLocalGuide (values) {
		this.props.applyLocalGuide(values, this.props.navigation)
	}
	render() {
		const { handleSubmit, isSubmitting } = this.props
		return (
			<Container style={styles.container}>
				<Content style={styles.content}>
					<Form style={styles.formFields}>
						<Field
							name='isDriver'
							label='Will you be a driver'
							component={PickerInput}
							validate={[required]} >
							{
								Platform.OS === 'android' ? <Item label='Choose one...' /> : null
							}
							<Item label={'Yes'} value={true}/>
							<Item label={'No'} value={false}/>
						</Field>
						<Field
							name='reason'
							placeholder='Write something...'
							label='Why do you want to become a hopfer?'
							autoFocus={true}
							autoCorrect={true}
							autoCapitalize='words'
							component={TextInput}
							validate={[required, maxLength20]} />
						<Field
							name='country'
							label='Country of origin'
							component={PickerInput}
							validate={[required]} >
							{
								Platform.OS === 'android' ? <Item label='Choose one...' /> : null
							}
							{
								_.map(COUNTRY, (item) => (
									<Item label={item} value={item}/>
								))
							}
						</Field>
						<Field
							name='language'
							label='Language'
							component={PickerInput}
							validate={[required]} >
							{
								Platform.OS === 'android' ? <Item label='Choose one...' /> : null
							}
							{
								_.map(LANG, (obj) => (
									<Item label={obj.language} value={obj.language}/>
								))
							}
						</Field>
						<Button primary onPress={handleSubmit(this.applyLocalGuide.bind(this))}>
							<Text>Save</Text>
						</Button>
					</Form>
				</Content>
			</Container>
			
		)
	}
}

const styles = {
  container: {
		flex: 1,
		backgroundColor: '#FFFFFF',
		// paddingTop: 80,

	},
	content: {
		marginTop: 80,
		paddingLeft: 30,
		paddingRight: 30,
		// paddingBottom: 30
	},
  formFields: {
		margin: 0,
		marginBottom: 25,
	},
}

ApplyLocalGuide = reduxForm({
	form: 'ApplyLocalGuideForm',
})(ApplyLocalGuide)

export default ApplyLocalGuide
