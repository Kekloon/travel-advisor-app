import React, { Component } from 'react'
import { Container, Header, Content, List, ListItem, Text, Icon, Left, Body, Right, Button, Toast, Thumbnail } from 'native-base'
import { Platform, RefreshControl, StyleSheet, Alert, StatusBar } from 'react-native'
import { COLOR, DIMENS } from '../../../config/constant'
import firebase from 'react-native-firebase'
import _ from 'lodash'
import { connect } from 'react-redux'
import Camera from 'react-native-camera'

class LocalGuideScanner extends Component {
  static navigationOptions = {
		tabBarVisible: false,
  }
  constructor(props) {
    super(props)
    // this.state = {
    //   user: {}
		// }
		this.onBarCodeRead = this.onBarCodeRead.bind(this)
  }
	onBarCodeRead = (e) => {
		const tripId = e.data
		this.props.completeTrip(tripId)
	}
  render () {
    const { navigation } = this.props
    return (
			<Container style={styles.container}>
        <Camera style={{height: DIMENS.screenHeight, width: DIMENS.screenWidth}} onBarCodeRead={this.onBarCodeRead.bind(this)}/>
      </Container>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: COLOR.grayLight,
  },
  list: {
    backgroundColor: '#FAFBFB',
  },
  listItem: {
    marginLeft: 0,
    // paddingLeft: 35,
    // paddingTop: 18,
    // paddingBottom: 18,

    height: 65,
  },
  listBody: {
    paddingLeft: 35,
    paddingTop: 20,
    paddingBottom: 20,
    height: 65,
    borderBottomWidth: 1,
    borderBottomColor: '#D3DCE1'
  },
  listRight: {
    height: 65,
    borderBottomWidth: 1,
    borderBottomColor: '#D3DCE1'
  },
  menuLabel: {
    fontSize: 22
  }
})

export default LocalGuideScanner
