import React, { Component } from 'react'
import { connect } from 'react-redux'
import { StackNavigator } from 'react-navigation'


import { LoginNavigator } from '../Authenication/LoginNavigation'
import { checkIsUserExist } from './UserAction'

import Main from '../Route'
import MainLoading from '../modules/MainLoading';
import UserRegister from '../User/container/UserRegisterContainer';
import mainpage from '../Authenication/container/PhoneLoginContainer'

export const CreateUserNavigate = (isUserExist) => {
    return StackNavigator({
        Main: { screen: Main },
        UserRegister: { screen: UserRegister },
        Login:{screen:LoginNavigator},

    }, {
            initialRouteName: isUserExist ? 'Main' : 'UserRegister',
            headerMode: 'none'

        })
}

class UserNavigation extends Component {
    constructor(props) {
        super(props)
        this.state = {
            isLoading: false,
            shouldUpdate: true,
            isUserExist: false,
        }
    }

    componentDidMount() {
        this.props.checkIsUserExist()
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            isLoading: nextProps.isLoading,
            shouldUpdate: nextProps.shouldUpdate,
            isUserExist: nextProps.isUserExist,

        })
    }

    shouldComponentUpdate() {
        return this.state.shouldUpdate
    }

    render() {
        const UserNavigate = CreateUserNavigate(this.state.isUserExist)
        return (
            this.props.isLoading ?
                <MainLoading />
                :
                <UserNavigate />
        )
    }
}

const mapStateToProps = (state) => ({
    isUserExist: state.userReducer.isUserExist,
    isLoading: state.userReducer.isLoading,
    shouldUpdate: state.userReducer.shouldUpdate
})

const mapDispatchToProps = (dispatch) => ({
    checkIsUserExist: () => {
        dispatch(checkIsUserExist())
    }
})

export default connect(mapStateToProps, mapDispatchToProps)(UserNavigation)