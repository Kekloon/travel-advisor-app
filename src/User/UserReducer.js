const initialState = {
    isLoading: false,
    errorMessage: null,
    isUserExist: false,
    shouldUpdate: true,
    user: null,
    isSuccess: false,
    isSuccessRegisterUser: false,

    updateUserProfile: {
        isLoading: false,
        isSuccess: false,
        errorMessage: null,
        isUploadingImage: false
    },
    getFollowersList:{
        isLoading:false,
        userFollowersList:null,
        errorMessage:null,
    },
    getFollowingList:{
        isLoading:false,
        userFollowingList:null,
        errorMessage:null,
    }

}

export default function (state = initialState, action) {
    switch (action.type) {
        case 'REQUEST_CHECK_USER_EXIST':
            return Object.assign({}, state, {
                isLoading: true,
                shouldUpdate: true,
            })
        case 'USER_IS_EXIST':
            return Object.assign({}, state, {
                isLoading: false,
                isUserExist: true,
                shouldUpdate: false,
            })
        case 'USER_IS_NOT_EXIST':
            return Object.assign({}, state, {
                isLoading: false,
                isUserExist: false,
                shouldUpdate: false,
            })
        case 'FAILED_CHECK_USER_EXIST':
            return Object.assign({}, state, {
                isLoading: false,
                errorMessage: action.err
            })
        case 'REQUEST_REGISTER_NEW_USER':
            return Object.assign({}, state, {
                isLoading: true,
                isSuccessRegisterUser: false,
            })
        case 'SUCCESS_REGISTER_NEW_USER':
            return Object.assign({}, state, {
                isLoading: false,
                isSuccessRegisterUser: true,
            })
        case 'FAILED_REGISTER_NEW_USER':
            return Object.assign({}, state, {
                isLoading: false,
                isSuccessRegisterUser: false,
                errorMessage: action.err
            })
        case 'REQUEST_GET_USER_DATA':
            return Object.assign({}, state, {
                isLoading: true,
            })
        case 'SUCCESS_GET_USER_DATA':
            return Object.assign({}, state, {
                isLoading: false,
                user: action.payload,
            })
        case 'FAILED_GET_USER_DATA':
            return Object.assign({}, state, {
                isLoading: false,
                errorMessage: action.err
            })
        case 'REQUEST_UPDATE_PROFILE':
            return Object.assign({}, state, {
                updateUserProfile: {
                    isLoading: true,
                    isSuccess: false,
                    isUploadingImage: true,
                }
            })
        case 'SUCCESS_UPDATE_PROFILE':
            return Object.assign({}, state, {
                updateUserProfile: {
                    isLoading: false,
                    isSuccess: true,
                    isUploadingImage: false,
                }
            })
        case 'FAILED_UPDATE_PROFILE':
            return Object.assign({}, state, {
                updateUserProfile: {
                    isLoading: false,
                    isSuccess: false,
                    isUploadingImage: false,
                    errorMessage: action.err
                }
            })
        case 'REQUEST_GET_FOLLOWERS_LIST':
        return Object.assign({},state,{
            getFollowersList:{
                isLoading:true,
            }
        })
        case 'SUCCESS_GET_USER_FOLLOWERS_LIST':
        return Object.assign({},state,{
            getFollowersList:{
                isLoading:false,
                userFollowersList:action.payload,
            }
        })
        case 'FAILED_GET_USER_FOLLOWERS_LIST':
        return Object.assign({},state,{
            getFollowersList:{
                isLoading:false,
                errorMessage:action.err,
            }
        })
        case 'REQUEST_GET_FOLLOWING_LIST':
        return Object.assign({},state,{
            getFollowingList:{
                isLoading:true,
            }
        })
        case 'SUCCESS_GET_USER_FOLLOWING_LIST':
        return Object.assign({},state,{
            getFollowingList:{
                isLoading:false,
                userFollowingList:action.payload,
            }
        })
        case 'FAILED_GET_USER_FOLLOWING_LIST':
        return Object.assign({},state,{
            getFollowingList:{
                isLoading:false,
                errorMessage:action.err,
            }
        })
        case 'SIGN_OUT':
        return state;
        default:
            return state
    }
}