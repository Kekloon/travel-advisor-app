import { connect } from 'react-redux'
import UserRegisterComponent from '../component/UserRegisterComponent'
import {registerUser} from '../UserAction'


const mapStateToProps = (state) => ({
    isLoading: state.userReducer.isLoading,
    isSuccessRegisterUser:state.userReducer.isSuccessRegisterUser,
    isUserExist:state.userReducer.isUserExist,
})

const mapDispatchToProps = (dispatch, props) => ({
    registerUser:(user)=>{
        dispatch(registerUser(user,props.navigation))
    }
})

export default connect(mapStateToProps, mapDispatchToProps)(UserRegisterComponent)

