import UserFollowingComponent from '../component/UserFollowingComponent'
import { connect } from 'react-redux'
import { getFollowingList } from '../UserAction'

const mapStateToProps =(state)=>({
    userFollowingList:state.userReducer.getFollowingList.userFollowingList,
    isLoading:state.userReducer.getFollowingList.isLoading
})

const mapDispatchToProps =(dispatch)=>({
    getFollowingList:()=>{
        dispatch(getFollowingList())
    },
})

export default connect(mapStateToProps,mapDispatchToProps)(UserFollowingComponent)