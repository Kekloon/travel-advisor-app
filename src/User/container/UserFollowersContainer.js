import UserFollowersComponent from '../component/UserFollowersComponent'
import { connect } from 'react-redux'
import { getFollowersList } from '../UserAction'

const mapStateToProps =(state)=>({
    userFollowersList:state.userReducer.getFollowersList.userFollowersList,
    isLoading:state.userReducer.getFollowersList.isLoading
})

const mapDispatchToProps =(dispatch)=>({
    getFollowersList:()=>{
        dispatch(getFollowersList())
    },
})

export default connect(mapStateToProps,mapDispatchToProps)(UserFollowersComponent)