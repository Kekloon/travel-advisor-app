import { connect } from 'react-redux'
import UserProfileComponent from '../component/UserProfileComponent'
import {signOut,updateUserName,updateUserGender,updateUserDateOfBirth,updateUserEmail,changeProfilePicture} from '../UserAction'

const mapStateToProps=(state)=>({
    user: state.userReducer.user,
    isLoading:state.userReducer.updateUserProfile.isLoading,
    isSuccess:state.userReducer.updateUserProfile.isSuccess,
    isUploadingImage:state.userReducer.updateUserProfile.isUploadingImage
})

const mapDispatchToProps =(dispatch)=>({
    signOut:()=>{
        dispatch(signOut())
    },
    updateUserName:(userName)=>{
        dispatch(updateUserName(userName))
    },
    updateUserGender:(userGender)=>{
        dispatch(updateUserGender(userGender))
    },
    updateUserEmail:(userEmail)=>{
        dispatch(updateUserEmail(userEmail))
    },
    updateUserDateOfBirth:(userDateOfBirth)=>{
        dispatch(updateUserDateOfBirth(userDateOfBirth))
    },
    changeProfilePicture:(picture)=>{
        dispatch(changeProfilePicture(picture))
    }


})

export default connect(mapStateToProps,mapDispatchToProps)(UserProfileComponent)