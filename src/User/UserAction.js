import firebase from 'react-native-firebase'
import { AsyncStorage, Alert,BackHandler } from 'react-native'
import { uploadFile, deleteFile } from '../config/helper'
const firestore = firebase.firestore()

export const checkIsUserExist = () => async (dispatch) => {
    console.log('checkIsUserExist')
    dispatch(_requestCheckUserExist())
    const userId = await AsyncStorage.getItem('uid')
    firestore.collection('user').doc(userId).get()
        .then((doc) => {
            if (doc.exists) {
                console.log('user is exist')
                dispatch(_userIsExist())
            }
            else {
                console.log('user is not exist')
                dispatch(_userIsNotExist())
            }
        }).catch((e) => dispatch(_failedCheckUserExist(err)))
}

export const _requestCheckUserExist = () => ({
    type: 'REQUEST_CHECK_USER_EXIST'
})

export const _userIsExist = () => ({
    type: 'USER_IS_EXIST'
})

export const _userIsNotExist = () => ({
    type: 'USER_IS_NOT_EXIST'
})

export const _failedCheckUserExist = (err) => ({
    type: 'FAILED_CHECK_USER_EXIST',
    err
})

export const registerUser = (user, navigation) => async (dispatch) => {
    dispatch(_requestRegisterNewUser())
    Alert.alert('Alert', 'Registering..... !', [], { cancelable: false })
    const userId = await AsyncStorage.getItem('uid')
    const phoneNumber = await AsyncStorage.getItem('phoneNumber')
    const userRef = firestore.collection('user')
    const following = []
    following.push(userId)
    await userRef.doc(userId).set({
        userId: userId,
        userPhoneNumber: phoneNumber,
        userName: user.userName,
        userEmail: user.email,
        userGender: user.gender,
        userDateOfBirth: user.dateOfBirth,
        userHistoryOfBookedHotel: [],
        userHistoryPurchaseEntryTicket: [],
        userFollowers: [],
        userFollowing: following,
        userPost: [],
        userProfilePicture: null,
        userSaveDestinationList: [],
        userSaveHotelList:[],
        userTripPlan: [],
        userPrivateMessage:[],
    }).then(dispatch(_successRegisterNewUser()))
        .catch((err) => {
            Alert.alert('Alert', 'Register failed !', [{ text: 'OK' }], { cancelable: true })
            dispatch(_failedRegisterNewUser(err))
        })
}

export const _requestRegisterNewUser = () => ({
    type: 'REQUEST_REGISTER_NEW_USER'
})

export const _successRegisterNewUser = () => ({
    type: 'SUCCESS_REGISTER_NEW_USER'
})

export const _failedRegisterNewUser = (err) => ({
    type: 'FAILED_REGISTER_NEW_USER',
    err
})

export const getUserData = () => async (dispatch) => {
    dispatch(_requestGetUserData())
    const userId = await AsyncStorage.getItem('uid')
    console.log(userId)
    userRef = firestore.collection('user').doc(userId)

    userRef.onSnapshot(async (doc) => {
        dispatch(_successGetUserData(doc.data()))
    }, (err) => dispatch(_failedGetUserData(err)))
}

export const _requestGetUserData = () => ({
    type: 'REQUEST_GET_USER_DATA'
})

export const _successGetUserData = (payload) => ({
    type: 'SUCCESS_GET_USER_DATA',
    payload
})

export const _failedGetUserData = (err) => ({
    type: 'FAILED_GET_USER_DATA',
    err,
})

export const signOut = () => async (dispatch) => {
    dispatch(_signOut())
    firebase.auth().signOut()
    await AsyncStorage.removeItem('uid')
    await AsyncStorage.removeItem('phoneNumber')
    BackHandler.exitApp()
    // remove asyncstorage item
}

export const _signOut = () => ({
    type: 'SIGN_OUT'
})

export const updateUserName = (userName) => async (dispatch) => {
    dispatch(_requestUpateProfile())
    try {
        const userId = await AsyncStorage.getItem('uid')
        userRef = firestore.collection('user').doc(userId)

        userRef.set({
            userName: userName
        }, { merge: true })
        dispatch(_successUpdateProfile())
    } catch (e) {
        dispatch(_failedUpdateProfile(e))
    }

}

export const updateUserGender = (userGender) => async (dispatch) => {
    dispatch(_requestUpateProfile())
    try {
        const userId = await AsyncStorage.getItem('uid')
        userRef = firestore.collection('user').doc(userId)

        userRef.set({
            userGender: userGender
        }, { merge: true })
        dispatch(_successUpdateProfile())
    } catch (e) {
        dispatch(_failedUpdateProfile(e))
    }
}

export const updateUserEmail = (userEmail) => async (dispatch) => {
    dispatch(_requestUpateProfile())
    try {
        const userId = await AsyncStorage.getItem('uid')
        userRef = firestore.collection('user').doc(userId)

        userRef.set({
            userEmail: userEmail
        }, { merge: true })
        dispatch(_successUpdateProfile())
    } catch (e) {
        dispatch(_failedUpdateProfile(e))
    }
}


export const updateUserDateOfBirth = (userDateOfBirth) => async (dispatch) => {
    dispatch(_requestUpateProfile())
    try {
        const userId = await AsyncStorage.getItem('uid')
        userRef = firestore.collection('user').doc(userId)

        userRef.set({
            userDateOfBirth: userDateOfBirth
        }, { merge: true })
        dispatch(_successUpdateProfile())
    } catch (e) {
        dispatch(_failedUpdateProfile(e))
    }
}


const _requestUpateProfile = () => ({
    type: 'REQUEST_UPDATE_PROFILE'
})

const _successUpdateProfile = () => ({
    type: 'SUCCESS_UPDATE_PROFILE'
})

const _failedUpdateProfile = (err) => ({
    type: 'FAILED_UPDATE_PROFILE',
    err
})

export const changeProfilePicture = (picture) => async (dispatch) => {
    dispatch(_requestUpateProfile())
    try {
        const userId = await AsyncStorage.getItem('uid')
        userRef = firestore.collection('user').doc(userId)
        const imageUrl = await uploadFile(picture)

        await userRef.get().then(async (doc) => {
            previousImageUrl = doc.data().userProfilePicture
            if (previousImageUrl) {
                await deleteFile(previousImageUrl)
            }
        })

        userRef.set({
            userProfilePicture: imageUrl
        }, { merge: true })
        dispatch(_successUpdateProfile())
    }

    catch (e) {
        dispatch(_failedUpdateProfile(e))
    }


}

export const getFollowersList = () => async (dispatch) => {
    dispatch(_requestGetFollowersList())
    const userId = await AsyncStorage.getItem('uid')
    userRef = firestore.collection('user').doc(userId)
    userRef.get().then(async(val) => {
        let userFollowersListId = []
        userFollowersListId=val.data().userFollowers
        const userFollowersList = []
        await Promise.all(userFollowersListId.map(async(followers)=>{
            userFollowersRef = firestore.collection('user').doc(followers)
           await userFollowersRef.get().then((followersData)=>{
                userFollowersList.push(followersData.data())
            })
        }))
        dispatch(_successGetUserFollowersList(userFollowersList))
    }).catch((e)=>dispatch(_failedGetUserFollowersList(e)))

}

const _requestGetFollowersList=()=>({
    type:'REQUEST_GET_FOLLOWERS_LIST'
})

const _successGetUserFollowersList=(payload)=>({
    type:'SUCCESS_GET_USER_FOLLOWERS_LIST',
    payload
})

const _failedGetUserFollowersList=(err)=>({
    type:'FAILED_GET_USER_FOLLOWERS_LIST',
    err
})

export const getFollowingList = () => async (dispatch) => {
    dispatch(_requestGetFollowingList())
    const userId = await AsyncStorage.getItem('uid')
    userRef = firestore.collection('user').doc(userId)
    userRef.get().then(async(val) => {
        let userFollowingId = []
        userFollowingId=val.data().userFollowing
        const userFollowingList = []
        await Promise.all(userFollowingId.map(async(following)=>{
            userFollowingRef = firestore.collection('user').doc(following)
           await userFollowingRef.get().then((followingData)=>{
            userFollowingList.push(followingData.data())
            })
        }))
        dispatch(_successGetUserFollowingList(userFollowingList))
    }).catch((e)=>dispatch(_failedGetUserFollowingList(e)))

}

const _requestGetFollowingList=()=>({
    type:'REQUEST_GET_FOLLOWING_LIST'
})

const _successGetUserFollowingList=(payload)=>({
    type:'SUCCESS_GET_USER_FOLLOWING_LIST',
    payload
})

const _failedGetUserFollowingList=(err)=>({
    type:'FAILED_GET_USER_FOLLOWING_LIST',
    err
})