import React, { Component } from 'react'
import { TouchableHighlight, Dimensions, Image, FlatList, StyleSheet, StatusBar, TouchableOpacity, ScrollView, Modal, RefreshControl } from 'react-native'
import { Container, Content, List, ListItem, Left, Right, Body, Header, Text, View, Button, Item, Thumbnail, Row, Icon, Input, Label, Radio, Toast, Card, CardItem, Separator, Spinner } from 'native-base'
import _ from 'lodash'

const {width} = Dimensions.get('window')
class UserFollowersComponent extends Component{
    constructor(props) {
        super(props)
        this.state = {
            userFollowersList: [],
            isLoading: true,
            searchText: {},
            searchResult: [],
        }
    }
    componentDidMount(){
        this.props.getFollowersList()
    }

    componentWillReceiveProps(nextProps){
        this.setState({
            isLoading:nextProps.isLoading,
            userFollowersList:nextProps.userFollowersList
        })
    }

    renderFollowers(val,key){
        const defaultProfileImage=require('../../assets/user_avatar.png')
        return(
            <List key={key}>
            <ListItem>
                    <TouchableHighlight underlayColor='transparent'
                        onPress={() => this.props.navigation.navigate('UserPostProfile',{passUser:val.userId})}
                    >
                     <View style={{ width: width - 30, height: 35, flexDirection: 'row' }}>
                        <Left maxWidth={50}
                        >
                            {
                                <Thumbnail small source={!_.isEmpty(val.userProfilePicture) ? { uri: val.userProfilePicture } : defaultProfileImage} />
                            }
                        </Left>
                            <Body maxWidth={width-80} style={{alignItems:'flex-start'}}>
                            <Text>
                            {val.userName}
                            </Text>
                            </Body>
                        </View>
                    </TouchableHighlight>
                </ListItem>
            </List>
        )

    }

    render(){
        console.log(this.state.userFollowersList)
        if (!_.isEmpty(this.state.searchText)) {
            const text = this.state.searchText
            this.state.searchResult = _.filter(this.state.userFollowersList, function (o) {
                return o.userName.toLowerCase().includes(text.toLowerCase())
            })
        }
        else {
            this.state.searchResult = this.state.userFollowersList
        }   

        return (
            <Container>
                <Header style={{ backgroundColor: '#00a680' }}>
                    <Left style={{ maxWidth: 70 }}>
                        <Icon style={{ backgroundColor: '#00a680', color: 'white', fontSize: 30 }} name="ios-search" />
                    </Left>
                    <Body marginLeft={-40} marginTop={-10} maxWidth={width} style={{ maxWidth: width, borderBottomWidth: 1, maxHeight: 40, textDecorationLine: 'none', borderBottomColor: 'dimgray' }}>
                        <Item style={{ borderBottomColor: 'transparent' }}>
                            <Input
                                value={this.state.searchText}
                                onChangeText={(text) => this.setState({ searchText: text })}
                                placeholder='Search' style={{ width: width, color: '#E4E2E2', fontSize: 18 }} />
                        </Item>
                    </Body>
                    <Right style={{ maxWidth: 30 }}>
                        <Icon style={{ backgroundColor: '#00a680', color: 'white', fontSize: 35 }} name="ios-people" />
                    </Right>
                </Header>
                {
                    this.state.isLoading ?
                        <Spinner />
                        :
                        _.isEmpty(this.state.userFollowersList) ?
                            <Text style={{ color: 'gray', fontSize: 25, alignSelf: 'center', marginTop: 30 }}>No result</Text>
                            :
                            this.state.searchResult.map((val, key) => {
                                return this.renderFollowers(val, key)
                            })

                }
            </Container>
        )
    }

}

export default UserFollowersComponent