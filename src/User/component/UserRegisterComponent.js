import React, { Component } from 'react'
import { Item, Input, Toast, Form, Container, Content, Header, Body, Title, Button, Text, View, Icon, Footer, ListItem, Label, Spinner } from 'native-base'
import { Alert } from 'react-native'
import DatePicker from 'react-native-datepicker'
import { StyleSheet, Picker, Image } from 'react-native'
import _ from 'lodash'
import MainLoading from '../../modules/MainLoading';
import { CreateUserNavigate } from '../UserNavigation';

class UserRegisterComponent extends Component {
    constructor(props) {
        super(props)
        this.state = {
            isLoading: false,
            userName: "",
            dateOfBirth: {},
            email: {},
            gender: {},
            coverImage: [],
            invalidInput: false,
            selected: 'male',
            currentDate: {},
            isSuccess:false,
            isSuccessRegisterUser:false,
            isButtonClicked:false,
        }
        this.showAlert = this.showAlert.bind(this)
        this.getCurrentDate = this.getCurrentDate.bind(this)
        this.onValueChange1 = this.onValueChange1.bind(this)
        this.onRegisterClick = this.onRegisterClick.bind(this)
        this.onRegisterSuccessClicked=this.onRegisterSuccessClicked.bind(this)
    }

    componentWillMount() {
        this.getCurrentDate()
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            isLoading: nextProps.isLoading,
            isSuccessRegisterUser: nextProps.isSuccessRegisterUser,
        })
    }
    componentDidUpdate(){
        if(this.state.isSuccessRegisterUser){
            this.state.isButtonClicked?
            this.showAlert()
            :
            null
        }
    }

    onRegisterClick() {
        if (!_.isEmpty(this.state.userName) && !_.isEmpty(this.state.email)) {
            this.setState({
                user: {
                    userName: this.state.userName,
                    email: this.state.email,
                    gender: this.state.selected,
                    dateOfBirth: !_.isEmpty(this.state.dateOfBirth)? this.state.dateOfBirth : this.state.currentDate
                },
                isButtonClicked:true,
            },() => this.props.registerUser(this.state.user)
            )
        }
        else {
            Alert.alert('Alert', 'Please enter all data :(', [{ text: 'OK' }]), { cancelable: true }
        }
    }

    getCurrentDate() {
        const date = new Date().getDate().toString()
        const month = 1 + new Date().getMonth()
        const year = new Date().getFullYear().toString()
        const currentMonth = month.toString()
        this.setState({
            currentDate: date + '-' + currentMonth + '-' + year
        })
    }

    onRegisterSuccessClicked() {
        this.props.navigation.navigate('Main') 
       }

    showAlert() {
        this.setState({
            isButtonClicked:false
        })
     Alert.alert('Alert', 'Profile register successfully!', [{ text: 'OK', onPress: () => this.onRegisterSuccessClicked() }], { cancelable: false }) 
    }

    onBlur = () => {
        let reg = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;;
        if (reg.test(this.state.email) === false) {
            this.setState({ invalidInput: true })
        }
        else {
            this.setState({ invalidInput: false })
        }
    }
    onValueChange1(value) {
        this.setState({
            selected: value
        });
    }

    render() {
        const { userName, dateOfBirth, email, gender, coverImage, invalidInput, isLoading, currentDate } = this.state
        return (
            <Container style={styles.container}>
                <Text style={styles.title}>New user register form</Text>
                <Content style={styles.content}>
                    <View >
                        <Label>Username : </Label>
                        <Item>
                            <Icon type='FontAwesome' name='user' />
                            <Input
                                value={userName}
                                onChangeText={(value) => this.setState({ userName: value })}
                            />
                        </Item>
                    </View>
                    <View>
                        <Label>Email : </Label>
                        <Item error={invalidInput}>
                            <Icon type='FontAwesome' name='envelope-o' />
                            <Input
                                keyboardType='email-address'
                                value={email}
                                onChangeText={(value) => this.setState({ email: value, invalidInput: false })}
                                onBlur={() => this.onBlur()}
                            />
                            {!invalidInput ? null : <Icon name='close-circle' />}
                        </Item>
                    </View>
                    <View>
                        <Label>Date of birth : </Label>
                        <Item>
                            <Icon type='FontAwesome' name='calendar-o' />
                            <DatePicker
                                date={_.isEmpty(dateOfBirth) ? currentDate : dateOfBirth}
                                androidMode='spinner'
                                mode="date"
                                placeholder="select date"
                                format="DD-MM-YYYY"
                                minDate="01-01-1950"
                                maxDate={new Date()}
                                confirmBtnText="Confirm"
                                cancelBtnText="Cancel"
                                customStyles={{
                                    dateInput: {
                                        borderColor: 'transparent'
                                    }
                                }}

                                onDateChange={(date) => { this.setState({ dateOfBirth: date }) }} />
                        </Item>
                    </View>
                    <View>
                        <Label>Gender : </Label>
                        <Item>
                            <Icon type='FontAwesome' name='intersex' />
                            <Picker
                                selectedValue={this.state.selected}
                                style={{ height: 50, width: 120 }}
                                onValueChange={(value) => this.setState({ selected: value })}>
                                <Picker.Item label="Male" value="male" />
                                <Picker.Item label="Female" value="female" />
                            </Picker>
                        </Item>
                    </View>
                    <Button full style={styles.buttonValidate} onPress={() => this.onRegisterClick()}>
                        {isLoading ? <Spinner color='green' />
                            : <Text style={{ color: '#00a680', fontSize: 15 }}>Register</Text>}
                    </Button>
                </Content>
            </Container>

        )
    }
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#00a680',
        justifyContent: 'center',
        flex: 1


        // alignItems: 'center',


    },
    title: {
        marginTop: 10,
        fontSize: 30,
        height: 50,
        color: '#ffffff',
        textAlign: 'center',
    },
    content: {
        marginTop: 20,
        marginLeft: 5,
        marginRight: 5,
    },
    item: {
        // flex: 1,
        // flexDirection: 'column',
    },
    buttonValidate: {
        backgroundColor: '#ffffff',
        borderStyle: 'solid',
        borderRadius: 10,
        borderColor: '#00c094',
        borderWidth: 1,
        marginTop: 15,
    }


})

export default UserRegisterComponent