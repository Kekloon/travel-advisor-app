import React, { Component } from 'react'
import { ScrollView, TouchableHighlight, Alert, Dimensions, Image, Picker } from 'react-native'
import { Container, Content, List, ListItem, Left, Right, Body, Header, Text, View, Button, Item, Thumbnail, Row, Icon, Input, Label, Radio, Toast, Card, CardItem,Separator, Spinner } from 'native-base'
import Modal from 'react-native-modal'
import DatePicker from 'react-native-datepicker'
import ImagePicker from 'react-native-image-crop-picker'
import _ from 'lodash'
import { _failedAutoAuthentication } from '../../Authenication/AuthenticationAction';

const { width } = Dimensions.get('window')

class UserProfileComponent extends Component {
    constructor(props) {
        super(props)
        this.state = {
            user: {},
            isUserProfilePicutreModalVisible: false,
            isUserNameModalVisible: false,
            isUserGenderModalVisible: false,
            isUserDateOfBirthModalVisible: false,
            isUserEmailModalVisible: false,

            isUserFollowingModalVisible: false,
            isUserFollowerModalVisible: false,

            isUserHistoryOfbookedHotelModalVisible: false,
            isUserHistoryOfPurchaseEntryTicketModalVisible: false,
            isUserPostModalVisible: false,

            isLoading: false,
            isSuccess:false,
            selectedGender: null,
            userName: null,
            userGender: null,
            userDateOfBirth: null,
            userEmail: null,

        }
        this.handleLogout = this.handleLogout.bind(this)
        // this.handleChangeUserName = this.handleChangeUserName.bind(this)
        // this.handleChangeProfilePicture = this.handleChangeProfilePicture.bind(this)
    }

    componentWillMount() {
        this.setState({
            selectedGender: this.state.user.userGender,
            user: this.props.user
        })
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            isLoading: nextProps.isLoading,
            user: nextProps.user,
            isSuccess:nextProps.isSuccess,
            isUploadingImage:nextProps.isUploadingImage
        })
        this.state.isSuccess?
            this.setState({
                isUserNameModalVisible:false,
                isUserDateOfBirthModalVisible:false,
                isUserEmailModalVisible:false,
                isUserGenderModalVisible:false,
                isSuccess:false,
            })
            :
            null
    }

    handleLogout() {
        this.props.signOut()
        this.props.navigation.navigate('Login')
    }

    showLogoutAlert() {
        Alert.alert('Logout', 'Are you sure to logout?',
            [{ text: 'Logout', onPress: () => this.handleLogout() },
            { text: 'Cancel' }
            ], { cancelable: false })
    }

    handleChangeProfilePicture() {
        ImagePicker.openPicker({
            cropping:true,
            width: 300,
            height: 400,
            cropperCircleOverlay:true,
        }).then(image => {
            this.props.changeProfilePicture(image)
        }).catch(e=>{
                console.log('failed')
        })

    }

    handleChangeUserName() {
        this.setState({
            isUserNameModalVisible: true,
            userName: this.state.user.userName
        })
    }
    userNameModal() {
        return (
            <View>
                <Modal
                    useNativeDriver={false}
                    isVisible={this.state.isUserNameModalVisible}
                    onBackdropPress={() => this.setState({ isUserNameModalVisible: false })}
                >
                    <Card style={{ width: width - 80, alignSelf: 'center', borderRadius: 20, maxHeight: 150 }}>
                        <View style={{ flex: 1, margin: 15 }}>
                            <Text style={{ fontSize: 20 }}>Username : </Text>
                            <Input
                                value={this.state.userName}
                                style={{ backgroundColor: '#F5F5F5', maxHeight: 40, borderRadius: 20, marginBottom: 10, marginTop: 2 }}
                                placeholder='please enter username'
                                onChangeText={(val) => this.setState({ userName: val })}
                            />
                            <Button 
                            onPress={()=>this.props.updateUserName(this.state.userName)}
                            full style={{ height: 30, backgroundColor: '#00a680', borderRadius: 10 }}>
                              {this.state.isLoading?<Spinner/>:<Text>Update</Text>}  
                            </Button>
                        </View>
                    </Card>
                </Modal>
            </View>
        )
    }


    handleChangeUserGender() {
        this.setState({
            isUserGenderModalVisible: true,
            userGender: this.state.user.userGender
        })
    }

    userGenderModal() {
        return (
            <View>
                <Modal
                    useNativeDriver={false}
                    isVisible={this.state.isUserGenderModalVisible}
                    onBackdropPress={() => this.setState({ isUserGenderModalVisible: false })}
                >
                    <Card style={{ width: width - 80, alignSelf: 'center', borderRadius: 20, maxHeight: 150 }}>
                        <View style={{ flex: 1, margin: 15 }}>
                            <Text style={{ fontSize: 20 }}>Gender : </Text>
                            <Picker
                                selectedValue={this.state.userGender}
                                style={{ height: 50, width: 120 }}
                                onValueChange={(value) => this.setState({ userGender: value })}
                            >
                                <Picker.Item label='Male' value='male' />
                                <Picker.Item label='Female' value='female' />
                            </Picker>
                            <Button onPress={()=>this.props.updateUserGender(this.state.userGender)} full style={{ height: 30, backgroundColor: '#00a680', borderRadius: 10 }}>
                            {this.state.isLoading?<Spinner/>:<Text>Update</Text>}  
                            </Button>
                        </View>
                    </Card>
                </Modal>
            </View>
        )

    }

    handleChangeUserDateOfBirth() {
        this.setState({
            isUserDateOfBirthModalVisible: true,
            userDateOfBirth: this.state.user.userDateOfBirth
        })
    }

    userDateOfBirthModal() {
        return (
            <View>
                <Modal
                    useNativeDriver={false}
                    isVisible={this.state.isUserDateOfBirthModalVisible}
                    onBackdropPress={() => this.setState({ isUserDateOfBirthModalVisible: false })}
                >
                    <Card style={{ width: width - 80, alignSelf: 'center', borderRadius: 20, maxHeight: 150 }}>
                        <View style={{ flex: 1, margin: 15 }}>
                            <Text style={{ fontSize: 20 }}>Date of Birth : </Text>
                            <Item>
                                <DatePicker
                                    date={this.state.userDateOfBirth}
                                    androidMode='spinner'
                                    mode="date"
                                    placeholder="select date"
                                    format="DD-MM-YYYY"
                                    minDate="01-01-1950"
                                    maxDate={new Date()}
                                    confirmBtnText="Confirm"
                                    cancelBtnText="Cancel"
                                    customStyles={{
                                        dateInput: {
                                            borderColor: 'transparent'
                                        }
                                    }}

                                    onDateChange={(date) => { this.setState({ userDateOfBirth: date }) }} />
                            </Item>
                            <Button onPress={()=>this.props.updateUserDateOfBirth(this.state.userDateOfBirth)} full style={{ height: 30, backgroundColor: '#00a680', borderRadius: 10 }}>
                            {this.state.isLoading?<Spinner/>:<Text>Update</Text>}  
                            </Button>
                        </View>
                    </Card>
                </Modal>
            </View>
        )
    }

    handleChangeUserEmail() {
        this.setState({
            isUserEmailModalVisible: true,
            userEmail: this.state.user.userEmail
        })
    }

    userEmailModal() {
        return (
            <View>
                <Modal
                    useNativeDriver={false}
                    isVisible={this.state.isUserEmailModalVisible}
                    onBackdropPress={() => this.setState({ isUserEmailModalVisible: false })}
                >
                    <Card style={{ width: width - 80, alignSelf: 'center', borderRadius: 20, maxHeight: 150 }}>
                        <View style={{ flex: 1, margin: 15 }}>
                            <Text style={{ fontSize: 20 }}>Email : </Text>
                            <Item>
                                <Input style={{ backgroundColor: '#F5F5F5', maxHeight: 40, borderRadius: 20, marginBottom: 10, marginTop: 2 }}
                                    keyboardType='email-address'
                                    value={this.state.userEmail}
                                    onChangeText={(value) => this.setState({ userEmail: value })}
                                />
                            </Item>
                            <Button onPress={()=>this.props.updateUserEmail(this.state.userEmail)} full style={{ height: 30, backgroundColor: '#00a680', borderRadius: 10 }}>
                            {this.state.isLoading?<Spinner/>:<Text>Update</Text>}  
                            </Button>
                        </View>
                    </Card>
                </Modal>
            </View>
        )

    }

    handleViewUserPost() {
        this.props.navigation.navigate('UserPostProfile',{passUser:this.state.user.userId})
    }

    handleCheckUserFollower() {
        this.props.navigation.navigate('UserFollowers')
    }

    handleCheckUserFollowing() {
        this.props.navigation.navigate('UserFollowing')
    }

    handleViewSaveList(){
        this.props.navigation.navigate('SaveAndHistoryMenu')
    }

    render() {
        const { user, isLoading } = this.state
        const defaultUserImage = require('../../assets/user_avatar.png')
        return (
            <Container>
                {this.userNameModal()}
                {this.userGenderModal()}
                {this.userDateOfBirthModal()}
                {this.userEmailModal()}
                {
                    _.isEmpty(user)?
                    null
                    :
                <ScrollView>
                    <Header style={{ backgroundColor: '#00a680' }} >
                        <Body>
                            <Text style={{ color: 'white', fontSize: 30 }}>User Profile</Text>
                        </Body>
                    </Header>
                    <List>
                        <ListItem>
                            <TouchableHighlight underlayColor='transparent'
                                onPress={() => this.handleChangeProfilePicture()}
                            >
                                <View style={{ width: width - 30, height: 80, flexDirection: 'row' }}>
                                    <Left style={{ flexDirection: 'row' }}>
                                    {
                                        this.state.isUploadingImage?
                                        <Spinner/>
                                        :<Thumbnail large source={!_.isEmpty(user.userProfilePicture) ? { uri: user.userProfilePicture } : defaultUserImage} />
                                    }
                                        <Text style={{ marginTop: 30, fontSize: 20, marginLeft: 20 }}>Profile Picutre</Text>
                                    </Left>
                                    <Right>
                                        <Icon type='Entypo' name="edit" />
                                    </Right>
                                </View>
                            </TouchableHighlight>
                        </ListItem>
                    </List>
                    <List>
                        <ListItem>
                            <TouchableHighlight underlayColor='transparent'
                                onPress={() => this.handleChangeUserName()}>
                                <View style={{ width: width - 30, height: 30, flexDirection: 'row' }}>
                                    <Left>
                                        <Icon style={{ fontSize: 30 }} type='FontAwesome' name='user-circle' />
                                    </Left>
                                    <Body>
                                        <Text style={{ marginTop: 5, width: 200 }}>{user.userName}</Text>
                                    </Body>
                                    <Right>
                                        <Icon type='Entypo' name="edit" />
                                    </Right>
                                </View>
                            </TouchableHighlight>
                        </ListItem>
                        <ListItem>
                            <TouchableHighlight underlayColor='transparent'
                                onPress={() => this.handleChangeUserGender()}>
                                <View style={{ width: width - 30, height: 30, flexDirection: 'row' }}>
                                    <Left>
                                        <Icon style={{ fontSize: 30 }} type='FontAwesome' name='intersex' />
                                    </Left>
                                    <Body>
                                        <Text style={{ marginTop: 5, width: 200 }}>{user.userGender}</Text>
                                    </Body>
                                    <Right>
                                        <Icon type='Entypo' name="edit" />
                                    </Right>
                                </View>
                            </TouchableHighlight>
                        </ListItem>
                        <ListItem>
                            <TouchableHighlight underlayColor='transparent'
                                onPress={() => this.handleChangeUserDateOfBirth()}>
                                <View style={{ width: width - 30, height: 30, flexDirection: 'row' }}>
                                    <Left>
                                        <Icon style={{ fontSize: 30 }} type='FontAwesome' name='birthday-cake' />
                                    </Left>
                                    <Body>
                                        <Text style={{ marginTop: 5, width: 200 }}>{user.userDateOfBirth}</Text>
                                    </Body>
                                    <Right>
                                        <Icon type='Entypo' name="edit" />
                                    </Right>
                                </View>
                            </TouchableHighlight>
                        </ListItem>
                        <ListItem>
                            <TouchableHighlight underlayColor='transparent'
                                onPress={() => this.handleChangeUserEmail()}>
                                <View style={{ width: width - 30, height: 40, flexDirection: 'row' }}>
                                    <Left>
                                        <Icon style={{ fontSize: 30 }} type='Entypo' name='email' />
                                    </Left>
                                    <Body>
                                        <Text style={{ marginTop: 5, width: 200 }}>{user.userEmail}</Text>
                                    </Body>
                                    <Right>
                                        <Icon type='Entypo' name="edit" />
                                    </Right>
                                </View>
                            </TouchableHighlight>
                        </ListItem>
                        <ListItem itemDivider height={30} style={{backgroundColor:'lightgray'}}>
                        </ListItem>
                        <ListItem>
                            <TouchableHighlight underlayColor='transparent'
                                onPress={() => this.handleViewUserPost()}>
                                <View style={{ width: width - 30, height: 40, flexDirection: 'row' }}>
                                    <Left>
                                        <Icon style={{ fontSize: 30 }} type='FontAwesome' name='list-alt' />
                                    </Left>
                                    <Body>
                                        <Text style={{ marginTop: 5, width: 200 }}>My post</Text>
                                    </Body>
                                    <Right>
                                        <Icon name="arrow-forward" />
                                    </Right>
                                </View>
                            </TouchableHighlight>
                        </ListItem>
                        <ListItem>
                            <TouchableHighlight underlayColor='transparent'
                                onPress={() => this.handleCheckUserFollower()}>
                                <View style={{ width: width - 30, height: 40, flexDirection: 'row' }}>
                                    <Left>
                                        <Icon style={{ fontSize: 30 }} type='FontAwesome' name='group' />
                                    </Left>
                                    <Body>
                                        <Text style={{ marginTop: 5, width: 200 }}>Followers</Text>
                                    </Body>
                                    <Right>
                                        <Icon name="arrow-forward" />
                                    </Right>
                                </View>
                            </TouchableHighlight>
                        </ListItem>
                        <ListItem>
                            <TouchableHighlight underlayColor='transparent'
                                onPress={() => this.handleCheckUserFollowing()}>
                                <View style={{ width: width - 30, height: 40, flexDirection: 'row' }}>
                                    <Left>
                                        <Icon style={{ fontSize: 30 }} type='FontAwesome' name='address-book' />
                                    </Left>
                                    <Body>
                                        <Text style={{ marginTop: 5, width: 200 }}>Following</Text>
                                    </Body>
                                    <Right>
                                        <Icon name="arrow-forward" />
                                    </Right>
                                </View>
                            </TouchableHighlight>
                        </ListItem>
                        <ListItem>
                            <TouchableHighlight underlayColor='transparent'
                                onPress={() => this.handleViewSaveList()}>
                                <View style={{ width: width - 30, height: 40, flexDirection: 'row' }}>
                                    <Left>
                                        <Icon style={{ fontSize: 30 }} type='FontAwesome' name='heart-o' />
                                    </Left>
                                    <Body>
                                        <Text style={{ marginTop: 5, width: 200 }}>Save & History</Text>
                                    </Body>
                                    <Right>
                                        <Icon name="arrow-forward" />
                                    </Right>
                                </View>
                            </TouchableHighlight>
                        </ListItem>
                       <ListItem itemDivider height={30} style={{backgroundColor:'lightgray'}}>
                        </ListItem>
                        <ListItem> 
                            <TouchableHighlight underlayColor='transparent'
                                onPress={() => this.handleLogout()}>
                                <View style={{ width: width - 30, height: 40, flexDirection: 'row' }}>
                                    <Left>
                                        <Icon style={{ fontSize: 30 }} type='FontAwesome' name='sign-out' />
                                    </Left>
                                    <Body>
                                        <Text style={{ marginTop: 5, width: 200 }}>Sign out</Text>
                                    </Body>
                                    <Right>
                                    </Right>
                                </View>
                            </TouchableHighlight>
                        </ListItem>
                    </List>
                </ScrollView>
                }
            </Container>
            
        )
    }

}

export default UserProfileComponent