// @flow
import { AsyncStorage } from "react-native";
// import devTools from "remote-redux-devtools";
import { composeWithDevTools } from 'redux-devtools-extension'
import { createStore, applyMiddleware, compose } from "redux";
import thunk from "redux-thunk";
import { persistStore } from "redux-persist";
import reducer from "../Reducers";

const composeEnhancers = composeWithDevTools({ realtime: true })

export default function configureStore(onCompletion: () => void): any {
	const enhancer = composeEnhancers(
		applyMiddleware(thunk)
	);

	const store = createStore(reducer, enhancer);
	persistStore(store, { storage: AsyncStorage }, onCompletion);

	return store;
}
