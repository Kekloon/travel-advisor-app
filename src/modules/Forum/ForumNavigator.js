import { StackNavigator } from 'react-navigation'
import ForumContainer from './container/ForumContainer'
import PostContainer from './container/PostContainer'
import PostDetailsComponent from './component/PostDetailsComponent'
import CreatePostComponent from './component/CreatePostComponent'

export const ForumNavigator = StackNavigator({
    ForumList: { screen: ForumContainer },
    PostList: { screen: PostContainer },
    PostDetails: { screen: PostDetailsComponent },
    CreatePost: { screen: CreatePostComponent },
}, {
        initialRouteName: 'ForumList',
        headerMode: 'none'
    })