import React, { Component } from 'react'
import { connect } from 'react-redux'
import { TouchableHighlight } from 'react-native'
import { Container, Content, Header, View, Button, Text, Icon, Left } from 'native-base'
import PostListComponent from '../component/PostListComponent'
import HeaderComponent from '../../../common/Header'
import LoadingComponent from '../../LoadingComponent'
import { getForumDetails, getForumPosts } from '../ForumAction'
import _ from 'lodash'

class PostContainer extends Component {
    static navigationOptions = {
        tabBarVisible: false
    }

    constructor(props) {
        super(props)
        this.state = {
            forum: {},
            isLoading: false,
        }
    }

    componentWillMount() {
        this.setState({
            forum: this.props.navigation.state.params.forum
        })
        this.props.getForumPosts(this.props.navigation.state.params.forum.id)
        this.props.getForumDetails(this.props.navigation.state.params.forum.id)
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            forum: nextProps.forum,
            isLoading: nextProps.isLoading
        })
    }

    render() {
        const { forum, isLoading } = this.state
        return (
            isLoading || _.isEmpty(forum) ?
                <LoadingComponent visible={true} />
                :
                <Container>
                    <HeaderComponent title={forum.title} onPress={() => this.props.navigation.navigate('ForumList')} />
                    <PostListComponent navigation={this.props.navigation} forum={forum}/>
                </Container>

        )
    }
}

const mapStateToProps = (state) => ({
    forum: state.forum.getForumDetails.payload,
    isLoading: state.forum.getForumDetails.isFetching,
})
const mapDispatchToProps = (dispatch) => ({
    getForumDetails: (forumId) => {
        dispatch(getForumDetails(forumId))
    },
    getForumPosts: (forumId) => {
        dispatch(getForumPosts(forumId))
    }
})
export default connect(mapStateToProps, mapDispatchToProps)(PostContainer)