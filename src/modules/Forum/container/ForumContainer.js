import React, { Component } from 'react'
import { Dimensions, ScrollView, Animated, StyleSheet, Platform } from 'react-native'
import { Container, Content, View, Header, Title, Icon, Body, Text, Button } from 'native-base'

import SlideShowComponent from '../component/SlideShowComponent'
import ForumListComponent from '../component/ForumListComponent'

const { width, height } = Dimensions.get('window')

const Header_Maximum_Height = width / 16 * 9;
const Header_Minimum_Height = 50;

class ForumContainer extends Component {
    constructor(props) {
        super(props)
        this.state = {
            responsiveWidth: width,
        }
        this.AnimatedHeaderValue = new Animated.Value(0);
    }

    render() {
        const AnimateHeaderBackgroundColor = this.AnimatedHeaderValue.interpolate(
            {
                inputRange: [0, (Header_Maximum_Height - Header_Minimum_Height)],
                outputRange: ['#009688', '#00BCD4'],
                extrapolate: 'clamp'
            });

        const AnimateHeaderHeight = this.AnimatedHeaderValue.interpolate(
            {
                inputRange: [0, (Header_Maximum_Height - Header_Minimum_Height)],
                outputRange: [Header_Maximum_Height, Header_Minimum_Height],
                extrapolate: 'clamp'
            });

        return (
            <Container>
                <View onLayout={(event) => {
                    let { x, y, width, height } = event.nativeEvent.layout
                    this.setState({ responsiveWidth: width })
                }} />
                <ScrollView
                    onScroll={Animated.event([{ nativeEvent: { contentOffset: { y: this.AnimatedHeaderValue } } }])}
                >
                    <ForumListComponent
                        height={Header_Maximum_Height}
                        navigation={this.props.navigation}
                    />
                </ScrollView>

                <Animated.View style={[styles.HeaderStyle, { height: AnimateHeaderHeight, backgroundColor: AnimateHeaderBackgroundColor }]}>
                    <SlideShowComponent height={Header_Maximum_Height} width={this.state.responsiveWidth} />
                </Animated.View>
            </Container>
        )
    }
}

const styles = StyleSheet.create({
    HeaderStyle:
        {
            justifyContent: 'center',
            alignItems: 'center',
            position: 'absolute',
            left: 0,
            right: 0,
            top: (Platform.OS == 'ios') ? 20 : 0,
        },
})


export default ForumContainer