const initialState = {
    getForumList: {
        isFetching: false,
        payload: [],
        errorMessage: null,
    },
    getForumDetails: {
        isFetching: false,
        payload: {},
        errorMessage: null,
    },
    getForumPosts: {
        isFetching: false,
        payload: [],
        errorMessage: null
    },
    createPost: {
        isPosting: false,
        success: false,
        errorMessage: null,
    },
    getComments: {
        isFetching: false,
        payload: [],
        errorMessage: null
    },
    sendComment: {
        isSending: false,
        success: false,
        errorMessage: null
    },
    getAllSlide: {
        isLoading: false,
        payload: [],
        errorMessage: null,
    }
}

export default function (state = initialState, action) {
    switch (action.type) {
        // Get forum list
        case 'REQUEST_GET_FORUM_LIST':
            return Object.assign({}, state, {
                getForumList: {
                    isFetching: true
                }
            })
        case 'SUCCESS_GET_FORUM_LIST':
            return Object.assign({}, state, {
                getForumList: {
                    isFetching: false,
                    payload: action.payload,
                },
            })
        case 'FAILED_GET_FORUM_LIST':
            return Object.assign({}, state, {
                getForumList: {
                    isFetching: false,
                    errorMessage: action.err
                }
            })
        // Get forum details
        case 'REQUEST_GET_FORUM_DETAILS':
            return Object.assign({}, state, {
                getForumDetails: {
                    isFetching: true,
                }
            })
        case 'SUCCESS_GET_FORUM_DETAILS':
            return Object.assign({}, state, {
                getForumDetails: {
                    isFetching: false,
                    payload: action.payload
                }
            })
        case 'FAILED_GET_FORUM_DETAILS':
            return Object.assign({}, state, {
                getForumDetails: {
                    isFetching: false,
                    errorMessage: action.err
                }
            })
        //Get forum's posts
        case 'REQUEST_GET_FORUM_POSTS':
            return Object.assign({}, state, {
                getForumPosts: {
                    isFetching: true,
                    payload: []
                }
            })
        case 'SUCCESS_GET_FORUM_POSTS':
            return Object.assign({}, state, {
                getForumPosts: {
                    isFetching: false,
                    payload: action.payload,
                }
            })
        case 'FAILED_GET_FORUM_POSTS':
            return Object.assign({}, state, {
                getForumPosts: {
                    isFetching: false,
                    errorMessage: action.err
                }
            })
        case 'REQUEST_CREATE_POST':
            return Object.assign({}, state, {
                createPost: {
                    isPosting: true,
                    success: true,
                }
            })
        case 'SUCCESS_CREATE_POST':
            return Object.assign({}, state, {
                createPost: {
                    isPosting: false,
                    success: true,
                }
            })
        case 'FAILED_CREATE_POST':
            return Object.assign({}, state, {
                createPost: {
                    isPosting: false,
                    success: false,
                    errorMessage: action.err
                }
            })
        case 'REQUEST_GET_COMMENTS':
            return Object.assign({}, state, {
                getComments: {
                    isFetching: true
                }
            })
        case 'SUCCESS_GET_COMMENTS':
            return Object.assign({}, state, {
                getComments: {
                    isFetching: false,
                    payload: action.payload
                }
            })
        case 'FAILED_GET_COMMENTS':
            return Object.assign({}, state, {
                getComments: {
                    isFetching: false,
                    errorMessage: action.err
                }
            })
        case 'REQUEST_SEND_COMMENT':
            return Object.assign({}, state, {
                sendComment: {
                    isSending: true,
                    success: false,
                }
            })
        case 'SUCCESS_SEND_COMMENT':
            return Object.assign({}, state, {
                sendComment: {
                    isSending: false,
                    success: true
                }
            })
        case 'FAILED_SEND_COMMENT':
            return Object.assign({}, state, {
                sendComment: {
                    isSending: false,
                    errorMessage: action.err
                }
            })
        case 'REQUEST_GET_ALL_SLIDE':
            return Object.assign({}, state, {
                getAllSlide: {
                    isLoading: true,
                }
            })
        case 'SUCCESS_GET_ALL_SLIDE':
            return Object.assign({}, state, {
                getAllSlide: {
                    isLoading: false,
                    payload: action.payload,
                }
            })
        case 'FAILED_GET_ALL_SLIDE':
            return Object.assign({}, state, {
                getAllSlide: {
                    isLoading: false,
                    errorMessage: action.err
                }
            })
        default:
            return state
    }
} 