import firebase from 'react-native-firebase'
import { AsyncStorage } from 'react-native'
import { uploadFile } from '../../config/helper'
import _ from 'lodash'
const firestore = firebase.firestore()

export const getForumList = () => (dispatch) => {
    dispatch(_requestGetForumList())
    forumRef = firestore.collection('forum')
    try {
        forumRef.onSnapshot(async (val) => {
            let forumList = []
            await Promise.all(val.docs.map((doc) => {
                const forum = {
                    ...doc.data(),
                    id: doc.id,
                    totalPost: doc.data().posts.length
                }
                forumList.push(forum)
            }))
            dispatch(_successGetForumList(forumList))
        })
    } catch (e) {
        dispatch(_failedGetForumList(e))
    }
}
const _requestGetForumList = () => ({
    type: 'REQUEST_GET_FORUM_LIST'
})
const _successGetForumList = (payload) => ({
    type: 'SUCCESS_GET_FORUM_LIST',
    payload
})
const _failedGetForumList = (err) => ({
    type: 'FAILED_GET_FORUM_LIST',
    err
})

export const getForumDetails = (forumId) => (dispatch) => {
    try {
        dispatch(_requestGetForumDetails())
        forumRef = firestore.collection('forum').doc(forumId)
        forumRef.onSnapshot((doc) => {
            const forum = {
                ...doc.data(),
                id: doc.id
            }
            dispatch(_successGetForumDetails(forum))
        })
    } catch (e) {
        dispatch(_failedGetForumDetails(e))
    }
}

const _requestGetForumDetails = () => ({
    type: 'REQUEST_GET_FORUM_DETAILS'
})
const _successGetForumDetails = (payload) => ({
    type: 'SUCCESS_GET_FORUM_DETAILS',
    payload
})
const _failedGetForumDetails = (err) => ({
    type: 'FAILED_GET_FORUM_DETAILS',
    err
})

export const getForumPosts = (forumId) => async (dispatch) => {
    dispatch(_requestGetForumPosts())
    try {
        postRef = firestore.collection('posts')

        postRef.where('forum', '==', forumId).onSnapshot(async (coll) => {
            const postList = []
            const posts = []
            await Promise.all(coll._docs.map(async (doc) => {
                let post = doc.data()

                await post.owner.get().then((user) => {
                    let owner = {
                        ...user.data(),
                        id: user.id
                    }

                    post = {
                        ...post,
                        id: doc.id,
                        owner: owner
                    }
                })
                postList.push(post)
            }))
            dispatch(_successGetForumPosts(postList))
        })
    } catch (e) {
        dispatch(_failedGetForumPosts(e))
    }
}

const _requestGetForumPosts = () => ({
    type: 'REQUEST_GET_FORUM_POSTS'
})
const _successGetForumPosts = (payload) => ({
    type: 'SUCCESS_GET_FORUM_POSTS',
    payload
})
const _failedGetForumPosts = (err) => ({
    type: 'FAILED_GET_FORUM_POSTS',
    err
})

export const createPost = (post) => async (dispatch) => {
    dispatch(_requestGetForumDetails())
    try {
        postRef = firestore.collection('posts')
        let imageUrl
        if (!_.isEmpty(post.imageFile)) {
            imageUrl = await uploadFile(post.imageFile)
        } else {
            imageUrl = ''
        }

        postRef.add({
            forum: post.forum,
            owner: firestore.collection('users').doc(post.owner.uid),
            createDate: new Date(),
            message: post.message,
            imageFile: imageUrl,
            comments: [],
            likes: [],
        })
            .then(async (postRef) => {
                const forumRef = firestore.collection('forum').doc(post.forum)
                let postList
                await forumRef.get().then((val) => {
                    postList = val.data().posts
                })
                postList.push(postRef)
                forumRef.set({
                    posts: postList
                }, { merge: true })
            })
            .then(() => dispatch(_successCreatePost()))

    } catch (e) {
        dispatch(_failedCreatePost())
    }
}
const _requestCreatePost = () => ({
    type: 'REQUEST_CREATE_POST'
})
const _successCreatePost = () => ({
    type: 'SUCCESS_CREATE_POST'
})
const _failedCreatePost = (err) => ({
    type: 'FAILED_CREATE_POST',
    err
})

export const getComments = (postId) => (dispatch) => {
    dispatch(_requestGetComments())
    try {
        const postRef = firestore.collection('posts').doc(postId)
        const userRef = firestore.collection('users')
        postRef.onSnapshot(async (doc) => {
            const commentList = doc.data().comments
            const comments = []
            await Promise.all(commentList.map(async (doc) => {
                let comment = doc
                await userRef.doc(doc.sender.uid).get().then((user) => {
                    comment = {
                        ...comment,
                        sender: user.data()
                    }
                    comments.push(comment)
                })
            }))
            dispatch(_successGetComments(comments))
        })

    } catch (e) {
        dispatch(_failedGetComments(e))
    }
}
const _requestGetComments = () => ({
    type: 'REQUEST_GET_COMMENTS'
})
const _successGetComments = (payload) => ({
    type: 'SUCCESS_GET_COMMENTS',
    payload
})
const _failedGetComments = (err) => ({
    type: 'FAILED_GET_COMMENTS',
    err
})

export const sendComment = (comment) => async (dispatch) => {
    dispatch(_requestSendComment())
    try {
        const userId = await AsyncStorage.getItem('uid')
        const postRef = firestore.collection('posts').doc(comment.post.id)
        const userRef = firestore.collection('users').doc(userId)
        let commentList
        await postRef.get().then((doc) => {
            commentList = doc.data().comments
        })

        let sender
        await userRef.get().then((user) => {
            sender = {
                ...user.data(),
                id: user.id
            }
        })

        const comm = {
            date: new Date(),
            message: comment.message,
            sender: sender
        }
        commentList.push(comm)
        postRef.set({
            comments: commentList
        }, { merge: true })
        dispatch(_successSendComment())
    } catch (e) {
        dispatch(_failedSendComment())
    }

}
const _requestSendComment = () => ({
    type: 'REQUEST_SEND_COMMENT'
})
const _successSendComment = () => ({
    type: 'SUCCESS_SEND_COMMENT'
})
const _failedSendComment = () => ({
    type: 'FAILED_SEND_COMMENT'
})

export const setLikePost = (like, postId) => async (dispatch) => {
    const userId = await AsyncStorage.getItem('uid')
    const postRef = firestore.collection('posts').doc(postId)
    postRef.get().then((post) => {
        const likes = post.data().likes
        if (like) {
            likes.push({
                user: userId
            })
            postRef.set({
                likes: likes
            }, { merge: true })
        } else {
            postRef.set({
                likes: _.pullAllBy(likes, [{ 'user': userId }], 'user'),
            }, { merge: true })
        }
    })
}

export const getAllSlide = () => (dispatch) => {
    dispatch(_requestGetAllSlide())

    try {
        const slideshowRef = firestore.collection('slideshow')
        slideshowRef.onSnapshot(async (val) => {
            const slideList = []
            await Promise.all(val.docs.map((doc) => {
                slideList.push(doc.data())
            }))
            dispatch(_successGetAllSlide(slideList))
        })

    } catch (e) {
        dispatch(_failedGetAllSlide(e))
    }
}
const _requestGetAllSlide = () => ({
    type: 'REQUEST_GET_ALL_SLIDE'
})
const _successGetAllSlide = (payload) => ({
    type: 'SUCCESS_GET_ALL_SLIDE',
    payload
})
const _failedGetAllSlide = (err) => ({
    type: 'FAILED_GET_ALL_SLIDE',
    err
})