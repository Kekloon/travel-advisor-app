import React, { Component } from 'react'
import { connect } from 'react-redux'
import { StyleSheet, ScrollView, Image, Dimensions } from 'react-native'
import { Container, Content, View, List, ListItem, Text, Icon, Thumbnail, Body, Right, Button, Toast } from 'native-base'
import _ from 'lodash'
import { getForumList, getForumPosts } from '../ForumAction'
import LoadingComponent from '../../LoadingComponent'

const { width, height } = Dimensions.get('window')

class ForumListComponent extends Component {
    constructor(props) {
        super(props)
        this.state = {
            isLoading: true,
            forumList: [],
        }
        this.handleForumClicked = this.handleForumClicked.bind(this)
    }

    componentWillMount() {
        this.props.getForumList()
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            isLoading: nextProps.isLoading,
            forumList: nextProps.forumList
        })
    }
    handleForumClicked(val) {
        this.props.navigation.navigate('PostList', { forum: val })
    }

    renderListItem(val, key) {
        return (
            <ListItem key={key} style={styles.listItem} onPress={() => this.handleForumClicked(val)}>
                <Image style={styles.image} source={{ uri: val.imageUrl }} />
                <Body>
                    <Text style={[styles.title, styles.textMargin]} >{val.title}</Text>
                    <View style={[styles.row, styles.textMargin]}>
                        <Icon style={[styles.icon, color.blue]} name='ios-chatbubbles' />
                        <Text style={[styles.description, color.blue]} numberOfLines={1} >{val.description}</Text>
                    </View>
                    <View style={[styles.row, styles.textMargin]}>
                        <Icon style={[styles.icon, color.grey]} name='ios-paper-outline' />
                        <Text note>{val.totalPost} Post</Text>
                    </View>
                </Body>
            </ListItem>
        )
    }

    render() {
        const { forumList, isLoading } = this.state

        const test = {
            imageUrl: 'https://firebasestorage.googleapis.com/v0/b/comicapp-74e4f.appspot.com/o/rc-upload-1523109681758-5?alt=media&token=51efc5fc-4bbf-4cf8-ac95-7def29e15e74',
            title: 'test',
            description: 'test123',
            totalPost: 0
        }
        return (
            <View style={{ marginTop: this.props.height }}>
                <LoadingComponent visible={isLoading} />
                <List style={{ marginTop: 7, marginBottom: 5 }}>
                    {
                        !_.isEmpty(forumList) ?
                            forumList.map((val, key) => {
                                return this.renderListItem(val, key)
                            })
                            :
                            null
                    }
                </List>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    listItem: {
        paddingBottom: 5,
        paddingTop: 5
    },
    image: {
        width: 95,
        height: 75,
        borderRadius: 4
    },
    row: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    textMargin: {
        marginTop: 5,
        marginBottom: 5,
    },
    title: {

    },
    description: {
        fontSize: 16
    },
    icon: {
        fontSize: 14,
        marginLeft: 12,
        marginRight: 5
    }
})

const color = StyleSheet.create({
    grey: {
        color: 'grey'
    },
    blue: {
        color: '#5DADE2'
    },
    red: {
        color: 'red'
    }
})

const mapStateToProps = (state) => ({
    isLoading: state.forum.getForumList.isFetching,
    forumList: state.forum.getForumList.payload
    // forumList: state.forum.forumList
})
const mapDispatchToProps = (dispatch) => ({
    getForumList: () => {
        dispatch(getForumList())
    },
    getForumPosts: (forum) => {
        dispatch(getForumPosts(forum))
    }
})

export default connect(mapStateToProps, mapDispatchToProps)(ForumListComponent)