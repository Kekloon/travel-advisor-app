import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Image, Dimensions } from 'react-native'
import { Container, Content, View, Item, Input, Button, Text, Footer, CardItem, Left, Body, Thumbnail, Right, Icon } from 'native-base'
import HeaderComponent from '../../../common/Header'
import ImagePicker from 'react-native-image-crop-picker'
import ActualImageComponent from '../../../common/ActualImageComponent'
import LoadingComponent from '../../LoadingComponent'
import _ from 'lodash'
import moment from 'moment'
import { createPost } from '../ForumAction'

const { width, height } = Dimensions.get('window')

class CreatePostComponent extends Component {
    static navigationOptions = {
        tabBarVisible: false
    }
    constructor(props) {
        super(props)
        this.state = {
            user: null,
            forum: null,
            imageFile: null,
            message: '',
            isPosting: false,
            isSuccess: false,
        }
    }

    componentWillMount() {
        this.setState({
            forum: this.props.navigation.state.params.forum,
            user: this.props.user
        })
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            isPosting: nextProps.isPosting,
            isSuccess: nextProps.isSuccess,
        })

        if (nextProps.isSuccess) {
            this.setState({
                imageFile: '',
                message: '',
                isPosting: false,
                isSuccess: false,
            })
            this.props.navigation.navigate('PostList', { forum: this.state.forum })
        }
    }

    handleSelectImageFile() {
        this.setState({
            imageFile: ''
        })
        ImagePicker.openPicker({
            cropping: false
        }).then(image => {
            this.setState({
                imageFile: image
            })
        });
    }

    handlePost() {
        const post = {
            imageFile: this.state.imageFile,
            message: this.state.message,
            forum: this.state.forum.id,
            owner: this.state.user
        }
        this.props.createPost(post)
        this.setState({
            isPosting: true
        })
    }

    render() {
        const { user, isPosting, isSuccess } = this.state
        console.log('Is posting: ', isPosting)
        return (
            <Container style={{ backgroundColor: 'white' }}>
                <HeaderComponent title='Create Post' onPress={() => this.props.navigation.navigate('PostList', { forum: this.state.forum })} />
                <Content>
                    <LoadingComponent visible={isPosting} />
                    <CardItem>
                        <Left>
                            <Thumbnail source={{ uri: user.profilePictureUrl }} />
                            <Body>
                                <Text numberOfLines={1} >{user.firstName + ' ' + user.lastName}</Text>
                                <Text note>{moment(new Date()).format('ddd DD MMMM YYYY')}</Text>
                            </Body>
                        </Left>
                        <Right>
                            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                {
                                    !_.isEmpty(this.state.imageFile) ?
                                        <Button bordered rounded danger
                                            style={{ marginLeft: 8 }}
                                            onPress={() => this.setState({ imageFile: '' })}
                                        >
                                            <Icon name='md-close' />
                                        </Button>
                                        :
                                        <Button bordered info rounded
                                            disabled={isPosting}
                                            onPress={() => this.handleSelectImageFile()}
                                        >
                                            <Icon name='md-photos' />
                                        </Button>
                                }
                            </View>
                        </Right>
                    </CardItem>
                    <Input
                        placeholder='Your message...'
                        multiline={true}
                        value={this.state.message}
                        disabled={isPosting}
                        onChangeText={(text) => this.setState({ message: text })} />
                    <View style={{ marginTop: 10, margin: 5 }}>
                        {
                            !_.isEmpty(this.state.imageFile) ?
                                <ActualImageComponent imageUrl={this.state.imageFile.path} />
                                :
                                null
                        }
                    </View>
                </Content>
                <View>
                    <Button block info rounded
                        style={{ alignItems: 'center', marginBottom: 3 }}
                        disabled={isPosting}
                        onPress={() => this.handlePost()}
                    >
                        <Text style={{ fontWeight: '600' }}>
                            {isPosting ? 'Posting' : 'Post'}
                        </Text>
                    </Button>
                </View>
            </Container>
        )
    }
}

const mapStateToProps = (state) => ({
    user: state.user.user,
    isPosting: state.forum.createPost.isPosting,
    isSuccess: state.forum.createPost.success
})
const mapDispatchToProps = (dispatch) => ({
    createPost: (post) => {
        dispatch(createPost(post))
    }
})

export default connect(mapStateToProps, mapDispatchToProps)(CreatePostComponent)