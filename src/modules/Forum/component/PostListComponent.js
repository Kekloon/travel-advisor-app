import React, { Component } from 'react'
import { connect } from 'react-redux'
import { StyleSheet, Image, Dimensions, ScrollView, FlatList } from 'react-native'
import { Container, Content, View, Text, List, ListItem, Icon, Card, CardItem, Left, Thumbnail, Body, Right, Button, Fab, Toast } from 'native-base'
import AutoHeightImage from 'react-native-auto-height-image'
import ActualImageComponent from '../../../common/ActualImageComponent'
import LoadingComponent from '../../LoadingComponent'
import _ from 'lodash'
import moment from 'moment'
import { getForumPosts, setLikePost } from '../ForumAction'

const { width, height } = Dimensions.get('window')

class PostListComponent extends Component {
    constructor(props) {
        super(props)
        this.state = {
            user: null,
            forum: null,
            posts: [],
            isLoading: true,
        }
        this.onCommentBtnPress = this.onCommentBtnPress.bind(this)
        this.handleBtnAdd = this.handleBtnAdd.bind(this)
        this.checkIsLiked = this.checkIsLiked.bind(this)
    }

    componentWillMount() {
        this.setState({
            forum: this.props.forum,
            user: this.props.user
        })
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            posts: _.orderBy(nextProps.posts, ['createDate'], ['desc']),
            isLoading: nextProps.isLoading,
            forum: nextProps.forum,
        })
    }

    onCommentBtnPress(post) {
        this.props.navigation.navigate('PostDetails', { post: post, forum: this.state.forum })
    }

    handleBtnAdd() {
        this.props.navigation.navigate('CreatePost', { forum: this.state.forum })
    }

    checkIsLiked(likesArray) {
        const userId = this.state.user.uid
        if (_.find(likesArray, { user: userId })) {
            return true
        } else {
            return false
        }
    }

    handleBtnLike(post) {
        const index = _.findIndex(this.state.posts, { id: post.id });
        const posts = this.state.posts
        if (_.find(post.likes, { user: this.state.user.uid })) {
            const userId = this.state.user.uid
            const updateLikes = _.pullAllBy(post.likes, [{ 'user': userId }], 'user');
            const updatePost = {
                ...post,
                likes: updateLikes
            }
            posts.splice(index, 1, updatePost)
            this.setState({
                posts,
            })
            this.props.setLikePost(false, post.id)
        } else {
            const updateLikes = post.likes
            updateLikes.push({ user: this.state.user.uid })
            const updatePost = {
                ...post,
                likes: updateLikes
            }
            posts.splice(index, 1, updatePost)
            this.setState({
                posts,
            })
            this.props.setLikePost(true, post.id)
        }
    }

    renderPost(val, key) {
        const liked = this.checkIsLiked(val.likes)
        return (
            <Card key={key} >
                <View>
                    <CardItem header style={{ width: width }}>
                        <Left>
                            <Thumbnail source={{ uri: val.owner.profilePictureUrl }} />
                            <Body>
                                <Text numberOfLines={1} >{val.owner.firstName + ' ' + val.owner.lastName}</Text>
                                <Text note>{moment(val.createDate).format('DD MMMM')}</Text>
                            </Body>
                        </Left>
                    </CardItem>
                    <CardItem >
                        <Text>{val.message}</Text>
                    </CardItem>
                    {
                        !_.isEmpty(val.imageFile) ?
                            <CardItem>
                                <ActualImageComponent imageUrl={val.imageFile} />
                            </CardItem>
                            : null
                    }
                    <CardItem footer style={{ top: 10 }}>
                        <Left>
                            <Button transparent iconLeft
                                onPress={() => this.handleBtnLike(val)}
                            >
                                <Icon style={{ color: liked ? '#5DADE2' : 'grey' }} active name="thumbs-up" />
                                <Text style={{ color: liked ? '#5DADE2' : 'grey' }} >{val.likes.length} Likes</Text>
                            </Button>
                        </Left>
                        <Right>
                            <Button transparent iconLeft
                                onPress={() => this.onCommentBtnPress(val)}
                            >
                                <Icon style={{ color: '#5DADE2' }} active name="chatbubbles" />
                                <Text style={{ color: '#5DADE2' }} >{val.comments.length} Comments</Text>
                            </Button>
                        </Right>
                    </CardItem>
                </View>
            </ Card>
        )
    }

    render() {
        const { forum, posts } = this.state
        return (
            <Container>
                <LoadingComponent visible={this.state.isLoading} />
                <Fab
                    direction="down"
                    containerStyle={{}}
                    style={{ backgroundColor: '#58D68D', zIndex: 1 }}
                    position="topRight"
                    onPress={() => this.handleBtnAdd()}
                >
                    <Icon name="md-add-circle" />
                </Fab>
                <ScrollView>
                    <View>
                        {
                            _.isEmpty(forum) ? null :
                                <Image style={coverStyle.image} source={{ uri: forum.imageUrl }} />
                        }
                        <View style={coverStyle.bottomContainer}>
                            <Icon style={coverStyle.icon} name='ios-paper-outline' />
                            <Text style={coverStyle.text} > Post: {_.isEmpty(forum) ? 0 : forum.posts.length} </Text>
                        </View>
                    </View>
                    <View>
                        {
                            !_.isEmpty(posts) ?
                                posts.map((val, key) => {
                                    return this.renderPost(val, key)
                                })
                                : null
                        }
                    </View>
                </ScrollView>
            </Container>
        )
    }
}

const coverStyle = StyleSheet.create({
    image: {
        height: height / 3.5
    },
    bottomContainer: {
        backgroundColor: 'rgba(0, 0, 0, 0.3)',
        flexDirection: 'row',
        alignItems: 'center',
        paddingTop: 8,
        paddingBottom: 8,
        position: 'absolute',
        bottom: 0,
        width: width
    },
    text: {
        color: 'white',
        marginLeft: 3
    },
    icon: {
        color: 'white',
        marginLeft: 10,
        fontSize: 22
    }
})

const mapStateTopProps = (state) => ({
    posts: state.forum.getForumPosts.payload,
    isLoading: state.forum.getForumPosts.isFetching,
    forum: state.forum.getForumDetails.payload,
    user: state.user.user
})
const mapDispathToProps = (dispatch) => ({
    getForumPosts: (forumId) => {
        dispatch(getForumPosts(forumId))
    },
    setLikePost: (like, postId) => {
        dispatch(setLikePost(like, postId))
    }
})

export default connect(mapStateTopProps, mapDispathToProps)(PostListComponent)