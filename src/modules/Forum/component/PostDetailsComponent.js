import React, { Component } from 'react'
import { connect } from 'react-redux'
import { StyleSheet, ScrollView, Image, TouchableHighlight, FlatList, Dimensions } from 'react-native'
import { Container, Content, View, Text, Button, Icon, List, ListItem, Card, CardItem, Item, Input, Body, Left, Right, Thumbnail, Toast } from 'native-base'
import ActualImageComponent from '../../../common/ActualImageComponent'
import LoadingComponent from '../../LoadingComponent'
import Header from '../../../common/Header'
import moment from 'moment'
import _ from 'lodash'
import { sendComment, getComments } from '../ForumAction'

const { width, height } = Dimensions.get('window')

class PostDetailsComponent extends Component {
    static navigationOptions = {
        tabBarVisible: false
    }

    constructor(props) {
        super(props)
        this.state = {
            forum: null,
            post: null,
            commentMessage: '',
            isLoading: false,
            data: [],
            isSending: false,
        }
        this.scrollToBottom = this.scrollToBottom.bind(this)
        this.renderFlatListHeader = this.renderFlatListHeader.bind(this)
    }

    componentWillMount() {
        this.setState({
            forum: this.props.navigation.state.params.forum,
            post: this.props.navigation.state.params.post,
        })

        this.props.getComments(this.props.navigation.state.params.post.id)
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            data: nextProps.commentList,
            isLoading: nextProps.isFetching,
            isSending: nextProps.isSending
        })
    }

    renderComment(item) {
        const localUserImage = require('../../../assets/user_avatar.png')
        return (
            <ListItem avatar>
                <Left>
                    {
                        item.sender.profilePictureUrl ?
                        <Thumbnail source={{ uri: item.sender.profilePictureUrl }} />
                        :
                        <Thumbnail source={localUserImage} />
                    }
                </Left>
                <Body>
                    <Text>{item.sender.firstName + ' ' + item.sender.lastName}</Text>
                    <Text style={{ fontSize: 12 }} note >{moment(item.date).format("LT DD-MM-YYYY")}</Text>
                    <Text style={{ paddingTop: 8 }}>{item.message}</Text>
                </Body>
            </ListItem>
        )
    }

    scrollToBottom(animated = true) {
        this.refs.flatlist.scrollToEnd({ animated })
    }

    handleBtnSend() {
        if (!_.isEmpty(this.state.commentMessage)) {
            const comment = {
                message: this.state.commentMessage,
                post: this.state.post
            }
            this.props.sendComment(comment)
            this.setState({
                commentMessage: ''
            })
            this.scrollToBottom()
        }
    }

    renderFlatListHeader() {
        const { post } = this.state
        return (
            <Card style={{ flex: 0 }} >
                <View>
                    <CardItem style={{ width: width }}>
                        <Left>
                            {
                                _.isEmpty(post.owner.profilePictureUrl) ? null :
                                    <Thumbnail source={{ uri: post.owner.profilePictureUrl }} />
                            }
                            <Body>
                                <Text>{post.owner.firstName + ' ' + post.owner.lastName}</Text>
                                <Text note>{moment(post.createDate).format('DD MMMM')}</Text>
                            </Body>
                        </Left>
                    </CardItem>

                    <CardItem>
                        <Text>{post.message}</Text>
                    </CardItem>
                    {
                        _.isEmpty(post.imageFile) ? null :
                            <CardItem cardBody style={{ margin: 5 }}>
                                <ActualImageComponent
                                    imageUrl={post.imageFile}
                                />
                            </CardItem>
                    }

                    <CardItem footer>
                        <Text note style={{ paddingLeft: 5, paddingRight: 5 }}>{post.likes.length} Likes</Text>
                    </CardItem>
                </View>
            </Card>
        )
    }

    render() {
        console.log(this.state.data)
        return (
            <Container>
                <Header
                    title='Post Details'
                    onPress={() => this.props.navigation.navigate('PostList', { forum: this.state.forum })}
                />
                {
                    this.state.isLoading ?
                        <LoadingComponent visible={true} />
                        :
                        <FlatList
                            ref='flatlist'
                            keyExtractor={(item, index) => index}
                            data={_.sortBy(this.state.data, 'date')}
                            renderItem={({ item }) => this.renderComment(item)}
                            ListHeaderComponent={this.renderFlatListHeader()}
                        />
                }
                {
                    this.state.isLoading ? null :
                        <View style={{ position: 'relative', bottom: 1 }}>
                            <Item regular >
                                <Input placeholder='Your comment...'
                                    value={this.state.commentMessage}
                                    disabled={this.state.isSending}
                                    onChangeText={(val) => this.setState({ commentMessage: val })}
                                />
                                <Button info style={{ alignSelf: 'center', borderRadius: 7, marginRight: 1 }}
                                    disabled={this.state.isSending}
                                    onPress={() => this.handleBtnSend()}>
                                    <Text>{this.state.isSending ? 'Sending' : 'Send'}</Text>
                                </Button>
                            </Item>
                        </View>
                }
            </Container >
        )
    }
}

const mapStateToProps = (state) => ({
    commentList: state.forum.getComments.payload,
    isFetching: state.forum.getComments.isFetching,
    isSending: state.forum.sendComment.isSending,
    isSuccess: state.forum.sendComment.success
})
const mapDispatchToProps = (dispatch) => ({
    sendComment: (comment) => {
        dispatch(sendComment(comment))
    },
    getComments: (postId) => {
        dispatch(getComments(postId))
    }
})

export default connect(mapStateToProps, mapDispatchToProps)(PostDetailsComponent)