import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { StyleSheet, TouchableHighlight, Image, Dimensions } from 'react-native'
import { View, Text, Toast } from 'native-base'
import Swiper from 'react-native-swiper'
import _ from 'lodash'

const { width, height } = Dimensions.get('window')

class SlideShowComponent extends Component {
    constructor(props) {
        super(props)
        this.state = {
            slides: [],
        }
    }

    componentWillMount() {
        this.setState({
            slides: this.props.slides,
        })
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            slides: nextProps.slides
        })
    }

    render() {
        const { slides } = this.state
        return (
            <View style={{ height: this.props.height, width: this.props.width }}>
                <Swiper
                    autoplay={true}
                    style={styles.wrapper}
                // showsButtons={true}
                >
                    {
                        _.isEmpty(slides) ? null :
                            slides.map((val, key) => {
                                return (
                                    <TouchableHighlight key={key} style={styles.slide1}>
                                        <View>
                                            <Image style={{ height: this.props.height, width: this.props.width }}
                                                source={{ uri: val.imageUrl }}
                                            />
                                            <View style={{ position: 'absolute', padding: 5, flex: 1, width: this.props.width, backgroundColor: 'rgba(0,0,0,0.5)', bottom: 0 }}>
                                                <Text style={{ color: 'white' }}>{val.title}</Text>
                                            </View>
                                        </View>
                                    </TouchableHighlight>
                                )
                            })
                    }
                </Swiper>
            </View>
        )
    }
}

SlideShowComponent.propTypes = {
    height: PropTypes.number
}

const styles = StyleSheet.create({
    wrapper: {
        zIndex: 1
    },
    slide1: {
        flex: 1,
        zIndex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#9DD6EB',
    },
    slide2: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#97CAE5',
        zIndex: 1
    },
    slide3: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#92BBD9',
        zIndex: 1
    },
    text: {
        color: '#fff',
        fontSize: 30,
        fontWeight: 'bold',
        zIndex: 1
    }
})

const mapStateToProps = (state) => ({
    slides: state.forum.getAllSlide.payload
})
const mapDispatchToProps = (dispatch) => ({

})

export default connect(mapStateToProps, mapDispatchToProps)(SlideShowComponent)