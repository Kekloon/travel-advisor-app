import React, { Component } from 'react'
import { Image, Text } from 'react-native'
import { View, Spinner } from 'native-base'

class MainLoading extends Component {
    render() {
        return (
            <View style={{
                flex: 1,
                justifyContent: 'center',
                alignItems: 'center',
                backgroundColor: '#D4E6F1'
            }}>
                <Image resizeMode={'center'} style={{width:300,height:300}} source={require('../assets/loading.png')} />
                <Spinner color='green' />
            </View>
        )
    }
}
export default MainLoading