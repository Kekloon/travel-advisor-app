const initialState = {
    isLoading: false,
    errorMessage: null,
    isUserExist: false,
    isSuccess: false,
    shouldUpdate: true,
    user: null,
    topUpPackage: {
        isFetching: false,
        payload: [],
        errorMessage: null
    },
    topUp: {
        isFetching: true,
        success: false,
        errorMessage: null
    },
    userCredits: {
        isLoading: false,
        payload: {},
        errorMessage: null
    },
    updateProfile: {
        isLoading: false,
        success: false,
        errorMessage: null
    }
}

export default function (state = initialState, action) {
    switch (action.type) {
        case 'CHECKKING_IS_USER_EXIST':
            return Object.assign({}, state, {
                isLoading: true,
                shouldUpdate: true
            })
        case 'USER_EXIST':
            return Object.assign({}, state, {
                isLoading: false,
                isUserExist: true,
                shouldUpdate: false
            })
        case 'USER_NOT_EXIST':
            return Object.assign({}, state, {
                isLoading: false,
                isUserExist: false,
                shouldUpdate: false
            })
        case 'FAILED_CHECKING_IS_NEW_USER':
            return Object.assign({}, state, {
                isLoading: false,
                errorMessage: action.err
            })
        case 'REQUEST_REGISTER_USER':
            return Object.assign({}, state, {
                isLoading: true,
                isSuccess: false
            })
        case 'SUCCESS_REGISTER_USER':
            return Object.assign({}, state, {
                isLoading: false,
                isSuccess: true
            })
        case 'FAILED_REGISTER_USER':
            return Object.assign({}, state, {
                isLoading: false,
                isSuccess: false,
                errorMessage: action.err
            })
        case 'REQUEST_GET_USER_DATA':
            return Object.assign({}, state, {
                isLoading: true,

            })
        case 'SUCCESS_GET_USER_DATA':
            return Object.assign({}, state, {
                isLoading: false,
                user: action.payload,
                errorMessage: action.err
            })
        case 'FAILED_GET_USER_DATA':
            return Object.assign({}, state, {
                isLoading: false,
                errorMessage: action.err
            })
        case 'REQUEST_GET_TOP_UP_PACKAGE':
            return Object.assign({}, state, {
                topUpPackage: {
                    isFetching: true
                }
            })
        case 'SUCCESS_GET_TOP_UP_PACKAGE':
            return Object.assign({}, state, {
                topUpPackage: {
                    isFetching: false,
                    payload: action.payload
                }
            })
        case 'FAILED_GET_TOP_UP_PACKAGE':
            return Object.assign({}, state, {
                topUpPackage: {
                    isFetching: false,
                    errorMessage: action.err
                }
            })
        case 'REQUEST_TOPUP':
            return Object.assign({}, state, {
                topUp: {
                    isFetching: true,
                    success: false
                }
            })
        case 'SUCCESS_TOPUP':
            return Object.assign({}, state, {
                topUp: {
                    isFetching: false,
                    success: true
                }
            })
        case 'FAILED_TOPUP':
            return Object.assign({}, state, {
                topUp: {
                    isFetching: false,
                    success: false
                }
            })
        case 'REQUEST_GET_USER_CREDITS':
            return Object.assign({}, state, {
                userCredits: {
                    isLoading: true
                }
            })
        case 'SUCCESS_GET_USER_CREDITS':
            return Object.assign({}, state, {
                userCredits: {
                    isLoading: false,
                    payload: action.payload
                }
            })
        case 'FAILED_GET_USER_CREDITS':
            return Object.assign({}, state, {
                userCredits: {
                    isLoading: false,
                    errorMessage: action.err
                }
            })
        case 'REQUEST_UPDATE_PROFILE':
            return Object.assign({}, state, {
                updateProfile: {
                    isLoading: true,
                    success: false
                }
            })
        case 'SUCCESS_UPDATE_PROFILE':
            return Object.assign({}, state, {
                updateProfile: {
                    isLoading: false,
                    success: true
                }
            })
        case 'FAILED_UPDATE_PROFILE':
            return Object.assign({}, state, {
                updateProfile: {
                    isLoading: false,
                    errorMessage: action.err
                }
            })
        default:
            return state
    }
}