import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Text, View } from 'native-base'
import _ from 'lodash'

import UserProfile from '../component/UserProfile'
import { signOut } from '../../Authentication/AuthenticationActions'
import { topUpCredits, changeProfilePicture, updateProfile } from '../UserAction'
import Loading from '../../LoadingComponent'

class UserProfileContainer extends Component {
    constructor(props) {
        super(props)
        this.state = {
            user: null,
            topUpPackageList: null,
            userCredits: null,
            isTopUpLoading: false
        }
    }

    componentWillMount() {
        this.setState({
            user: this.props.user,
            userCredits: this.props.userCredits,
            topUpPackageList: this.props.topUpPackageList
        })
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            user: nextProps.user,
            topUpPackageList: nextProps.topUpPackageList,
            userCredits: nextProps.userCredits,
            isTopUpLoading: nextProps.isTopUpLoading
        })
    }

    render() {
        return (
            this.state.user && this.state.userCredits ?
                <UserProfile
                    user={this.state.user}
                    userCredits={this.state.userCredits}
                    signOut={this.props.signOut}
                    navigation={this.props.navigation}
                    topUpPackageList={this.state.topUpPackageList}
                    isTopUpSuccess={this.props.isTopUpSuccess}
                    isTopUpLoading={this.state.isTopUpLoading}
                    topUpCredits={this.props.topUpCredits}
                    changeProfilePicture={this.props.changeProfilePicture}
                    updateProfile={this.props.updateProfile}
                    isUpdatingProfile={this.props.isUpdatingProfile}
                    isUpdateProfileSuccess={this.props.isUpdateProfileSuccess}
                />
                :
                <Loading />
        )
    }
}

const mapStateToProps = (state) => ({
    user: state.user.user,
    userCredits: state.user.userCredits.payload,
    topUpPackageList: state.user.topUpPackage.payload,
    isTopUpLoading: state.user.topUp.isFetching,
    isTopUpSuccess: state.user.topUp.success,
    isUpdatingProfile: state.user.updateProfile.isLoading,
    isUpdateProfileSuccess: state.user.updateProfile.success
})

const mapDispatchToProps = (dispatch) => ({
    signOut: () => {
        dispatch(signOut())
    },
    topUpCredits: (topUpPackage) => {
        dispatch(topUpCredits(topUpPackage))
    },
    changeProfilePicture: (picture) => {
        dispatch(changeProfilePicture(picture))
    },
    updateProfile: ({ input, id }) => {
        dispatch(updateProfile({ input, id }))
    }

})

export default connect(mapStateToProps, mapDispatchToProps)(UserProfileContainer)