import { connect } from 'react-redux'
import Register from '../component/Register'
import { registerUser } from '../UserAction'

const mapStateToProps = (state) => ({
    isLoading: state.user.isLoading,
    isSuccess: state.user.isSuccess
})

const mapDispatchToProps = (dispatch, props) => ({
    registerUser: (user) => {
        dispatch(registerUser(user, props.navigation))
    }
})

const RegisterContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(Register)

export default RegisterContainer