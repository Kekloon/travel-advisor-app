import React, { Component } from 'react'
import { Container, Content, Item, Input, ListItem, Text, Radio, Header, Button, View, Label, Toast } from 'native-base'
import { Alert } from 'react-native'
import DatePicker from 'react-native-datepicker'
import _ from 'lodash'

class RegisterComponent extends Component {
    constructor(props) {
        super(props)
        this.state = {
            isLoading: false,
            isSuccess: false,
            isRegisterBtnClicked: false,
            firstName: "",
            lastName: "",
            dateOfBirth: "",
            email: "",
            gender: "male",
            user: {}
        }
        this.setGender = this.setGender.bind(this)
        this.showAlert = this.showAlert.bind(this)
        this.getCurrentDate = this.getCurrentDate.bind(this)
        this.onRegisterClicked = this.onRegisterClicked.bind(this)
        this.onRegisterSuccessClicked = this.onRegisterSuccessClicked.bind(this)
    }

    componentWillMount() {
        this.getCurrentDate()
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            isLoading: nextProps.isLoading,
            isSuccess: nextProps.isSuccess
        })
    }

    setGender(gender) {
        gender === "male" ?
            this.setState({ gender: "male" })
            :
            this.setState({ gender: "female" })
    }

    getCurrentDate() {
        const date = new Date().getDate().toString()
        const month = new Date().getMonth().toString()
        const year = new Date().getFullYear().toString()

        this.setState({
            dateOfBirth: date + "-" + month + "-" + year
        })
    }

    showAlert() {
        const title = "Register"
        const { isLoading, isSuccess, isRegisterBtnClicked } = this.state

        isRegisterBtnClicked ?
            isLoading ? Alert.alert(title, 'Registering profile.....', [], { cancelable: false }) :
                isSuccess ? Alert.alert(title, 'Profile register successfully!', [{ text: 'OK', onPress: () => this.onRegisterSuccessClicked() }], { cancelable: false }) :
                    Alert.alert(title, 'Failed register profile!!', [{ text: 'OK', onPress: () => this.setState({ isRegisterBtnClicked: false }) }], { cancelable: false })
            : null
    }

    onRegisterSuccessClicked() {
        this.setState({
            isRegisterBtnClicked: false
        })
        this.props.navigation.navigate('Main')
    }

    onRegisterClicked() {
        if (!_.isEmpty(this.state.firstName) && !_.isEmpty(this.state.lastName) && !_.isEmpty(this.state.email)) {
            this.setState({
                user: {
                    firstName: this.state.firstName,
                    lastName: this.state.lastName,
                    dateOfBirth: this.state.dateOfBirth,
                    email: this.state.email,
                    gender: this.state.gender
                },
                isLoading: true,
                isRegisterBtnClicked: true
            }, () => this.props.registerUser(this.state.user))
        } else {
            Toast.show({ text: 'Please fill in the black!', type: 'danger' })
        }
    }

    componentDidUpdate() {
        this.showAlert()
    }

    render() {
        const { firstName, lastName, dateOfBirth, email, gender } = this.state
        return (
            <Container style={{ margin: 15 }}>
                <View style={{ alignItems: 'center', margin: 15 }}>
                    <Text style={{ fontSize: 27 }} > Sign Up </Text>
                </View>
                <Content style={{ margin: 10 }}>
                    <Item stackedLabel>
                        <Label>First Name</Label>
                        <Input
                            value={firstName}
                            onChangeText={(value) => this.setState({ firstName: value })} />
                    </Item>
                    <Item stackedLabel>
                        <Label>Last Name</Label>
                        <Input
                            value={lastName}
                            onChangeText={(value) => this.setState({ lastName: value })} />
                    </Item>
                    <Item stackedLabel>
                        <Label>Email</Label>
                        <Input
                            keyboardType='email-address'
                            value={email}
                            onChangeText={(value) => this.setState({ email: value })} />
                    </Item>
                    <View style={{ margin: 10 }} />
                    <Item style={{ alignItems: 'center' }}>
                        <Label>Birthdate : </Label>
                        <DatePicker
                            style={{
                                flex: 1,
                            }}
                            date={_.isEmpty(dateOfBirth) ? new Date() : dateOfBirth }
                            androidMode='spinner'
                            mode="date"
                            placeholder="select date"
                            format="DD-MM-YYYY"
                            minDate="01-01-1950"
                            maxDate={new Date()}
                            confirmBtnText="Confirm"
                            cancelBtnText="Cancel"
                            customStyles={{
                                dateInput: {
                                    paddingLeft: 0,
                                    paddingRight: 0,
                                    borderColor: 'transparent'
                                }
                            }}

                            onDateChange={(date) => { this.setState({ dateOfBirth: date }) }} />
                    </Item>
                    <View style={{ margin: 20 }} />
                    <Item style={{ paddingBottom: 8 }}>
                        <Label>Gender : </Label>
                        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                            <View style={{ flexDirection: 'row', alignItems: 'center', flex: 1 }}>
                                <Text style={{ marginLeft: 70 }}>Male  </Text>
                                <Radio selected={gender === "male" ? true : false} onPress={() => this.setGender("male")} />
                            </View>
                            <View style={{ flexDirection: 'row', alignItems: 'center', flex: 1 }}>
                                <Text>Female  </Text>
                                <Radio selected={gender === "female" ? true : false} onPress={() => this.setGender("female")} />
                            </View>
                        </View>
                    </Item>
                </Content>
                <Button block
                    style={{
                        marginBottom: 10
                    }}
                    onPress={() => this.onRegisterClicked()}>
                    <Text>Sign Up</Text>
                </Button>
            </Container>
        )
    }
}

export default RegisterComponent