import React, { Component } from 'react'
import { ScrollView, TouchableHighlight, Alert, Dimensions, Image } from 'react-native'
import { Container, Content, List, ListItem, Left, Right, Body, Text, View, Button, Item, Thumbnail, Row, Icon, Input, Label, Radio, Toast } from 'native-base'
import Modal from 'react-native-modal'
import DatePicker from 'react-native-datepicker'
import LoadingComponent from '../../LoadingComponent'
import ImagePicker from 'react-native-image-crop-picker'
import { reduxForm } from 'redux-form'
import _ from 'lodash'
import { email } from '../../../config/validations'

const { width, height } = Dimensions.get('window')

class UserProfile extends Component {
    constructor(props) {
        super(props)
        this.state = {
            user: {},
            topUpPackageList: [],
            isTextModalVisible: false,
            isGenderModalVisible: false,
            isTopUpModalVisible: false,
            textModalLabel: '',
            textModalPlaceholder: '',
            selectedtopUpPackageList: null,
            selectedGender: null,
            isTopUpLoading: false,
            input: null,
            isEditing: false,
            invalidEmail: false,
            editId: null,
            isUpdatingProfile: false,
            responsiveWidth: width,
        }
        this.handleLogout = this.handleLogout.bind(this)
    }

    componentWillMount() {
        this.setState({
            selectedGender: this.state.user.gender,
            topUpPackageList: this.props.topUpPackageList
        })
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            user: nextProps.user,
            topUpPackageList: nextProps.topUpPackageList,
            isTopUpLoading: nextProps.isTopUpLoading,
            isUpdatingProfile: nextProps.isUpdatingProfile
        })
        if (nextProps.isTopUpSuccess) {
            this.setState({
                isTopUpModalVisible: false
            })
        }

        if (nextProps.isUpdateProfileSuccess) {
            this.setState({
                isGenderModalVisible: false,
                isTextModalVisible: false
            })
        }
    }
    handleLogout() {
        this.props.signOut()
        this.props.navigation.navigate('Login')
    }

    showLogoutAlert() {
        Alert.alert('Logout', 'Are you sure to logout?', [
            { text: 'Logout', onPress: () => this.handleLogout() },
            { text: 'Cancel' }
        ], { cancelable: false })
    }

    renderListItem({ label, thumbnailUrl, text, mode, visibleModal, id }) {
        return (
            <ListItem
                style={{ backgroundColor: 'rgba(253,254,254, 0.5)', marginLeft: 0 }}
                onPress={() => mode === 'date' ?
                    this.refs.datepicker.onPressDate()
                    :
                    mode === 'topup' ?
                        this.setState({ isTopUpModalVisible: true })
                        :
                        this.setState({ [visibleModal]: true, textModalLabel: label, textModalPlaceholder: text, editId: id })}
            >
                {
                    mode === 'topup' ?
                        <View style={{ flexDirection: 'row', alignItems: 'center', marginLeft: 13 }}>
                            <Icon style={{ color: '#EC7063' }} name='ios-card' />
                            <Text style={{ marginLeft: 3, color: '#EC7063' }}>{label}</Text>
                        </View>
                        :
                        <Text style={{ marginLeft: 13 }}>{label}</Text>
                }
                <Body />
                {
                    thumbnailUrl ?
                        <Thumbnail square size={80} source={{ uri: thumbnailUrl }} />
                        :
                        <Text note>{text}</Text>
                }
                <Icon ios='ios-arrow-forward-outline' android="ios-arrow-forward" style={{ color: mode === 'topup' ? '#EC7063' : 'grey', marginLeft: 20 }} />
            </ListItem>
        )
    }

    handleUpdateProfile() {
        if (this.state.isEditing) {
            if (!_.isEmpty(this.state.input)) {
                if (this.state.textModalLabel === 'Email') {
                    if (email(this.state.input)) {
                        this.setState({
                            invalidEmail: true
                        })
                    } else {
                        this.props.updateProfile({ input: this.state.input, id: this.state.editId })
                    }
                } else {
                    this.props.updateProfile({ input: this.state.input, id: this.state.editId })
                }
            }
        } else {
            this.setState({
                isEditing: true
            })
        }
    }

    // Modal 
    textModal() {
        const { isTextModalVisible, textModalLabel, textModalPlaceholder, isUpdatingProfile } = this.state
        return (
            <View>
                <Modal
                    useNativeDriver={true}
                    isVisible={this.state.isTextModalVisible}
                    onBackdropPress={() => this.setState({ isTextModalVisible: false, isEditing: false })}
                >
                    <View style={{ backgroundColor: 'white', borderRadius: 10 }}>
                        <LoadingComponent visible={isUpdatingProfile} />
                        <View style={{ margin: 20 }}>

                            {
                                _.isEmpty(this.state.input) && this.state.isEditing || this.state.invalidEmail ?
                                    <Item stackedLabel style={{ height: 70 }} error>
                                        <Label>{textModalLabel}</Label>
                                        {
                                            textModalLabel === 'Email' ?
                                                <Input placeholder={textModalPlaceholder} keyboardType='email-address' onChangeText={(text) => this.setState({ input: text, isEditing: true, invalidEmail: false })} />
                                                :
                                                <Input placeholder={textModalPlaceholder} onChangeText={(text) => this.setState({ input: text, isEditing: true })} />
                                        }
                                    </Item>
                                    :
                                    <Item stackedLabel style={{ height: 70 }}>
                                        <Label>{textModalLabel}</Label>
                                        {
                                            textModalLabel === 'Email' ?
                                                <Input placeholder={textModalPlaceholder} keyboardType='email-address' onChangeText={(text) => this.setState({ input: text, isEditing: true, invalidEmail: false })} />
                                                :
                                                <Input placeholder={textModalPlaceholder} onChangeText={(text) => this.setState({ input: text, isEditing: true })} />
                                        }
                                    </Item>
                            }
                            {_.isEmpty(this.state.input) && this.state.isEditing ? <Text style={{ color: 'red' }}>Please input</Text> : null}
                            {this.state.invalidEmail ? <Text style={{ color: 'red' }}>Invalid email format</Text> : null}
                        </View>
                        <View style={{ flexDirection: 'row', alignSelf: 'flex-end', marginBottom: 15, marginRight: 10 }}>
                            <Button small light style={{ margin: 8 }}
                                disabled={isUpdatingProfile}
                                onPress={() => this.handleUpdateProfile()}
                            >
                                <Text>Update</Text>
                            </Button>
                        </View>
                    </View>
                </Modal>
            </View >
        )
    }

    genderModal() {
        const { user } = this.props
        const { input, isUpdatingProfile } = this.state
        return (
            <View>
                <Modal
                    isVisible={this.state.isGenderModalVisible}
                    onBackdropPress={()=> isUpdatingProfile ? null : this.setState({ isGenderModalVisible: false, isEditing: false })}
                >
                    <View style={{ backgroundColor: 'white', borderRadius: 10 }}>
                        <LoadingComponent visible={isUpdatingProfile} />
                        <ListItem onPress={() => this.setState({ input: 'male', isEditing: true })}>
                            <Text>Male</Text>
                            <Body />
                            <Right>
                                <Radio selected={input === 'male' ? true : false} onPress={() => this.setState({ input: 'male', isEditing: true })} />
                            </Right>
                        </ListItem>
                        <ListItem onPress={() => this.setState({ input: 'female', isEditing: true })}>
                            <Text>Female</Text>
                            <Body />
                            <Right>
                                <Radio selected={input === 'female' ? true : false} onPress={() => this.setState({ input: 'female', isEditing: true })} />
                            </Right>
                        </ListItem>
                        <View style={{ flexDirection: 'row', alignSelf: 'flex-end', margin: 10 }}>
                            <Button small light style={{ margin: 8 }}
                                disabled={isUpdatingProfile}
                                onPress={() => this.handleUpdateProfile()}
                            >
                                <Text>Update</Text>
                            </Button>
                        </View>
                    </View>
                </Modal>
            </View >
        )
    }

    dateModal() {
        const { user } = this.props
        return (
            <View style={{ display: 'none' }}>
                <DatePicker
                    ref='datepicker'
                    date={user.dateOfBirth}
                    mode="date"
                    placeholder="Select date"
                    format="DD-MM-YYYY"
                    minDate="01-01-1950"
                    maxDate={new Date()}
                    confirmBtnText="Confirm"
                    cancelBtnText="Cancel"
                    showIcon={false}
                    customStyles={{
                        display: 'none'
                    }}
                    onDateChange={(date) => this.props.updateProfile({ input: date, id: 'dateOfBirth' })}
                />
            </View>
        )
    }

    topUpModal() {
        return (
            <View>
                <Modal
                    animationIn='slideInDown'
                    animationOut='slideOutUp'
                    animationOutTiming={500}
                    useNativeDriver={true}
                    isVisible={this.state.isTopUpModalVisible}
                    onBackdropPress={() => this.setState({ isTopUpModalVisible: false })}
                >
                    <View style={{ backgroundColor: 'white', margin: 15, borderRadius: 10 }}>
                        <View style={{ alignItems: 'center', marginTop: 5 }}>
                            <Text style={{ fontSize: 24, fontWeight: 'bold', color: '#EC7063' }}>Credits Shop</Text>
                            <Text note>Unlock comic's episodes</Text>
                        </View>
                        <ScrollView>
                            {
                                this.state.topUpPackageList.map((val, key) => {
                                    return (
                                        <TouchableHighlight key={key}
                                            onPress={() => this.setState({ selectedtopUpPackageList: val })}
                                            underlayColor='white'
                                            style={{
                                                flex: 1,
                                                backgroundColor: 'white',
                                                borderRadius: 4,
                                                borderWidth: 1.5,
                                                borderColor: this.state.selectedtopUpPackageList === val ? '#EC7063' : 'grey',
                                                margin: 10,
                                                marginLeft: 50,
                                                marginRight: 50
                                            }} >
                                            <View style={{ alignItems: 'center', margin: 10 }}>
                                                <Text>RM {val.price}</Text>
                                                <Text note>{val.credits} credits</Text>
                                            </View>
                                        </TouchableHighlight>
                                    )
                                })
                            }

                        </ScrollView>
                        <LoadingComponent visible={this.state.isTopUpLoading} />
                        <Button block
                            disabled={this.state.isTopUpLoading}
                            style={{ borderRadius: 10, margin: 5, backgroundColor: '#EC7063' }}
                            onPress={() => this.state.selectedtopUpPackageList ?
                                Alert.alert(
                                    'Top up confirmation',
                                    'Selected package:\nRM ' + Number.parseFloat(this.state.selectedtopUpPackageList.price).toFixed(2) + '\n' + 'Credits: ' + this.state.selectedtopUpPackageList.credits,
                                    [
                                        { text: 'Pay', onPress: () => this.handlePay() },
                                        { text: 'Back' }
                                    ]
                                )
                                :
                                Toast.show({ text: 'No package was selected! ', type: 'warning' })}
                        >
                            <Text>Buy</Text>
                        </Button>
                    </View>
                </Modal>
            </View>
        )
    }

    handlePay() {
        this.props.topUpCredits(this.state.selectedtopUpPackageList)
    }

    handleChangeProfilePicture() {
        ImagePicker.openPicker({
            width: 300,
            height: 400,
            cropping: true
        }).then(image => {
            this.props.changeProfilePicture(image)
        });
    }

    render() {
        const { user, userCredits } = this.props
        const localUserImage = require('../../../assets/user_avatar.png')
        return (
            <Container style={{ backgroundColor: '#EBF5FB' }}>
                {this.textModal()}
                {this.dateModal()}
                {this.genderModal()}
                {this.topUpModal()}
                <View onLayout={(event) => {
                    let { x, y, width, height } = event.nativeEvent.layout
                    this.setState({ responsiveWidth: width })
                }} />
                <Content>
                    <List>
                        <View style={{ height: height / 3.5, flexDirection: 'row', alignItems: 'center', backgroundColor: 'transparent' }}>
                            <View style={{ alignItems: 'center', width: this.state.responsiveWidth }}>
                                <TouchableHighlight underlayColor='transparent'
                                    onPress={() => this.handleChangeProfilePicture()}>
                                    <Thumbnail large source={user.profilePictureUrl ? { uri: user.profilePictureUrl } : localUserImage} />
                                </TouchableHighlight>
                                <View style={{ marginTop: 10 }}>
                                    <Text>{user.firstName + " " + user.lastName}</Text>
                                    <Text style={{ color: 'grey' }}> Credits: <Text style={{ color: 'green' }}>{userCredits.balance}</Text></Text>
                                </View>
                            </View>
                        </View>

                        <Button small iconRight transparent style={{ position: 'absolute', right: 1, marginTop: 8 }}
                            onPress={() => this.showLogoutAlert()}>
                            <Text style={{ paddingRight: 5 }}>Logout</Text>
                            <Icon name='exit' />
                        </Button>
                        {this.renderListItem({ label: 'First name', id: 'firstName', text: user.firstName, visibleModal: 'isTextModalVisible' })}
                        {this.renderListItem({ label: 'Last name', id: 'lastName', text: user.lastName, visibleModal: 'isTextModalVisible' })}
                        <View style={{ height: 10 }} />
                        {this.renderListItem({ label: 'Email', id: 'email', text: user.email, visibleModal: 'isTextModalVisible' })}
                        {this.renderListItem({ label: 'Birthdate', id: 'dateOfBirth', text: user.dateOfBirth, mode: 'date' })}
                        {this.renderListItem({ label: 'Gender', id: 'gender', text: user.gender, visibleModal: 'isGenderModalVisible' })}
                        <View style={{ height: 10 }} />
                        {this.renderListItem({ label: 'Top up', mode: 'topup' })}
                    </List>
                </Content>
            </Container>
        )
    }
}

export default UserProfile