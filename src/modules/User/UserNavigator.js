import React, { Component } from 'react'
import { connect } from 'react-redux'
import { StackNavigator } from 'react-navigation'
import { Text } from 'native-base'
//Path
import Register from './container/RegisterContainer'
import UserProfile from './container/UserProfileContainer'
import MainLoading from '../MainLoading'
import { LoginNavigator } from '../Authentication/LoginNavigator'
import { checkIsUserExist } from './UserAction'
import Main from '../../Route'

export const createUserNavigator = (isUserExist) => {
    return StackNavigator({
        Register: { screen: Register },
        Main: { screen: Main },
        Login: { screen: LoginNavigator }
    }, {
            initialRouteName: isUserExist ? 'Main' : 'Register',
            headerMode: 'none'
        })
}

class UserNavigatorComponent extends Component {
    constructor(props) {
        super(props)
        this.state = {
            isLoading: false,
            shouldUpdate: true
        }
    }

    componentWillMount() {
        this.props.checkIsUserExist()
        this.setState({
            isLoading: true
        })
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            isLoading: nextProps.isLoading,
            shouldUpdate: nextProps.shouldUpdate
        })
    }

    shouldComponentUpdate() {
        return this.state.shouldUpdate
    }

    render() {
        const UserNavigator = createUserNavigator(this.props.isUserExist)
        return (
            this.props.isLoading ?
                <MainLoading />
                :
                <UserNavigator />
        )
    }
}

const mapStateToProps = (state) => ({
    isUserExist: state.user.isUserExist,
    isLoading: state.user.isLoading,
    shouldUpdate: state.user.shouldUpdate
})

const mapDispatchToProps = (dispatch) => ({
    checkIsUserExist: () => {
        dispatch(checkIsUserExist())
    }
})

export default connect(mapStateToProps, mapDispatchToProps)(UserNavigatorComponent)