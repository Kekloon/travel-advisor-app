import firebase from 'react-native-firebase'
import { AsyncStorage, Alert } from 'react-native'
import { Toast } from 'native-base'
import { uploadFile, deleteFile } from '../../config/helper'
import _ from 'lodash'
const db = firebase.firestore()

// -------------------------------------- Checking is user exist ------------------------------------- //
export const checkIsUserExist = () => async (dispatch) => {
    dispatch(_checkingIsUserExist())
    const userId = await AsyncStorage.getItem('uid')
    db.collection('users').doc(userId).get()
        .then((doc) => {
            if (doc.exists) {
                dispatch(_userExist())
            } else {
                dispatch(_userNotExist())
            }
        }).catch((e) => dispatch(_failedCheckingIsUserExist()))
}

export const _checkingIsUserExist = () => ({
    type: 'CHECKKING_IS_USER_EXIST'
})

export const _userExist = () => ({
    type: 'USER_EXIST'
})

export const _userNotExist = () => ({
    type: 'USER_NOT_EXIST'
})

export const _failedCheckingIsUserExist = (err) => ({
    type: 'FAILED_CHECKING_IS_NEW_USER',
    err
})


// -------------------------------------- Register user for the first time login ------------------------------------- //
export const registerUser = (user, navigation) => async (dispatch) => {
    dispatch(_requestRegisterUser())
    const userRef = db.collection("users")
    const userId = await AsyncStorage.getItem('uid')
    const phoneNum = await AsyncStorage.getItem('phoneNum')

    const creditsRef = db.collection('credits')
    const subscribeRef = db.collection('subscribe')

    let subscribeId = ''
    await subscribeRef.add({
        comics: []
    }).then((doc) => {
        subscribeId = doc.id
    })

    creditsRef.add({
        bonus: 100,
        paymentHistory: [],
        topUpHistory: []
    })
        .then((docRef) => {
            userRef.doc(userId).set({
                uid: userId,
                phoneNum: phoneNum,
                firstName: user.firstName,
                lastName: user.lastName,
                dateOfBirth: user.dateOfBirth,
                email: user.email,
                gender: user.gender,
                creditRef: creditsRef.doc(docRef.id),
                subscription: subscribeId
            }).then(dispatch(_successRegisterUser()))
                .catch((e) => dispatch(_failedRegisterUser()))
        })
        .catch(function (error) {
        });
}

export const _requestRegisterUser = () => ({
    type: 'REQUEST_REGISTER_USER'
})

export const _successRegisterUser = () => ({
    type: 'SUCCESS_REGISTER_USER'
})

export const _failedRegisterUser = (err) => ({
    type: 'FAILED_REGISTER_USER',
    err
})

export const getUserCredits = () => async (dispatch) => {
    let credits = {
        bonus: 0,
        paid: 0,
        topUp: 0,
        balance: 0
    }

    try {
        dispatch(_requestGetUserCredits())
        const userId = await AsyncStorage.getItem('uid')
        userRef = db.collection('users').doc(userId)

        let creditRef = null
        await userRef.get().then((user) => {
            if (user.data().creditRef) {
                creditRef = user.data().creditRef
            }
        })

        creditRef.onSnapshot(async (doc) => {

            credits = {
                bonus: doc.data().bonus ? doc.data().bonus : 0,
                paid: 0,
                topUp: 0,
                balance: 0,
                paymentHistory: doc.data().paymentHistory
            }
            let balance = 0

            const topUpHistory = doc.data().topUpHistory

            if (topUpHistory) {
                await Promise.all(topUpHistory.map(async (topUpRef) => {
                    await topUpRef.topUpPackage.get().then((doc) => {
                        credits.topUp += Number.parseFloat(doc.data().credits)
                    })
                }))
            }

            if (doc.data().paymentHistory) {
                await Promise.all(doc.data().paymentHistory.map(async (payment) => {
                    credits.paid += Number.parseFloat(payment.price)
                }))
            }

            credits.balance = credits.bonus + credits.topUp - credits.paid
            dispatch(_successGetUserCredits(credits))
        })

    } catch (e) {
        dispatch(_failedGetUserCredits(e))
    }
}

const _requestGetUserCredits = () => ({
    type: 'REQUEST_GET_USER_CREDITS'
})
const _successGetUserCredits = (payload) => ({
    type: 'SUCCESS_GET_USER_CREDITS',
    payload
})
const _failedGetUserCredits = (err) => ({
    type: 'FAILED_GET_USER_CREDITS',
    err
})
// -------------------------------------- Get user profile data ------------------------------------- //
export const getUserData = () => async (dispatch) => {
    dispatch(_requestGetUserData())
    const userId = await AsyncStorage.getItem('uid')
    userRef = db.collection('users').doc(userId)

    userRef.onSnapshot(async (doc) => {
        dispatch(_successGetUserData(doc.data()))
    }, (err) => dispatch(_failedGetUserData(err)))
}

export const _requestGetUserData = () => ({
    type: 'REQUEST_GET_USER_DATA'
})

export const _successGetUserData = (payload) => ({
    type: 'SUCCESS_GET_USER_DATA',
    payload
})

export const _failedGetUserData = (err) => ({
    type: 'FAILED_GET_USER_DATA',
    err
})

// -------------------------------------- Get top up package ------------------------------------- //
export const getTopUpPackageList = () => (dispatch) => {
    const topUpPackageRef = db.collection('topupPackage')
    try {
        dispatch(_requestGetTopUpPackage)
        topUpPackageRef.where('isActive', '==', true).onSnapshot(async (val) => {
            let topUpPackageList = []
            await Promise.all(val._docs.map((doc) => {
                topUpPackageList.push(doc.data())
            }))
            dispatch(_successGetTopUpPackage(_.sortBy(topUpPackageList, ['price'])))
            dispatch(_failedTopUp())
        })

    } catch (e) {
        dispatch(_failedGetTopUpPackage(e))
    }
}

const _requestGetTopUpPackage = () => ({
    type: 'REQUEST_GET_TOP_UP_PACKAGE'
})
const _successGetTopUpPackage = (payload) => ({
    type: 'SUCCESS_GET_TOP_UP_PACKAGE',
    payload
})
const _failedGetTopUpPackage = (err) => ({
    type: 'FAILED_GET_TOP_UP_PACKAGE',
    err
})

export const topUpCredits = (topUpPackage) => async (dispatch) => {
    dispatch(_requestTopUp())
    try {
        const userId = await AsyncStorage.getItem('uid')
        userRef = db.collection('users').doc(userId)

        userRef.get().then(async (user) => {
            const creditRef = user.data().creditRef
            let topUpHistory = []

            await creditRef.get().then((doc) => {
                if (doc.data().topUpHistory) {
                    topUpHistory = doc.data().topUpHistory
                }
            }).then(async () => {
                let order = {
                    topUpPackage: topUpPackage.id,
                    date: new Date()
                }
                topUpHistory.push(order)
                creditRef.set({
                    topUpHistory: topUpHistory
                }, { merge: true }).then(() => dispatch(_successTopUp()))
                    .then(() => Toast.show({ text: 'Top up success!', type: 'success' }))
            })


        })
    } catch (e) {
        dispatch(_failedTopUp(e))
    }
}

const _requestTopUp = () => ({
    type: 'REQUEST_TOPUP'
})
const _successTopUp = () => ({
    type: 'SUCCESS_TOPUP'
})
const _failedTopUp = (err) => ({
    type: 'FAILED_TOPUP',
    err
})

export const changeProfilePicture = (picture) => async (dispatch) => {
    dispatch(_requestUpdateProfile())
    Alert.alert('', 'Changing your profile picture please wait...', [], { cancelable: true })
    try {
        const userId = await AsyncStorage.getItem('uid')
        userRef = db.collection('users').doc(userId)
        const imageUrl = await uploadFile(picture)

        await userRef.get().then(async (doc) => {
            previousImageUrl = doc.data().profilePictureUrl
            if (previousImageUrl) {
                await deleteFile(previousImageUrl)
            }
        })

        userRef.set({
            profilePictureUrl: imageUrl
        }, { merge: true })
        dispatch(_successUpdateProfile())
        Alert.alert('', 'Change profile picture successfully!', [
            { text: 'OK' },
        ], { cancelable: true })
    } catch (e) {
        dispatch(_failedUpdateProfile(e))
        Alert.alert('Error', 'Failed to change your profile picture!', [
            { text: 'OK' },
        ], { cancelable: true })
    }
}

const _deleteUserProfilePicture = () => ({
})

export const updateProfile = ({ input, id }) => async (dispatch) => {
    try {
        dispatch(_requestUpdateProfile())
        const userId = await AsyncStorage.getItem('uid')
        userRef = db.collection('users').doc(userId)

        userRef.set({
            [id]: input
        }, { merge: true })
        dispatch(_successUpdateProfile())
        Toast.show({ text: 'Update successfull!', type: 'success' })
    } catch (e) {
        dispatch(_failedUpdateProfile())
    }
}

const _requestUpdateProfile = () => ({
    type: 'REQUEST_UPDATE_PROFILE'
})
const _successUpdateProfile = () => ({
    type: 'SUCCESS_UPDATE_PROFILE'
})
const _failedUpdateProfile = (err) => ({
    type: 'FAILED_UPDATE_PROFILE',
    err
})