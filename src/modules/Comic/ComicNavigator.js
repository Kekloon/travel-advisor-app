import { StackNavigator } from 'react-navigation'
//Component Path
import Category from './container/CategoryContainer'
import FilteredList from './component/FilteredList'
import ComicDetails from './component/ComicDetails'
import ReadComic from './component/ReadComic'
import Bookshelf from './container/BookshelfContainer'
import HomeContainer from './container/HomeContainer'

export const CategoryNavigator = StackNavigator({
    Category: { screen: Category },
    FilteredList: { screen: FilteredList },
    ComicDetails: { screen: ComicDetails },
    ReadComic: { screen: ReadComic },
},{
    initialRouteName: 'Category',
    headerMode: 'none'
})

export const BookshelfNavigator = StackNavigator({
    Bookshelf: { screen: Bookshelf },
    ComicDetails: { screen: ComicDetails },
    ReadComic: { screen: ReadComic }
},{
    initialRouteName: 'Bookshelf',
    headerMode: 'none'
})

export const HomeNavigator = StackNavigator({
    Home: { screen: HomeContainer },
    ComicDetails: { screen: ComicDetails },
    ReadComic: { screen: ReadComic },
}, {
    initialRouteName: 'Home',
    headerMode: 'none'
})