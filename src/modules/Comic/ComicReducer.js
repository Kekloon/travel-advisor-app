const initialState = {
    isLoading: false,
    errorMessage: "",
    categoryList: [],
    categoryFilter: null,
    comics: [],
    toViewComic: null,
    loadingToViewComic: false,
    toViewChapter: null,
    chapterData: null,
    isSubscribed: false,
    subscribedList: [],
    getSubscribed: {
        isLoading: false,
        payload: []
    },
    comments: {
        isLoading: false,
        comments: [],
        errorMessage: null,
        isSending: false
    },
    tabBarVisible: true,
    payChapter: {
        isLoading: false,
        success: false,
        errorMessage: null
    },
    routeHistory: null,
    getAllComics: {
        isFetching: false,
        payload: [],
        errorMessage: null
    },
    getChapterPage: {
        isLoading: false,
        payload: [],
        errorMessage: null
    }
}

export default function (state = initialState, action) {
    switch (action.type) {
        case 'REQUEST_GET_CATEGORIES':
            return Object.assign({}, state, {
                isLoading: true
            })
        case 'SUCCESS_GET_CATEGORIES':
            return Object.assign({}, state, {
                isLoading: false,
                categoryList: action.payload
            })
        case 'FAILED_GET_CATEGORIES':
            return Object.assign({}, state, {
                isLoading: false,
                errorMessage: action.err
            })
        case 'SET_CATEGORY_FILTER':
            return Object.assign({}, state, {
                categoryFilter: action.category
            })
        case 'REQUEST_GET_COMIC':
            return Object.assign({}, state, {
                isLoading: true
            })
        case 'SUCCESS_GET_COMIC':
            return Object.assign({}, state, {
                comics: action.payload,
                isLoading: false
            })
        case 'REQUEST_GET_SUBSCRIBED_COMICS':
            return Object.assign({}, state, {
                getSubscribed: {
                    isLoading: true
                }
            })
        case 'SUCCESS_GET_SUBSCRIBED_COMICS':
            return Object.assign({}, state, {
                getSubscribed: {
                    isLoading: false,
                    payload: action.payload
                }
            })
        case 'FAILED_GET_COMIC':
            return Object.assign({}, state, {
                isLoading: false,
                errorMessage: action.err
            })
        case 'SET_TO_VIEW_COMIC':
            return Object.assign({}, state, {
                toViewComic: action.comic,
                loadingToViewComic: false,

            })
        case 'REQUEST_SET_TO_VIEW_COMIC':
            return Object.assign({}, state, {
                loadingToViewComic: true,
                routeHistory: action.routeHistory
            })
        case 'REQUEST_GET_CHAPTER':
            return Object.assign({}, state, {
                getChapterPage: {
                    isLoading: true
                }
            })
        case 'SUCCESS_GET_CHAPTER':
            return Object.assign({}, state, {
                getChapterPage: {
                    isLoading: false,
                    payload: action.chapterData
                }
            })
        case 'FAILED_GET_CHAPTER':
            return Object.assign({}, state, {
                getChapterPage: {
                    isLoading: false,
                    errorMessage: action.err
                }
            })
        case 'SET_TO_VIEW_CHAPTER':
            return Object.assign({}, state, {
                toViewChapter: action.chapter
            })
        case 'CHECK_IS_SUBSCRIBED':
            return Object.assign({}, state, {
                isLoading: true
            })
        case 'SUCCESS_CHECK_IS_SUBSCRIBED':
            return Object.assign({}, state, {
                isLoading: false,
                isSubscribed: action.payload
            })
        case 'FAILED_CHECK_IS_SUBSCRIBED':
            return Object.assign({}, state, {
                isLoading: false,
                errorMessage: action.err
            })
        case 'REQUEST_GET_COMMENTS':
            return Object.assign({}, state, {
                comments: {
                    isLoading: true
                }
            })
        case 'SUCCESS_GET_COMMENTS':
            return Object.assign({}, state, {
                comments: {
                    isLoading: false,
                    comments: action.payload
                }
            })
        case 'FAILED_GET_COMMENTS':
            return Object.assign({}, state, {
                comments: {
                    isLoading: false,
                    errorMessage: action.err
                }
            })
        case 'REQUEST_SEND_COMMENT':
            return Object.assign({}, state, {
                comments: {
                    isSending: true
                }
            })
        case 'SUCCESS_SEND_COMMENT':
            return Object.assign({}, state, {
                comments: {
                    isSending: false
                }
            })
        case 'FAILED_SEND_COMMENT':
            return Object.assign({}, state, {
                comments: {
                    isSending: false,
                    errorMessage: action.err
                }
            })
        case 'SET_TAB_BAR_VISIBLE':
            return Object.assign({}, state, {
                tabBarVisible: action.visible
            })
        case 'REQUEST_PAY_CHAPTER':
            return Object.assign({}, state, {
                payChapter: {
                    isLoading: true,
                    success: false
                }
            })
        case 'SUCCESS_PAY_CHAPTER':
            return Object.assign({}, state, {
                payChapter: {
                    isLoading: false,
                    success: true
                }
            })
        case 'FAILED_PAY_CHAPTER':
            return Object.assign({}, state, {
                payChapter: {
                    isLoading: false,
                    errorMessage: action.err
                }
            })
        case 'REQUEST_GET_ALL_COMICS':
            return Object.assign({}, state, {
                getAllComics: {
                    isFetching: true,
                }
            })
        case 'SUCCESS_GET_ALL_COMICS':
            return Object.assign({}, state, {
                getAllComics: {
                    isFetching: false,
                    payload: action.payload
                }
            })
        case 'FAILED_GET_ALL_COMICS':
            return Object.assign({}, state, {
                getAllComics: {
                    isFetching: false,
                    errorMessage: action.err
                }
            })
        default:
            return state
    }
}