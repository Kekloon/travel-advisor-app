import React, { Component } from 'react'
import { connect } from 'react-redux'
import { TouchableHighlight, Dimensions, Image, FlatList, StyleSheet, StatusBar } from 'react-native'
import { Container, Text, Icon, View, Toast, Card, ListItem, Body } from 'native-base'
import { setToViewComic } from '../ComicAction'

const { width, height } = Dimensions.get('window')
class Home extends Component {
    constructor(props) {
        super(props)
        this.state = {
            comics: []
        }
        this.handleComicClicked = this.handleComicClicked.bind(this)
    }

    componentWillMount() {
        this.setState({
            comics: this.props.comics
        })
        StatusBar.setHidden(false)
    }
    componentWillReceiveProps(nextProps) {
        this.setState({
            comics: nextProps.comics
        })
    }

    getHeight() {
        // ratio 11:17
        return ((width / 3.23) / 11) * 17
    }

    handleComicClicked(comic) {
        this.props.setToViewComic(comic, 'Home')
        this.props.navigation.navigate('ComicDetails', { comic: comic, routeHistory: 'Home' })
    }

    renderHorizontalImage(item) {
        return (
            <TouchableHighlight onPress={() => this.handleComicClicked(item)}
                style={{ margin: 5, borderRadius: 5 }} >
                <View>
                    <Image source={{ uri: item.coverImage }}
                        resizeMode={'cover'}
                        style={{ height: this.getHeight(), width: width / 3.23, borderRadius: 5 }}
                    />
                </View>
            </TouchableHighlight>
        )
    }

    renderList(item) {
        return (
            <ListItem onPress={() => this.handleComicClicked(item)}>
                <Image style={styles.image} source={{ uri: item.secondCoverImage }} />
                <Body>
                    <Text style={[styles.title, styles.textMargin]} >{item.title}</Text>
                    <View style={[styles.row, styles.textMargin]}>
                        <Icon style={[styles.icon, color.blue]} name='ios-chatbubbles' />
                        <Text style={[styles.description, color.blue]} numberOfLines={1} >{item.description}</Text>
                    </View>
                    <View style={[styles.row, styles.textMargin]}>
                        <Icon style={[styles.icon, color.grey]} name='ios-paper-outline' />
                        <Text note>Author: {item.author.name}</Text>
                    </View>
                </Body>
            </ListItem>
        )
    }

    render() {
        return (
            <View style={{ marginTop: this.props.height }}>
                <Card>
                    <View style={{ flexDirection: 'column' }}>
                        <View style={{ margin: 8, flexDirection: 'row', alignItems: 'center' }}>
                            <Icon style={{ color: '#F1948A', marginRight: 5 }} name='md-ribbon' />
                            <Text style={{ color: '#EC7063', fontWeight: '500' }}>New released</Text>
                        </View>
                        <View style={{ margin: 5 }}>
                            <FlatList
                                horizontal={true}
                                showsHorizontalScrollIndicator={false}
                                ref='flatlist'
                                keyExtractor={item => item.id}
                                data={this.state.comics}
                                renderItem={({ item }) => this.renderHorizontalImage(item)}
                            />
                        </View>
                    </View>
                </Card>
                <Card>
                    <View style={{ flexDirection: 'column' }}>
                        <View style={{ marginTop: 12, marginLeft: 10, flexDirection: 'row', alignItems: 'center' }}>
                            <Icon style={{ marginRight: 5, color: '#E59866' }} name='md-flame' />
                            <Text style={{ color: '#EB984E', fontWeight: '500' }}>Top 5</Text>
                        </View>
                        <View style={{ margin: 5 }}>
                            <FlatList
                                ref='flatlist'
                                keyExtractor={item => item.id}
                                data={_.slice(this.state.comics, [0], [5])}
                                renderItem={({ item }) => this.renderList(item)}
                            />
                        </View>
                    </View>
                </Card>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    listItem: {
        paddingBottom: 5,
        paddingTop: 5
    },
    image: {
        width: 95,
        height: 75,
        borderRadius: 4
    },
    row: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    textMargin: {
        marginTop: 5,
        marginBottom: 5,
    },
    title: {

    },
    description: {
        fontSize: 16
    },
    icon: {
        fontSize: 14,
        marginLeft: 12,
        marginRight: 5
    }
})

const color = StyleSheet.create({
    grey: {
        color: 'grey'
    },
    blue: {
        color: '#5DADE2'
    },
    red: {
        color: 'red'
    }
})

const mapStateToProps = (state) => ({
    comics: state.comic.getAllComics.payload
})
const mapDispatchToProps = (dispatch) => ({
    setToViewComic: (comic, routeHistory) => {
        dispatch(setToViewComic(comic, routeHistory))
    }
})

export default connect(mapStateToProps, mapDispatchToProps)(Home)