import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Image, Dimensions, TouchableHighlight, FlatList, ScrollView, StatusBar } from 'react-native'
import { Container, Content, Text, View, Toast, Button, Drawer, Icon } from 'native-base'
import { getChapter, setToViewComic } from '../ComicAction'
import AutoHeightImage from 'react-native-auto-height-image'
import LoadingComponent from '../../LoadingComponent'
import ZoomView from 'react-native-zoom-view'
import _ from 'lodash'

const { width, height } = Dimensions.get('window')

class ReadComic extends Component {
    static navigationOptions = {
        tabBarVisible: false
    }
    constructor(props) {
        super(props)
        this.state = {
            comic: null,
            chapter: null,
            chapterData: [],
            isLoading: true,
            responsiveWidth: 0,
            responsiveHeight: height,
            imageHeight: 0,
            currentPage: 1,
            visible: false,
            routeHistory: '',
        }
        this.handleViewableItemsChanged = this.handleViewableItemsChanged.bind(this)
        this.viewabilityConfig = { viewAreaCoveragePercentThreshold: 70 }
        this.handleBtnBack = this.handleBtnBack.bind(this)
    }

    componentWillMount() {
        // this.props.getChapter(this.props.toViewChapter)
        this.setState({
            chapter: this.props.navigation.state.params.chapter,
            comic: this.props.navigation.state.params.comic,
            routeHistory: this.props.navigation.state.params.routeHistory
        })
        this.props.getChapter(this.props.navigation.state.params.chapter)
        StatusBar.setHidden(true, 'fade')
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            chapterData: nextProps.chapterData,
            isLoading: nextProps.isLoading
        })
    }

    componentWillUnmount() {
        this.setState({
            chapterData: []
        })
    }

    handleBtnBack() {
        this.props.navigation.navigate('ComicDetails', { tabHistory: 1, comic: this.state.comic, routeHistory: this.state.routeHistory })
    }

    renderImage(item) {
        return (
            <AutoHeightImage
                width={this.state.responsiveWidth}
                source={{ uri: item.imageUrl }} />
        )
    }

    renderFooter() {
        return (
            <View style={{
                position: 'absolute', zIndex: 1, backgroundColor: 'rgba(0, 0, 0, 0.5)'
                , height: 25, width: 150, bottom: 0, right: 0,
                justifyContent: 'center',
            }}>
                <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }}>
                    <Text note style={{ color: 'white' }}>Page {this.state.currentPage}/{_.isEmpty(this.state.chapterData) ? '' : this.state.chapterData.length}</Text>
                    <Text note style={{ color: 'white', marginLeft: 8 }}>Chapter {this.state.chapter.number}</Text>
                </View>
            </View>
        )
    }

    handleViewableItemsChanged(info) {
        this.setState({
            currentPage: info.changed[0].item.number
        })
    }

    closeDrawer = () => {
        this.drawer._root.close()
    }
    openDrawer = () => {
        this.drawer._root.open()
    }

    render() {
        const { isLoading, chapterData, chapter } = this.state
        return (
            isLoading ? <LoadingComponent visible={true} /> :
                <Drawer
                    styles={{ drawer: { width: this.state.responsiveWidth * 0.3 } }}
                    openDrawerOffset={0.7}
                    tapToClose={true}
                    ref={(ref) => { this.drawer = ref; }}
                    onClose={() => this.closeDrawer()}
                    panOpenMask={0.20}
                    panCloseMask={0.80}
                    captureGestures="open"
                    content={
                        <View style={{ backgroundColor: 'rgba(0, 0, 0, 0.5)', flex: 1 }}>
                            <Button transparent iconLeft onPress={() => this.handleBtnBack()} style={{ width: this.state.responsiveWidth * 0.3, backgroundColor: 'rgba(93, 109, 126, 0.5)', marginTop: 25, justifyContent: 'center' }}>
                                <Icon style={{ color: 'white' }} name='ios-arrow-back' />
                                <Text style={{ color: 'white', fontWeight: '600' }}>Back</Text>
                            </Button>
                        </View>
                    }
                >
                    <Container>
                        <View onLayout={(event) => {
                            let { x, y, width, height } = event.nativeEvent.layout
                            this.setState({
                                responsiveWidth: width,
                            })
                        }} />
                        <ZoomView
                            cropHeight={height}
                            cropWidth={this.state.responsiveWidth}
                            imageWidth={this.state.responsiveWidth}
                            imageHeight={height}
                        >
                            {
                                _.isEmpty(chapterData) ? null :
                                    <FlatList
                                        ref='flatlist'
                                        keyExtractor={item => item.number}
                                        extraData={this.state.responsiveWidth}
                                        data={_.sortBy(chapterData, ['number'])}
                                        renderItem={({ item }) => this.renderImage(item)}
                                        viewabilityConfig={this.viewabilityConfig}
                                        onViewableItemsChanged={this.handleViewableItemsChanged}
                                    />
                            }
                        </ZoomView>
                        {this.renderFooter()}
                    </Container>
                </Drawer>
        )
    }
}

const mapStateToProps = (state) => ({
    toViewChapter: state.comic.toViewChapter,
    chapterData: state.comic.getChapterPage.payload,
    isLoading: state.comic.getChapterPage.isLoading
})
const mapDispatchToProps = (dispatch) => ({
    getChapter: (chapter) => {
        dispatch(getChapter(chapter))
    },
    setToViewComic: (comic, routeHistory) => {
        dispatch(setToViewComic(comic, routeHistory))
    }
})

export default connect(mapStateToProps, mapDispatchToProps)(ReadComic)