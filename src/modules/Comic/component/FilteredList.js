import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Image, TouchableHighlight, StatusBar } from 'react-native'
import { Container, View, Header, Body, Title, Content, List, ListItem, Text, Thumbnail, Item, Icon, Input, Left, Right, Button } from 'native-base'
import { getFilteredComics, setToViewComic } from '../ComicAction'
import Loading from '../../LoadingComponent'
import HeaderComponent from '../../../common/Header'

class FilteredList extends Component {
    static navigationOptions = {
        tabBarVisible: false
    }
    constructor(props) {
        super(props)
        this.state = {
            comic: [],
            isLoading: false
        }
    }

    componentWillMount() {
        this.props.getFilteredComics(this.props.categoryFilter)
        StatusBar.setHidden(false)
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            comics: nextProps.comics,
            isLoading: nextProps.isLoading
        })
    }

    handleToViewComic(comic) {
        this.props.setToViewComic(comic, 'FilteredList')
        this.props.navigation.navigate('ComicDetails', { comic: comic, routeHistory: 'FilteredList' })
    }

    render() {
        const { isLoading, comics } = this.state
        const { categoryFilter } = this.props
        return (
            this.props.isLoading ? <Loading visible={true} /> :
                <Container>
                    <HeaderComponent 
                        title={categoryFilter.data().name}
                        onPress={() => this.props.navigation.navigate('Category')}
                    />
                    {
                        <Content style={{ backgroundColor: 'white' }}>
                            <List>
                                {
                                    comics ?
                                        comics.length > 0 ?
                                            comics.map((val, key) => {
                                                return <ListItem key={key} onPress={() => this.handleToViewComic(val)}>
                                                    <Thumbnail large square source={{ uri: val.coverImage }} />
                                                    <Body>
                                                        <Text>{val.title}</Text>
                                                        <Text numberOfLines={1} note>{val.description}</Text>
                                                        <Text note>{val.chapters.length} Chapters</Text>
                                                    </Body>
                                                </ListItem>
                                            })
                                            : null
                                        : null
                                }
                            </List>
                        </Content>

                    }
                </Container>
        )
    }
}

const mapStateToProps = (state) => ({
    categoryFilter: state.comic.categoryFilter,
    comics: state.comic.comics,
    isLoading: state.comic.isLoading
})

const mapDispatchToProps = (dispatch) => ({
    getFilteredComics: (filter) => {
        dispatch(getFilteredComics(filter))
    },
    setToViewComic: (comic, routeHistory) => {
        dispatch(setToViewComic(comic, routeHistory))
    }
})

export default connect(mapStateToProps, mapDispatchToProps)(FilteredList)