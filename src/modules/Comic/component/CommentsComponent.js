import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Dimensions, FlatList } from 'react-native'
import { Toast, Container, Content, View, List, ListItem, Left, Right, Body, Thumbnail, Text, Item, Input, Button } from 'native-base'
import InfiniteScroll from 'react-native-infinite-scroll'
import { getComments, sendComment } from '../ComicAction'
import LoadingComponent from '../../LoadingComponent'
import moment from 'moment'
import _ from 'lodash'

const { width, height } = Dimensions.get('window')

class CommentsComponent extends Component {
    constructor(props) {
        super(props)
        this.state = {
            comicId: null,
            comments: [],
            isLoading: true,
            commentMessage: '',
            isSending: false,
        }
        this.scrollToBottom = this.scrollToBottom.bind(this)
        this.sortCommentsByDate = this.sortCommentsByDate.bind(this)
    }

    componentWillMount() {
        this.setState({
            comicId: this.props.comicId,
        })
        this.props.getComments(this.props.comicId)
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            isLoading: nextProps.isLoading,
            isSending: nextProps.isSending
        })

        if (nextProps.comments) {
            this.setState({
                comments: nextProps.comments
            })
        }

        if (!nextProps.isSending) {
            this.scrollToBottom()
        }
    }

    sortCommentsByDate() {
        let comments = this.state.comments
        comments = _.sortBy(comments, function (dateObj) {
            return new Date(dateObj.date);
        });
        return comments
    }

    handleSendComment() {
        const { comicId, commentMessage } = this.state
        if (!_.isEmpty(commentMessage)) {
            this.props.sendComment(comicId, commentMessage)
            this.setState({
                commentMessage: '',
                isSending: true
            })
        }
    }

    scrollToBottom(animated = true) {
        this.refs.flatlist.scrollToEnd({ animated })
    }

    renderComments(item) {
        const localUserImage = require('../../../assets/user_avatar.png')
        const val = item.item
        return (
            <ListItem avatar key={item.index}>
                <Left>
                    <Thumbnail small
                        source={val.user.profilePictureUrl ? { uri: val.user.profilePictureUrl } : localUserImage}
                    />
                </Left>
                <Body>
                    <Text note>{val.username}</Text>
                    <Text>{val.message}</Text>
                </Body>
                <Right>
                    <Text note>{moment(val.date).format("DD-MM-YYYY")}</Text>
                </Right>
            </ListItem>
        )
    }

    render() {
        const { isLoading, commentMessage, isSending } = this.state
        return (
            <Container style={{ flex: 1 }}>
            <LoadingComponent visible={isLoading}/>
                <FlatList
                    ref='flatlist'
                    keyExtractor={item => item.id}
                    refreshing={isLoading}
                    data={this.sortCommentsByDate()}
                    renderItem={(item, index) => this.renderComments(item)}
                    extraData={this.state.comments}
                />
                <View style={{ position: 'relative', bottom: 1 }}>
                    <Item regular>
                        <Input placeholder='Your comment ...' value={commentMessage} disabled={isSending} onChangeText={(val) => this.setState({ commentMessage: val })} />
                        <Button info style={{ alignSelf: 'center', borderRadius: 7, marginRight: 1 }}
                            disabled={isSending}
                            onPress={() => this.handleSendComment()}>
                            <Text>{isSending ? 'Sending' : 'Send'}</Text>
                        </Button>
                    </Item>
                </View>
            </Container>
        )
    }
}

const mapStateToProps = (state) => ({
    comments: state.comic.comments.comments,
    isLoading: state.comic.comments.isLoading,
    isSending: state.comic.comments.isSending,
})
const mapDispatchToProps = (dispatch) => ({
    getComments: (comicId) => {
        dispatch(getComments(comicId))
    },
    sendComment: (comicId, msg) => {
        dispatch(sendComment(comicId, msg))
    },
})

export default connect(mapStateToProps, mapDispatchToProps)(CommentsComponent)