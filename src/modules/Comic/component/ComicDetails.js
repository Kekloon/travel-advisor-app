import React, { Component } from 'react'
import { connect } from 'react-redux'
import { setToViewChapter, checkSubsription, addToSubscription, removeFromSubscription, sendComment, payChapter, checkUnPaidChapters, getComments } from '../ComicAction'
import { Image, TouchableHighlight, Dimensions, StyleSheet, ScrollView, ListView, Alert, Animated, Platform, StatusBar } from 'react-native'
import { Container, Content, Text, View, Icon, Button, Toast, Header, Tab, Tabs, TabHeading, List, ListItem, Left, Body, Right, Thumbnail, Item, Input, Label } from 'native-base'
import InfiniteScroll from 'react-native-infinite-scroll'
import Modal from 'react-native-modal'
import LoadingComponent from '../../LoadingComponent'
import CommentsComponent from '../component/CommentsComponent'
import _ from 'lodash'

const { width, height } = Dimensions.get('window')

class ComicDetails extends Component {
    static navigationOptions = {
        tabBarVisible: false
    }

    constructor(props) {
        super(props)
        this.scroll = null;
        this.state = {
            comic: null,
            userCredits: {
                balance: 0
            },
            isLoading: false,
            isSubscribe: false,
            responsiveWidth: 0,
            defaultToViewChapter: null,
            commentMessage: '',
            isSending: false,
            isModalVisible: false,
            selectedChapter: null,
            isPaying: false,
            unpaidChapters: [],
            unpaidAmount: 0,
            userCreditsUpdated: false,
            contentHeight: 0,
            scrollViewHeight: 0,
            tabHistory: 0,
            routeHistory: '',
            defaultCalculateAllChapter: 0,
        }
        this.getCategoriesString = this.getCategoriesString.bind(this)
        this.setViewChapter = this.setViewChapter.bind(this)
        this.handleAddToSubscribe = this.handleAddToSubscribe.bind(this)
        this.handleRemoveFromSubscribe = this.handleRemoveFromSubscribe.bind(this)
    }

    componentWillMount() {
        this.setState({
            comic: this.props.toViewComic,
            userCredits: this.props.userCredits,
            routeHistory: this.props.navigation.state.params.routeHistory
        })
        this.props.checkSubsription(this.props.navigation.state.params.comic.id)
        StatusBar.setHidden(true, 'fade')
    }

    componentDidMount() {
        setTimeout(() => {
            this.setState({
                tabHistory: _.isEmpty(this.props.navigation.state.params) ? 0 : this.props.navigation.state.params.tabHistory
            })
        }, 1000);
    }

    componentDidUpdate() {
        if (this.state.userCreditsUpdated) {
            let unpaidAmount = 0
            let unpaidChapters = []
            this.state.comic.chapters.map((chapterDoc) => {

                if (!this.checkIsChapterPaid(chapterDoc) && chapterDoc.price > 0) {
                    unpaidAmount += chapterDoc.price
                    unpaidChapters.push(chapterDoc)
                }
            })
            this.setState({
                unpaidAmount: unpaidAmount,
                unpaidChapters: unpaidChapters,
                userCreditsUpdated: false
            })
        }
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            isLoading: nextProps.isLoading,
            comic: nextProps.toViewComic,
            isSubscribe: nextProps.isSubscribed,
            isSending: nextProps.isSending,
            userCredits: nextProps.userCredits,
            isPaying: nextProps.isPaying,
        })

        if (this.state.comic) {
            if (nextProps.userCredits) {
                this.setState({
                    userCreditsUpdated: true
                })
            }
        }

        if (nextProps.paySuccess) {
            this.setState({
                isModalVisible: false
            })
        }

        if (!_.isEmpty(nextProps.toViewComic) && this.state.defaultCalculateAllChapter === 0) {
            let unpaidAmount = 0
            let unpaidChapters = []
            nextProps.toViewComic.chapters.map((chapterDoc) => {

                if (!this.checkIsChapterPaid(chapterDoc) && chapterDoc.price > 0) {
                    unpaidAmount += chapterDoc.price
                    unpaidChapters.push(chapterDoc)
                }
            })
            this.setState({
                unpaidAmount: unpaidAmount,
                unpaidChapters: unpaidChapters,
                userCreditsUpdated: false,
                defaultCalculateAllChapter: 1
            })
        }
    }

    getCategoriesString() {
        const categories = this.state.comic.categories
        let categoriesString = ''

        categories.forEach((val, key) => {
            if (key === 0) {
                categoriesString = val.name
            } else {
                categoriesString = categoriesString + ', ' + val.name
            }
        })
        return categoriesString
    }

    setIsSubscribe(bool) {
        this.setState({
            isSubscribe: bool
        })
    }

    sortChapters() {
        return this.state.comic.chapters.sort((a, b) => a.number - b.number)
    }

    setViewChapter(chapter, key) {
        if (key === 0) {
            this.setState({
                defaultToViewChapter: chapter
            })
        }
        this.props.navigation.navigate('ReadComic', { chapter: chapter, comic: this.state.comic, routeHistory: this.state.routeHistory })
        this.props.setToViewChapter(chapter)
    }

    getAdjustedFontSize = (size) => {
        return parseInt(size) * width * (1.8 - 0.002 * width) / 400;
    }

    unlockModal() {
        return (
            <View>
                <Modal
                    isVisible={this.state.isModalVisible}
                    onBackdropPress={() => this.setState({ isModalVisible: false })}
                    
                >
                    <View style={{ backgroundColor: 'white', borderRadius: 10 }}>
                        <View style={{ margin: 20, alignItems: 'center' }}>
                            <Text style={{ fontSize: 24, fontWeight: 'bold' }}>{this.state.comic.title}</Text>
                            <Text note> Your available credits: {this.state.userCredits.balance}</Text>
                        </View>
                        <LoadingComponent visible={this.state.isPaying} />
                        <View style={{ flexDirection: 'column' }}>

                            <View style={{ flexDirection: 'row', marginBottom: 25, marginRight: 10, alignItems: 'center' }}>
                                <View style={{ flex: 1, marginLeft: 50, flexDirection: 'row' }}>
                                    <View style={{ margin: 5 }}>
                                        <Icon name='md-unlock' style={{ color: 'green' }} />
                                    </View>
                                    <View>
                                        <Text style={{ fontWeight: 'bold', fontSize: this.getAdjustedFontSize(18), color: 'green' }}>Chapter {this.state.selectedChapter ? this.state.selectedChapter.number : 0}</Text>
                                        <Text note>Unlock this chapter</Text>
                                    </View>
                                </View>
                                <Button small light style={{ flex: 0, margin: 8 }} iconRight
                                    disabled={this.state.isPaying}
                                    onPress={() => this.handleUnlock(this.state.selectedChapter)}
                                >
                                    <Text style={{ color: 'green' }}>{this.state.selectedChapter ? this.state.selectedChapter.price : 0}$</Text>
                                    <Icon name='md-unlock' style={{ color: '#ABB2B9', fontSize: 17 }} />
                                </Button>
                            </View>

                            <View style={{ flexDirection: 'row', marginBottom: 25, marginRight: 10, alignItems: 'center' }}>
                                <View style={{ flex: 1, marginLeft: 50, flexDirection: 'row' }}>
                                    <View style={{ margin: 5 }}>
                                        <Icon name='md-unlock' style={{ color: 'green' }} />
                                    </View>
                                    <View>
                                        <Text style={{ fontWeight: 'bold', fontSize: this.getAdjustedFontSize(18), color: 'green' }}>All Chapters ({this.state.unpaidChapters.length})</Text>
                                        <Text note>Unlock all chapters</Text>
                                    </View>
                                </View>
                                <Button small light style={{ flex: 0, margin: 8 }} iconRight
                                    disabled={this.state.isPaying}
                                    onPress={() => this.handleUnlock(null, this.state.unpaidChapters)}
                                >
                                    <Text style={{ color: 'green', }}>{this.state.unpaidAmount}$</Text>
                                    <Icon name='md-unlock' style={{ color: '#ABB2B9', fontSize: 17 }} />
                                </Button>
                            </View>
                        </View>
                    </View>
                </Modal>
            </View>
        )
    }

    handleGoToTopUpPage() {
        this.setState({
            isModalVisible: false
        })
        this.props.navigation.navigate('User')
    }

    handleUnlock(chapter, allChapter) {
        const { userCredits, selectedChapter, unpaidAmount } = this.state
        if (allChapter) {
            if (userCredits.balance < unpaidAmount) {
                Alert.alert('Unlock', 'Your credits is not enough to unlock!'
                    + '\nYour credits: ' + userCredits.balance
                    + '\nCredits needed: ' + unpaidAmount
                    , [
                        { text: 'Top up', onPress: () => this.handleGoToTopUpPage() },
                        { text: 'Cancel' }
                    ], { cancelable: false })
            } else {
                Alert.alert('Unlock', 'Are you sure to unlock all chapters?', [
                    { text: 'Unlock', onPress: () => this.props.payChapter(null, this.state.unpaidChapters) },
                    { text: 'Cancel' }
                ], { cancelable: false })
            }
        } else {
            if (userCredits.balance < selectedChapter.price) {
                Alert.alert('Unlock', 'Your credits is not enough to unlock! \n'
                    + 'Your credits: ' + userCredits.balance
                    + '\nCredits needed: ' + selectedChapter.price
                    , [
                        { text: 'Top up', onPress: () => this.handleGoToTopUpPage() },
                        { text: 'Cancel' }
                    ], { cancelable: false })
            } else {
                Alert.alert('Unlock', 'Are you sure to chapter ' + chapter.number + ' ?', [
                    { text: 'Unlock', onPress: () => this.props.payChapter(this.state.selectedChapter) },
                    { text: 'Cancel' }
                ], { cancelable: false })
            }
        }

    }

    renderChapterButton() {
        return (
            <View style={{ flexDirection: 'row', flexWrap: 'wrap', width: width }}>
                {
                    this.sortChapters().map((chapter, key) => {
                        return (
                            <Button bordered dark
                                key={key}
                                style={styles.chaptersBtn}
                                onPress={() => chapter.price === 0 || this.checkIsChapterPaid(chapter) ?
                                    this.setViewChapter(chapter, key)
                                    :
                                    this.setState({ isModalVisible: true, selectedChapter: chapter })}
                            >
                                <Text>{chapter.number}</Text>
                                {
                                    chapter.price > 0 ?
                                        this.checkIsChapterPaid(chapter) === true ?
                                            <View style={{ position: 'absolute', bottom: 1, width: width / 4 - 17, backgroundColor: '#ABEBC6', alignItems: 'center', borderRadius: 2 }}>
                                                <Text style={{ fontSize: 8 }}>Unlocked</Text>
                                            </View>
                                            :
                                            <View style={{ position: 'absolute', bottom: 1, width: width / 4 - 17, backgroundColor: '#F1948A', alignItems: 'center', borderRadius: 2, flexDirection: 'row', justifyContent: 'center' }}>
                                                <Icon name='md-lock' style={{ color: 'black', fontSize: 7 }} />
                                                <Text style={{ fontSize: 8, marginLeft: 7 }}>Locked</Text>
                                            </View>
                                        :
                                        <View style={{ position: 'absolute', bottom: 1, width: width / 4 - 17, backgroundColor: '#ABEBC6', alignItems: 'center', borderRadius: 2 }}>
                                            <Text style={{ fontSize: 8 }}>Free</Text>
                                        </View>
                                }
                            </Button>
                        )
                    })
                }
            </View>
        )
    }

    checkIsChapterPaid(chapter) {
        const { userCredits } = this.state
        let unpaidChapter = []
        let unpaidAmount = 0
        let result = false
        if (chapter.price === 0) {
            return true
        }
        if (userCredits.paymentHistory) {
            userCredits.paymentHistory.map((val) => {
                if (chapter.chapter.id === val.chapter.id) {
                    result = true
                } else {
                    unpaidAmount += val.price
                    unpaidChapter.push(chapter)
                }
            })
            return result
        } else {
            return result
        }
    }

    handleAddToSubscribe(comicId) {
        this.props.addToSubscription(comicId)
        this.setState({
            isSubscribe: true
        })
    }

    handleRemoveFromSubscribe(comicId) {
        this.props.removeFromSubscription(comicId)
        this.setState({
            isSubscribe: false,
        })
    }

    render() {
        const { isLoading, comic, isSubscribe, userCredits } = this.state
        return (
            comic === null || this.props.isLoading ? <LoadingComponent visible={true} /> :
                <Container style={{ backgroundColor: 'white' }}>
                    <View>
                        <View style={{ position: 'absolute', zIndex: 1, margin: 10 }}>
                            <Icon name='ios-arrow-dropleft' onPress={() => this.props.navigation.navigate(this.state.routeHistory)} />
                        </View>
                        {this.unlockModal()}
                        <LoadingComponent visible={isLoading} />
                        <View onLayout={(event) => {
                            let { x, y, width, height } = event.nativeEvent.layout
                            this.setState({
                                responsiveWidth: width
                            })
                        }} />

                        <Image source={{ uri: comic.secondCoverImage }}
                            resizeMode={'cover'}
                            style={{
                                width: this.state.responsiveWidth,
                                height: height / 2.8
                            }}
                        />

                        <View style={styles.titleContainer}>
                            <Text style={styles.textTitle} >{comic.title}</Text>
                            <Text style={styles.textCategory} >{this.getCategoriesString()}</Text>
                        </View>

                        <View style={{ height: 65 }}>
                            <View style={{
                                flex: 1,
                                flexDirection: 'row',
                                justifyContent: 'space-between',
                                alignItems: 'center'
                            }} >
                                {
                                    isSubscribe === false ?
                                        <View style={{ flex: 1, flexDirection: 'row', padding: 22, alignItems: 'center' }}>
                                            <Icon name='bookmark' onPress={() => this.handleAddToSubscribe(comic.id)} style={{ color: 'grey', fontSize: 25 }} />
                                            <Text onPress={() => this.handleAddToSubscribe(comic.id)} style={{ marginLeft: 5, fontSize: 15 }} >Subscribe</Text>
                                        </View>
                                        :
                                        <View style={{ flex: 1, flexDirection: 'row', padding: 22, alignItems: 'center' }} >
                                            <Icon name='bookmark' onPress={() => this.handleRemoveFromSubscribe(comic.id)} style={{ color: '#FF5733', fontSize: 25 }} />
                                            <Text onPress={() => this.handleRemoveFromSubscribe(comic.id)} style={{ marginLeft: 5, fontSize: 15, color: '#FF5733' }} >Subscribed</Text>
                                        </View>
                                }
                                {

                                }
                                <View>
                                    <Button warning
                                        onPress={() =>
                                            this.checkIsChapterPaid(this.sortChapters()[0]) ?
                                                this.setViewChapter(this.sortChapters()[0])
                                                :
                                                this.setState({ isModalVisible: true, selectedChapter: this.sortChapters()[0] })
                                        }
                                        style={{ margin: 5, marginRight: 20, height: 37, width: 180, justifyContent: 'center', borderRadius: 5 }}>
                                        <Text> Start reading </Text>
                                    </Button>
                                </View>
                            </View>
                        </View>
                    </View>

                    <Tabs tabBarUnderlineStyle={{ backgroundColor: '#FB7400', height: 2, }}
                        page={this.state.tabHistory}
                        onChangeTab={(index) => this.setState({ tabHistory: index.i })}
                    >
                        <Tab heading={<TabHeading style={{ backgroundColor: 'white' }}><Text style={{ color: 'black' }} > Details</Text></TabHeading>}>
                            <ScrollView style={{ margin: 13 }}>
                                <View style={{ flexDirection: 'row' }}>
                                    <Text style={{ fontWeight: '600', marginBottom: 5 }}>Author : </Text>
                                    <Text>{comic.author.name}</Text>
                                </View>
                                <Text style={{ fontWeight: '600', marginBottom: 2 }}>Description : </Text>
                                <Text>{comic.description}</Text>
                            </ScrollView>
                        </Tab>
                        <Tab heading={<TabHeading style={{ backgroundColor: 'white' }}><Text style={{ color: 'black' }} >Chapters</Text></TabHeading>}>
                            <ScrollView style={{
                                margin: 13,
                                flexDirection: 'row',
                                flexWrap: 'wrap'
                            }}>
                                {
                                    this.renderChapterButton()
                                }
                            </ScrollView>
                        </Tab>
                        <Tab heading={<TabHeading style={{ backgroundColor: 'white' }}><Text style={{ color: 'black' }} >Comments</Text></TabHeading>}>
                            <CommentsComponent comicId={comic.id} />
                        </Tab>
                    </Tabs>
                </Container>
        )
    }
}

const styles = StyleSheet.create({
    image: {
    },
    titleContainer: {
        backgroundColor: 'rgba(52, 52, 52, 0.5)',
        padding: 10,
        position: 'absolute',
        width: width,
        top: height / 2.8 - 60
    },
    textTitle: {
        color: 'white',
        fontSize: 22
    },
    textCategory: {
        color: 'white',
        fontSize: 13
    },
    chaptersBtn: {
        width: width / 4 - 17,
        height: 40,
        margin: 5,
        justifyContent: 'center',
        borderColor: '#D5D8DC'
    }
})

const mapStateToProps = (state) => ({
    toViewComic: state.comic.toViewComic,
    isLoading: state.comic.isLoading || state.comic.loadingToViewComic,
    isSubscribed: state.comic.isSubscribed,
    userCredits: state.user.userCredits.payload,
    isPaying: state.comic.payChapter.isLoading,
    paySuccess: state.comic.payChapter.success,
    routeHistory: state.comic.routeHistory,
})
const mapDispatchToProps = (dispatch) => ({
    setToViewChapter: (chapter) => {
        dispatch(setToViewChapter(chapter))
    },
    checkSubsription: (comicId) => {
        dispatch(checkSubsription(comicId))
    },
    addToSubscription: (comicId) => {
        dispatch(addToSubscription(comicId))
    },
    removeFromSubscription: (comicId) => {
        dispatch(removeFromSubscription(comicId))
    },
    sendComment: (comicId, msg) => {
        dispatch(sendComment(comicId, msg))
    },
    payChapter: (chapter, allChapter) => {
        dispatch(payChapter(chapter, allChapter))
    },
    getComments: (comicId) => {
        dispatch(getComments(comicId))
    },
})
export default connect(mapStateToProps, mapDispatchToProps)(ComicDetails)