import React, { Component } from 'react'
import { connect } from 'react-redux'
import { StyleSheet, Dimensions, TouchableHighlight, Image, Keyboard, StatusBar } from 'react-native'
import { Container, View, Text, Toast, Header, Item, Icon, Input, ListItem, Thumbnail, Body } from 'native-base'
import InfiniteScroll from 'react-native-infinite-scroll'
import Loading from '../../LoadingComponent'
import { getAllComics } from '../ComicAction'

const { width, height } = Dimensions.get('window')
class Category extends Component {

    constructor(props) {
        super(props)
        this.state = {
            categoryList: [],
            tabBarVisible: true,
            isSearching: false,
            filterList: [],
            isLoading: true,
            comics: [],
            searchingText: null,
            responsiveWidth: width / 3 - 20
        }

        this.renderCategoryItem = this.renderCategoryItem.bind(this)
        this.getHeight = this.getHeight.bind(this)
        this.showList = this.showList.bind(this)
        this.closeSearching = this.closeSearching.bind(this)
        this.handleComicClicked = this.handleComicClicked.bind(this)
    }

    componentWillMount() {
        this.setState({
            comics: this.props.comics
        })
        StatusBar.setHidden(false)
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            comics: nextProps.comics
        })
    }


    showList(val) {
        this.props.setCategoryFilter(val)
        this.props.navigation.navigate('FilteredList')
    }

    renderCategoryItem(val, key) {
        return (
            <View style={{  width: this.state.responsiveWidth, margin: 10 }} key={key}>
                <TouchableHighlight
                    onPress={() => this.showList(val)}
                    style={{ borderRadius: 8 }}
                >

                    <Image source={{ uri: val._data.coverImage }}
                        resizeMode={'cover'}
                        style={{ height: this.getHeight(), borderRadius: 8 }}
                    />
                </TouchableHighlight>
                <Text numberOfLines={1} style={styles.text}>{val._data.name}</Text>
            </View>
        )
    }

    getHeight() {
        // ration 4:3
        return (this.state.responsiveWidth / 4) * 3
    }

    closeSearching() {
        this.setState({
            isSearching: false,
            searchingText: '',
        }, () => Keyboard.dismiss())
    }

    renderSearchListItem(comic, key) {
        return (
            <ListItem key={key} onPress={() => this.handleComicClicked(comic)}>
                <Thumbnail large square source={{ uri: comic.coverImage }} />
                <Body>
                    <Text>{comic.title}</Text>
                    <Text numberOfLines={1} note>{comic.description}</Text>
                    <Text note>{comic.chapters.length} Chapters</Text>
                </Body>
            </ListItem>
        )
    }

    handleComicClicked(comic) {
        this.props.setToViewComic(comic, 'Category')
        this.props.navigation.navigate('ComicDetails', { comic: comic, routeHistory: 'Category' })
    }

    render() {
        let myReg = new RegExp(_.isEmpty(this.state.searchingText) ? null : this.state.searchingText + ".*", 'i')
        const searchedComic = _.filter(this.state.comics, function (o) { return o.title.match(myReg) })
        return (
            <Container>
                <Header searchBar rounded noShadow={true} style={{ backgroundColor: 'transparent' }}>
                    <Item>
                        <Icon name="ios-search" />
                        <Input placeholder="Search" onFocus={() => this.setState({ isSearching: true })} onChangeText={(text) => this.setState({ searchingText: text })} />
                        {
                            !this.state.isSearching ? null :
                                <Icon name='md-close-circle' onPress={() => this.closeSearching()} />
                        }
                    </Item>
                </Header>
                <View onLayout={(event) => {
                    let { x, y, width, height } = event.nativeEvent.layout
                    this.setState({ responsiveWidth: width / 3 - 20 })
                }} />
                <Loading visible={this.props.isLoading || this.props.isFetching} />
                <InfiniteScroll horizontal={false}>
                    {
                        this.state.isSearching ?
                            !_.isEmpty(searchedComic) ?
                                searchedComic.map((val, key) => {
                                    return this.renderSearchListItem(val, key)
                                })
                                : null
                            :
                            <View style={styles.container}>
                                {
                                    this.props.categoryList.map((val, key) => {
                                        return this.renderCategoryItem(val, key)
                                    })
                                }
                            </View>
                    }
                </InfiniteScroll>
            </Container>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        flexWrap: 'wrap'
    },
    item: {
        width: width,
        margin: 10
    },
    text: {
        textAlign: 'center',
        marginTop: 5
    }
});

const mapStateToProps = (state) => ({
    comics: state.comic.getAllComics.payload
})
const mapDispatchToProps = (dispatch) => ({
    getAllComics: () => {
        dispatch(getAllComics())
    }
})
export default connect(mapStateToProps, mapDispatchToProps)(Category)
// export default Category