import React, { Component } from 'react'
import { Container, View, Text, Row, Grid, Col, Header, Body, Title, Icon } from 'native-base'
import { Image, TouchableHighlight, Dimensions, StatusBar } from 'react-native'
import InfiniteScroll from 'react-native-infinite-scroll'
import Loading from '../../LoadingComponent'
import _ from 'lodash'

const DIMENSIONS = Dimensions.get('window') // ratio 11:17
const { width, height } = Dimensions.get('window')
class Bookshelf extends Component {
    static navigationOptions = {
        title: 'Bookshelf',
        // tabBarVisible: this.props.tabBarVisible
    }

    constructor(props) {
        super(props)
        this.state = {
            subscribedList: [],
            isLoading: true,
            responsiveWidth: width,
        }
        this.getHeight = this.getHeight.bind(this)
        this.handleComicClicked = this.handleComicClicked.bind(this)
    }

    componentWillMount() {
        // this.props.getSubsribedComics()
        this.setState({
            isLoading: this.props.isLoading,
            subscribedList: this.props.subscribedList
        })
        StatusBar.setHidden(false)
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            isLoading: nextProps.isLoading,
            subscribedList: nextProps.subscribedList
        })
    }

    handleComicClicked(comic) {
        this.props.setToViewComic(comic, 'Bookshelf')
        this.props.navigation.navigate('ComicDetails', { comic: comic, routeHistory: 'Bookshelf' })
    }

    getHeight() {
        // ratio 11:17
        // return ((DIMENSIONS.width / 3.23) / 11) * 17
        return ((this.state.responsiveWidth / 3 - 10) / 11) * 17
    }

    render() {
        const { isLoading, subscribedList } = this.state
        return (
            <Container>
                <Header style={{ backgroundColor: '#EEEEEE', }}>
                    <Body>
                        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                            <Icon name='ios-bookmark-outline' />
                            <Text style={{ marginLeft: 8, color: 'black' }}>Your bookshelf</Text>
                        </View>
                    </Body>
                </Header>
                <View onLayout={(event) => {
                    let { x, y, width, height } = event.nativeEvent.layout
                    this.setState({ responsiveWidth: width })
                }} />
                <Loading visible={isLoading} />
                <InfiniteScroll horizontal={false}>
                    <View style={style.container}>
                        {
                            subscribedList.map((val, key) => {
                                return <View
                                    style={{
                                        width: this.state.responsiveWidth / 3 - 10,
                                        margin: 5,
                                        marginBottom: 15
                                    }}
                                    key={key}>
                                    <TouchableHighlight
                                        onPress={() => this.handleComicClicked(val)}>
                                        <Image source={{ uri: val.coverImage }}
                                            resizeMode={'cover'}
                                            style={{ height: this.getHeight(), borderRadius: 3 }}
                                        />
                                    </TouchableHighlight>
                                    <Text numberOfLines={1} style={style.card.text}>{val.title}</Text>
                                    <Text style={style.card.text} note>Chapter {!_.isEmpty(val.chapters) ? _.maxBy(val.chapters, 'number').number : 0}</Text>
                                </View>
                            })
                        }
                    </View>
                </InfiniteScroll>
            </Container >
        )
    }
}

const style = {
    container: {
        flexDirection: 'row',
        flexWrap: 'wrap'
    },
    card: {
        item: {
            width: DIMENSIONS.width / 3.23,
            margin: 5,
            marginBottom: 15
        },
        text: {
            textAlign: 'center'
        }
    }
}

export default Bookshelf