import { connect } from 'react-redux'
import Category from '../component/Category'
// Action
import { getCategories, setCategoryFilter, setToViewComic } from '../ComicAction'

const mapStateToProps = (state) => ({
    isLoading: state.comic.isLoading,
    categoryList: state.comic.categoryList,
    tabBarVisible: state.comic.tabBarVisible,
    comics: state.comic.getAllComics.payload
})

const mapDispatchToProps = (dispatch) => ({
    getCategories: () => {
        dispatch(getCategories())
    },
    setCategoryFilter: (category) => {
        dispatch(setCategoryFilter(category))
    },
    setToViewComic: (comic, routeHistory) => {
        dispatch(setToViewComic(comic, routeHistory))
    }
})

const CategoryContainer = connect(mapStateToProps, mapDispatchToProps)(Category)
export default CategoryContainer