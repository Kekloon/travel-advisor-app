import { connect } from 'react-redux'
import Bookshelf from '../component/Bookshelf'
import { getSubsribedComics, setToViewComic } from '../ComicAction'

const mapStateToProps = (state) => ({
    subscribedList: state.comic.getSubscribed.payload,
    isLoading: state.comic.getSubscribed.isLoading,
    tabBarVisible: state.comic.tabBarVisible
})

const mapDispatchToProps = (dispatch) => ({
    getSubsribedComics: () => {
        dispatch(getSubsribedComics())
    },
    setToViewComic: (comic, routeHistory) => {
        dispatch(setToViewComic(comic, routeHistory))
    }
})

const BookshelfContainer = connect(mapStateToProps, mapDispatchToProps)(Bookshelf)
export default BookshelfContainer