import React, { Component } from 'react'
import { Dimensions, ScrollView, Animated, StyleSheet, Platform } from 'react-native'
import { Container, Content, View, Header, Title, Icon, Body, Text, Button } from 'native-base'
import Home from '../component/Home'
import SlideShowComponent from '../../Forum/component/SlideShowComponent'
import ForumListComponent from '../../Forum/component/ForumListComponent'
const { width, height } = Dimensions.get('window')

const Header_Maximum_Height = width / 16 * 9;
const Header_Minimum_Height = 50;

class HomeContainer extends Component {
    constructor(props) {
        super(props)
        this.state = {
            startswiper: false,
            responsiveWidth: width,
            maxH: 300,
        }
        this.AnimatedHeaderValue = new Animated.Value(0);
    }

    componentWillMount() {
        setTimeout(() => { this.setState({ startswiper: true }) }, 500);
    }

    getHeight() {
        //16:9
        return this.state.responsiveWidth / 16 * 9
    }

    render() {
        const AnimateHeaderBackgroundColor = this.AnimatedHeaderValue.interpolate(
            {
                inputRange: [0, (Header_Maximum_Height - Header_Minimum_Height)],
                outputRange: ['#009688', '#00BCD4'],
                extrapolate: 'clamp'
            });

        const AnimateHeaderHeight = this.AnimatedHeaderValue.interpolate(
            {
                inputRange: [0, (Header_Maximum_Height - Header_Minimum_Height)],
                outputRange: [Header_Maximum_Height, Header_Minimum_Height],
                extrapolate: 'clamp'
            });

        return (
            <Container>
                <View onLayout={(event) => {
                    let { x, y, width, height } = event.nativeEvent.layout
                    this.setState({ responsiveWidth: width })
                }} />
                <ScrollView onScroll={Animated.event([{ nativeEvent: { contentOffset: { y: this.AnimatedHeaderValue } } }])} >
                    <Home height={Header_Maximum_Height + 5} navigation={this.props.navigation} />
                </ScrollView>

                <Animated.View style={[styles.HeaderStyle, { height: AnimateHeaderHeight, backgroundColor: AnimateHeaderBackgroundColor }]}>
                    {
                        this.state.startswiper ?
                            <SlideShowComponent height={Header_Maximum_Height} width={this.state.responsiveWidth} />
                            :
                            null
                    }
                </Animated.View>
            </Container>
        )
    }
}

const styles = StyleSheet.create({
    HeaderStyle:
        {
            justifyContent: 'center',
            alignItems: 'center',
            position: 'absolute',
            left: 0,
            right: 0,
            // top: (Platform.OS == 'ios') ? 20 : 0,
        },
})


export default HomeContainer