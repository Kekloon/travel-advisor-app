import firebase from 'react-native-firebase'
import { AsyncStorage } from 'react-native'
import { Toast } from 'native-base'
import _ from 'lodash'
import { getUserCreditRef } from '../../config/helper'
const firestore = firebase.firestore()

export const getAllComics = () => (dispatch) => {
    dispatch(_requestGetAllComics())

    try {
        const comicsRef = firestore.collection('comics')

        comicsRef.onSnapshot(async (col) => {
            const comicList = []

            await Promise.all(col._docs.map(async (doc) => {
                // comicList.push(doc.data())
                let author
                await doc.data().author.get().then((doc) => author = doc.data())
                const categories = []
                await Promise.all(doc.data().categories.map(async (doc) => {
                    doc.get().then((val) => {
                        categories.push(val.data())
                    })
                }))
                const comic = {
                    ...doc.data(),
                    author: author,
                    categories: categories
                }
                comicList.push(comic)
            }))

            dispatch(_successGetAllComics(comicList))
        })

    } catch (e) {
        dispatch(_failedGetAllComics(e))
    }
}

const _requestGetAllComics = () => ({
    type: 'REQUEST_GET_ALL_COMICS'
})
const _successGetAllComics = (payload) => ({
    type: 'SUCCESS_GET_ALL_COMICS',
    payload
})
const _failedGetAllComics = (err) => ({
    type: 'FAILED_GET_ALL_COMICS',
    err
})
export const setCategoryFilter = (category) => ({
    type: 'SET_CATEGORY_FILTER',
    category
})

export const setToViewComic = (comic, routeHistory) => (dispatch) => {
    dispatch(_requestSetToViewComic(routeHistory))
    comicRef = firestore.collection('comics').doc(comic.id)
    dispatch(checkSubsription(comic.id))

    comicRef.onSnapshot(async (doc) => {
        let comic = doc.data()
        if (comic.author && comic.categories) {
            const author = await _getAuthorData(comic.author)
            const categories = await _getCategoryListData(comic.categories)
            comic = {
                ...comic,
                author,
                categories
            }
        }
        dispatch(_setToViewComic(comic))
    })
}

const _requestSetToViewComic = (routeHistory) => ({
    type: 'REQUEST_SET_TO_VIEW_COMIC',
    routeHistory
})

const _setToViewComic = (comic, routeHistory) => ({
    type: 'SET_TO_VIEW_COMIC',
    comic,
    routeHistory
})


export const setToViewChapter = (chapter) => ({
    type: 'SET_TO_VIEW_CHAPTER',
    chapter
})

export const setTabBarVisible = (visible) => ({
    type: 'SET_TAB_BAR_VISIBLE',
    visible
})

// -----------------------------------------GET Comic's Chapters------------------------------------------ //
export const getChapter = (chapter) => (dispatch) => {
    dispatch(_requestGetChapter())
    const chapterRef = firestore.collection('chapters').doc(chapter.chapter.id)
    chapterRef.get().then((val) => {
        dispatch(_successGetChapter(val.data().image))
    }
    )
        .catch((e) => dispatch(_failedGetChapter(e)))
}

export const _requestGetChapter = () => ({
    type: 'REQUEST_GET_CHAPTER'
})
export const _successGetChapter = (chapterData) => ({
    type: 'SUCCESS_GET_CHAPTER',
    chapterData
})
export const _failedGetChapter = (err) => ({
    type: 'FAILED_GET_CHAPTER',
    err
})

// -----------------------------------------GET CATEGORIES------------------------------------------ //
export const getCategories = () => async (dispatch) => {
    try {
        dispatch(_requestGetCategories())
        firestore.collection('categories').onSnapshot((val) => {
            dispatch(_successGetCategories(val.docs))
        })
    } catch (e) {
        dispatch(_failedGetCategories(e))
    }
}

export const _requestGetCategories = () => ({
    type: 'REQUEST_GET_CATEGORIES'
})

export const _successGetCategories = (payload) => ({
    type: 'SUCCESS_GET_CATEGORIES',
    payload
})

export const _failedGetCategories = (err) => ({
    type: 'FAILED_GET_CATEGORIES',
    err
})

// -------------------------------------- Get Comics ------------------------------------- //
export const getFilteredComics = (filter) => (dispatch) => {
    const comicsRef = firestore.collection('comics')
    try {
        dispatch(_requestGetComic())
        comicsRef.onSnapshot(() => {
            const categoriesObject = 'categoriesObject.'
            const filterId = filter.data().id
            comicsRef.where(categoriesObject + filterId, '==', true).get().then(async (val) => {
                let comics = []
                const comicList = []
                val.forEach((doc) => {
                    comicList.push(doc.data())
                    doc.data().author.get().then((val) => {
                    })
                })

                await Promise.all(comicList.map(async (comic) => {
                    if (comic.author && comic.categories) {
                        const author = await _getAuthorData(comic.author)
                        const categories = await _getCategoryListData(comic.categories)
                        comics.push({
                            ...comic,
                            author,
                            categories
                        })
                    }
                }))

                dispatch(_successGetComic(comics))
            }).catch((e) => dispatch(_failedGetComic(e)))
        })
    } catch (e) {
        dispatch(_failedGetComic(e))
    }
}

const _getAuthorData = (author) => {
    return new Promise((resolve, reject) => {
        author.onSnapshot((author) => {
            resolve(author.data())
        }, (e) => {
            reject(e)
        })
    })
}

const _getCategoryData = (category) => {
    return new Promise((resolve, reject) => {
        category.onSnapshot((category) => {
            resolve(category.data())
        }, (e) => {
            reject(e)
        })
    })
}

const _getCategoryListData = (categories) => {
    return new Promise(async (resolve, reject) => {
        const categoriesData = []
        try {
            await Promise.all(categories.map(async (ref) => {
                const data = await _getCategoryData(ref)
                categoriesData.push(data)
            }))
            resolve(categoriesData)
        } catch (e) {
            reject(e)
        }
    })
}

export const _requestGetComic = () => ({
    type: 'REQUEST_GET_COMIC'
})
export const _successGetComic = (payload) => ({
    type: 'SUCCESS_GET_COMIC',
    payload
})
export const _failedGetComic = (err) => ({
    type: 'FAILED_GET_COMIC',
    err
})

// -------------------------------------- Subscription ------------------------------------- //

// --------------------------- GET Subscribed Comics -----------------------------
export const getSubsribedComics = () => async (dispatch) => {
    dispatch(_requesetGetSubscribedComics())
    const userId = await AsyncStorage.getItem('uid')
    userRef = firestore.collection('users').doc(userId)
    comicsRef = firestore.collection('comics')
    subscribeRef = firestore.collection('subscribe')

    try {
        userRef.onSnapshot(async (doc) => {
            const subscriptionId = doc.data().subscription
            subscribeRef.doc(subscriptionId).onSnapshot(async (val) => {
                let comics = []
                if (val.data().comics) {
                    const subscribedComics = val.data().comics
                    await Promise.all(subscribedComics.map(async (comicId) => {
                        await comicsRef.doc(comicId).get().then(async (val) => {
                            let comic = val.data()
                            let categories = []
                            let author = ''
                            await val.data().categories.map(async (doc) => {
                                await doc.get().then((val) => {
                                    categories.push(val.data())
                                })
                            })

                            await val.data().author.get().then((val) => {
                                author = val.data()
                            })
                            comics.push({
                                ...comic,
                                categories,
                                author
                            })
                        })
                    })).then(() => {
                        dispatch(_successGetSubsribedComics(comics))
                    })
                } else {
                    dispatch(_successGetSubsribedComics(comics))
                }
            })
        })
    } catch (e) {
        dispatch(_failedGetComic(e))
    }
}

const _requesetGetSubscribedComics = () => ({
    type: 'REQUEST_GET_SUBSCRIBED_COMICS'
})

const _successGetSubsribedComics = (payload) => ({
    type: 'SUCCESS_GET_SUBSCRIBED_COMICS',
    payload
})

const _getSubscriptionId = async () => {
    const userId = await AsyncStorage.getItem('uid')
    const userRef = firestore.collection('users').doc(userId)
    return new Promise((resolve, reject) => {
        userRef.get().then((doc) => {
            resolve(doc.data().subscription)
        }).catch((e) => {
            reject(e)
        })
    })
}

export const checkSubsription = (comicId) => async (dispatch) => {
    dispatch(_checkIsSubscribed())
    subscriptionId = await _getSubscriptionId()
    subscribeRef = firestore.collection('subscribe').doc(subscriptionId)
    try {
        subscribeRef.onSnapshot(async (doc) => {
            let result = false
            if (doc.data().comics) {
                await Promise.all(doc.data().comics.map((val) => {
                    if (comicId === val) {
                        result = true
                    }
                }))
            }
            dispatch(_successCheckIsSubscribed(result))
        })
    } catch (e) {
        dispatch(_failedCheckIsSubscribed(e))
    }
}
export const _checkIsSubscribed = () => ({
    type: 'CHECK_IS_SUBSCRIBED'
})
export const _successCheckIsSubscribed = (payload) => ({
    type: 'SUCCESS_CHECK_IS_SUBSCRIBED',
    payload
})
export const _failedCheckIsSubscribed = (err) => ({
    type: 'FAILED_CHECK_IS_SUBSCRIBED',
    err
})


export const addToSubscription = (comicId) => async (dispatch) => {
    dispatch(_requestSetSubscribed())
    subscriptionId = await _getSubscriptionId()
    subscribeRef = firestore.collection('subscribe').doc(subscriptionId)
    try {
        let comics = []
        await subscribeRef.get().then((doc) => {
            if (doc.data().comics) {
                comics = doc.data().comics
            }
        })
        comics.push(comicId)
        subscribeRef.set({
            comics: comics
        }, { merge: true })
        Toast.show({ text: 'Successfully added to your Bookshelf', type: 'success' })
        dispatch(_successSetSubscribed())

    } catch (e) {
        dispatch(_failedSetSubscribed(e))
    }
}
const _requestSetSubscribed = () => ({
    type: 'REQUEST_SET_SUBSCRIBED'
})
const _successSetSubscribed = () => ({
    type: 'REQUEST_SET_SUBSCRIBED'
})
const _failedSetSubscribed = (err) => ({
    type: 'REQUEST_SET_SUBSCRIBED',
    err
})

export const removeFromSubscription = (comicId) => async (dispatch) => {
    dispatch(_requestRemoveFromSubscription())
    subscriptionId = await _getSubscriptionId()
    subscribeRef = firestore.collection('subscribe').doc(subscriptionId)
    try {
        let comics = []
        await subscribeRef.get().then((doc) => {
            comics = doc.data().comics
        })

        _.pull(comics, comicId)
        subscribeRef.set({
            comics: comics
        }, { merge: true })

        Toast.show({ text: 'Success remove from your bookshelf!', type: 'success' })
        dispatch(_successRemoveFromSubscription())
    } catch (e) {
        dispatch(_failedRemoveFromSubscription())
    }
}

const _requestRemoveFromSubscription = () => ({
    type: 'REQUEST_REMOVE_FROM_SUBSCRIPTION'
})
const _successRemoveFromSubscription = () => ({
    type: 'SUCCESS_REMOVE_FROM_SUBSCRIPTION'
})
const _failedRemoveFromSubscription = () => ({
    type: 'FAILED_REMOVE_FROM_SUBSCRIPTION'
})

export const getComments = (comicId) => (dispatch) => {
    comicRef = firestore.collection('comics').doc(comicId)

    try {
        dispatch(_requestGetComments())
        comicRef.onSnapshot(async (val) => {
            let comments = []
            if (val.data().comments) {
                await Promise.all(val.data().comments.map(async (ref) => {
                    await ref.get().then(async (doc) => {
                        let name = ''
                        let user
                        await doc.data().sender.get().then((val) => {
                            name = val.data().firstName + ' ' + val.data().lastName
                            user = val.data()
                        })
                        comments.push({
                            ...doc.data(),
                            username: name,
                            user: user,
                            id: doc.id
                        })
                    })
                }))
            }
            dispatch(_successGetComments(comments))
        })
    } catch (err) {
        dispatch(_failedGetComments(err))
    }
}

const _requestGetComments = () => ({
    type: 'REQUEST_GET_COMMENTS'
})
const _successGetComments = (payload) => ({
    type: 'SUCCESS_GET_COMMENTS',
    payload
})
const _failedGetComments = (err) => ({
    type: 'FAILED_GET_COMMENTS',
    err
})

export const sendComment = (comicId, msg) => async (dispatch) => {
    dispatch(_requestSendComment())

    const date = new Date()
    comicRef = firestore.collection('comics').doc(comicId)
    const userId = await AsyncStorage.getItem('uid')
    usersRef = firestore.collection('users')
    commentsRef = firestore.collection('comments')

    try {
        let comments = []
        await comicRef.get().then((val) => {
            if (val.data().comments) {
                comments = val.data().comments
            }
        })

        let commentId = null
        await commentsRef.add({}).then((doc) => {
            commentId = doc.id
            commentsRef.doc(doc.id).set({
                sender: usersRef.doc(userId),
                message: msg,
                date: date
            })
        })

        comments.push(
            commentsRef.doc(commentId)
        )

        comicRef.set({
            comments: comments
        }, { merge: true })
            .then(() => dispatch(_successSendComment()))

    } catch (e) {
        dispatch(_failedSendComment(e))
    }
}

const _requestSendComment = () => ({
    type: 'REQUEST_SEND_COMMENT'
})
const _successSendComment = () => ({
    type: 'SUCCESS_SEND_COMMENT'
})
const _failedSendComment = (err) => ({
    type: 'FAILED_SEND_COMMENT',
    err
})

export const payChapter = (chapter, allChapter) => async (dispatch) => {
    try {
        dispatch(_requestPayChapter())
        const creditRef = await getUserCreditRef()
        creditRef.get().then((doc) => {
            let paymentHistory = []
            if (doc.data().paymentHistory) {
                paymentHistory = doc.data().paymentHistory
            }
            if (allChapter) {
                allChapter.map((chapter) => {
                    let order = {
                        date: new Date(),
                        chapter: chapter.chapter,
                        price: chapter.price
                    }
                    paymentHistory.push(order)
                })

            } else {
                let order = {
                    date: new Date(),
                    chapter: chapter.chapter,
                    price: chapter.price
                }
                paymentHistory.push(order)
            }

            creditRef.set({
                paymentHistory: paymentHistory
            }, { merge: true })
                .then(() => dispatch(_successPayChapter()))
                .then(() => Toast.show({ text: 'Successfully unlock!', type: 'success' }))
        })
    } catch (e) {
        dispatch(_failedPayChapter(e))
    }
}

const _requestPayChapter = () => ({
    type: 'REQUEST_PAY_CHAPTER'
})
const _successPayChapter = () => ({
    type: 'SUCCESS_PAY_CHAPTER'
})
const _failedPayChapter = (err) => ({
    type: 'FAILED_PAY_CHAPTER'
})