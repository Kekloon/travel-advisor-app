import React, { Component } from 'react'
import { View, Text, Spinner } from 'native-base'

class LoadingComponent extends Component {

    render() {
        return (
            // <View style={{
            //     flex: 1,
            //     flexDirection: 'column',
            //     justifyContent: 'center',
            //     alignItems: 'center',
            //     backgroundColor: 'transparent',
            //     display: this.props.visible ? null : 'none',
            //     position: 'absolute'
            // }}>
            <View
                style={{ position: 'absolute', top: 0, left: 0, right: 0, bottom: 0, justifyContent: 'center', alignItems: 'center', display: this.props.visible ? null : 'none', }}
            >
                <Spinner />
            </View>
        )
    }
}

export default LoadingComponent