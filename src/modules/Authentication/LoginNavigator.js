import React, { Component } from 'react'
import { StackNavigator } from 'react-navigation'
import { connect } from 'react-redux'

//Path
import Login from './container/LoginContainer'
import PhoneLogin from './container/PhoneLoginContainer'
import PhoneValidation from './container/PhoneValidationContainer'

import UserNavigatorComponent from '../User/UserNavigator'
import { checkIsUserExist } from '../User/UserAction'

export const LoginNavigator = StackNavigator({
    Login: { screen: Login },
    PhoneLogin: { screen: PhoneLogin },
    PhoneValidation: { screen: PhoneValidation },
}, {
        initialRouteName: 'Login',
        headerMode: "none"
    })

class AutoLogin extends Component {
    constructor(props) {
        super(props)
        this.state = {
            isAuthenticated: null
        }

        this.goToMainPage = this.goToMainPage.bind(this)
    }

    componentWillMount() {
        this.props.checkIsUserExist()
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            isAuthenticated: nextProps.isAuthenticated
        })
    }

    goToMainPage() {
        //handle navigation
        const navigateAction = NavigationActions.navigate({
            type: 'Navigation/NAVIGATE',
            routeName: 'Tab',
            params: {},
        });
        this.props.appNavRef.dispatch(navigateAction)
    };


    render() {
        return (
            this.props.isAuthenticated ?
                <UserNavigatorComponent />
                :
                <LoginNavigator />

        )
    }
}


const mapStateToProps = (state) => ({
    isAuthenticated: state.authentication.isAuthenticated,
    isLoading: state.user.isLoading,
    appNavRef: state.authentication.appNavRef
})

const mapDispatchToProps = (dispatch) => ({
    checkIsUserExist: () => {
        dispatch(checkIsUserExist())
    }
})

export default connect(mapStateToProps, mapDispatchToProps)(AutoLogin)


