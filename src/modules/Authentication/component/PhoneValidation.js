import React, { Component } from 'react'
import { StyleSheet, Alert } from 'react-native'
import { View, Text, Button, Item, Input, Toast } from 'native-base'
import _ from 'lodash'

class PhoneValidation extends Component {
    constructor(props) {
        super(props)
        this.state = {
            code: null
        }
        this.onChanged = this.onChanged.bind(this)
        this.onBtnClicked = this.onBtnClicked.bind(this)
    }

    onChanged(text) {
        this.setState({
            code: text.replace(/[^0-9]/g, '')
        })
    }

    onBtnClicked() {
        if(_.isEmpty(this.state.code)){
            Toast.show({ text: 'Please key in verification code!', type:'danger'})
        }else{
            this.props.onConfirmClicked(this.state.code)
        }
    }

    render() {
        return (
            <View style={{
                flex: 1,
                justifyContent: 'center',
                alignItems: 'center',
                margin: 30
            }}>
                <Text> Verification </Text>
                <Item
                style={{
                    marginTop: 15,
                    marginBottom: 20
                }}
                 regular>
                    <Input
                        placeholder="Verification Code"
                        onChangeText={(value) => this.onChanged(value)}
                        value={this.state.code}
                        keyboardType="numeric"
                        maxLength={9}
                    />
                </Item>
                <Button block onPress={() => this.onBtnClicked()}>
                    <Text>Confirm</Text>
                </Button>
            </View>
        )
    }
}

export default PhoneValidation