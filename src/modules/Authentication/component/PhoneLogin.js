
import React, { Component } from "react";
import { Item, Input, Toast, Form, Container, Content, Header, Body, Title, Button, Text, View, Icon, Footer, ListItem } from "native-base";
import { Alert, StyleSheet, Keyboard } from "react-native";
import Modal from 'react-native-modal'
import { Field, reduxForm } from "redux-form";
import _ from 'lodash'

class PhoneLogin extends Component {
	constructor(props) {
		super(props)
		this.state = {
			phoneNumber: '',
			invalidInput: false,
			modalVisible: false,
		}
		this.onChanged = this.onChanged.bind(this)
		this.onBtnClicked = this.onBtnClicked.bind(this)
		this.renderModal = this.renderModal.bind(this)
	}

	componentWillReceiveProps(nextProps) {
		if (nextProps.isCodeSending) {
			this.setState({
				modalVisible: true
			})
		}
		if (nextProps.isCodeSended) {
			this.setState({
				modalVisible: false
			})
		}
	}

	onChanged(text) {
		this.setState({
			phoneNumber: text.replace(/[^0-9]/g, ''),
			invalidInput: false
		})
	}


	onBlur() {
		if (_.isEmpty(this.state.phoneNumber)) {
			this.setState({ invalidInput: true })
			return false
		}
		if (this.state.phoneNumber.length < 9) {
			this.setState({ invalidInput: true })
			return false
		}
		return true
	}

	onBtnClicked() {
		if (this.onBlur()) {
			Keyboard.dismiss
			this.props.sendCode('+60' + this.state.phoneNumber)
		}
	}

	renderModal() {
		return (
			<View>
				<Modal
					animationIn='slideInDown'
					animationOut='slideOutUp'
					animationOutTiming={500}
					useNativeDriver={true}
					isVisible={this.state.modalVisible}
					backdropOpacity={0.3}
				>
					<View style={{ backgroundColor: 'white', justifyContent: 'center', alignItems: 'center', borderRadius: 5 }}>
						<Text style={{ padding: 20 }}>Code is sending...</Text>
					</View>
				</Modal>
			</View>
		)
	}

	render() {
		const { invalidInput } = this.state
		const error1 = Item.error
		const touched1 = Item.touched
		return (
			<View style={{
				flex: 1,
				justifyContent: 'center',
				alignItems: 'center',
				margin: 25
			}}>
				{this.renderModal()}
				<Text style={styles.title}>Enter Your Phone Number</Text>
				<View style={styles.flowLayout}>
					<Item rounded style={styles.phoneCode}>
						<Text style={styles.textCenter}> +60 </Text>
					</Item>
					<Item error={invalidInput} rounded style={styles.phoneInput}>
						<Input
							placeholder="Phone number"
							onChangeText={(value) => this.onChanged(value)}
							value={this.state.phoneNumber}
							keyboardType="numeric"
							maxLength={10}
							onBlur={() => this.onBlur()}
						/>
						{!invalidInput ? null : <Icon name='close-circle' />}
					</Item>
				</View>
				<Text note>Phone number must more than 8</Text>
				<Button block style={{ marginTop: 20 }} onPress={() => this.onBtnClicked()}>
					<Text>Sign in</Text>
				</Button>
			</View>
		);
	}
}

const styles = StyleSheet.create({
	phoneCode: {
		width: 60
	},
	title: {
		textAlign: 'center',
		marginBottom: 15
	},
	phoneInput: {
		flex: 1.0,
	},
	flowLayout: {
		flexDirection: "row"
	}
})
export default PhoneLogin;
