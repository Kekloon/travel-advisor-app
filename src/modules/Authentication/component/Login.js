import React, { Component } from 'react'
import { Container, Button, Text, Icon } from 'native-base'
import { createUserNavigator } from '../../User/UserNavigator'

class Login extends Component {
    constructor(props) {
        super(props)
    }

    render() {
        const { navigation } = this.props
        const { isUserExist, isAuthenticated } = this.props
        const UserPage = createUserNavigator(isUserExist)
        return (
            isAuthenticated ?
                <UserPage />
                :
                <Container style={{ flex: 1, flexDirection: 'column', justifyContent: 'center', alignItems: 'center' }}>
                    <Button block iconLeft style={{ margin: 10 }} onPress={() => navigation.navigate('PhoneLogin')}>
                        <Icon name='phone-portrait' />
                        <Text> Phone number sign In </Text>
                    </Button>
                </Container>
        )
    }
}

export default Login