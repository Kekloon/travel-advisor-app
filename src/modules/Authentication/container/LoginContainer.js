import React from 'react'
import { connect } from 'react-redux'

import Login from '../component/Login'

const mapStateToProps = (state) => ({
    isAuthenticated: state.authentication.isAuthenticated,
    isUserExist: state.user.isUserExist
})

const mapDispatchToProps = (dispatch) => ({

})

const LoginContainer = connect(mapStateToProps, null)(Login)
export default LoginContainer

