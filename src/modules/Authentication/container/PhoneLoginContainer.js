import React, { Component } from "react";
import { connect } from "react-redux";
import PhoneLogin from "../component/PhoneLogin";
import { sendCode } from "../AuthenticationActions";

class PhoneLoginContainer extends Component {
    constructor(props) {
        super(props)
    }

    render() {
        return (
            <PhoneLogin
                sendCode={this.props.sendCode}
                isCodeSending={this.props.isCodeSending}
                isCodeSended={this.props.isCodeSended}
            />
        )
    }
}

const mapStateToProps = (state) => ({
    errorMessage: state.authentication.errorMessage,
    isCodeSending: state.authentication.isCodeSending,
    isCodeSended: state.authentication.isCodeSended,
    isAuthenticated: state.authentication.isAuthenticated
})

const mapDispatchToProps = (dispatch, props) => ({
    sendCode: (phoneNum) => {
        dispatch(sendCode(phoneNum, props.navigation))
    }
});

export default connect(mapStateToProps, mapDispatchToProps)(PhoneLoginContainer)