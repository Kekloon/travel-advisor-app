import React, { Component } from 'react'
import { connect } from 'react-redux'

import PhoneValidation from '../component/PhoneValidation'
import { confirmCode } from '../AuthenticationActions'

class PhoneValidationContainer extends Component {
    constructor(props) {
        super(props)
        this.onConfirmClicked = this.onConfirmClicked.bind(this)
    }

    onConfirmClicked(code) {
        this.props.confirmCode(this.props.confirmResult, code)
    }

    render() {
        return (
            <PhoneValidation
                onConfirmClicked={this.onConfirmClicked}
            />
        )
    }
}

const mapStateToProps = (state) => ({
    isCodeConfirming: state.authentication.isAuthenticating,
    isCodeConfirmed: state.authentication.isAuthenticated,
    isLoading: state.authentication.isLoading,
    confirmResult: state.authentication.confirmResult
})

const mapDispatchToProps = (dispatch) => ({
    confirmCode: (confirmResult, code) => {
        dispatch(confirmCode(confirmResult, code))
    }
})

export default connect(mapStateToProps, mapDispatchToProps)(PhoneValidationContainer)