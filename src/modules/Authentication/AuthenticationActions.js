import firebase from 'react-native-firebase'
import { AsyncStorage } from 'react-native'
import { Alert } from 'react-native'
import axios from 'axios'

// Send Code to phone
export const sendCode = (phoneNumber, navigation) => async (dispatch) => {
  dispatch(_requestSendCode())

  // Alert.alert('Alert', 'Sending code to ' + phoneNumber + ' ...', [], { cancelable: false })

  firebase.auth().signInWithPhoneNumber(phoneNumber)
    .then((confirmResult) => dispatch(_successSendCode(confirmResult)))
    // .then(() => Alert.alert('Alert', 'Code has been sent', [{ text: 'OK', onPress: () => navigation.navigate('PhoneValidation') }], { cancelable: false }))
    .then(() => navigation.navigate('PhoneValidation'))
    .catch((err) => {
      Alert.alert('Alert', 'Sign in with phone number error!', [{ text: 'OK' }], { cancelable: true })
      dispatch(_failedSendCode(err))
    })
}

export const _requestSendCode = () => ({
  type: 'REQUEST_SEND_CODE'
})

export const _successSendCode = (payload) => ({
  type: 'SUCCESS_SEND_CODE',
  payload
})

export const _failedSendCode = (err) => ({
  type: 'FAILED_SEND_CODE',
  err
})


// Confirm Code
export const confirmCode = (confirmResult, code) => async (dispatch) => {
  dispatch(_requestConfimCode())
  Alert.alert('Alert', 'Code verifying...', [], { cancelable: true })

  if (confirmResult && code.length) {
    confirmResult.confirm(code)
      .then((user) => dispatch(_successConfirmCode(user)))
      .then(() => Alert.alert('Alert', 'Code has been verified!', [{ text: 'OK', onPress: () => navigation.navigate('UserNavigate') }], { cancelable: false }))
      .catch((error) => {
        Alert.alert('Alert', 'Code error!', [{ text: 'OK' }], { cancelable: true })
        dispatch(_failedConfirmCode(error))
      });
  } else {
    console.log('3')
    dispatch(_failedConfirmCode())
    Alert.alert('Alert', 'Code error!', [{ text: 'OK' }], { cancelable: true })
  }
}

export const _requestConfimCode = () => ({
  type: 'REQUEST_CONFIRM_CODE'
})

export const _successConfirmCode = (payload) => ({
  type: 'SUCCESS_CONFIRM_CODE',
  payload
})

export const _failedConfirmCode = (err) => ({
  type: 'FAILED_CONFIRM_CODE',
  err
})


// AUTO Authentication
export const autoAuthentication = () => async (dispatch) => {
  dispatch(_requestAutoAuthentication())

  firebase.auth().onAuthStateChanged(async (payload) => {
    const userData = payload && payload._user
    if (payload) {
      await AsyncStorage.setItem('uid', userData.uid)
      await AsyncStorage.setItem('phoneNum', userData.phoneNumber)
      dispatch(_successAutoAuthentication(userData))
    } else {
      dispatch(_failedAutoAuthentication())
    }
  })
}

export const _requestAutoAuthentication = () => ({
  type: 'REQUEST_AUTO_AUTHENTICATION'
})

export const _successAutoAuthentication = (payload) => ({
  type: 'SUCCESS_AUTO_AUTHENTICATION',
  payload
})

export const _failedAutoAuthentication = () => ({
  type: 'FAILED_AUTO_AUTHENTICATION'
})


// Sign Out
export const signOut = () => (dispatch) => {
  dispatch(_signOut())
  firebase.auth().signOut()
    .then(() => console.log('Sign out successful'))
    .catch((e) => console.log('Sign out failed: ', e))
}

export const _signOut = () => ({
  type: 'SIGN_OUT'
})