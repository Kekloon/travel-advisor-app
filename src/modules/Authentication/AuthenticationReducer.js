const initialState = {
  isCodeSending: false,
  isCodeSended: false,
  isCodeConfirming: false,
  isCodeConfirmed: false,
  isAuthenticating: false,
  isAuthenticated: false,
  isLoading: false,
  autoAuthenticate: {
    isLoading: false
  },
  confirmResult: null,
  errorMessage: null,
  emailSignIn: {
    isSigningIn: false,
    isSuccess: false,
    errorMessage: null,
  }
}

export default function (state = initialState, action) {
  switch (action.type) {
    // Send Code    
    case 'REQUEST_SEND_CODE':
      return Object.assign({}, state, {
        isCodeSending: true,
        isCodeSended: false
      })
    case 'SUCCESS_SEND_CODE':
      return Object.assign({}, state, {
        isCodeSending: false,
        isCodeSended: true,
        confirmResult: action.payload
      })
    case 'FAILED_SEND_CODE':
      return Object.assign({}, state, {
        errorMessage: action.err,
        isCodeSending: false,
        isCodeSended: false
      })

    // Confirm Code      
    case 'REQUEST_CONFIRM_CODE':
      return Object.assign({}, state, {
        isCodeConfirming: true,
        isLoading: true
      })
    case 'SUCCESS_CONFIRM_CODE':
      return Object.assign({}, state, {
        isCodeConfirming: false,
        isCodeConfirmed: true,
        isAuthenticated: true,
        isLoading: false
      })
    case 'FAILED_CONFIRM_CODE':
      return Object.assign({}, state, {
        isCodeConfirming: false,
        isCodeConfirmed: false,
        isLoading: false,
        errorMessage: action.err
      })

    // Auto Authentication
    case 'REQUEST_AUTO_AUTHENTICATION':
      return Object.assign({}, state, {
        isAuthenticating: true,
        autoAuthenticate: {
          isLoading: true
        }
      })
    case 'SUCCESS_AUTO_AUTHENTICATION':
      return Object.assign({}, state, {
        isAuthenticating: false,
        isAuthenticated: true,
        autoAuthenticate: {
          isLoading: false
        },
        user: action.payload
      })
    case 'FAILED_AUTO_AUTHENTICATION':
      return Object.assign({}, state, {
        isAuthenticating: false,
        isAuthenticated: false,
        autoAuthenticate: {
          isLoading: false
        },
        errorMessage: action.err
      })
    case 'REQUEST_EMAIL_SIGN_IN':
      return Object.assign({}, state, {
        emailSignIn: {
          isSigningIn: true,
          isSuccess: false,
        }
      })
    case 'SUCCESS_EMAIL_SIGN_IN':
      return Object.assign({}, state, {
        emailSignIn: {
          isSigningIn: false,
          isSuccess: true
        }
      })
    case 'FAILED_EMAIL_SIGN_IN':
      return Object.assign({}, state, {
        emailSignIn: {
          isSigningIn: false,
          isSuccess: false,
          errorMessage: action.err
        }
      })

    // Sign Out
    case 'SIGN_OUT':
      return state;

    default:
      return state;
  }
}
