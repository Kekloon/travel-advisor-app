import firebase from 'react-native-firebase'
import { AsyncStorage } from 'react-native'
import _ from 'lodash'

const firestore = firebase.firestore()

export const getHotelList = () => async (dispatch) => {
    dispatch(_requestGetHotelList())
    try {
        let hotelPendingList = []
        let hotelList = []
        let list = []
        await firestore.collection('hotel').get().then(async (val) => {
            await val.forEach(async (doc) => {
                hotelPendingList.push(doc.data())
            })
        })
        await Promise.all(hotelPendingList.map(async (val) => {
            countryRef = firestore.collection('countryList').doc(val.hotelCountry)
            await countryRef.get().then(async (countryVal) => {
                list = {
                    ...val,
                    hotelCountryName: countryVal.data().name
                }
                hotelList.push(list)
            })
        }))
        dispatch(_successGetHotelList(hotelList))

    }

    catch (e) {
        dispatch(_failedGetHotelList(e))
    }
}


export const _requestGetHotelList = () => ({
    type: 'REQUEST_GET_HOTEL_LIST'
})

export const _successGetHotelList = (payload) => ({
    type: 'SUCCESS_GET_HOTEL_LIST',
    payload,
})

export const _failedGetHotelList = (err) => ({
    type: 'FAILED_GET_HOTEL_LIST',
    err,
})

export const getHotelComment = (hotelData) => async (dispatch) => {
    dispatch(_requestGetHotelComment())
    try {
        let hotel = null,
            hotelRef = firestore.collection('hotel').doc(hotelData.hotelId)
        await hotelRef.get().then(async (val) => {
            hotel = val.data()
        })
        const hotelCommentList = []
        await Promise.all(hotel.hotelComment.map(async (val) => {
            let commentData = val
            userRef = firestore.collection('user').doc(commentData.user)
            await userRef.get().then((userData) => {
                let commentUserData = {
                    ...commentData,
                    userCommentData: userData.data()
                }
                hotelCommentList.push(commentUserData)
            })
        }))
        dispatch(_successGetHotelComment(hotelCommentList))
    }
    catch (e) {
        dispatch(_failedGetHotelComment(e))
    }
}

const _requestGetHotelComment = () => ({
    type: 'REQUEST_GET_HOTEL_COMMENT'
})

const _successGetHotelComment = (payload) => ({
    type: 'SUCCESS_GET_HOTEL_COMMENT',
    payload
})

const _failedGetHotelComment = (err) => ({
    type: 'FAILED_GET_HOTEL_COMMENT',
    err
})


export const getHotelRate = (hotelData) => async (dispatch) => {
    dispatch(_requestGetHotelRate())
    try {
        hotelRef = firestore.collection('hotel').doc(hotelData.hotelId)
        await hotelRef.onSnapshot(async (doc) => {
            const hotelRateList = doc.data().hotelRate
            dispatch(_successGetHotelRate(hotelRateList))
        })
    }
    catch (e) {
        dispatch(_failedGetHotelRate(e))
    }
}


const _requestGetHotelRate = () => ({
    type: 'REQUEST_GET_HOTEL_RATE'
})

const _successGetHotelRate = (payload) => ({
    type: 'SUCCESS_GET_HOTEL_RATE',
    payload
})

const _failedGetHotelRate = (err) => ({
    type: 'FAILED_GET_HOTEL_RATE',
    err
})


export const updateHotelReviewRate = (userRate, boolean, userId, hotelData) => async (dispatch) => {
    const hotelRef = firestore.collection('hotel').doc(hotelData.hotelId)
    hotelRef.get().then(async (hotel) => {
        const hotelRateList = hotel.data().hotelRate
        if (boolean) {
            const index = _.findIndex(hotelRateList, { user: userId })
            hotelRateList[index].rate = userRate
            hotelRef.set({
                hotelRate: hotelRateList
            }, { merge: true })
        }
        else {
            let newReview = {
                user: userId,
                rate: userRate,
            }
            hotelRateList.push(newReview)

            hotelRef.set({
                hotelRate: hotelRateList
            }, { merge: true })
        }
    })
}

export const bookHotel = (hotelData, hotelPackageData, totalAmount, checkInDate, checkOutDate, amount) => async (dispatch) => {
    const userId = await AsyncStorage.getItem('uid')
    hotelTransactionRef = firestore.collection('hotelTransaction')
    let hotelTransactionId = {}
    await hotelTransactionRef.add({}).then((doc) => {
        hotelTransactionId = doc.id
        hotelTransactionRef.doc(doc.id).set({
            hotelTransactionId: doc.id,
            hotelTransactionUser: userId,
            hotelTransactionHotelId: hotelData.hotelId,
            hotelTransactionPackage: hotelPackageData,
            hotelTransactionTotalAmount: totalAmount,
            hotelTransactionHoteCheckInDate: checkInDate,
            hotelTransactionHoteCheckOutDate: checkOutDate,
            hotelTransactionRoomAmount: amount,
            hotelTransactionDate: new Date(),
        })
    })

    userRef = firestore.collection('user').doc(userId)
    await userRef.get().then((user) => {
        const userHistoryOfBookedHotel = user.data().userHistoryOfBookedHotel
        userHistoryOfBookedHotel.push(hotelTransactionId)
        userRef.set({
            userHistoryOfBookedHotel: userHistoryOfBookedHotel
        }, { merge: true })
    })
}

export const getFullHotelComment = (hotelData) => async (dispatch) => {
    dispatch(_requestGetFullHotelComment())
    try {
        let hotel = null,
            hotelRef = firestore.collection('hotel').doc(hotelData.hotelId)
        await hotelRef.onSnapshot(async (val) => {
            hotel = val.data()
            const hotelCommentList = []
            await Promise.all(hotel.hotelComment.map(async (val) => {
                let commentData = val
                userRef = firestore.collection('user').doc(commentData.user)
                await userRef.get().then((userData) => {
                    let commentUserData = {
                        ...commentData,
                        userCommentData: userData.data()
                    }
                    hotelCommentList.push(commentUserData)
                })
            }))
            dispatch(_successGetFullHotelComment(hotelCommentList))

        })

    }
    catch (e) {
        dispatch(_failedGetFullHotelComment(e))
    }
}

const _requestGetFullHotelComment = () => ({
    type: 'REQUEST_GET_FULL_HOTEL_COMMENT'
})

const _successGetFullHotelComment = (payload) => ({
    type: 'SUCCESS_GET_FULL_HOTEL_COMMNET',
    payload
})

const _failedGetFullHotelComment = (err) => ({
    type: 'FAILED_GET_FULL_Hotel_COMMNET',
    err
})

export const sendHotelComment = (comment) => async (dispatch) => {
    try {
        userId = await AsyncStorage.getItem('uid')
        hotelRef = firestore.collection('hotel').doc(comment.hotelData.hotelId)
        userRef = firestore.collection('user').doc(userId)
        let commentsList
        await hotelRef.get().then((doc) => {
            commentsList = doc.data().hotelComment
        })
        const newComment = {
            commentDate: new Date(),
            comment: comment.message,
            user: userId
        }

        commentsList.push(newComment)
        hotelRef.set({
            hotelComment: commentsList
        }, { merge: true })

    }
    catch(e){

    }
}

export const saveHotelList=(boolean,hotelId)=>async(dispatch)=>{
    const userId= await AsyncStorage.getItem('uid')
    userRef=firestore.collection('user').doc(userId)
    userRef.get().then((user)=>{
        const userSaveHotelListData =user.data().userSaveHotelList
        if(boolean){
            userSaveHotelListData.push({
                hotel:hotelId
            })
            userRef.set({
                userSaveHotelList:userSaveHotelListData
            },{merge:true})
        }
        else{
            userRef.set({
                userSaveHotelList:_.pullAllBy(userSaveHotelListData,[{'hotel':hotelId}],'hotel')
            },{merge:true})
        }
    })
}