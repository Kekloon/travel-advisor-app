const initialState = {
    getHotelList: {
        hotelList: null,
        errorMessage: null,
        isLoading: false,
    },
    getHotelComment:{
        hotelCommentList:null,
        errorMessage:null,
        isLoading:false,
    },
    getHotelRate:{
        hotelRateList:null,
        isLoading:false,
        errorMessage:null,
    },
    getFullHotelComment:{
        hotelFullCommentList:null,
        isLoading:false,
        errorMessage:false,
    }
}

export default function (state = initialState, action) {
    switch (action.type) {
        case 'REQUEST_GET_HOTEL_LIST':
        return Object.assign({},state,{
            getHotelList:{
                isLoading:true,
            }
        })
        case 'SUCCESS_GET_HOTEL_LIST':
        return Object.assign({},state,{
            getHotelList:{
                isLoading:false,
                hotelList:action.payload
            }
        })
        case 'FAILED_GET_HOTEL_LIST':
        return Object.assign({},state,{
            getHotelList: {
                errorMessage: action.err,
                isLoading: false,
            }
        })
        case 'REQUEST_GET_HOTEL_COMMENT':
        return Object.assign({},state,{
            getHotelComment:{
                isLoading:true,
            },
        })
        case 'SUCCESS_GET_HOTEL_COMMENT':
        return Object.assign({},state,{
            getHotelComment:{
                hotelCommentList:action.payload,
                isLoading:false,
            },
        })
        case 'FAILED_GET_HOTEL_COMMENT':
        return Object.assign({},state,{
            getHotelComment:{
                errorMessage:action.err,
                isLoading:false,
            },
        })
        case 'REQUEST_GET_HOTEL_RATE':
        return Object.assign({},state,{
            getHotelRate:{
                isLoading:true,
            }
        })
        case 'SUCCESS_GET_HOTEL_RATE':
        return Object.assign({},state,{
            getHotelRate:{
                hotelRateList:action.payload,
                isLoading:false,
            }
        })
        case 'FAILED_GET_HOTEL_RATE':
        return Object.assign({},state,{
            getHotelRate:{
                isLoading:false,
                errorMessage:action.err,
            }
        })
        case 'REQUEST_GET_FULL_HOTEL_COMMENT':
        return Object.assign({},state,{
            getFullHotelComment:{
                isLoading:true,
            }
        })
        case 'SUCCESS_GET_FULL_HOTEL_COMMNET':
        return Object.assign({},state,{
            getFullHotelComment:{
                hotelFullCommentList:action.payload,
                isLoading:false,
            }
        })
        case 'FAILED_GET_FULL_Hotel_COMMNET':
        return Object.assign({},state,{
            getFullHotelComment:{
                isLoading:false,
                errorMessage:action.err,
            }
        })

        default:
            return state
    }
}