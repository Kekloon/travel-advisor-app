import { StackNavigator } from 'react-navigation'
import HotelList from './container/HotelListContainer'
import HotelDetails from './container/HotelDetailsContainer'

export const HotelNavigator = StackNavigator({
    HotelList: { screen: HotelList },
    HotelDetails: { screen: HotelDetails }
},{
    initialRouteName:'HotelList',
    headerMode:'none'
}) 