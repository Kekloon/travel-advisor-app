import HotelCommentListComponent from '../component/HotelCommentListComponent'
import { connect } from 'react-redux'
import { getFullHotelComment, sendHotelComment } from '../HotelAction'

const mapStateToProps = (State) => ({
    hotelFullCommentList: State.hotelReducer.getFullHotelComment.hotelFullCommentList,
    isLoading: State.hotelReducer.getFullHotelComment.isLoading
})

const mapDispatchToProps = (dispatch) => ({
    getFullHotelComment: (hotelData) => {
        dispatch(getFullHotelComment(hotelData))
    },
    sendHotelComment: (comment) => {
        dispatch(sendHotelComment(comment))
    }
})

export default connect(mapStateToProps, mapDispatchToProps)(HotelCommentListComponent)