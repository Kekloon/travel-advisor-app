import hotelDetailsComponent from '../component/HotelDetailsComponent'
import { connect } from 'react-redux'
import { getHotelComment, getHotelRate,updateHotelReviewRate,bookHotel,saveHotelList } from '../HotelAction'

const mapStateToProps = (state) => ({
    isLoading: state.hotelReducer.getHotelComment.isLoading,
    hotelCommentList:state.hotelReducer.getHotelComment.hotelCommentList,
    hotelRateList:state.hotelReducer.getHotelRate.hotelRateList
})

const mapDispatchToProps = (dispatch) => ({
    getHotelComment:(hotelData)=>{
        dispatch(getHotelComment(hotelData))
    },
    getHotelRate:(hotelData)=>{
        dispatch(getHotelRate(hotelData))
    },
    updateHotelReviewRate:(userRate,boolean,userId,hotelData)=>{
        dispatch(updateHotelReviewRate(userRate,boolean,userId,hotelData))
    },
    bookHotel:(hotelData,hotelPackageData,totalAmount,checkInDate,checkOutDate,amount)=>{
        dispatch(bookHotel(hotelData,hotelPackageData,totalAmount,checkInDate,checkOutDate,amount))
    },
    saveHotelList:(boolean,hotelId)=>{
        dispatch(saveHotelList(boolean,hotelId))
    }

})

export default connect(mapStateToProps, mapDispatchToProps)(hotelDetailsComponent)