import { connect } from 'react-redux'
import HotelListComponent from '../component/HotelListComponent'
import { getHotelList } from '../HotelAction'

const mapStateToProps = (state) => ({
    hotelList: state.hotelReducer.getHotelList.hotelList,
    isLoading: state.hotelReducer.getHotelList.isLoading,
    user: state.userReducer.user
})

const mapDispatchToProp = (dispatch) => ({
    getHotelList: () => {
        dispatch(getHotelList())
    }
})

export default connect(mapStateToProps, mapDispatchToProp)(HotelListComponent)