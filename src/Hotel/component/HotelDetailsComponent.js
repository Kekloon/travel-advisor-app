import React, { Component } from 'react'
import { TouchableHighlight, Dimensions, Image, FlatList, StyleSheet, StatusBar, TouchableOpacity, ScrollView, RefreshControl, Animated, Alert } from 'react-native'
import { Container, Item, Text, Icon, View, Toast, Card, ListItem, Body, List, Thumbnail, Input, CardItem, Left, Right, Button, Content, Spinner, Header } from 'native-base'
import moment from 'moment'
import _ from 'lodash'
import Slideshow from 'react-native-slideshow'
import StarRating from 'react-native-star-rating'
import { Popup } from 'react-native-map-link'
import Modal from 'react-native-modal';
import HotelCommentListContainer from '../container/HotelCommentListContainer'
import DatePicker from 'react-native-datepicker'

const { width } = Dimensions.get('window')

class hotelDetailComponent extends Component {
    constructor(props) {
        super(props)
        this.state = {
            user: {},
            hotelData: {},
            isLoading: false,
            rate: 0,
            myRate: 0,
            isVisible: false,
            isWriteReviewModalVisible: false,
            hotelCommentList: [],
            refreshing: false,
            isLoading: false,
            hotelRateList: [],
            isCommentModalVisible: false,
            passHotelData: null,
            isBookHotelModalVisible: false,
            amount: '1',
            hotelPackageData: {},
            checkInDate:{},
            checkOutDate:{},
        }
        this.handleBack = this.handleBack.bind(this)
        this.sumRate = this.sumRate.bind(this)
        this.setPopUpVisible = this.setPopUpVisible.bind(this)
        this.setWriteReviewModalVisible = this.setWriteReviewModalVisible.bind(this)
        this.checkUserRate = this.checkUserRate.bind(this)
        this._onRefresh = this._onRefresh.bind(this)
        this.setCommentModalVisible = this.setCommentModalVisible.bind(this)
        this.saveHotel = this.saveHotel.bind(this)
        this.setBookHotelModalVisible = this.setBookHotelModalVisible.bind(this)
        this.handleBtnBookHotel = this.handleBtnBookHotel.bind(this)
        this.submitBookHotel = this.submitBookHotel.bind(this)
        this.handleBookingHotelBack = this.handleBookingHotelBack.bind(this)
    }

    _onRefresh() {
        this.setState({ refreshing: true })
        this.props.getHotelComment(this.props.navigation.state.params.passHotel)
        this.props.getHotelRate(this.props.navigation.state.params.passHotel)
        this.setState({ refreshing: false })
    }

    checkIsSavedHotel(hotelId) {
        if (_.find(this.state.user.userSaveHotelList, { hotel: hotelId })) {
            return true
        }
        else {
            false
        }
    }

    saveHotel() {
        const userData = this.state.user
        if (_.find(userData.userSaveHotelList, { hotel: this.state.hotelData.hotelId })) {
            const savelist = userData.userSaveHotelList
            const updateSavedHotelListArray = _.pullAllBy(savelist, [{ 'hotel': this.state.hotelData.hotelId }], 'hotel')
            userData.userSaveHotelList = updateSavedHotelListArray
            this.setState({
                user: userData
            })
            this.props.saveHotelList(false, this.state.hotelData.hotelId)
        }
        else {
            let newSaveHotel = {
                hotel: this.state.hotelData.hotelId
            }

            userData.userSaveHotelList.push(newSaveHotel)
            this.setState({
                user: userData
            })
            this.props.saveHotelList(true, this.state.hotelData.hotelId)
        }
    }

    setPopUpVisible() {
        this.setState({
            isVisible: true
        })
    }

    onStarRatingPress(rating) {
        this.setState({
            myRate: rating
        })
    }

    checkUserRate(rateArray) {
        let rate = 0
        if (_.find(rateArray, { user: this.state.user.userId })) {
            rateArray.map((val) => {
                if (val.user == this.state.user.userId) {
                    rate = val.rate
                }
            })
            return rate
        }
        else {
            return 0
        }
    }

    sumRate(rateArray) {
        if (_.isEmpty(rateArray)) {
            return 0
        }
        else {
            let averageRate = 0
            rateArray.map((val) => {
                averageRate = averageRate + val.rate
            })
            averageRate = averageRate / rateArray.length
            return averageRate
        }
    }

    handleUpdateUserReview(userRate, rateArray) {
        this.setState({
            isWriteReviewModalVisible: false,
        })
        if (_.find(rateArray, { user: this.state.user.userId })) {
            this.props.updateHotelReviewRate(userRate, true, this.state.user.userId, this.state.hotelData)
        }
        else {
            this.props.updateHotelReviewRate(userRate, false, this.state.user.userId, this.state.hotelData)

        }
    }

    ReviewModal() {
        return (
            <View>
                <Modal
                    useNativeDriver={false}
                    isVisible={this.state.isWriteReviewModalVisible}
                    onBackdropPress={() => this.setState({ isWriteReviewModalVisible: false, myRate: 0 })}
                >
                    <Card style={{ width: width - 80, alignSelf: 'center', borderRadius: 20, maxHeight: 150 }}>
                        <View style={{ flex: 1, margin: 15 }}>
                            <Text style={{ fontSize: 20 }}>Give your review : </Text>
                            <Body style={{ marginTop: 5 }}>
                                <StarRating
                                    disabled={false}
                                    maxStars={5}
                                    rating={this.state.myRate}
                                    fullStarColor={'#00a680'}
                                    starSize={40}
                                    selectedStar={(rating) => this.onStarRatingPress(rating)}
                                />
                            </Body>
                            <Button onPress={() => this.handleUpdateUserReview(this.state.myRate, this.state.hotelRateList)} full style={{ height: 30, backgroundColor: '#00a680', borderRadius: 10 }}>
                                {this.state.isLoading ? <Spinner /> : <Text>Update</Text>}
                            </Button>
                        </View>
                    </Card>

                </Modal>
            </View>
        )
    }
    renderAmenities(val, key) {
        return (
            <View style={{ flexDirection: 'row', flex: 1 }} >
                {
                    val === 'Wifi' ?
                        <Text style={{ marginBottom: 5 }} key={key}><Icon type='Ionicons' name='ios-wifi' /> {val}</Text>
                        :
                        val === 'Breakfast' ?
                            <Text style={{ marginBottom: 5 }} key={key}><Icon style={{ fontSize: 23 }} type='FontAwesome' name='coffee' /> {val}</Text>
                            :
                            val == 'Free parking' ?
                                <Text style={{ marginBottom: 5 }} key={key}><Icon type='Ionicons' name='ios-car' /> {val}</Text>
                                :
                                val == 'Kitchen' ?
                                    <Text style={{ marginBottom: 5 }} key={key}><Icon style={{ fontSize: 33 }} type='Ionicons' name='ios-home' /> {val}</Text>
                                    :
                                    null
                }
            </View>
        )

    }

    componentWillMount() {
        this.setState({
            user: this.props.navigation.state.params.user,
            hotelData: this.props.navigation.state.params.passHotel,
        })
        this.props.getHotelComment(this.props.navigation.state.params.passHotel)
        this.props.getHotelRate(this.props.navigation.state.params.passHotel)

    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            hotelCommentList: _.orderBy(nextProps.hotelCommentList, ['commentDate'], ['desc']),
            isLoading: nextProps.isLoading,
            hotelRateList: nextProps.hotelRateList
        })
    }

    setWriteReviewModalVisible() {
        this.setState({
            isWriteReviewModalVisible: true,
        })
    }

    setCommentModalVisible(val) {
        this.setState({
            isCommentModalVisible: true,
            passHotelData: val
        })
    }

    handleBack() {
        this.setState({
            isCommentModalVisible: false
        })
    }

    setBookHotelModalVisible(val) {
        this.setState({
            isBookHotelModalVisible: true,
            hotelPackageData: val,
            checkInDate:{},
            checkOutDate:{},
            amount:'1'
        })
    }
    handleBookingHotelBack(){
        this.setState({
            isBookHotelModalVisible:false
        })
    }

    handleBtnBookHotel(hotelPackageData) {
        const amount = parseInt(this.state.amount, 10)
        if(_.isEmpty(this.state.checkInDate)){
            Alert.alert('Alert', 'Please select check in and check out date', [
                { text: 'Ok' },
            ], { cancelable: false })       
        }

        else if(_.isEmpty(this.state.checkOutDate)){
            Alert.alert('Alert', 'Please select check in and check out date', [
                { text: 'Ok' },
            ], { cancelable: false })       
        }

        else if (amount !== parseInt(amount, 10)) {
            Alert.alert('Alert','Please enter Amount', [
                { text: 'OK' },
            ], { cancelable: false })
        }
        else{
            const start = moment(this.state.checkInDate)
            const end = moment(this.state.checkOutDate)
            const duration = moment.duration(end.diff(start));
            const days = duration.asDays()
            if(days <1){
                Alert.alert('Alert', 'Please select correct check in and check out date.', [
                    { text: 'OK' },
                ], { cancelable: false }) 
            }
            
            else{
                const totalAmount = this.state.amount * days * hotelPackageData.hotelPrice
                Alert.alert('Confirmation', 'Total amount: RM' + totalAmount, [
                    { text: 'Cancel' },
                    { text: 'OK', onPress: () => this.submitBookHotel(hotelPackageData,totalAmount,this.state.hotelData) },
                ], { cancelable: false })
            }
        }
    }

    submitBookHotel(hotelPackageData,totalAmount,hotelData) {
        this.setState({
            isBookHotelModalVisible: false
        })
        this.props.bookHotel(hotelData,hotelPackageData,totalAmount,this.state.checkInDate,this.state.checkOutDate,this.state.amount)
        Alert.alert('Alert', 'Successful booked hotel you can check it in hotel booking history.', [
            { text: 'OK' },
        ], { cancelable: false })
    }

    renderHotelPackage(val, key) {
        return (
            <ListItem style={{ marginLeft: 0 }} key={key}>
                <Body>
                    <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                        <View style={{ width: width - 105 }}>
                            <Text style={{ marginLeft: 0 }}>Hotel package name: {val.hotelPackageName}</Text>
                        </View>
                        <Text style={{}}>RM {val.hotelPrice}</Text>
                    </View>
                    <View style={{ marginRight: -10, marginTop: 5 }}>
                        <Button full style={{ marginLeft: 0, backgroundColor: '#00a680', borderRadius: 5 }}
                            onPress={() => this.setBookHotelModalVisible(val)}
                        ><Text style={{ fontSize: 18, marginTop: -5 }}>Book Now</Text></Button>
                    </View>
                </Body>
            </ListItem>
        )
    }

    renderHotelBedType(val, key) {
        return (
            <Text key={key}>{val}</Text>
        )
    }

    render() {
        const defaultProfileImage = require('../../assets/user_avatar.png')
        const sumRate = this.sumRate(this.state.hotelRateList)
        const userRate = this.checkUserRate(this.state.hotelRateList)
        const isSavedHotel = this.checkIsSavedHotel(this.state.hotelData.hotelId)
        const today = moment();
        const maximumDate = moment(today).add(365,'days')

        return (
            <Container style={{ backgroundColor: 'white' }}>
                <Modal
                    style={{ width: width, marginLeft: 0, marginTop: 0, marginBottom: 0 }}
                    animationType="slide"
                    transparent={true}
                    visible={this.state.isCommentModalVisible}
                    onRequestClose={() => { }}>
                    <Header style={{ backgroundColor: 'white' }}>
                        <View style={{ position: 'absolute', left: 1 }}>
                            <Button transparent onPress={this.handleBack}
                            >
                                <Icon name='arrow-back' />
                            </Button>
                        </View>
                        <View>
                            <Text style={{ alignSelf: 'center', fontSize: 25, marginTop: 5 }}> Hotel Comments</Text>
                        </View>
                        <View style={{ position: 'absolute', right: 1, marginTop: 7, marginRight: 5 }}>
                        </View>
                    </Header>
                    <HotelCommentListContainer hotelData={this.state.passHotelData} user={this.state.user} />
                </Modal>

                <View>
                    <Modal
                        useNativeDriver={false}
                        isVisible={this.state.isBookHotelModalVisible}
                        onBackdropPress={() => this.setState({ isBookHotelModalVisible: false })}
                    >
                        <Card style={{ width: width - 30, alignSelf: 'center', borderRadius: 20, maxHeight: 500 }}>
                            <View style={{ flex: 1, margin: 15 }}>
                            <View style={{flexDirection:'row'}}>
                            <Left style={{maxWidth:20}}>
                            <TouchableOpacity
                            onPress={this.handleBookingHotelBack}>
                                <Icon name='arrow-back'/>
                            </TouchableOpacity>
                            </Left>
                            <Body>
                                <Text style={{ fontSize: 20, color: '#00a680',fontWeight:'bold' }}>Hotel Booking</Text>
                            </Body>
                            </View>
                                <ListItem style={{ maxHeight: 45 }}>
                                    <Left style={{ maxWidth: 200 }}>
                                        <Text>Package Name:</Text>
                                    </Left>
                                    <Body>
                                        <Text>
                                            {this.state.hotelPackageData.hotelPackageName}
                                        </Text>
                                    </Body>
                                </ListItem>
                                <ListItem style={{ maxHeight: 45 }}>
                                    <Left style={{ maxWidth: 200 }}>
                                        <Text>Price:</Text>
                                    </Left>
                                    <Body>
                                        <Text>RM{this.state.hotelPackageData.hotelPrice}</Text>
                                    </Body>
                                </ListItem>
                                <ListItem style={{ maxHeight: 45 }}>
                                    <Left style={{ maxWidth: 200 }}>
                                        <Text>Maximum Guests:</Text>
                                    </Left>
                                    <Body>
                                        <Text>
                                            {this.state.hotelPackageData.MaximumGuest} guests
                                        </Text>
                                    </Body>
                                </ListItem>
                                <ListItem>
                                    <Left style={{ maxWidth: 200 }}>
                                        <Text>Bed type:</Text>
                                    </Left>
                                    <Body>
                                        {
                                            !_.isEmpty(this.state.hotelPackageData) ?
                                                this.state.hotelPackageData.hotelBedType.map((val, key) => {
                                                    return this.renderHotelBedType(val, key)
                                                })
                                                :
                                                null
                                        }
                                    </Body>
                                </ListItem>
                                <ListItem style={{ height: 70 }}>
                                    <Left style={{ maxWidth: 150 }}>
                                        <Text>Amount of room:</Text>
                                    </Left>
                                    <Body>
                                        <Input
                                            value={this.state.amount}
                                            style={{ backgroundColor: '#F5F5F5', maxHeight: 100, height: 50, borderRadius: 20, maxWidth: 70, marginLeft: 10 }}
                                            placeholder='Amount'
                                            keyboardType='numeric'
                                            onChangeText={(val) => this.setState({ amount: val })}
                                        />
                                    </Body>
                                </ListItem>
                                <ListItem style={{ maxHeight: 45 }}>
                                    <Left style={{ maxWidth: 150 }}>
                                        <Text>Check in Date:</Text>
                                    </Left>
                                    <Body>
                                        <DatePicker
                                            date={_.isEmpty(this.state.checkInDate) ? null : this.state.checkInDate}
                                            androidMode='spinner'
                                            mode="date"
                                            placeholder="Select date"
                                            minDate={new Date()}
                                            maxDate={maximumDate}
                                            confirmBtnText="Confirm"
                                            cancelBtnText="Cancel"
                                            customStyles={{
                                                dateInput: {
                                                    borderColor: 'transparent'
                                                }
                                            }}

                                            onDateChange={(date) => { this.setState({ checkInDate: date }) }} />
                                    </Body>
                                </ListItem>
                                <ListItem style={{ maxHeight: 45 }}>
                                    <Left style={{ maxWidth: 150 }}>
                                        <Text>Check out Date:</Text>
                                    </Left>
                                    <Body>
                                        <DatePicker
                                            date={_.isEmpty(this.state.checkOutDate) ? null: this.state.checkOutDate}
                                            androidMode='spinner'
                                            mode="date"
                                            placeholder="Select date"
                                            minDate={new Date()}
                                            maxDate={maximumDate}
                                            confirmBtnText="Confirm"
                                            cancelBtnText="Cancel"
                                            customStyles={{
                                                dateInput: {
                                                    borderColor: 'transparent'
                                                }
                                            }}

                                            onDateChange={(date) => { this.setState({ checkOutDate: date }) }} />
                                    </Body>
                                </ListItem>
                                <Button onPress={() => this.handleBtnBookHotel(this.state.hotelPackageData)} full style={{ height: 40, backgroundColor: '#00a680', borderRadius: 10 }}>
                                    {this.state.isLoading ? <Spinner /> : <Text>Book now</Text>}
                                </Button>
                            </View>
                        </Card>
                    </Modal>
                </View>
                {this.ReviewModal()}
                <ScrollView
                    refreshControl={
                        <RefreshControl
                            refreshing={this.state.refreshing}
                            onRefresh={this._onRefresh}
                        />
                    }
                >
                    <Slideshow style={{ width: width, height: 300, alignSelf: 'center' }}
                        dataSource={this.state.hotelData.hotelImage}
                    />
                    <List>
                        <ListItem style={{ borderBottomWidth: 1, borderBottomColor: 'lightgray' }}>
                            <Body>
                                <Text style={{ fontWeight: 'bold', fontSize: 20, marginLeft: 0 }}>{this.state.hotelData.hotelName}</Text>
                                <View style={{ flex: 1, flexDirection: 'row' }}>
                                    <StarRating
                                        disabled={true}
                                        maxStars={5}
                                        rating={sumRate}
                                        fullStarColor={'#00a680'}
                                        starSize={20}
                                        selectedStar={(rating) => this.onStarRatingPress(rating)}
                                    />
                                    <Text note>{_.isEmpty(this.state.hotelRateList) ? '  ' + 0 : '  ' + this.state.hotelRateList.length} reviews</Text>
                                </View>
                            </Body>
                            <Right style={{ maxWidth: 30 }}>
                                <TouchableOpacity
                                    onPress={() => this.saveHotel()}
                                >
                                    <Icon style={{ color: isSavedHotel ? '#00a680' : 'gray', fontSize: 30 }} name="heart" />
                                </TouchableOpacity>
                            </Right>
                        </ListItem>
                        <ListItem style={{ borderBottomWidth: 1, borderBottomColor: 'lightgray' }}>
                            <Body>
                                <Text style={{ marginLeft: 0, color: '#00a680', fontSize: 20, fontWeight: 'bold' }}>Amenities</Text>
                                {
                                    this.state.hotelData.hotelAmenities.map((val, key) => {
                                        return this.renderAmenities(val, key)

                                    })
                                }
                            </Body>
                        </ListItem>
                        <ListItem style={{ borderBottomWidth: 1, borderBottomColor: 'lightgray' }}>
                            <Body>
                                <Text style={{ marginLeft: 0, color: '#00a680', fontSize: 20, fontWeight: 'bold' }}>Description</Text>
                                <Text style={{ marginLeft: 0, marginTop: 10 }}>{this.state.hotelData.hotelDescription}</Text>
                                {/* <Accordion dataArray={dataArray} expanded={0}/> */}
                            </Body>
                        </ListItem>
                        <ListItem style={{ borderBottomWidth: 1, borderBottomColor: 'lightgray' }}>
                            <Body>
                                <Text style={{ marginLeft: 0, color: '#00a680', fontSize: 20, fontWeight: 'bold' }}>
                                    <Icon
                                        style={{ color: '#00a680' }}
                                        type='FontAwesome'
                                        name='map-marker'
                                    /> {<Text> </Text>}
                                    Location</Text>
                                <TouchableOpacity
                                    onPress={this.setPopUpVisible}
                                >
                                    <Text style={{ marginLeft: 0, textDecorationLine: 'underline' }}>
                                        {this.state.hotelData.hotelAddress}, {this.state.hotelData.hotelPostalCode}, {this.state.hotelData.hotelCity}, {this.state.hotelData.hotelCountryName}</Text>
                                </TouchableOpacity>
                            </Body>
                        </ListItem>
                        <ListItem style={{ borderBottomWidth: 1, borderBottomColor: 'lightgray' }}>
                            <Body>
                                <Text style={{ marginLeft: 0, color: '#00a680', fontSize: 20, fontWeight: 'bold' }}>
                                    Hotel Room Package
                            </Text>
                                {
                                    this.state.hotelData.hotelPackage.map((val, key) => {
                                        return this.renderHotelPackage(val, key)
                                    })
                                }
                            </Body>
                        </ListItem>
                        <ListItem style={{ borderBottomWidth: 1, borderBottomColor: 'lightgray' }}>
                            <Body>
                                <Text style={{ marginLeft: 0, color: '#00a680', fontSize: 20, fontWeight: 'bold' }}>
                                    Review
                            </Text>
                                <Left>
                                    <StarRating
                                        disabled={true}
                                        maxStars={5}
                                        rating={userRate}
                                        fullStarColor={'#00a680'}
                                        starSize={40}
                                    />
                                </Left>
                                <Button style={{ backgroundColor: '#00a680', borderRadius: 5, marginTop: 5 }}
                                    full
                                    onPress={() => this.setWriteReviewModalVisible()}
                                >
                                    <Text style={{ fontSize: 18, marginTop: -5 }}>
                                        <Icon style={{ color: 'white', fontSize: 20 }} type='FontAwesome' name='star-o' />
                                        <Text> </Text>
                                        {
                                            userRate == 0 ? <Text style={{ color: 'white', fontSize: 18 }}>Give a review</Text> : <Text style={{ color: 'white', fontSize: 18 }}>Edit a review</Text>
                                        }
                                    </Text>
                                </Button>
                            </Body>
                        </ListItem>

                        <ListItem style={{ borderBottomWidth: 1, borderBottomColor: 'white' }}>
                            <Body>
                                <Text style={{ marginLeft: 0, color: '#00a680', fontSize: 20, fontWeight: 'bold' }}>
                                    Comment</Text>
                            </Body>
                        </ListItem>

                        {
                            !_.isEmpty(this.state.hotelCommentList) ?
                                <View>
                                    <ListItem avatar style={{ borderBottomWidth: 1, borderBottomColor: 'lightgray', marginTop: -10 }}>
                                        <Left>
                                            <TouchableOpacity
                                                onPress={() => this.props.navigation.navigate('UserPostProfile', { passUser: this.state.hotelCommentList[0].userCommentData.userId })}>
                                                <Thumbnail small source={!_.isEmpty(this.state.hotelCommentList[0].userCommentData.userProfilePicture) ? { uri: this.state.hotelCommentList[0].userCommentData.userProfilePicture } : defaultProfileImage} />
                                            </TouchableOpacity>
                                        </Left>
                                        <Body>
                                            <Text style={{ fontWeight: 'bold' }}>{this.state.hotelCommentList[0].userCommentData.userName}</Text>
                                            <Text>{this.state.hotelCommentList[0].comment}</Text>
                                        </Body>
                                        <Right>
                                            <Text note>{moment(this.state.hotelCommentList[0].commentDate, 'day').fromNow()}</Text>
                                        </Right>
                                    </ListItem>
                                    {
                                        !_.isEmpty(this.state.hotelCommentList[1]) ?
                                            <ListItem avatar style={{ borderBottomWidth: 1, borderBottomColor: 'lightgray', marginTop: -10 }}>
                                                <Left>
                                                    <TouchableOpacity
                                                        onPress={() => this.props.navigation.navigate('UserPostProfile', { passUser: this.state.hotelCommentList[1].userCommentData.userId })}>
                                                        <Thumbnail small source={!_.isEmpty(this.state.hotelCommentList[1].userCommentData.userProfilePicture) ? { uri: this.state.hotelCommentList[1].userCommentData.userProfilePicture } : defaultProfileImage} />
                                                    </TouchableOpacity>
                                                </Left>
                                                <Body>
                                                    <Text style={{ fontWeight: 'bold' }}>{this.state.hotelCommentList[1].userCommentData.userName}</Text>
                                                    <Text>{this.state.hotelCommentList[1].comment}</Text>
                                                </Body>
                                                <Right>
                                                    <Text note>{moment(this.state.hotelCommentList[1].commentDate, 'day').fromNow()}</Text>
                                                </Right>
                                            </ListItem> :
                                            <ListItem avatar style={{ borderBottomWidth: 1, borderBottomColor: 'lightgray' }}>
                                                <Left>
                                                    <Thumbnail small source={defaultProfileImage} />
                                                </Left>
                                                <Body>
                                                    <Text style={{ fontWeight: 'bold' }}>No comment</Text>
                                                </Body>
                                            </ListItem>
                                    }
                                    {
                                        !_.isEmpty(this.state.hotelCommentList[2]) ?
                                            <ListItem avatar style={{ borderBottomWidth: 1, borderBottomColor: 'lightgray', marginTop: -10 }}>
                                                <Left>
                                                    <TouchableOpacity
                                                        onPress={() => this.props.navigation.navigate('UserPostProfile', { passUser: this.state.hotelCommentList[2].userCommentData.userId })}>
                                                        <Thumbnail small source={!_.isEmpty(this.state.hotelCommentList[2].userCommentData.userProfilePicture) ? { uri: this.state.hotelCommentList[2].userCommentData.userProfilePicture } : defaultProfileImage} />
                                                    </TouchableOpacity>
                                                </Left>
                                                <Body>
                                                    <Text style={{ fontWeight: 'bold' }}>{this.state.hotelCommentList[2].userCommentData.userName}</Text>
                                                    <Text>{this.state.hotelCommentList[2].comment}</Text>
                                                </Body>
                                                <Right>
                                                    <Text note>{moment(this.state.hotelCommentList[2].commentDate, 'day').fromNow()}</Text>
                                                </Right>
                                            </ListItem> :
                                            <ListItem avatar style={{ borderBottomWidth: 1, borderBottomColor: 'lightgray' }}>
                                                <Left>
                                                    <Thumbnail small source={defaultProfileImage} />
                                                </Left>
                                                <Body>
                                                    <Text style={{ fontWeight: 'bold' }}>No comment</Text>
                                                </Body>
                                            </ListItem>
                                    }
                                </View>
                                :
                                <ListItem avatar style={{ borderBottomWidth: 1, borderBottomColor: 'lightgray' }}>
                                    <Text style={{ fontWeight: 'bold' }}>No comment</Text>
                                </ListItem>
                        }
                        <ListItem>
                            <TouchableHighlight
                                underlayColor='transparent'
                                onPress={() => this.setCommentModalVisible(this.state.hotelData)}
                            >
                                <View style={{ width: width }}>
                                    <Text>
                                        See all comment .....
                        </Text>
                                </View>
                            </TouchableHighlight>
                        </ListItem>

                    </List>
                </ScrollView>
                <Popup
                    isVisible={this.state.isVisible}
                    onCancelPressed={() => this.setState({ isVisible: false })}
                    onAppPressed={() => this.setState({ isVisible: false })}
                    onBackButtonPressed={() => this.setState({ isVisible: false })}
                    modalProps={{
                        animationIn: 'slideInUp'
                    }}
                    appsWhiteList={['google-maps']}
                    options={{
                        latitude: this.state.hotelData.hotelLatitude,
                        longitude: this.state.hotelData.hotelLongitude,
                        title: this.state.hotelData.hotelName,
                    }}
                // style={{}}
                />
            </Container>
        )

    }
}

export default hotelDetailComponent
