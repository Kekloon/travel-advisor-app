import React, { Component } from 'react'
import { TouchableHighlight, Dimensions, Image, FlatList, StyleSheet, StatusBar, TouchableOpacity, ScrollView, Modal, RefreshControl } from 'react-native'
import { Container, Item, Text, Icon, View, Toast, Card, ListItem, Body, List, Thumbnail, Input, CardItem, Left, Right, Button, Content, Spinner, Header, Picker } from 'native-base'
import _ from 'lodash'
import StarRating from 'react-native-star-rating'

const { width } = Dimensions.get('window')

class HotelListComponent extends Component {
    constructor(props) {
        super(props)
        this.state = {
            hotelList: [],
            isLoading: false,
            user: {},
            refreshing: false,
            searchText: {},
            searchResult: [],
            rate: 0,
            selectedSearchType: 'hotel'
        }
        this._onRefresh = this._onRefresh.bind(this)
        this.sumRate = this.sumRate.bind(this)
    }

    componentWillMount() {
        this.setState({
            user: this.props.user
        })
    }

    componentDidMount() {
        this.props.getHotelList()
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            hotelList: nextProps.hotelList,
            isLoading: nextProps.isLoading,
        })
    }

    _onRefresh() {
        this.setState({
            refreshing: true,
            searchText: {},
            searchResult: [],
            selectedSearchType: 'hotel'
        })
        this.props.getHotelList()
        this.setState({ refreshing: false })
    }

    onStarRatingPress(rating) {
        this.setState({
            rate: rating,
        })
    }

    sumRate(rateArray) {
        if (_.isEmpty(rateArray)) {
            return 0
        }
        else {
            let averageRate = 0
            rateArray.map((val, key) => {
                averageRate = averageRate + val.rate
            })
            averageRate = averageRate / rateArray.length
            return averageRate
        }
    }

    renderHotelList(val, key) {
        const sumRate = this.sumRate(val.hotelRate)
        return (
            <List key={key}>
                <ListItem>
                    <TouchableHighlight underlayColor='transparent'
                        onPress={() => this.props.navigation.navigate('HotelDetails', { passHotel: val, user: this.state.user })}
                    >
                        <View style={{ width: width - 30, height: 150, flexDirection: 'row' }}>
                            <Left>
                                <Thumbnail style={{ height: 140, width: 150 }} source={{ uri: val.hotelCoverImage }} />
                            </Left>
                            <Body style={{ alignContent: 'flex-start', alignSelf: 'flex-start', alignItems: 'flex-start',marginTop:10 }}>
                                <Text style={{ fontWeight: 'bold' }}>{val.hotelName}</Text>
                                <Text>
                                    City : {val.hotelCity}
                                </Text>
                                <Text>Country : {val.hotelCountryName}</Text>
                                <StarRating
                                    disabled={true}
                                    maxStars={5}
                                    rating={sumRate}
                                    fullStarColor={'#00a680'}
                                    starSize={20}
                                    selectedStar={(rating) => this.onStarRatingPress(rating)}
                                />
                                <Text note>{_.isEmpty(val.hotelRate) ? 0 : val.hotelRate.length} reviews</Text>
                            </Body>
                        </View>
                    </TouchableHighlight>
                </ListItem>
            </List>
        )
    }

    render() {
        if (!_.isEmpty(this.state.searchText)) {
            const text = this.state.searchText
            if (this.state.selectedSearchType === 'hotel') {
                this.state.searchResult = _.filter(this.state.hotelList, function (o) {
                    return o.hotelName.toLowerCase().includes(text.toLowerCase())
                })
            }
            if (this.state.selectedSearchType === 'country') {
                this.state.searchResult = _.filter(this.state.hotelList, function (o) {
                    return o.hotelCountryName.toLowerCase().includes(text.toLowerCase())
                })
            }
            if (this.state.selectedSearchType === 'city') {
                this.state.searchResult = _.filter(this.state.hotelList, function (o) {
                    return o.hotelCity.toLowerCase().includes(text.toLowerCase())
                })
            }
        }

        else {
            this.state.searchResult = this.state.hotelList
        }

        return (
            <Container>
                <Header style={{ backgroundColor: '#00a680' }}>
                    <Left style={{ maxWidth: 70 }}>
                        <Icon style={{ backgroundColor: '#00a680', color: 'white', fontSize: 30 }} name="ios-search" />
                    </Left>
                    <Body marginLeft={-40} marginTop={-10} maxWidth={width} style={{ maxWidth: width, borderBottomWidth: 1, maxHeight: 40, textDecorationLine: 'none', borderBottomColor: 'dimgray' }}>
                        <Item style={{ borderBottomColor: 'transparent' }}>
                            <Input
                                value={this.state.searchText}
                                onChangeText={(text) => this.setState({ searchText: text })}
                                placeholder='Search' style={{ width: width, color: '#E4E2E2', fontSize: 18 }} />
                            <Text style={{ fontSize: 20 }}>|</Text>
                            <Picker
                                mode='dropdown'
                                style={{ width: 145, borderWidth: 20 }}
                                selectedValue={this.state.selectedSearchType}
                                onValueChange={(value) => this.setState({ selectedSearchType: value })}
                            >
                                <Picker.Item label='Hotel' value='hotel' />
                                <Picker.Item label='Country' value='country' />
                                <Picker.Item label='City' value='city' />
                            </Picker>
                        </Item>
                    </Body>
                </Header>
                <ScrollView
                    refreshControl={
                        <RefreshControl
                            refreshing={this.state.refreshing}
                            onRefresh={this._onRefresh}
                        />
                    }
                >
                    {
                        this.state.isLoading
                            ?
                            <Spinner />
                            :
                            _.isEmpty(this.state.hotelList) ?
                                <Text style={{ color: 'gray', fontSize: 25, alignSelf: 'center', marginTop: 30 }}>No result</Text> :
                                this.state.searchResult.map((val, key) => {
                                    return this.renderHotelList(val, key)
                                })
                    }
                </ScrollView>
            </Container>
        )
    }
}

export default HotelListComponent
