import React, { Component } from 'react'
import { TouchableHighlight, Dimensions, Image, FlatList, StyleSheet, StatusBar, TouchableOpacity, ScrollView, Modal, RefreshControl } from 'react-native'
import { Container, Item, Text, Icon, View, Toast, Card, ListItem, Body, List, Thumbnail, Input, CardItem, Left, Right, Button, Content, Spinner, Header, Picker } from 'native-base'
import _ from 'lodash'
import moment from 'moment'


const { width } = Dimensions.get('window')
class HotelCommentListComponent extends Component {
    constructor(props) {
        super(props)
        this.state = {
            isLoading: false,
            commentMessage: {},
            hotelFullCommentList: [],
            user: {},
            isSending: false,
            isSended: false,
            hotelData: null,
        }
    }

    componentDidMount() {
        this.setState({
            hotelData: this.props.hotelData,
            user: this.props.user,
        })

        this.props.getFullHotelComment(this.props.hotelData)
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            hotelFullCommentList: _.orderBy(nextProps.hotelFullCommentList, ['commentDate'], ['asc']),
            isLoading: nextProps.isLoading,
            isSended: nextProps.isSended,
            isSending: nextProps.isSending
        })
    }

    renderComment(data) {
        const defaultProfileImage = require('../../assets/user_avatar.png')
        return (
            <ListItem avatar>
                <Left style={{ marginTop: -20 }}>
                    {
                        data.userCommentData.userProfilePicture ?
                            <Thumbnail small source={{ uri: data.userCommentData.userProfilePicture }} />
                            :
                            <Thumbnail small source={defaultProfileImage} />
                    }
                </Left>
                <Body style={{ marginLeft: 5, marginRight: 5 }}>
                    <Card style={{ borderRadius: 15, backgroundColor: '#F5F5F5' }}>
                        <Text style={{ marginLeft: 10, fontWeight: 'bold' }}>{data.userCommentData.userName}</Text>
                        <Text style={{ marginLeft: 10 }}>{data.comment}</Text>
                    </Card>
                    <Text style={{ marginLeft: 10 }} note>{moment(data.commentDate, 'day').fromNow()}</Text>
                </Body>
            </ListItem>
        )
    }

    handleBtnSend() {
        if (!_.isEmpty(this.state.commentMessage)) {
            const comment = {
                message: this.state.commentMessage,
                hotelData: this.state.hotelData
            }
            this.props.sendHotelComment(comment)
            this.setState({
                commentMessage: {},
            })
        }
    }

    render() {
        const defaultProfileImage = require('../../assets/user_avatar.png')
        return (
            <Container style={{ backgroundColor: 'white' }}>
                {
                    <FlatList
                        inverted
                        ref='flatlist'
                        keyExtractor={(item, index) => index}
                        data={_.orderBy(this.state.hotelFullCommentList, ['commentDate'], ['desc'])}
                        renderItem={({ item }) => this.renderComment(item)}
                    />

                }
                <View style={{ position: 'relative', bottom: 1 }}>
                    <Item regular style={{ borderTopWidth: 1, height: 55 }}>
                        <Left style={{ marginLeft: 5 }}>
                            {!_.isEmpty(this.state.user) ?
                                <Thumbnail small source={!_.isEmpty(this.state.user.userProfilePicture) ? { uri: this.state.user.userProfilePicture } : defaultProfileImage} />
                                : null
                            }
                        </Left>
                        <Body>
                            <Input placeholder='   Write a comment...'
                                style={{ backgroundColor: '#F5F5F5', borderRadius: 30, width: 250, margin: 5 }}
                                value={this.state.commentMessage}
                                disabled={this.state.isSending}
                                onChangeText={(val) => this.setState({ commentMessage: val })}
                            />
                        </Body>
                        <Right>
                            {
                                this.state.isSending ?
                                    <Spinner />
                                    :
                                    <Button transparent onPress={() => this.handleBtnSend()}
                                    >
                                        <Icon type='Ionicons' name='md-send' style={{ color: _.isEmpty(this.state.commentMessage) ? 'gray' : '#00a680', margin: 5 }} />
                                    </Button>
                            }
                        </Right>
                    </Item>
                </View>
            </Container>
        )
    }
}

export default HotelCommentListComponent