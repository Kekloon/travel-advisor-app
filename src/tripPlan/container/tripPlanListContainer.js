import tripPlanListComponent from '../component/tripPlanListComponent'
import { connect } from 'react-redux'
import { getDestinationList } from '../../Destination/DestinationAction'
import { getHotelList } from '../../Hotel/HotelAction'
import { getUserPlanningList,addNewPlanList,deletePlanTrip,handleSavetripPlan } from '../tripPlanAction'

const mapStateToProps = (state) => ({
    user: state.userReducer.user,
    hotelList: state.hotelReducer.getHotelList.hotelList,
    destinationList: state.destinationReducer.getDestinationList.destinationList,
    userTripPlanningList:state.tripPlanReducer.getUserPlanningList.userTripPlanningList
})

const mapDispatchToProps = (dispatch) => ({
    getDestinationList: () => {
        dispatch(getDestinationList())
    },
    getHotelList: () => {
        dispatch(getHotelList())
    },
    getUserPlanningList: (user) => {
        dispatch(getUserPlanningList(user))
    },
    addNewPlanList:(newList,user)=>{
        dispatch(addNewPlanList(newList,user))
    },
    deletePlanTrip:(newArray,user)=>{
        dispatch(deletePlanTrip(newArray,user))
    },
    handleSavetripPlan:(newArray,user)=>{
        dispatch(handleSavetripPlan(newArray,user))
    }
})

export default connect(mapStateToProps, mapDispatchToProps)(tripPlanListComponent)