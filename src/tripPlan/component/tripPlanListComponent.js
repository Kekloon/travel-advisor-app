import React, { Component } from 'react'
import { TouchableHighlight, Dimensions, Image, FlatList, StyleSheet, StatusBar, TouchableOpacity, ScrollView, RefreshControl, Animated, Alert, Easing, Platform } from 'react-native'
import { Container, Item, Text, Icon, View, Toast, Card, ListItem, Body, List, Thumbnail, Input, CardItem, Left, Right, Button, Content, Spinner, Header, Accordion, Fab, Picker } from 'native-base'
import moment from 'moment'
import _ from 'lodash'
import Modal from 'react-native-modal'
import SortableList from 'react-native-sortable-list'


const window = Dimensions.get('window')
const { width } = Dimensions.get('window')

class tripPlanListComponent extends Component {
    constructor(props) {
        super(props)
        this.state = {
            data: [],
            newArray: [],
            destinationList: [],
            hotelList: [],
            user: {},
            isModalVisible: false,
            selectedSearchType: 'destination',
            searchText: {},
            searchResult: [],
        }
        this.handleChange = this.handleChange.bind(this)
        this.setModalVisible = this.setModalVisible.bind(this)
        this.addNewPlanningDestinationList = this.addNewPlanningDestinationList.bind(this)
        this.addNewPlanningHotelList = this.addNewPlanningHotelList.bind(this)
        this.handleSavetripPlan =this.handleSavetripPlan.bind(this)
    }

    _renderRow = ({ data, active,navigation,user,newArray,deletePlanTrip }) => {
        return <Row data={data}  active={active} navigation={this.props.navigation} user={this.props.user} newArray={this.state.newArray} deletePlanTrip={this.props.deletePlanTrip}/>

    }

    handleChange(val) {
        const pendingArray = []
        val.map((index) => {
            pendingArray.push(this.state.data[index])
        })
        this.setState({
            newArray: pendingArray
        })
    }

    handleSavetripPlan(){
        Alert.alert('Alert','Successful saved trip list ' , [{ text: 'OK'}], { cancelable: false })
            this.props.handleSavetripPlan(this.state.newArray,this.state.user)
    }

    setModalVisible() {
        this.setState({
            isModalVisible: true
        })
    }


    componentWillMount() {
        this.setState({
            user: this.props.user
        })
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            destinationList: nextProps.destinationList,
            hotelList: nextProps.hotelList,
            data: nextProps.userTripPlanningList,
            newArray:nextProps.userTripPlanningList,
        })
    }

    componentDidMount() {
        this.props.getDestinationList()
        this.props.getHotelList()
        this.props.getUserPlanningList(this.state.user)
    }

    addNewPlanningDestinationList(val) {
        if(_.find(this.state.data,{destinationId:val.destinationId})){
            Alert.alert('Alert', val.destinationName +' currently exist in the list' , [{ text: 'OK'}], { cancelable: false })
        }
        else{
            let list = {
                ...val,
                type: 'destination'
            }
            this.state.newArray.push(list)
            this.setState({
                data: this.state.newArray,
            })
    
            this.props.addNewPlanList(list,this.state.user)
            Alert.alert('Alert','Successful added to trip list ' , [{ text: 'OK'}], { cancelable: false })

        }
    }

    addNewPlanningHotelList(val) {

        if(_.find(this.state.data,{hotelId:val.hotelId})){
            Alert.alert('Alert', val.hotelName +' currently exist in the list' , [{ text: 'OK'}], { cancelable: false })
        }
        else{
            let list = {
                ...val,
                type: 'hotel'
            }
            this.state.newArray.push(list)
            this.setState({
                data: this.state.newArray,
            })
    
            this.props.addNewPlanList(list,this.state.user)
            Alert.alert('Alert','Successful added to trip list ' , [{ text: 'OK'}], { cancelable: false })

        }
    }

    

    renderNewList(val, key) {
        return (
            this.state.selectedSearchType === 'destination' ?
                <List key={key} style={{ backgroundColor: 'white' }}>
                    <ListItem>
                        <TouchableHighlight underlayColor='transparent'
                            onPress={() => this.addNewPlanningDestinationList(val)}
                        >
                            <View style={{ width: width - 45, height: 160, flexDirection: 'row' }}>
                                <Left>
                                    <Thumbnail style={{ height: 140, width: 150 }} source={{ uri: val.destinationCoverImage }} />
                                </Left>
                                <Body style={{ alignContent: 'flex-start', alignSelf: 'flex-start', alignItems: 'flex-start', marginTop: 10 }}>
                                    <Text style={{ fontWeight: 'bold' }}>{val.destinationName}</Text>
                                    <Text>
                                        {
                                            val.destinationCategory === 'tour' ? <Text>Type: Tour</Text> :
                                                val.destinationCategory === 'themePark' ? <Text>Type: Theme Park</Text> :
                                                    val.destinationCategory === 'museums' ? <Text>Type: Museums</Text> :
                                                        val.destinationCategory === 'shopping' ? <Text>Type: Shopping</Text> :
                                                            val.destinationCategory === 'restaurant' ? <Text>Type: Restaurant</Text> :
                                                                null
                                        }
                                    </Text>
                                    <Text>
                                        {
                                            !_.isEmpty(val.destinationTicketPrice) ? 'Ticket: RM' + val.destinationTicketPrice : null
                                        }
                                    </Text>
                                    <Text>
                                        City : {val.destinationCity}
                                    </Text>
                                    <Text>Country : {val.destinationCountryName}</Text>
                                </Body>
                            </View>
                        </TouchableHighlight>
                    </ListItem>
                </List>
                :
                <List key={key} style={{ backgroundColor: 'white' }}>
                    <ListItem>
                        <TouchableHighlight underlayColor='transparent'
                            onPress={() => this.addNewPlanningHotelList(val)}
                        >
                            <View style={{ width: width - 45, height: 150, flexDirection: 'row' }}>
                                <Left>
                                    <Thumbnail style={{ height: 140, width: 150 }} source={{ uri: val.hotelCoverImage }} />
                                </Left>
                                <Body style={{ alignContent: 'flex-start', alignSelf: 'flex-start', alignItems: 'flex-start', marginTop: 10 }}>
                                    <Text style={{ fontWeight: 'bold' }}>{val.hotelName}</Text>
                                    <Text>
                                        City : {val.hotelCity}
                                    </Text>
                                    <Text>Country : {val.hotelCountryName}</Text>
                                </Body>
                            </View>
                        </TouchableHighlight>
                    </ListItem>
                </List>
        )

    }

    render() {
        if (!_.isEmpty(this.state.searchText)) {

            const text = this.state.searchText
            if (this.state.selectedSearchType === 'destination') {
                this.state.searchResult = _.filter(this.state.destinationList, function (o) {
                    return o.destinationName.toLowerCase().includes(text.toLowerCase())
                })
            }
            else if (this.state.selectedSearchType === 'hotel') {
                this.state.searchResult = _.filter(this.state.hotelList, function (o) {
                    return o.hotelName.toLowerCase().includes(text.toLowerCase())
                })
            }
        }
        else {
            if (this.state.selectedSearchType === 'destination') {
                this.state.searchResult = this.state.destinationList
            }
            else {
                this.state.searchResult = this.state.hotelList
            }
        }
        return (
            <Container>
                <Modal
                    useNativeDriver={false}
                    isVisible={this.state.isModalVisible}
                    onBackdropPress={() => this.setState({ isModalVisible: false })}
                >
                    <Header style={{ backgroundColor: '#00a680' }}>
                        <Left style={{ maxWidth: 70 }}>
                            <TouchableOpacity
                                onPress={() => this.setState({ isModalVisible: false })}
                            >
                                <Icon style={{ backgroundColor: '#00a680', color: 'white', fontSize: 30 }} name="arrow-back" />
                            </TouchableOpacity>
                        </Left>
                        <Body marginLeft={-40} marginTop={-10} maxWidth={width} style={{ maxWidth: width, borderBottomWidth: 1, maxHeight: 40, textDecorationLine: 'none', borderBottomColor: 'dimgray' }}>
                            <Item style={{ borderBottomColor: 'transparent' }}>
                                <Input
                                    value={this.state.searchText}
                                    onChangeText={(text) => this.setState({ searchText: text })}
                                    placeholder='Search' style={{ width: width, color: '#E4E2E2', fontSize: 18 }} />
                                <Text style={{ fontSize: 20 }}>|</Text>
                                <Picker
                                    mode='dropdown'
                                    style={{ width: 145, borderWidth: 20 }}
                                    selectedValue={this.state.selectedSearchType}
                                    onValueChange={(value) => this.setState({ selectedSearchType: value })}
                                >
                                    <Picker.Item label='Destination' value='destination' />
                                    <Picker.Item label='Hotel' value='hotel' />
                                </Picker>
                            </Item>
                        </Body>
                    </Header>
                    <ScrollView>
                        {
                            this.state.isLoading
                                ?
                                <Spinner />
                                :
                                _.isEmpty(this.state.searchResult) ?
                                    <List style={{ backgroundColor: 'white', height: 600 }}>
                                        <Text style={{ color: 'gray', fontSize: 25, alignSelf: 'center', marginTop: 30 }}>No result</Text>
                                    </List> :
                                    this.state.searchResult.map((val, key) => {
                                        return this.renderNewList(val, key)
                                    })

                        }
                    </ScrollView>
                </Modal>
                <Header style={{ backgroundColor: '#00a680' }}>
                    <Body>
                        <Text style={{ color: 'white', fontSize: 30, alignSelf: 'flex-end' }}>Trip Planning</Text>
                    </Body>
                    <Right style={{ maxWidth: 100, marginRight: -15 }}>
                        <Button transparent
                        onPress={()=> this.handleSavetripPlan()}
                        >
                            <Text>Save</Text>
                        </Button>
                    </Right>
                </Header>
                        {
                            _.isEmpty(this.state.data)?null:
                            <View style={styles.container}>
                            <SortableList
                                onChangeOrder={(value) => this.handleChange(value)}
                                style={styles.list}
                                contentContainerStyle={styles.contentContainer}
                                data={this.state.data}
                                renderRow={this._renderRow} />
        
                        </View>
                        }
              
                <Fab
                    active={true}
                    direction="down"
                    containerStyle={{}}
                    style={{ backgroundColor: '#00a680' }}
                    position="bottomRight"
                    onPress={() => this.setModalVisible()}
                >
                    <Icon style={{ fontSize: 40 }} type='FontAwesome' name='plus-circle' />
                </Fab>
            </Container>
        );
    }

}


class Row extends Component {
    constructor(props) {
        super(props);

        this._active = new Animated.Value(0);

        this._style = {
            ...Platform.select({
                ios: {
                    transform: [{
                        scale: this._active.interpolate({
                            inputRange: [0, 1],
                            outputRange: [1, 1.1],
                        }),
                    }],
                    shadowRadius: this._active.interpolate({
                        inputRange: [0, 1],
                        outputRange: [2, 10],
                    }),
                },

                android: {
                    transform: [{
                        scale: this._active.interpolate({
                            inputRange: [0, 1],
                            outputRange: [1, 1.07],
                        }),
                    }],
                    elevation: this._active.interpolate({
                        inputRange: [0, 1],
                        outputRange: [2, 6],
                    }),
                },
            })
        };
    }

    componentWillReceiveProps(nextProps) {
        if (this.props.active !== nextProps.active) {
            Animated.timing(this._active, {
                duration: 300,
                easing: Easing.bounce,
                toValue: Number(nextProps.active),
            }).start();
        }
    }

    
    handleDelete(data){
        if(data.type==='destination'){
            this.props.newArray =_.pullAllBy(this.props.newArray,[{'destinationId':data.destinationId}],'destinationId')
            this.props.deletePlanTrip(this.props.newArray,this.props.user)
        }
        else{
            this.props.newArray =_.pullAllBy(this.props.newArray,[{'hotelId':data.hotelId}],'hotelId')
            this.props.deletePlanTrip(this.props.newArray,this.props.user)
        }
    }

    render() {
        const { data, active, user } = this.props;
        return (
            <Animated.View style={[
                styles.row,
                this._style,
            ]}>
                {
                    _.isEmpty(data)?null
                    :
                    data.type === 'destination' ?
                        <View style={{ flexDirection: 'row' }}>
                            <Image source={{ uri: data.destinationCoverImage }} style={styles.image} />
                            <Text style={styles.text}>{data.destinationName}</Text>
                            <Button transparent style={{ backgroundColor: 'white' }}
                                onPress={() => this.props.navigation.navigate('DestinationDetails', { passDestination: data, user: user })}
                            ><Text style={{ color: '#00a680' }}>View</Text></Button>
                            <Button transparent style={{ backgroundColor: 'white' }}
                                onPress={() => this.handleDelete(data)}
                            ><Text style={{ color: '#00a680' }}>Delete</Text></Button>
                        </View>
                        :
                        <View style={{ flexDirection: 'row' }}>
                            <Image source={{ uri: data.hotelCoverImage }} style={styles.image} />
                            <Text style={styles.text}>{data.hotelName}</Text>
                            <Button transparent style={{ backgroundColor: 'white' }}
                                onPress={() => this.props.navigation.navigate('HotelDetails', { passHotel: data, user: user })}
                            ><Text style={{ color: '#00a680' }}>View</Text></Button>
                            <Button transparent style={{ backgroundColor: 'white' }}
                                onPress={() => this.handleDelete(data)}
                            ><Text style={{ color: '#00a680' }}>Delete</Text></Button>
                        </View>
                }

            </Animated.View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#eee',

        ...Platform.select({
            ios: {
                paddingTop: 20,
            },
        }),
    },

    title: {
        fontSize: 20,
        paddingVertical: 20,
        color: '#999999',
    },

    list: {
        flex: 1,
    },

    contentContainer: {
        width: window.width,

        ...Platform.select({
            ios: {
                paddingHorizontal: 30,
            },

            android: {
                paddingHorizontal: 0,
            }
        })
    },

    row: {
        flexDirection: 'row',
        alignItems: 'flex-start',
        backgroundColor: '#fff',
        padding: 16,
        height: 80,
        flex: 1,
        marginTop: 7,
        marginBottom: 5,
        borderRadius: 4,


        ...Platform.select({
            ios: {
                width: window.width,
                shadowColor: 'rgba(0,0,0,0.2)',
                shadowOpacity: 1,
                shadowOffset: { height: 2, width: 2 },
                shadowRadius: 2,
            },

            android: {
                marginLeft: 0,
                width: window.width,
                elevation: 0,
            },
        })
    },

    image: {
        width: 50,
        height: 50,
        marginRight: 30,
        borderRadius: 25,
    },

    text: {
        width: window.width - 220,
        marginLeft: -10,
        marginTop: -5,
        fontSize: 16,
        color: '#222222',
    },
});



export default tripPlanListComponent