import firebase from 'react-native-firebase'
import { AsyncStorage } from 'react-native'
import _ from 'lodash'
import { _failedRegisterNewUser } from '../User/UserAction';

const firestore = firebase.firestore()

export const getUserPlanningList = (user) => async (dispatch) => {
    dispatch(_requestGetUserPlanningList())
    try {
        let userTripList = []
        userRef = firestore.collection('user').doc(user.userId)
        await userRef.onSnapshot((val) => {
            userTripList = val.data().userTripPlan
            dispatch(_successGetUserPlanningList(userTripList))
        })

    }
    catch (e) {
        dispatch(_failedGetUserPlanningList(e))
    }
}

export const _requestGetUserPlanningList = () => ({
    type: 'REQUEST_GET_USER_PLANNING_LIST'
})

export const _successGetUserPlanningList = (payload) => ({
    type: 'SUCCESS_GET_USER_PLANNING_LIST',
    payload
})

export const _failedGetUserPlanningList = (err) => ({
    type: 'FAILED_GET_USER_PLANNING_LIST',
    err
})

export const addNewPlanList = (newList, user) => async (dispatch) => {
    let userPlanList = []
    userRef = firestore.collection('user').doc(user.userId)
    await userRef.get().then((val) => {
        userPlanList = val.data().userTripPlan
        userPlanList.push(newList)
    })

    userRef.set({
        userTripPlan: userPlanList
    }, { merge: true })
}

export const deletePlanTrip = (newArray, user) => async (dispatch) => {
    userRef = firestore.collection('user').doc(user.userId)
    userRef.set({
        userTripPlan: newArray
    }, { merge: true })

}

export const handleSavetripPlan = (newArray,user)=>async(dispatch)=>{
    console.log(newArray)
    userRef = firestore.collection('user').doc(user.userId)
    userRef.set({
        userTripPlan:newArray
    },{merge:true})
}