import { StackNavigator } from 'react-navigation'
import tripPlanList from '../tripPlan/container/tripPlanListContainer'

export const tripPlanNavigator = StackNavigator({
    tripPlanList: { screen: tripPlanList },
}, {
        initialRouteName: 'tripPlanList',
        headerMode: 'none'
    })