const initialState ={
    getUserPlanningList:{
        userTripPlanningList:null,
        errorMessage:null,
        isLoading:false,
    }

}

export default function(state=initialState,action){
    switch(action.type){
     case 'REQUEST_GET_USER_PLANNING_LIST':
     return Object.assign({},state,{
        getUserPlanningList:{
            isLoading:true,
        }
     })
     case 'SUCCESS_GET_USER_PLANNING_LIST':
     return Object.assign({},state,{
        getUserPlanningList:{
            userTripPlanningList:action.payload,
            isLoading:false,
        }
     })
     case 'FAILED_GET_USER_PLANNING_LIST':
     return Object.assign({},state,{
        getUserPlanningList:{
            errorMessage:action.payload,
            isLoading:false,
        }
     })
    default:
    return state
    }
}