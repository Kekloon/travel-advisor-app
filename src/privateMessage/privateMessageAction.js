import firebase from 'react-native-firebase'
import { AsyncStorage } from 'react-native'
import _ from 'lodash'

const firestore = firebase.firestore()

export const getUserPrivateMessageList = (user) => async (dispatch) => {
    dispatch(_requestGetUserPrivateMessageList())
    try {
        privateMessageRef = firestore.collection('privateMessage')
        privateMessageRef.onSnapshot(async () => {
            let privateMessagePendingList = []
            let privateMessageList = []
            let list = []
            const Admin = 'Admin'
            privateMessageRef = firestore.collection('privateMessage').where('Owner', '==', user.userId)
            privateMessageRef.get().then(async (Fdoc) => {
                await Fdoc.forEach(async (Fval) => {
                    privateMessagePendingList.push(Fval.data())
                })
                privateMessageRef = firestore.collection('privateMessage').where('subOwner', '==', user.userId)
                privateMessageRef.get().then(async (Sdoc) => {
                    await Sdoc.forEach(async (Sval) => {
                        privateMessagePendingList.push(Sval.data())
                    })
                    privateMessageList = []
                    await Promise.all(privateMessagePendingList.map(async (val) => {
                        list = []
                        if (val.Owner === user.userId && val.subOwner !== 'Admin') {
                            userRef = firestore.collection('user').doc(val.subOwner)
                            await userRef.get().then(async (userData) => {
                                list = {
                                    ...val,
                                    userData: userData.data()
                                }
                                privateMessageList.push(list)
                            })
                        }
                        else if (val.subOwner === user.userId) {
                            userRef = firestore.collection('user').doc(val.Owner)
                            await userRef.get().then(async (userData) => {
                                list = {
                                    ...val,
                                    userData: userData.data()
                                }
                                privateMessageList.push(list)
                            })
                        }
                        else if (val.subOwner === 'Admin') {
                            list = {
                                ...val,
                                userData: Admin
                            }
                            privateMessageList.push(list)
                        }
                    }))
                    dispatch(_successGetUserPrivateMessageList(privateMessageList))
                })
            })
        })

    }
    catch (e) {
        dispatch(_failedGetUserPrivateMessageList(e))
    }
}

const _requestGetUserPrivateMessageList = () => ({
    type: 'REQUEST_GET_USER_PRIVATE_MESSAGE_LIST'
})

const _successGetUserPrivateMessageList = (payload) => ({
    type: 'SUCCESS_GET_USER_PRIVATE_MESSAGE_LIST',
    payload
})

const _failedGetUserPrivateMessageList = (err) => ({
    type: 'FAILED_GET_USER_PRIVATE_MESSAGE_LIST',
    err
})

export const getUserContactList = (user) => async (dispatch) => {
    dispatch(_requestGetUserContactList())
    try {
        let userFollowingList = []
        let userContactList = []

        userRef = firestore.collection('user').doc(user.userId)
        await userRef.get().then((val) => {
            userFollowingList = val.data().userFollowing
        })

        await Promise.all(userFollowingList.map(async (val) => {
            userRef = firestore.collection('user').doc(val)
            await userRef.get().then(async (userData) => {
                userContactList.push(userData.data())
            })
        }))
        dispatch(_successGetUserContactList(userContactList))
    }
    catch (e) {
        dispatch(_failedGetUserContactList(e))
    }
}

const _requestGetUserContactList = () => ({
    type: 'REQUEST_GET_USER_CONTACT_LIST'
})

const _successGetUserContactList = (payload) => ({
    type: 'SUCCESS_GET_USER_CONTACT_LIST',
    payload
})

const _failedGetUserContactList = (err) => ({
    type: 'FAILED_GET_USER_CONTACT_LIST',
    err
})

export const getRealTimePrivateMessage = (docId) => async (dispatch) => {
    dispatch(_requestGetRealTimePrivateMessage())
    try {
        privateMessageRef = firestore.collection('privateMessage').doc(docId)
        await privateMessageRef.onSnapshot(async (val) => {
            dispatch(_successGetRealTimePrivateMessage(val.data()))
        })

    }
    catch (e) {
        dispatch(_failedGetRealTimePrivateMessage(e))
    }
}

const _requestGetRealTimePrivateMessage = () => ({
    type: 'REQUEST_GET_REAL_TIME_PRIVATE_MESSAGE'
})

const _successGetRealTimePrivateMessage = (payload) => ({
    type: 'SUCCESS_GET_REAL_TIME_PRIVATE_MESSAGE',
    payload
})

const _failedGetRealTimePrivateMessage = (err) => ({
    type: 'FAILED_GET_REAL_TIME_PRIVATE_MESSAGE',
    err
})

export const sendPrivateMessage = (isExist, commentMessage, privateMessageData, userId, userData) => async (dispatch) => {
    if (isExist) {
        privateMessageList = []
        privateMessageRef = firestore.collection('privateMessage').doc(privateMessageData)
        await privateMessageRef.get().then((doc) => {
            privateMessageList = doc.data().Message
        })

        const newMessage = {
            MessageDate: new Date(),
            Word: commentMessage,
            Sender: userId,
            Receiver: userData.userId || 'Admin'
        }

        privateMessageList.push(newMessage)
        privateMessageRef.set({
            Message: privateMessageList,
            LastEdit: new Date()
        }, { merge: true })
    }
    else {
        let privateMessageId = {}
        privateMessageList = []
        let newMessage = [{
            MessageDate: new Date(),
            Word: commentMessage,
            Sender: userId,
            Receiver: userData.userId || 'Admin'
        }]
        privateMessageRef = firestore.collection('privateMessage')
        await privateMessageRef.add({}).then(async (doc) => {
            privateMessageId = doc.id
            privateMessageRef = firestore.collection('privateMessage').doc(doc.id)
            privateMessageRef.set({
                ChatRoomId: doc.id,
                LastEdit: new Date(),
                Message: newMessage,
                Owner: userId,
                subOwner: userData.userId || 'Admin'
            })
        })
        OwnerRef = firestore.collection('user').doc(userId)
        OwnerPrivateMessageList = []
        await OwnerRef.get().then(async (val) => {
            OwnerPrivateMessageList = val.data().userPrivateMessage
        })
        OwnerPrivateMessageList.push(privateMessageId)
        OwnerRef.set({
            userPrivateMessage: OwnerPrivateMessageList
        }, { merge: true })

        if (userData == 'Admin') {
            dispatch(successCreateNewChatRoom(privateMessageId))
        }
        else {
            subOwnerRef = firestore.collection('user').doc(userData.userId)
            subOwnerRefPrivateMessageList = []
            await subOwnerRef.get().then(async (val) => {
                subOwnerRefPrivateMessageList = val.data().userPrivateMessage
            })
            subOwnerRefPrivateMessageList.push(privateMessageId)
            subOwnerRef.set({
                userPrivateMessage: subOwnerRefPrivateMessageList
            }, { merge: true })

            dispatch(successCreateNewChatRoom(privateMessageId))

        }
    }
}

const successCreateNewChatRoom = (payload) => ({
    type: 'SUCCESS_CREATE_NEW_CHAT_ROOM',
    payload
})