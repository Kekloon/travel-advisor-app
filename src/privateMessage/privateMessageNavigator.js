import { StackNavigator } from 'react-navigation'
import privateMessageList from '../privateMessage/container/privateMessageListContainer'
import privateMessageContact from './container/privateMessageContactListContainer'
import privateMessageDetails from './container/privateMessageDetailsListContainer'

export const PrivateMessageNavigator = StackNavigator({
    privateMessageList: { screen: privateMessageList },
    privateMessageContact: { screen: privateMessageContact },
    privateMessageDetails: { screen: privateMessageDetails }
}, {
        initialRouteName: 'privateMessageList',
        headerMode: 'none'
    })