const initialState = {
    getUserPrivateMessageList: {
        userPrivateMessageList: null,
        errorMessage: null,
        isLoading: false,
    },
    getUserContactList:{
        userContactList:null,
        errorMessage:null,
        isLoading:false,
    },
    getRealTimePrivateMessage:{
        privateMessageData:null,
        errorMessage:null,
        isLoading:false,
    },
    sendPrivateMessage:{
        ChatRoomId:null
    },
}

export default function (state = initialState, action){
    switch(action.type){
        case 'REQUEST_GET_USER_PRIVATE_MESSAGE_LIST':
        return Object.assign({},state,{
            getUserPrivateMessageList: {
                isLoading: true,
            }, 
        })
        case 'SUCCESS_GET_USER_PRIVATE_MESSAGE_LIST':
        return Object.assign({},state,{
            getUserPrivateMessageList: {
                userPrivateMessageList: action.payload,
                isLoading: false,
            }, 
        })
        case 'FAILED_GET_USER_PRIVATE_MESSAGE_LIST':
        return Object.assign({},state,{
            getUserPrivateMessageList: {
                errorMessage: action.payload,
                isLoading: false,
            }, 
        })
        case 'REQUEST_GET_USER_CONTACT_LIST':
        return Object.assign({},state,{
            getUserContactList:{
                isLoading:true,
            }
        })
        case 'SUCCESS_GET_USER_CONTACT_LIST':
        return Object.assign({},state,{
            getUserContactList:{
                userContactList:action.payload,
                isLoading:false,
            }
        })
        case 'FAILED_GET_USER_CONTACT_LIST':
        return Object.assign({},state,{
            getUserContactList:{
                errorMessage:action.err,
                isLoading:false,
            }
        })
        case 'REQUEST_GET_REAL_TIME_PRIVATE_MESSAGE':
        return Object.assign({},state,{
            getRealTimePrivateMessage:{
                isLoading:true,
            }
        })
        case 'SUCCESS_GET_REAL_TIME_PRIVATE_MESSAGE':
        return Object.assign({},state,{
            getRealTimePrivateMessage:{
                privateMessageData:action.payload,
                isLoading:false,
            }
        })
        case 'FAILED_GET_REAL_TIME_PRIVATE_MESSAGE':
        return Object.assign({},state,{
            getRealTimePrivateMessage:{
                errorMessage:action.err,
                isLoading:false,
            }
        })
        case 'SUCCESS_CREATE_NEW_CHAT_ROOM':
        return Object.assign({},state,{
            sendPrivateMessage:{
                ChatRoomId:action.payload
            },
        })
        default:
        return state
    }
}