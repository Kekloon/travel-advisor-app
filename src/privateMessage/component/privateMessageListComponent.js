import React, { Component } from 'react'
import { TouchableHighlight, Dimensions, Image, FlatList, StyleSheet, StatusBar, TouchableOpacity, ScrollView, RefreshControl, Animated, Alert } from 'react-native'
import { Container, Item, Text, Icon, View, Toast, Card, ListItem, Body, List, Thumbnail, Input, CardItem, Left, Right, Button, Content, Spinner, Header, Accordion, Fab } from 'native-base'
import moment from 'moment'
import _ from 'lodash'

const { width } = Dimensions.get('window')

class privateMessageListComponent extends Component {
    constructor(props) {
        super(props)
        this.state = {
            userPrivateMessageList: [],
            isLoading: true,
            user: {},
        }

    }

    componentWillMount() {
        this.setState({
            user: this.props.user
        })
    }

    componentDidMount() {
        this.props.getUserPrivateMessageList(this.state.user)
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            userPrivateMessageList: _.orderBy(nextProps.userPrivateMessageList, ['LastEdit'], ['desc']),
            isLoading: nextProps.isLoading,
        })
    }

    renderUserPrivateMessageList(val, key) {
        const latestMessageIndex = val.Message.length - 1
        const latestMessage = val.Message[latestMessageIndex]
        const defaultProfileImage = require('../../assets/user_avatar.png')
        return (
            <List key={key}>
            {
                val.subOwner =='Admin'?
                <ListItem>
                    <TouchableHighlight underlayColor='transparent'
                    onPress={()=>this.props.navigation.navigate('privateMessageDetails', { isExist: true, user: this.state.user, privateMessageData: val, userData: val.userData })
                }
                    >
                        <View style={{ width: width - 30, flexDirection: 'row' }}>
                            <Left style={{ maxWidth: 70 }}>
                                {
                                    <Thumbnail source={defaultProfileImage} />
                                }
                            </Left>
                            <Body maxWidth={width - 80} style={{ alignItems: 'flex-start' }}>
                                <Text style={{ fontWeight: 'bold', fontSize: 18 }}>
                                    Advisor
                                </Text>
                                <Text style={{ fontSize: 16, color: 'lightgray', marginLeft: 2 }}>
                                    {
                                        latestMessage.Sender === this.state.user.userId ?
                                            <Text style={{ color: 'gray' }}>You: {latestMessage.Word}</Text> :
                                            <Text style={{ color: 'gray' }}>Advisor: {latestMessage.Word}</Text>
                                    }
                                </Text>
                            </Body>
                            <Right style={{ maxWidth: 80, marginTop: -20 }}>
                                <Text note>
                                    {
                                        moment(val.LastEdit).format('DD/MM/YYYY')
                                    }
                                </Text>
                            </Right>
                        </View>
                    </TouchableHighlight>
                </ListItem>
                :
                <ListItem>
                    <TouchableHighlight underlayColor='transparent'
                    onPress={()=>this.props.navigation.navigate('privateMessageDetails', { isExist: true, user: this.state.user, privateMessageData: val, userData: val.userData })}
                    >
                        <View style={{ width: width - 30, flexDirection: 'row' }}>
                            <Left style={{ maxWidth: 70 }}>
                                {
                                    <Thumbnail source={!_.isEmpty(val.userData.userProfilePicture) ? { uri: val.userData.userProfilePicture } : defaultProfileImage} />
                                }
                            </Left>
                            <Body maxWidth={width - 80} style={{ alignItems: 'flex-start' }}>
                                <Text style={{ fontWeight: 'bold', fontSize: 18 }}>
                                    {val.userData.userName}
                                </Text>
                                <Text style={{ fontSize: 16, color: 'lightgray', marginLeft: 2 }}>
                                    {
                                        latestMessage.Sender === this.state.user.userId ?
                                            <Text style={{ color: 'gray' }}>You: {latestMessage.Word}</Text> :
                                            <Text style={{ color: 'gray' }}>{val.userData.userName}: {latestMessage.Word}</Text>
                                    }
                                </Text>
                            </Body>
                            <Right style={{ maxWidth: 80, marginTop: -20 }}>
                                <Text note>
                                    {
                                        moment(val.LastEdit).format('DD/MM/YYYY')
                                    }
                                </Text>
                            </Right>
                        </View>
                    </TouchableHighlight>
                </ListItem>
            }
            </List>
        )
    }

    render() {
        return (
            <Container>
                <ScrollView>
                    <Header style={{ backgroundColor: '#00a680' }}>
                        <Body>
                            <Text style={{ color: 'white', alignSelf: 'center', fontSize: 30 }}>
                                Private Message
                        </Text>
                        </Body>
                    </Header>
                    {
                        this.state.isLoading ?
                            <Spinner />
                            :
                            _.isEmpty(this.state.userPrivateMessageList) ?
                                <Text style={{ color: 'gray', fontSize: 25, alignSelf: 'center', marginTop: 30 }}>No Message</Text>
                                :
                                this.state.userPrivateMessageList.map((val, key) => {
                                    return this.renderUserPrivateMessageList(val, key)
                                })
                    }
                </ScrollView>
                <Fab
                    active={true}
                    direction="down"
                    containerStyle={{}}
                    style={{ backgroundColor: '#00a680' }}
                    position="bottomRight"
                    onPress={() => this.props.navigation.navigate('privateMessageContact', { user: this.state.user, userPrivateMessageList: this.state.userPrivateMessageList, navigation:this.props.navigation })}
                >
                    <Icon style={{ fontSize: 35 }} type='Ionicons' name='ios-chatbubbles' />
                </Fab>
            </Container>
        )
    }
}

export default privateMessageListComponent