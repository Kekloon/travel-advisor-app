import React, { Component } from 'react'
import { TouchableHighlight, Dimensions, Image, FlatList, StyleSheet, StatusBar, TouchableOpacity, ScrollView, RefreshControl, Animated, Alert } from 'react-native'
import { Container, Item, Text, Icon, View, Toast, Card, ListItem, Body, List, Thumbnail, Input, CardItem, Left, Right, Button, Content, Spinner, Header, Accordion, Fab } from 'native-base'
import moment from 'moment'
import _ from 'lodash'

const { width } = Dimensions.get('window')

class privateMessageContactListComponent extends Component {
    constructor(props) {
        super(props)
        this.state = {
            userContactList: [],
            isLoading: true,
            user: {},
            userPrivateMessageList: [],
        }
        this.checkExistChat = this.checkExistChat.bind(this)
        this.checkExistAdminChat = this.checkExistAdminChat.bind(this)
    }

    componentWillMount() {
        this.setState({
            user: this.props.navigation.state.params.user,
            userPrivateMessageList: this.props.navigation.state.params.userPrivateMessageList,
        })
    }

    componentDidMount() {
        this.props.getUserContactList(this.state.user)
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            userContactList: nextProps.userContactList,
            isLoading: nextProps.isLoading
        })
    }

    checkExistChat(val) {
        this.state.userPrivateMessageList.map((data) => {
            if (data.Owner == this.state.user.userId && data.subOwner == val.userId) {
                console.log('userExist owner user sub userdata')
                this.props.navigation.navigate('privateMessageDetails', { isExist: true, user: this.state.user, privateMessageData: data, userData: val })

            }
            else if (data.subOwner == this.state.user.userId && data.Owner == val.userId) {
                console.log('userExist Subowner user owner userdata')
                this.props.navigation.navigate('privateMessageDetails', { isExist: true, user: this.state.user, privateMessageData: data, userData: val })
            }
        })
        if (_.find(this.state.userPrivateMessageList, { Owner: val.userId })) {
        }
        else if (_.find(this.state.userPrivateMessageList, { subOwner: val.userId })) {

        }
        else {
            console.log('user is not exist')
            this.props.navigation.navigate('privateMessageDetails', { isExist: false, user: this.state.user, privateMessageData: null, userData: val })
        }

    }

    checkExistAdminChat(val) {
        this.state.userPrivateMessageList.map((data) => {
            if (data.Owner == this.state.user.userId && data.subOwner == val) {
                console.log('Admin exist')
                this.props.navigation.navigate('privateMessageDetails', { isExist: true, user: this.state.user, privateMessageData: data, userData: val })
            }
        })
        if (_.find(this.state.userPrivateMessageList, { subOwner: 'Admin' })) {

        }
        else {
            console.log('Admin not exist')
            this.props.navigation.navigate('privateMessageDetails', { isExist: false, user: this.state.user, privateMessageData: null, userData: val })
        }
    }

    renderUserContactList(val, key) {
        const defaultProfileImage = require('../../assets/user_avatar.png')
        return (
            <List key={key}>
                {
                    val.userId == this.state.user.userId ? null :
                        <ListItem style={{ borderBottomColor: '#EEEEEE', maxHeight: 60 }}>
                            <TouchableHighlight underlayColor='transparent'
                                onPress={() => this.checkExistChat(val)}
                            >
                                <View style={{ width: width - 30, flexDirection: 'row' }}>

                                    <Left style={{ maxWidth: 50 }}>
                                        {

                                            <Thumbnail small source={!_.isEmpty(val.userProfilePicture) ? { uri: val.userProfilePicture } : defaultProfileImage} />
                                        }
                                    </Left>
                                    <Body maxWidth={width - 80} style={{ alignItems: 'flex-start' }}>
                                        <Text style={{ fontSize: 16, fontWeight: '500' }}>
                                            {val.userName}
                                        </Text>
                                    </Body>
                                </View>
                            </TouchableHighlight>
                        </ListItem>
                }
            </List>
        )
    }

    render() {
        const defaultProfileImage = require('../../assets/user_avatar.png')
        return (
            <Container>
                <Header style={{ backgroundColor: '#00a680' }}>
                    <Left style={{ maxWidth: 60 }}>
                        <TouchableOpacity
                            onPress={() => this.props.navigation.navigate('privateMessageList')}>
                            <Icon style={{ marginLeft: 10 }} name='arrow-back' />
                        </TouchableOpacity>
                    </Left>
                    <Body>
                        <Text style={{ color: 'white', fontSize: 20, fontWeight: 'bold' }}>Select contact</Text>
                        <Text style={{ color: 'white' }} note>{_.isEmpty(this.state.userContactList) ? null : this.state.userContactList.length} contacts</Text>
                    </Body>
                </Header>
                <ScrollView>
                    <ListItem style={{ maxHeight: 60 }}>
                        <TouchableHighlight underlayColor='transparent'
                            onPress={() => this.checkExistAdminChat('Admin')}
                        >
                            <View style={{ width: width - 30, flexDirection: 'row' }}>

                                <Left style={{ maxWidth: 50 }}>
                                    {
                                        <Thumbnail small source={defaultProfileImage} />
                                    }
                                </Left>
                                <Body maxWidth={width - 80} style={{ alignItems: 'flex-start' }}>
                                    <Text style={{ fontSize: 16, fontWeight: '500' }}>
                                        Advisor
                            </Text>
                                </Body>
                            </View>
                        </TouchableHighlight>
                    </ListItem>
                    {
                        this.state.isLoading ?
                            <Spinner />
                            :
                            _.isEmpty(this.state.userContactList) ?
                                <Text style={{ color: 'gray', fontSize: 25, alignSelf: 'center', marginTop: 30 }}>No Message</Text>
                                :
                                this.state.userContactList.map((val, key) => {
                                    return this.renderUserContactList(val, key)
                                })
                    }
                </ScrollView>
            </Container>
        )
    }
}

export default privateMessageContactListComponent

