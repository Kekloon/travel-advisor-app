import React, { Component } from 'react'
import { TouchableHighlight, Dimensions, Image, FlatList, StyleSheet, StatusBar, TouchableOpacity, ScrollView, Modal, RefreshControl } from 'react-native'
import { Container, Item, Text, Icon, View, Toast, Card, ListItem, Body, List, Thumbnail, Input, CardItem, Left, Right, Button, Content, Spinner, Header, Picker } from 'native-base'
import _ from 'lodash'
import moment from 'moment'

const { width } = Dimensions.get('window')

class privateMessageDetailsListComponent extends Component {
    constructor(props) {
        super(props)
        this.state = {
            isExist: false,
            user: {},
            privateMessageData: null,
            userData: {},
            isLoading: false,
            commentMessage: {},
            isSending: false,
            firstTimeCall:true,
        }
    }

    componentWillMount() {
        this.setState({
            user: this.props.navigation.state.params.user,
            isExist: this.props.navigation.state.params.isExist,
            userData: this.props.navigation.state.params.userData
        })
    }

    componentDidMount() {

        if (!_.isEmpty(this.props.navigation.state.params.privateMessageData)) {
            this.props.getRealTimePrivateMessage(this.props.navigation.state.params.privateMessageData.ChatRoomId)
        }
        else {

        }
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            privateMessageData: nextProps.privateMessageData,
            isLoading: nextProps.isLoading,
        })
        if(!_.isEmpty(nextProps.ChatRoomId) && this.state.firstTimeCall==true){
            this.props.getRealTimePrivateMessage(nextProps.ChatRoomId)
            this.setState({
                firstTimeCall:false
            })
        }

    }

    handleBtnSend() {
        if (!_.isEmpty(this.state.commentMessage)) {
            if (this.state.isExist) {
                this.props.sendPrivateMessage(true, this.state.commentMessage, this.state.privateMessageData.ChatRoomId, this.state.user.userId, this.state.userData)
                this.setState({
                    commentMessage:{}
                })
            }
            else {
                this.props.sendPrivateMessage(false, this.state.commentMessage, null, this.state.user.userId, this.state.userData)
                this.setState({
                    isExist: true,
                    commentMessage:{}
                })
            }

        }


    }

    renderPrivateMessage(val) {
        const defaultProfileImage = require('../../assets/user_avatar.png')
        return (
            <List>
                {
                    val.Sender !== this.state.user.userId ?
                        <ListItem style={{ borderBottomColor: '#EEEEEE' }}>
                            <Left style={{ maxWidth: width - 80 }}>
                            <Thumbnail small source={!_.isEmpty(this.state.userData.userProfilePicture) ? { uri: this.state.userData.userProfilePicture } : defaultProfileImage} />
                                <Card style={{ borderRadius: 15, backgroundColor: '#F5F5F5', marginLeft: 10 }}>
                                    <Text style={{ marginLeft: 10 }}>{val.Word}</Text>
                                <Text style={{marginLeft:10}} note>{moment(val.MessageDate, 'day').fromNow()}</Text>
                                </Card>
                            </Left>
                            <Right>
                            </Right>
                        </ListItem> :
                        <ListItem style={{ alignSelf: 'flex-end', borderBottomColor: '#EEEEEE' }}>
                            <Left style={{ maxWidth: width - 300, backgroundColor: 'black' }}>
                            </Left>
                            <Card style={{ borderRadius: 15, backgroundColor: '#F5F5F5', marginRight: -5, width: 180 }}>
                                <Text style={{ marginLeft: 10 }}>{val.Word}</Text>
                            <Text style={{marginLeft:10}} note>{moment(val.MessageDate, 'day').fromNow()}</Text>
                            </Card>

                        </ListItem>
                }
            </List>
        )
    }

    render() {
        const defaultProfileImage = require('../../assets/user_avatar.png')
        return (
            <Container>
                <Header style={{ backgroundColor: '#00a680' }}>
                    <Left style={{ maxWidth: 40 }}>
                        <TouchableOpacity
                            onPress={() => this.props.navigation.navigate('privateMessageList')}>
                            <Icon style={{ marginLeft: 10 }} name='arrow-back' />
                        </TouchableOpacity>
                    </Left>
                    <Body>
                        {
                            this.state.userData == 'Admin' ?
                                <View style={{ flexDirection: 'row' }}>
                                    <Thumbnail small source={defaultProfileImage} />
                                    <Text style={{ color: 'white', fontSize: 20, marginLeft: 10, marginTop: 5 }}>Advisor</Text>
                                </View>
                                :
                                <View style={{ flexDirection: 'row' }}>
                                    <Thumbnail small source={!_.isEmpty(this.state.userData.userProfilePicture) ? { uri: this.state.userData.userProfilePicture } : defaultProfileImage} />
                                    <Text style={{ color: 'white', fontSize: 20, marginLeft: 10, marginTop: 5 }}>{this.state.userData.userName}</Text>
                                </View>
                        }
                    </Body>
                </Header>
                    {
                        this.state.isLoading ?
                            <Spinner />
                            :
                            _.isEmpty(this.state.privateMessageData) ?
                            <FlatList
                            inverted
                            ref='flatlist'
                            keyExtractor={(item, index) => index}
                        />
                                // <Text style={{ color: 'gray', fontSize: 25, alignSelf: 'center', marginTop: 30 }}>No Message</Text>
                                :
                                <FlatList
                                inverted
                                ref='flatlist'
                                keyExtractor={(item, index) => index}
                                data={_.orderBy(this.state.privateMessageData.Message, ['MessageDate'], ['desc'])}
                                renderItem={({ item }) => this.renderPrivateMessage(item)}
                            // ListHeaderComponent={this.renderFlatListHeader()}
                            />
                                // this.state.privateMessageData.Message.map((val, key) => {
                                //     return this.renderPrivateMessage(val, key)
                                // })
                    }
                <View style={{ position:'relative', bottom: 0,marginBottom:0 }}>
                    <Item regular style={{ borderTopWidth: 1, height: 55 }}>
                        <Left style={{ marginLeft: 5 }}>
                            {!_.isEmpty(this.state.user) ?
                                <Thumbnail small source={!_.isEmpty(this.state.user.userProfilePicture) ? { uri: this.state.user.userProfilePicture } : defaultProfileImage} />
                                : null
                            }
                        </Left>
                        <Body>
                            <Input placeholder='   Write a message...'
                                style={{ backgroundColor: '#F5F5F5', borderRadius: 30, width: 250, margin: 5 }}
                                value={this.state.commentMessage}
                                onChangeText={(val) => this.setState({ commentMessage: val })}
                            />
                        </Body>
                        <Right>
                            {
                                this.state.isSending ?
                                    <Spinner />
                                    :
                                    <Button transparent onPress={() => this.handleBtnSend()}
                                    >
                                        <Icon type='Ionicons' name='md-send' style={{ color: _.isEmpty(this.state.commentMessage) ? 'gray' : '#00a680', margin: 5 }} />
                                    </Button>
                            }
                        </Right>
                    </Item>
                </View>
            </Container>
        )
    }
}
export default privateMessageDetailsListComponent