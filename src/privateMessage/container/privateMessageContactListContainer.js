import privateMessageContactListComponent from '../component/privateMessageContactListComponent'
import { connect } from 'react-redux'
import { getUserContactList } from '../privateMessageAction'

const mapStateToProps = (state) => ({
    userContactList: state.privateMessageReducer.getUserContactList.userContactList,
    isLoading: state.privateMessageReducer.getUserContactList.isLoading,
})

const mapDispatchToProps = (dispatch) => ({
    getUserContactList: (user) => {
        dispatch(getUserContactList(user))
    }
})

export default connect(mapStateToProps, mapDispatchToProps)(privateMessageContactListComponent)