import privateMessageListComponent from '../component/privateMessageListComponent'
import { connect } from 'react-redux'
import { getUserPrivateMessageList } from '../privateMessageAction'

const mapStateToProps = (state) => ({
    userPrivateMessageList: state.privateMessageReducer.getUserPrivateMessageList.userPrivateMessageList,
    isLoading: state.privateMessageReducer.getUserPrivateMessageList.isLoading,
    user:state.userReducer.user
})

const mapDispatchToProps = (dispatch) => ({
    getUserPrivateMessageList: (user) => {
        dispatch(getUserPrivateMessageList(user))
    }
})

export default connect(mapStateToProps, mapDispatchToProps)(privateMessageListComponent)