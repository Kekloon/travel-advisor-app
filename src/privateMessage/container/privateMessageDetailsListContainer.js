import privateMessageDetailsListComponent from '../component/privateMessageDetailsListComponent'
import { connect } from 'react-redux'
import { getRealTimePrivateMessage, sendPrivateMessage } from '../privateMessageAction'

const mapStateToProps = (state) => ({
    privateMessageData: state.privateMessageReducer.getRealTimePrivateMessage.privateMessageData,
    isLoading: state.privateMessageReducer.getRealTimePrivateMessage.isLoading,
    ChatRoomId: state.privateMessageReducer.sendPrivateMessage.ChatRoomId,
})

const mapDispatchToProps = (dispatch) => ({
    getRealTimePrivateMessage: (docId) => {
        dispatch(getRealTimePrivateMessage(docId))
    },
    sendPrivateMessage: (isExist, commentMessage, privateMessageData, userId, userData)=>{
        dispatch(sendPrivateMessage(isExist,commentMessage,privateMessageData,userId,userData))
    },
})

export default connect(mapStateToProps, mapDispatchToProps)(privateMessageDetailsListComponent)