import { Platform, AsyncStorage } from "react-native";
import Color from "color";
import { DIMENS } from "./constant";
import fileExtension from "file-extension";
import UUIDGenerator from "react-native-uuid-generator";
import firebase from "react-native-firebase";

const firestore = firebase.firestore()
const storage = firebase.storage()

export const getUserCreditRef = () => {
  return new Promise(async (resolve, reject) => {
    try {
      const userId = await AsyncStorage.getItem('uid')
      userRef = firestore.collection('users').doc(userId)
      userRef.get().then((doc) => {
        resolve(doc.data().creditRef)
      })
    } catch (e) {
      reject(e)
    }
  })
}

export const deleteFile = (imgUrl) => {
  return new Promise((resolve, reject) => {
    try {
      let file = storage.refFromURL(imgUrl)
      file.delete().then(() => {
        resolve(true)
      }).catch((e) => {
        reject(e)
      })
    } catch (e) {
      reject(e)
    }
  })
}

export const getURI = () => {
  if (Platform.OS === "android") {
    return "http://192.168.0.103:3000/api"
  } else {
    return "http://localhost:3000/api"
  }
}

export const uploadFile = (filePath) => {
  const fileExt = fileExtension(filePath.path)
  return new Promise((resolve, reject) => {
    UUIDGenerator.getRandomUUID()
      .then((uuid) => {
        const filename = `${uuid}.${fileExt}`
        let storageRef = storage.ref().child(uuid)
        let uploadTask = storageRef.putFile(filePath.path)
        uploadTask.on('state_changed', (snapshot) => {
          let progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
        }, err => {
          reject(err)
        }, () => {
          uploadTask.then((res) => {
            const downloadURL = res.downloadURL;
            resolve(downloadURL)
          })
        })
      })
  })
}

export const toTimeElapse = function (seconds) {
  if (seconds < 10) { seconds = "0" + seconds; }
  return '00:' + seconds;
}