import React, { Component } from 'react'
import {connect} from 'react-redux'
import PhoneValidateComponent from '../component/PhoneValidateComponent'
import { confirmCode } from '../AuthenticationAction'

class PhoneValidationContainer extends Component {
    constructor(props){
        super(props)
        this.onClicked = this.onClicked.bind(this)
    }

    onClicked(code){
        this.props.confirmCode(this.props.confirmResult,code)
    }

    render(){
        return(
            <PhoneValidateComponent
            onClicked={this.onClicked}/>
        )
    }
}

const mapStateToProps =(state)=>({
    isLoading:state.authenticationReducer.isLoading,
    confirmResult:state.authenticationReducer.confirmResult
})

const mapDispatchToProps =(dispatch)=>({
    confirmCode:(confirmResult,code)=>{
        dispatch(confirmCode(confirmResult,code))
    }
})

export default connect (mapStateToProps,mapDispatchToProps)(PhoneValidationContainer)