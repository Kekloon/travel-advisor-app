import React, { Component } from 'react'
import {connect } from 'react-redux'
import PhoneLoginComponent from '../component/PhoneLoginComponent'
import { sendCode} from '../AuthenticationAction'

class PhoneLoginContainer extends Component {
    constructor(props){
        super(props)

    }
    render(){
        return(
            <PhoneLoginComponent sendCode={this.props.sendCode}/>
        )
    }

    
}

const mapStateToProps=(state)=>({
    isLoading:state.authenticationReducer.isLoading,
    isAuthenticated:state.authenticationReducer.isAuthenticated,
    isUserExist:state.userReducer.isUserExist,

})

const mapDispatchToProps = (dispatch,props)=>({
    sendCode:(phoneNumber)=>{
        dispatch(sendCode(phoneNumber,props.navigation))
    }

})

export default connect (mapStateToProps,mapDispatchToProps)(PhoneLoginContainer)