import firebase from 'react-native-firebase'
import { AsyncStorage, Alert } from 'react-native'

export const sendCode = (phoneNumber, navigation) => async (dispatch) => {
    dispatch(_requestSendCode())
    Alert.alert('Alert', 'Sending code...', [], { cancelable: false })
    await firebase.auth().signInWithPhoneNumber(phoneNumber)
        .then((confirmResult) => dispatch(_successSendCode(confirmResult)))
        .then(() => Alert.alert('Alert', 'Code has been sent', [{ text: 'OK', onPress: () => navigation.navigate('PhoneValidateContainer') }], { cancelable: false }))
        .catch((err) => {
            Alert.alert('Alert', 'Sign in with phone number error!', [{ text: 'OK' }], { cancelable: true })
            dispatch(_failedSendCode(err))
        })
}

export const _requestSendCode = () => ({
    type: 'REQUEST_SEND_CODE'
})

export const _successSendCode = (payload) => ({
    type: 'SUCCESS_SEND_CODE',
    payload
})

export const _failedSendCode = (err) => ({
    type: 'FAILED_SEND_CODE',
    err
})

export const confirmCode = (confirmResult, code) => async (dispatch) => {
    dispatch(_requestConfirmCode())
    Alert.alert('Alert', 'Code verifying...', [], { cancelable: true })
    if (confirmResult && code.length == 6) {
        confirmResult.confirm(code)
        .then(() => Alert.alert('Alert', 'Code has been verified!', [{ text: 'OK'}], { cancelable: false }))
        .then((user) => dispatch(_successConfirmCode(user)))
            .catch((error) => {
                Alert.alert('Alert', 'Code has failed verified!', [{ text: 'OK'}], { cancelable: true })
                dispatch(_failedConfirmCode(error))
            }
            )
    }
    else {
        dispatch(_failedConfirmCode(err))
        Alert.alert('Alert', 'Code error!', [{ text: 'OK' }], { cancelable: true })
    }
}

export const _requestConfirmCode = () => ({
    type: 'REQUEST_CONFIRM_CODE'
})

export const _successConfirmCode = (payload) => ({
    type: 'SUCCESS_CONFIRM_CODE',
    payload
})

export const _failedConfirmCode = (err) => ({
    type: 'FAILED_CONFIRM_CODE',
    err
})

export const autoAuthentication = () => async (dispatch) => {
    dispatch(_requestAutoauthentication())
    firebase.auth().onAuthStateChanged(async (payload) => {
        const userData = payload && payload._user
        if (userData) {
            await AsyncStorage.setItem('uid', userData.uid)
            await AsyncStorage.setItem('phoneNumber', userData.phoneNumber)
            dispatch(_successAutoAuthentication(userData))
        }
        else {
            dispatch(_failedAutoAuthentication())
        }
    })
}

export const _requestAutoauthentication = () => ({
    type: 'REQUEST_AUTO_AUTHENTICATION'
})

export const _successAutoAuthentication = (payload) => ({
    type: 'SUCCESS_AUTO_AUTHENTICATION',
    payload
})

export const _failedAutoAuthentication = () => ({
    type: 'FAILED_AUTO_AUTHENTICATION'
})

export const signOut = () => (dispatch) => {
    dispatch(_signOut())
    firebase.auth().signOut()
        .then(() => console.log('Sign out successful'))
        .catch((e) => console.log('Sign out failed: ', e))
}

export const _signOut = () => ({
    type: 'SIGN_OUT'
})