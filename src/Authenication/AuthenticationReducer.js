const initialState = {
    isLoading: false,
    confirmResult: null,
    errorMessage: null,
    isAuthenticated: false,
    isAutoAuthenticated: false,
    autoAuthenticate: {
        isLoading: false
    },

}

export default function (state = initialState, action) {
    switch (action.type) {
        case 'REQUEST_SEND_CODE':
            return Object.assign({}, state, {
                isLoading: true
            })
        case 'SUCCESS_SEND_CODE':
            return Object.assign({}, state, {
                confirmResult: action.payload,
                isLoading: false
            })
        case 'FAILED_SEND_CODE':
            return Object.assign({}, state, {
                isLoading: false,
                errorMessage: action.err
            })
        case 'REQUEST_CONFIRM_CODE':
            return Object.assign({}, state, {
                isLoading: true,
                isAuthenticated: false,
            })
        case 'SUCCESS_CONFIRM_CODE':
            return Object.assign({}, state, {
                isLoading: false,
                isAuthenticated: true,

            })
        case 'FAILED_CONFIRM_CODE':
            return Object.assign({}, state, {
                isLoading: false,
                errorMessage: action.err,
                isAuthenticated: false,
            })
        case 'REQUEST_AUTO_AUTHENTICATION':
            return Object.assign({}, state, {
                isAutoAuthenticated: false,
                isLoading: true,
                autoAuthenticate: {
                    isLoading: true
                },
            })
        case 'SUCCESS_AUTO_AUTHENTICATION':
            return Object.assign({}, state, {
                isAutoAuthenticated: true,
                isLoading: false,
                user: action.payload,
                autoAuthenticate: {
                    isLoading: false
                },
            })
        case 'FAILED_AUTO_AUTHENTICATION':
            return Object.assign({}, state, {
                isLoading: false,
                isAutoAuthenticated: false,
                autoAuthenticate: {
                    isLoading: false,
                },
            })
        case 'SIGN_OUT':
            return state;
        default:
            return state;
    }
}