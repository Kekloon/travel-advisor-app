import React, { Component } from 'react'
import { Item, Input, Toast, Form, Container, Content, Header, Body, Title, Button, Text, View, Icon, Footer, ListItem, Label, Alert } from 'native-base'
import { StyleSheet, Keyboard } from 'react-native'
import { CreateUserNavigate } from '../../User/UserNavigation'

class PhoneLoginComponent extends Component {
    constructor(props) {
        super(props)
        this.state = {
            phoneNumber: '',
            isLoading: false,
            invalidInput: false,
            isAuthenticated: false,
        }
        this.onChanged = this.onChanged.bind(this)
        this.onClick = this.onClick.bind(this)
    }
    onChanged(text) {
        this.setState({
            phoneNumber: text,
            invalidInput: false
        })
    }

    onBlur() {
        if (_.isEmpty(this.state.phoneNumber)) {
            this.setState({ invalidInput: true })
            return false
        }
        if (this.state.phoneNumber.length < 10) {
            this.setState({ invalidInput: true })
            return false
        }
        return true
    }

    onClick() {
        if (this.onBlur()) {
            Keyboard.dismiss
            this.props.sendCode('+6' + this.state.phoneNumber)
        }
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            isAuthenticated: nextProps.isAuthenticated
        })
    }

    render() {
        const { invalidInput, isLoading,isAuthenticated } = this.state
        const {isUserExist}=this.props
        const UserNavigate = CreateUserNavigate(isUserExist)

        return (
            isAuthenticated ?
                <UserNavigate />
                :
                <Container style={styles.container}>
                    <View style={styles.wholeLayout}>
                        <Text style={styles.title}>Welcome to Travel Advisor</Text>
                        <Text style={styles.phoneLabel}>Please enter your phone number :</Text>
                        <View style={styles.phoneInput}>
                            <Item error={invalidInput}>
                                <Icon type="FontAwesome" name="phone" />
                                {/* <Label>Phone number</Label> */}
                                <Input style={styles.InputField}
                                    placeholder='Example: 0123456789'
                                    onChangeText={(value) => this.onChanged(value)}
                                    // value={this.state.phoneNumber}
                                    keyboardType='numeric'
                                    maxLength={11}
                                    onBlur={() => this.onBlur()}
                                >
                                </Input>
                                {!invalidInput ? null : <Icon name='close-circle' />}
                            </Item>
                            <Text style={{ height: 20 }}>
                                {
                                    !invalidInput ? null :
                                        <Text>Invalid Phone number ! !</Text>
                                }
                            </Text>
                        </View>
                        <Button full style={styles.buttonValidate} onPress={() => this.onClick()}>
                            <Text style={{ color: '#00a680', fontSize: 17 }}>Validate</Text>
                        </Button>
                    </View>
                </Container>
        )
    }
}


const styles = StyleSheet.create({
    wholeLayout: {
        // backgroundColor: '#00a680',

    },
    container: {
        backgroundColor: '#00a680',
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',

    },
    title: {
        fontSize: 25,
        textAlign: 'center',
        height: 60,
        color: '#ffffff'
    },
    phoneInput: {

    },
    InputField: {

    },

    flowLayout: {

    },
    buttonValidate: {
        backgroundColor: '#ffffff',
        borderStyle: 'solid',
        borderRadius: 10,
        borderColor: '#00c094',
        borderWidth: 1,
        marginTop: 15,

    },
    phoneLabel: {
        height: 20,
    }
})

export default PhoneLoginComponent