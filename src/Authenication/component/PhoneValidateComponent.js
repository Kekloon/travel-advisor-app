import React, { Component } from 'react'
import { View, Text, Button, Item, Input, Container,Toast } from 'native-base'
import { StyleSheet } from 'react-native'

class PhoneValidateComponent extends Component {
    constructor(props) {
        super(props)
        this.state = {
            code: null,
        }
        this.onChanged = this.onChanged.bind(this)
        this.onClicked = this.onClicked.bind(this)
    }

    onChanged(text) {
        this.setState({
            code: text.replace(/[^0-9]/g, '')
        })
    }

    onClicked() {
        if (_.isEmpty(this.state.code)) {
            Toast.show({ text: 'Please key in verification code!', type: 'danger' })
        }
        else {
            this.props.onClicked(this.state.code)
        }
    }

    render() {
        return (
           
                <Container style={styles.container}>

                    <Text style ={styles.title}> Verification </Text>
                    <Item
                        style={{
                            marginTop: 15,
                            marginBottom: 20
                        }}
                        regular>
                        <Input style={styles.Input}
                            placeholder="Verification Code"
                            onChangeText={(value) => this.onChanged(value)}
                            value={this.state.code}
                            keyboardType="numeric"
                            maxLength={9}
                        />
                    </Item>
                    <Button full style={styles.buttonValidate} onPress={() => this.onClicked()}>
                        <Text style={{ color: '#00a680', fontSize: 17 }}>Validate</Text>
                    </Button>
                </Container>
        )
    }
}


const styles = StyleSheet.create({
    Input: {
        fontSize:15,
        textAlign:'center',
        width:60
    },
    container: {
        backgroundColor: '#00a680',
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',

    },
    title: {
        fontSize: 25,
        textAlign: 'center',
        height: 60,
        color: '#ffffff'
    },
    buttonValidate: {
        backgroundColor: '#ffffff',
        borderStyle: 'solid',
        borderRadius: 10,
        borderColor: '#00c094',
        borderWidth: 1,
        marginTop: 15,

    },
    phoneLabel: {
        height: 20,
    }
})
export default PhoneValidateComponent