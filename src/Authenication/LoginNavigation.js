import React, { Component } from 'react'
import { StackNavigator } from 'react-navigation'
import { connect } from 'react-redux'

import PhoneLoginContainer from '../Authenication/container/PhoneLoginContainer'
import UserNavigation from '../User/UserNavigation';
import PhoneValidateContainer from '../Authenication/container/PhoneValidateContainer'
import { checkIsUserExist } from '../User/UserAction'


export const LoginNavigator = StackNavigator({
    PhoneLoginContainer: { screen: PhoneLoginContainer },
    PhoneValidateContainer: { screen: PhoneValidateContainer }
},
    {
        initialRouteName:'PhoneLoginContainer',
        headerMode:'none'

    })

class AutoLogin extends Component {
    constructor(props){
        super(props)
        this.state={
            isAutoAuthenticated:false
        }
    }


    componentWillMount(){
        // this.props.checkIsUserExist()
    }

    componentWillReceiveProps(nextProps){
        this.setState({
            isAutoAuthenticated:nextProps.isAutoAuthenticated
        })
    }

    // goToMainPage(){
    //   const navigate = Navigation.navigate({
    //       rounteName:'Tab',
    //       params:{},
    //   });
    //   this.props.appNavRef.dispatch(navigate) 
    // };

    render(){
        return(
            this.props.isAutoAuthenticated?
            <UserNavigation/>
            :
            <LoginNavigator/>
        )
    }
}

const mapStateToProps =(state)=>({
    isAutoAuthenticated:state.authenticationReducer.isAutoAuthenticated,
    // appNavRef:state.authenticationReducer.appNavRef
})

const mapDispatchToProps =(dispatch)=>({
    checkIsUserExist:()=>{
        dispatch(checkIsUserExist())
    }
})

export default connect (mapStateToProps,mapDispatchToProps)(AutoLogin)