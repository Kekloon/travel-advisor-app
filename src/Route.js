import React, { Component } from 'react'
import { connect } from 'react-redux'
import { TabNavigator, StackNavigator } from "react-navigation";
import { Icon } from 'native-base'
import { StatusBar } from 'react-native'

import Home from './modules/Comic/container/HomeContainer'
import { CategoryNavigator, BookshelfNavigator, HomeNavigator } from './modules/Comic/ComicNavigator'
import { getTopUpPackageList, getUserCredits } from './modules/User/UserAction'
import { getAllComics, getCategories, getSubsribedComics } from './modules/Comic/ComicAction'
import { getAllSlide } from './modules/Forum/ForumAction'
import { ForumNavigator } from './modules/Forum/ForumNavigator'


import MainLoading from './modules/MainLoading'
import { HomePageNavigator } from '../src/HomePage/HomePageNavigator'
import UserProfileNavigator from '../src/User/container/UserProfileContainer'
import { DestinationNavigator } from '../src/Destination/DesitnationNavigator'
import { HotelNavigator } from '../src/Hotel/HotelNavigator'
import { SaveAndHistoryNavigator } from '../src/SaveAndHistoryMenu/SaveAndHistoryMenuNavigator'
import { PrivateMessageNavigator } from '../src/privateMessage/privateMessageNavigator'
import { tripPlanNavigator } from '../src/tripPlan/tripPlanNavigator'
import { getUserData } from '../src/User/UserAction'

const MainTabNavigator = TabNavigator(
	{
		HomePage: {
			screen: HomePageNavigator,
			navigationOptions: {
				tabBarLabel: 'Home',
				tabBarIcon: () => <Icon name='home' />
			}
		},
		privateMessage: {
			screen: PrivateMessageNavigator,
			navigationOptions: {
				tabBarLabel: 'PM',
				tabBarIcon: () => <Icon type='Ionicons' name='ios-chatbubbles' />
			}
		},
		tripPlan:{
			screen:tripPlanNavigator,
			navigationOptions:{
				tabBarLabel:'Plan',
				tabBarIcon:()=><Icon type='FontAwesome' name='list-ul'/>
			}
		},
		Destination: {
			screen: DestinationNavigator,
			navigationOptions: {
				tabBarLabel: 'DES',
				tabBarIcon: () => <Icon type='Ionicons' name='md-globe' />
			}
		},
		Hotel: {
			screen: HotelNavigator,
			navigationOptions: {
				tabBarLabel: 'Hotel',
				tabBarIcon: () => <Icon type='FontAwesome' name='hotel' />
			}
		},
		SaveAndHistory: {
			screen: SaveAndHistoryNavigator,
			navigationOptions: {
				tabBarLabel: 'S & H',
				tabBarIcon: () => <Icon type='FontAwesome' name='history' />
			}
		},
		UserProfile: {
			screen: UserProfileNavigator,
			navigationOptions: {
				tabBarLabel: 'Me',
				tabBarIcon: () => <Icon type='FontAwesome' name='user-secret' />
			}

		},
		
		// Bookshelf: {
			// 	screen: BookshelfNavigator,
			// 	navigationOptions: {
				// 		tabBarLabel: 'Bookshelf',
				// 		tabBarIcon: () => <Icon name='bookmarks' size={35} />
				// 	}
				// },
				// User: {
					// 	screen: UserProfile,
					// 	style: { display: 'none' },
					// 	navigationOptions: {
						// 		tabBarLabel: 'User',
						// 		tabBarIcon: () => <Icon name='contact' size={35} />
						// 	}
		// },
	},
	{
		tabBarOptions: {
			activeTintColor: '#00a680',
			inactiveTintColor: 'gray',
			style: {
				backgroundColor: '#EEEEEE',
			},
			indicatorStyle: {
				backgroundColor: '#00a680',
			},
			labelStyle: {
				fontSize: 10,
				margin: 1
			},
			showIcon: true,
			showLabel: true
		},
		tabBarPosition: 'bottom',
		animationEnabled: true,
		swipeEnabled: true
	});


class Main extends Component {

	componentWillMount() {
		this.props.getUserData()
	}

	render() {
		return (
			this.props.isLoading ?
				<MainLoading />
				:
				<MainTabNavigator />
		)
	}
}

const mapStateToProps = (state) => ({
	isLoading: state.userReducer.isLoading,
	// isFetching: state.comic.getAllComics.isFetching
})

const mapDispatchToProps = (dispatch) => ({
	getUserData: () => {
		dispatch(getUserData())
	},
})

export default connect(mapStateToProps, mapDispatchToProps)(Main)