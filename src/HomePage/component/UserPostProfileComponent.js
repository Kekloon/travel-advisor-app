import React, { Component } from 'react'
import { connect } from 'react-redux'
import { TouchableHighlight, Dimensions, Image, FlatList, StyleSheet, StatusBar, TouchableOpacity, ScrollView, Modal, RefreshControl } from 'react-native'
import { Container, Item, Text, Icon, View, Toast, Card, ListItem, Body, List, Thumbnail, Input, CardItem, Left, Right, Button, Content, Spinner, Header } from 'native-base'
import UserPostCommentContainer from '../container/UserPostCommentContainer'
import moment from 'moment'
import _ from 'lodash'
import ImageSlider from 'react-native-image-slider';


const { width, height } = Dimensions.get('window')

class UserPostProfileComponent extends Component {
    constructor(props) {
        super(props)
        this.state = {
            userData: {},
            phoneWidth: width,
            userPosts: [],
            isLoading: true,
            isSended: false,
            commentList: [],
            isSending: false,
            passPost: [],
            refreshing: false,
            modalVisible: false,
            user: {},
            checkIsFollowing: false,
            passUser: {}

        }
        this.checkIsLiked = this.checkIsLiked.bind(this)
        this.handleBtnLike = this.handleBtnLike.bind(this)
        this.setModalVisible = this.setModalVisible.bind(this)
        this.handleBack = this.handleBack.bind(this)
        this._onRefresh = this._onRefresh.bind(this)
        // this.checkIsFollowing = this.checkIsFollowing.bind(this)
        this.unfollow = this.unfollow.bind(this)
        this.follow = this.follow.bind(this)
    }

    componentWillMount() {
        this.setState({
            user:this.props.user,
            passUser: this.props.navigation.state.params.passUser,
        })
    }
    
    componentDidMount(){
        this.props.getUserPost(this.state.passUser)
        this.props.getOnceUserData(this.state.passUser)
        this.props.checkIsFollowingUser(this.state.passUser)

    }
    componentWillReceiveProps(nextProps) {
        if (!_.isEmpty(nextProps.userPosts)) {
            this.setState({
                userPosts: _.orderBy(nextProps.userPosts, ['postCreateDate'], ['desc']),
                isLoading: nextProps.isLoading,
                userData: nextProps.userData,
                checkIsFollowing:nextProps.checkIsFollowing
            })
        }
        else{
                this.setState({
                userPosts: [],
                isLoading: nextProps.isLoading,
                userData: nextProps.userData,
                checkIsFollowing:nextProps.checkIsFollowing 
                })
            }
    }

    handleBack() {
        this.setState({
            modalVisible: false
        })
    }

    _onRefresh() {
        this.setState({ refreshing: true });
        this.props.getUserPost(this.state.passUser)
        this.setState({ refreshing: false })
    }

    setModalVisible(val) {
        this.setState({
            modalVisible: true,
            passPost: val
        })
    }

    handleBtnLike(post) {
        const index = _.findIndex(this.state.user, { id: post.postId })
        const posts = this.state.userPosts
        if (_.find(post.postLikes, { user: this.state.user.userId })) {
            const userId = this.state.user.userId
            const upDateLikesArray = _.pullAllBy(post.postLikes, [{ 'user': userId }], 'user');
            const updatePost = {
                ...post,
                postLikes: upDateLikesArray
            }
            this.setState({
                posts,
            })
            this.props.setLikePost(false, post.postId)
        }
        else {
            const upDateLikesArray = post.postLikes
            upDateLikesArray.push({ user: this.state.user.userId })
            const updatePost = {
                ...post,
                postLikes: upDateLikesArray
            }
            this.setState({
                posts,
            })
            this.props.setLikePost(true, post.postId)
        }

    }

    checkIsLiked(likedUserArray) {
        const userId = this.state.user.userId
        if (_.find(likedUserArray, { user: userId })) {
            return true
        }
        else {
            return false
        }
    }

    // checkIsFollowing(user, userData) {
    //     if (_.find(user.userFollowing = userData.userId)) {
    //         this.setState({
    //             checkIsFollowing: true
    //         })
    //     }
    //     else {
    //         this.setState({
    //             checkIsFollowing: false
    //         })
    //     }
    // }
    follow() {
        this.setState({
            checkIsFollowing: true
        })
        this.props.setFollow(this.state.userData, this.state.user, false)
    }

    unfollow() {
        this.setState({
            checkIsFollowing: false
        })
        this.props.setFollow(this.state.userData, this.state.user, true)

    }

    renderPost(val, key) {
        const defaultProfileImage = require('../../assets/user_avatar.png')
        const liked = this.checkIsLiked(val.postLikes)
        return (
            <Card key={key}>
                <CardItem>
                    <Thumbnail small source={!_.isEmpty(val.owner.userProfilePicture) ? { uri: val.owner.userProfilePicture } : defaultProfileImage} />
                    <Body style={{ marginLeft: 10 }}>
                        <Text>{val.owner.userName}</Text>
                        <Text note>{moment(val.postCreateDate, 'day').fromNow()}</Text>
                    </Body>
                </CardItem>
                <CardItem>
                    <Left style={{ marginLeft: -20, marginTop: -15 }}>
                        <Text>{val.postMessage}</Text>
                    </Left>
                </CardItem>
                <CardItem cardBody>
                    {
                        <ImageSlider style={{ width: this.state.phoneWidth - 20, height: 250, alignSelf: 'center' }}
                            images={val.postImage}
                            customButtons={(position, move) => (
                                <View style={styles.buttons}>
                                    {val.postImage.map((image, index) => {
                                        return (
                                            <TouchableHighlight
                                                key={index}
                                                underlayColor="#ccc"
                                                onPress={() => move(index)}
                                                style={styles.button}
                                            >
                                                <Text style={position === index && styles.buttonSelected}>
                                                    -
                                                 </Text>
                                            </TouchableHighlight>
                                        );
                                    })}
                                </View>
                            )}
                        />
                    }
                </CardItem>
                <CardItem>
                    <Left>
                        <Button transparent
                            onPress={() => this.handleBtnLike(val)}
                        >
                            <Icon style={{ color: liked ? '#ED4A6A' : 'gray' }} name="heart" />
                            <Text>{val.postLikes.length} likes</Text>
                        </Button>
                    </Left>
                    <Right>
                        <Button style={{ marginRight: -15 }} transparent
                            onPress={() => this.setModalVisible(val)}
                        >
                            <Icon type='FontAwesome' active name="comments" />
                            <Text>{val.postComments.length} Comments</Text>
                        </Button>
                    </Right>
                </CardItem>
            </Card>
        )
    }


    render() {
        let { postWidth } = this.state.phoneWidth
        const defaultProfileImage = require('../../assets/user_avatar.png')      
        return (
            <Container>
                <Modal
                    animationType="slide"
                    transparent={true}
                    visible={this.state.modalVisible}
                    onRequestClose={() => { }}>
                    <Header style={{ backgroundColor: 'white' }}>
                        <View style={{ position: 'absolute', left: 1 }}>
                            <Button transparent onPress={this.handleBack}
                            >
                                <Icon name='arrow-back' />
                            </Button>
                        </View>
                        <View>
                            <Text style={{ alignSelf: 'center', fontSize: 25, marginTop: 5 }}>Comments</Text>
                        </View>
                        <View style={{ position: 'absolute', right: 1, marginTop: 7, marginRight: 5 }}>
                        </View>
                    </Header>
                    <UserPostCommentContainer post={this.state.passPost} userCommented={this.state.userCommented} user={this.state.user} />
                </Modal>
                <ScrollView
                    refreshControl={
                        <RefreshControl
                            refreshing={this.state.refreshing}
                            onRefresh={this._onRefresh}
                        />
                    }
                >
                    {
                        !_.isEmpty(this.state.userData) ?
                            <Card style={{ height: 140, borderRadius: 20 }}>
                                <CardItem style={{ height: 140, width: width }}>
                                    <Left style={{ maxWidth: 110 }}>
                                        <View>
                                            <Thumbnail large source={!_.isEmpty(this.state.userData.userProfilePicture) ? { uri: this.state.userData.userProfilePicture } : defaultProfileImage} />
                                            <Text style={{ marginLeft: -5, textAlign: 'center', fontWeight: 'bold' }}>{this.state.userData.userName}</Text>
                                        </View>
                                    </Left>
                                    <Body marginTop={10}>
                                        <View style={{ flex: 1, flexDirection: 'row' }}>
                                            <View style={{ width: 60 }}>
                                                <Text style={{ textAlign: 'center', fontWeight: 'bold', fontSize: 20 }}>{this.state.userData.userPost.length}</Text>
                                                <Text style={{ textAlign: 'center', color: 'gray' }}>posts</Text>
                                            </View>
                                            <View style={{ width: 80 }}>
                                                <Text style={{ textAlign: 'center', fontSize: 20, fontWeight: 'bold' }}>{!_.isEmpty(this.state.userData.userFollowers) ? this.state.userData.userFollowers.length : 0}</Text>
                                                <Text style={{ textAlign: 'center', color: 'gray' }}>followers</Text>
                                            </View>
                                            <View style={{ width: 80 }}>
                                                <Text style={{ textAlign: 'center', fontSize: 20, fontWeight: 'bold' }}>{!_.isEmpty(this.state.userData.userFollowing) ? this.state.userData.userFollowing.length : 0}</Text>
                                                <Text style={{ textAlign: 'center', color: 'gray' }}>following</Text>
                                            </View>
                                        </View>
                                        {
                                            this.state.user.userId == this.state.userData.userId ?
                                                <Button disabled={true} full style={{ height: 35, marginBottom: 12, borderRadius: 20 }}>
                                                    <Text>Following</Text>
                                                </Button>
                                                : this.state.checkIsFollowing ?
                                                    <Button
                                                        onPress={() => this.unfollow()}
                                                        full style={{ height: 35, marginBottom: 12, backgroundColor: '#00a680', borderRadius: 20 }}>
                                                        <Text>Unfollow</Text>
                                                    </Button>
                                                    : <Button
                                                        onPress={() => this.follow()}
                                                        full style={{ height: 35, marginBottom: 12, backgroundColor: '#00a680', borderRadius: 20 }}>
                                                        <Text>Follow</Text>
                                                    </Button>
                                        }
                                    </Body>
                                </CardItem>
                            </Card>
                            :
                            null
                    }
                    {
                        this.state.isLoading ?
                            <Spinner />
                            :
                            _.isEmpty(this.state.userPosts) ?
                                <Text style={{ color: 'gray', fontSize: 25, alignSelf: 'center', marginTop: 30 }}>Currently no any post :(</Text>
                                :
                                this.state.userPosts.map((val, key) => {
                                    return this.renderPost(val, key)
                                })
                    }
                </ScrollView>
            </Container >
        )

    }

}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    scrollViewContainer: {
        flexDirection: 'row',
        backgroundColor: '#222',
    },
    image: {
        width: 200,
        height: '100%',
    },
    buttons: {
        height: 15,
        marginTop: -25,
        marginBottom: 10,
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',

    },
    button: {
        margin: 3,
        width: 8,
        height: 8,
        borderRadius: 8 / 2,
        backgroundColor: 'gray',
        opacity: 0.9,
    },
    buttonSelected: {
        opacity: 1,
        borderRadius: 8 / 2,
        backgroundColor: '#00a680',
        fontSize: 100,
        width: 8,
        height: 8,
    },
});


export default UserPostProfileComponent