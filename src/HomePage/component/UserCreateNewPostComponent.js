import React, { Component } from 'react'
import { Image, Dimensions, TouchableHighlight, TouchableOpacity,StyleSheet } from 'react-native'
import { Container, Header, Content, View, Item, Input, Button, Text, Footer, CardItem, Left, Body, Thumbnail, Right, Icon, Card, Textarea } from 'native-base'
import ImagePicker from 'react-native-image-crop-picker'
import ActualImageComponent from '../../common/ActualImageComponent'
import _ from 'lodash'
import moment from 'moment'
import ImageSlider from 'react-native-image-slider';
import { combineReducers } from 'redux';

const { width } = Dimensions.get('window')

class UserCreateNewPostComponent extends Component {
    constructor(props) {
        super(props)
        this.state = {
            user: null,
            imageFile: [],
            message: '',
            isLoading: false,
            post: {},
            phoneWidth: width,
            isSuccess:false,

        }
        this.handleBack = this.handleBack.bind(this)
    }

    componentWillMount() {
        this.setState({
            user: this.props.user
        })
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            isLoading: nextProps.isLoading,
            isSuccess:nextProps.isSuccess
        })
        if(this.state.isSuccess){
            this.props.navigation.navigate('MainPage')
            this.setState({
                isSuccess:false,
                message:{},
                imageFile:[]
            })
        }

    }
    handleBack() {
        this.props.navigation.navigate('MainPage')
    }

    handleSelectImageFile() {
        this.setState({
            imageFile: []
        })
        ImagePicker.openPicker({
            cropping: false,
            multiple: true,
        }).then(image => {
            this.setState({
                imageFile: image,
            })
        }).catch(e=>{
            console.log('failed')
    })
    }

    handleCreateNewPost() {
        this.setState({
            post: {
                owner: this.state.user,
                message: this.state.message,
                imageFile: this.state.imageFile,
            }
        }, () => this.props.createPost(this.state.post))
    }




    render() {
        const defaultProfileImage = require('../../assets/user_avatar.png')
        const { user, isLoading } = this.state
        const imagePath=[]
        return (
            <Container style={{ backgroundColor: '#FFFFFF' }}>
                <Header style={{ backgroundColor: '#00a680' }}>
                    <View style={{ position: 'absolute', left: 1 }}>
                        <Button transparent onPress={this.handleBack}
                        >
                            <Icon style={{ color: '#FFFFFF' }} name='arrow-back' />
                        </Button>
                    </View>
                    <View>
                        <Text style={{ alignSelf: 'center', fontSize: 25, color: '#FFFFFF', marginTop: 5 }}>Create New Post</Text>
                    </View>
                    <View style={{ position: 'absolute', right: 1, marginTop: 7, marginRight: 5 }}>
                        <TouchableOpacity
                            disabled={_.isEmpty(this.state.imageFile) || _.isEmpty(this.state.message) ? true : false}
                            onPress={() => this.handleCreateNewPost()}
                        >
                            {_.isEmpty(this.state.imageFile) || _.isEmpty(this.state.message) ?
                                <Icon type='FontAwesome' name='send-o' style={{ color: 'gray', fontSize: 16, marginTop: 5, fontSize:25 }}/> :
                                <Icon type='FontAwesome' name='send-o' style={{ color: 'white', fontSize: 16, marginTop: 5,fontSize:25 }}/> 
                            }
                        </TouchableOpacity>
                    </View>
                </Header>
                <Content>
                    <CardItem>

                        <Left>
                        <Thumbnail source={!_.isEmpty(this.state.user.userProfilePicture) ? { uri: this.state.user.userProfilePicture } : defaultProfileImage} />
                            <Text style={{ fontSize: 30, marginLeft: 20 }}>{user.userName}</Text>
                        </Left>
                        <Right>
                            <Button transparent style={{ marginRight: 10 }}
                                onPress={() => this.handleSelectImageFile()}
                            >
                                <Icon style={{ fontSize: 30, color: '#00a680' }} type='FontAwesome' name='photo' />
                            </Button>
                            <Text style={{ marginRight: 5, marginTop: -10 }}>Photo</Text>
                        </Right>
                    </CardItem>
                    <Card transparent>
                        <Textarea
                            rowSpan={4}
                            placeholder='Type your message here...'
                            multiline={true}
                            value={this.state.message}
                            onChangeText={(text) => this.setState({ message: text })}
                        />
                        {
                            
                            this.state.imageFile.map((image, index) => {
                                imagePath.push(image.path)
                            })
                        }
                        <View style={{ marginTop: 10, margin: 5 }}>
                            {
                                !_.isEmpty(this.state.imageFile) ?
                                <ImageSlider style={{width:this.state.phoneWidth-20, height:250, alignSelf:'center'}}
                                images={imagePath}
                                customButtons={(position, move) => (
                                    <View style={styles.buttons}>
                                      {imagePath.map((image, index) => {
                                        return (
                                          <TouchableHighlight 
                                            key={index}
                                            underlayColor="#ccc"
                                            onPress={() => move(index)}
                                            style={styles.button}
                                          >
                                            <Text style={position === index && styles.buttonSelected}>
                                            -
                                            </Text>
                                              
                                          </TouchableHighlight>
                                        );
                                      })}
                                    </View>
                                  )}
                            />
                            :
                            null
                    }
                        </View>
                    </Card>
                </Content>
            </Container>
        )
    }

}

const styles = StyleSheet.create({
    container: {
      flex: 1,
    },
    scrollViewContainer: {
      flexDirection: 'row',
      backgroundColor: '#222',
    },
    image: {
      width: 200,
      height: '100%',
    },
    buttons: {
      height: 15,
      marginTop: -25,
      marginBottom: 10,
      justifyContent: 'center',
      alignItems: 'center',
      flexDirection: 'row',
      
    },
    button: {
      margin: 3,
      width: 8,
      height: 8,
      borderRadius: 8 / 2,
      backgroundColor: 'gray',
      opacity: 0.9,
    },
    buttonSelected: {
      opacity: 1,
      borderRadius: 8 / 2,
      backgroundColor:'#00a680',
      fontSize:100,
      width: 8,
      height: 8,
 

    },
  });

export default UserCreateNewPostComponent