import React, { Component } from 'react'
import { StyleSheet, ScrollView, Image, TouchableHighlight, FlatList, Dimensions } from 'react-native'
import { Container, Content, View, Text, Button, Icon, List, ListItem, Card, CardItem, Item, Input, Body, Left, Right, Thumbnail, Toast, Spinner, Header, Footer } from 'native-base'
import moment from 'moment'
import _ from 'lodash'

const { width } = Dimensions.get('window')

class UserPostCommentComponent extends Component {
    constructor(props) {
        super(props)
        this.state = {
            post: null,
            isLoading: false,
            commentMessage: {},
            isSended: false,
            commentList: [],
            isSending: false,
            user: {},
            userCommented:0
        }
    }

    componentDidMount() {
        this.setState({
            post: this.props.post,
            user: this.props.user,
            userCommented:this.props.userCommented
        })
        this.props.getComments(this.props.post.postId)
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            commentList: nextProps.commentList,
            isLoading: nextProps.isLoading,
            isSended: nextProps.isSended,
            isSending: nextProps.isSending
        })
    }

    handleBack() {
        this.props.navigation.navigate('MainPage')
    }

    renderComment(data) {
        const defaultProfileImage = require('../../assets/user_avatar.png')
        return (
            <ListItem avatar>
                <Left style={{marginTop:-20}}>
                    {
                    data.sender.userProfilePicture ?
                        <Thumbnail small source={{ uri: data.sender.userProfilePicture }} />
                        :
                        <Thumbnail small source={defaultProfileImage} />
                    }
                </Left>
                <Body style={{marginLeft:5,marginRight:5}}>
                    <Card style={{borderRadius:15,backgroundColor:'#F5F5F5'}}>
                        <Text style={{marginLeft:10, fontWeight:'bold'}}>{data.sender.userName}</Text>
                        <Text style={{marginLeft:10}}>{data.commentMessage}</Text>
                    </Card>
                    <Text style={{marginLeft:10}} note>{moment(data.commentDate, 'day').fromNow()}</Text>
                </Body>
            </ListItem>
        )
    }

    handleBtnSend() {
        if (!_.isEmpty(this.state.commentMessage)) {
            const comment = {
                message: this.state.commentMessage,
                post: this.state.post
            }
            this.props.sendComment(comment)
            const count = this.state.userCommented+1
            this.setState({
                commentMessage: {},
                userCommented:count
            })
        }

    }

    render() {
        const defaultProfileImage = require('../../assets/user_avatar.png')
        return (
            <Container style={{ backgroundColor: 'white' }}>
                {
                        <FlatList
                            inverted
                            ref='flatlist'
                            keyExtractor={(item, index) => index}
                            data={_.orderBy(this.state.commentList, ['commentDate'], ['desc'])}
                            renderItem={({ item }) => this.renderComment(item)}
                        // ListHeaderComponent={this.renderFlatListHeader()}
                        />

                }
                <View style={{ position: 'relative', bottom: 1 }}>
                    <Item regular style={{ borderTopWidth: 1, height: 55 }}>
                        <Left style={{marginLeft:5}}>
                            {!_.isEmpty(this.state.user) ?
                         <Thumbnail small source={!_.isEmpty(this.state.user.userProfilePicture) ? { uri: this.state.user.userProfilePicture } : defaultProfileImage} />
                        : null
                            }
                        </Left>
                        <Body>
                            <Input placeholder='   Write a comment...'
                                style={{ backgroundColor: '#F5F5F5', borderRadius: 30, width: 250, margin: 5 }}
                                value={this.state.commentMessage}
                                disabled={this.state.isSending}
                                onChangeText={(val) => this.setState({ commentMessage: val })}
                            />
                        </Body>
                        <Right>
                            {
                                this.state.isSending ?
                                    <Spinner />
                                    :
                                    <Button transparent onPress={() => this.handleBtnSend()}
                                    >
                                        <Icon type='Ionicons' name='md-send' style={{ color: _.isEmpty(this.state.commentMessage) ? 'gray' : '#00a680', margin: 5 }} />
                                    </Button>
                            }
                        </Right>
                    </Item>
                </View>
            </Container>
        )
    }
}

export default UserPostCommentComponent