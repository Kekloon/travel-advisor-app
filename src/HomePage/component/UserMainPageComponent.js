import React, { Component } from 'react'
import { connect } from 'react-redux'
import { TouchableHighlight, Dimensions, Image, FlatList, StyleSheet, StatusBar, TouchableOpacity, ScrollView, Modal, RefreshControl } from 'react-native'
import { Container, Item, Text, Icon, View, Toast, Card, ListItem, Body, List, Thumbnail, Input, CardItem, Left, Right, Button, Content, Spinner, Header } from 'native-base'
import ImageSlider from 'react-native-image-slider';
import moment from 'moment'
import { combineReducers, compose } from 'redux';
import _ from 'lodash'
import UserPostCommentContainer from '../container/UserPostCommentContainer'

const { width, height } = Dimensions.get('window')
class UserMainPageComponent extends Component {
    constructor(props) {
        super(props)
        this.state = {
            user: {},
            phoneWidth: width,
            posts: [],
            isLoading: true,
            tempPost: [],
            modalVisible: false,
            passPost: [],
            refreshing: false,
            userCommented: 0,
            firstime:true,

        }
        this.handleCreateNewPost = this.handleCreateNewPost.bind(this)
        this.checkIsLiked = this.checkIsLiked.bind(this)
        this.handleBtnLike = this.handleBtnLike.bind(this)
        this.setModalVisible = this.setModalVisible.bind(this)
        this.handleBack = this.handleBack.bind(this)
        this._onRefresh = this._onRefresh.bind(this)
    }

    componentWillMount() {
        this.setState({
            user: this.props.user
        
        })
    }
    
    componentDidMount() {
        if(this.state.firstime){
            this.props.getPostList()
            this.setState({
                firstime:false,
            })
        }
    }

    componentWillReceiveProps(nextProps) {
        console.log(nextProps.posts)
        this.setState({
            posts: _.orderBy(nextProps.posts, ['postCreateDate'], ['desc']),
            isLoading: nextProps.isLoading,
        })
    }

    handleBack() {
        this.setState({
            modalVisible: false,
        })
    }

    _onRefresh() {
        this.setState({ refreshing: true });
        this.props.getPostList(this.props.user)
        this.setState({ refreshing: false });
        this.setState({
            user: this.props.user
        })
    }

    setModalVisible(val) {
        this.setState({
            modalVisible: true,
            passPost: val
        });
    }

    handleBtnLike(post) {
        const index = _.findIndex(this.state.posts, { id: post.postId })
        const posts = this.state.posts
        if (_.find(post.postLikes, { user: this.state.user.userId })) {
            const userId = this.state.user.userId
            const upDateLikesArray = _.pullAllBy(post.postLikes, [{ 'user': userId }], 'user');
            const updatePost = {
                ...post,
                postLikes: upDateLikesArray
            }
            this.setState({
                posts,
            })
            this.props.setLikePost(false, post.postId)
        }
        else {
            const upDateLikesArray = post.postLikes
            upDateLikesArray.push({ user: this.state.user.userId })
            const updatePost = {
                ...post,
                postLikes: upDateLikesArray
            }
            this.setState({
                posts,
            })
            this.props.setLikePost(true, post.postId)
        }

    }

    checkIsLiked(likedUserArray) {
        const userId = this.state.user.userId
        if (_.find(likedUserArray, { user: userId })) {
            return true
        }
        else {
            return false
        }
    }


    handleCreateNewPost() {
        this.props.navigation.navigate('UserCreatePost')
    }

    renderPost(val, key) {
        console.log('1')
        const defaultProfileImage = require('../../assets/user_avatar.png')
        const liked = this.checkIsLiked(val.postLikes)
        return (
            <Card key={key}>
                <CardItem>
                    <TouchableOpacity
                        onPress={() => this.props.navigation.navigate('UserPostProfile', { passUser: val.owner.userId })}>
                        <Thumbnail small source={!_.isEmpty(val.owner.userProfilePicture) ? { uri: val.owner.userProfilePicture } : defaultProfileImage} />
                    </TouchableOpacity>
                    <Body style={{ marginLeft: 10 }}>
                        <Text>{val.owner.userName}</Text>
                        <Text note>{moment(val.postCreateDate, 'day').fromNow()}</Text>
                    </Body>
                </CardItem>
                <CardItem>
                    <Left style={{ marginLeft: -20, marginTop: -15 }}>
                        <Text>{val.postMessage}</Text>
                    </Left>
                </CardItem>
                <CardItem cardBody>
                    {
                        <ImageSlider style={{ width: this.state.phoneWidth - 20, height: 250, alignSelf: 'center' }}
                            images={val.postImage}
                            customButtons={(position, move) => (
                                <View style={styles.buttons}>
                                    {val.postImage.map((image, index) => {
                                        return (
                                            <TouchableHighlight
                                                key={index}
                                                underlayColor="#ccc"
                                                onPress={() => move(index)}
                                                style={styles.button}
                                            >
                                                <Text style={position === index && styles.buttonSelected}>
                                                    -
                                                 </Text>
                                            </TouchableHighlight>
                                        );
                                    })}
                                </View>
                            )}
                        />
                    }
                </CardItem>
                <CardItem>
                    <Left>
                        <Button transparent
                            onPress={() => this.handleBtnLike(val)}
                        >
                            <Icon style={{ color: liked ? '#ED4A6A' : 'gray' }} name="heart" />
                            <Text>{val.postLikes.length} likes</Text>
                        </Button>
                    </Left>
                    <Right>
                        <Button style={{ marginRight: -15 }} transparent
                            onPress={() => this.setModalVisible(val)}
                        >
                            <Icon type='FontAwesome' active name="comments" />
                            <Text>{val.postComments.length} Comments</Text>
                        </Button>
                    </Right>
                </CardItem>
            </Card>
        )
    }

    render() {
        let { postWidth } = this.state.phoneWidth
        const defaultProfileImage = require('../../assets/user_avatar.png')
        return (
            <Container>
                <Modal
                    animationType="slide"
                    transparent={true}
                    visible={this.state.modalVisible}
                    onRequestClose={() => {}}>
                    <Header style={{ backgroundColor: 'white' }}>
                        <View style={{ position: 'absolute', left: 1 }}>
                            <Button transparent onPress={this.handleBack}
                            >
                                <Icon name='arrow-back' />
                            </Button>
                        </View>
                        <View>
                            <Text style={{ alignSelf: 'center', fontSize: 25, marginTop: 5 }}>Comments</Text>
                        </View>
                        <View style={{ position: 'absolute', right: 1, marginTop: 7, marginRight: 5 }}>
                        </View>
                    </Header>
                    <UserPostCommentContainer post={this.state.passPost} userCommented={this.state.userCommented} user={this.state.user} />
                </Modal>
                <ScrollView
                // stickyHeaderIndices={[1]}
                // showsVerticalScrollIndicator={false}
                    refreshControl={
                        <RefreshControl
                            refreshing={this.state.refreshing}
                            onRefresh={this._onRefresh}
                        />
                    }
                >
                <Header style={{ backgroundColor: '#00a680' }}>
                        <Left style={{maxWidth:70}}>
                        <Icon style={{ backgroundColor: '#00a680',color:'white',fontSize:30 }} name="ios-search" />
                        </Left>
                        <Body marginLeft={-40} maxWidth={width} style={{maxWidth:width,borderBottomWidth:1,maxHeight:60,textDecorationLine:'none',borderBottomColor:'dimgray'}}>
                        <Item style={{ borderBottomColor:'transparent'}}>
                        <TouchableOpacity
                        onPress={()=>this.props.navigation.navigate('UserSearch')}
                        >
                        <Text style={{ padding: 3, width: width, color: '#E4E2E2',fontSize:18 }}>Search</Text>
                        </TouchableOpacity>
                        </Item>
                        </Body>
                        <Right style={{maxWidth:30}}>
                        <Icon  style={{ backgroundColor: '#00a680',color:'white',fontSize:35 }} name="ios-people" />
                        </Right>
                </Header>
                    <TouchableOpacity
                        onPress={() => this.handleCreateNewPost()}
                        style={{ height: 80 }}
                    >
                        <Card>
                            <CardItem>
                                <Left>
                                    {
                                        !_.isEmpty(this.state.user) ?
                                            <TouchableOpacity
                                                onPress={() => this.props.navigation.navigate('UserPostProfile', { passUser: this.state.user.userId })}
                                            >
                                                <Thumbnail small source={!_.isEmpty(this.state.user.userProfilePicture) ? { uri: this.state.user.userProfilePicture } : defaultProfileImage} />
                                            </TouchableOpacity>
                                            :
                                            null
                                    }
                                    <Item rounded style={{ marginLeft: 12 }}>
                                        <TouchableOpacity
                                            onPress={() => this.handleCreateNewPost()}
                                            style={{ marginLeft: 12 }}
                                        >
                                            <Text style={{ padding: 6, width: this.state.phoneWidth - 150, color: 'gray' }}> What's on your mind</Text>
                                        </TouchableOpacity>
                                    </Item>
                                    <Icon style={{ marginLeft: 15, color: '#00a680' }} type='FontAwesome' name='tripadvisor' />
                                </Left>
                            </CardItem>
                        </Card>
                    </TouchableOpacity>
                    {
                        this.state.isLoading ?
                            <Spinner />
                            :
                            _.isEmpty(this.state.posts) ?
                                <Text style={{ color: 'gray', fontSize: 25, alignSelf: 'center', marginTop: 30 }}>Currently no any post :(</Text>
                                :
                                this.state.posts.map((val, key) => {
                                    return this.renderPost(val, key)
                                })
                    }
                </ScrollView>
            </Container >
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    scrollViewContainer: {
        flexDirection: 'row',
        backgroundColor: '#222',
    },
    image: {
        width: 200,
        height: '100%',
    },
    buttons: {
        height: 15,
        marginTop: -25,
        marginBottom: 10,
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',

    },
    button: {
        margin: 3,
        width: 8,
        height: 8,
        borderRadius: 8 / 2,
        backgroundColor: 'gray',
        opacity: 0.9,
    },
    buttonSelected: {
        opacity: 1,
        borderRadius: 8 / 2,
        backgroundColor: '#00a680',
        fontSize: 100,
        width: 8,
        height: 8,
    },
});


export default UserMainPageComponent

