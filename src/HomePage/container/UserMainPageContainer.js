import React, { Component } from 'react'
import UserMainPageComponent from '../component/UserMainPageComponent'
import { connect } from 'react-redux'
import { getPostList, setLikePost } from '../UserHomePageAction';

const mapStateToProps = (state) => ({
    posts: state.homePageReducer.getPostListt.posts,
    isLoading: state.homePageReducer.getPostListt.isLoading,
    user: state.userReducer.user

})

const mapDispatchToProps = (dispatch) => ({
    getPostList: () => {
        dispatch(getPostList())
    },
    setLikePost: (like, postId) => {
        dispatch(setLikePost(like, postId))
    }
})

export default connect(mapStateToProps, mapDispatchToProps)(UserMainPageComponent)