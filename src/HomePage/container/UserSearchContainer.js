import UserSearchComponent from '../component/UserSearchComponent'
import { connect } from 'react-redux'
import { getUserList } from '../UserHomePageAction'

const mapStateToProps =(state)=>({
    userList:state.homePageReducer.getUserList.userList,
    isLoading:state.homePageReducer.getUserList.isLoading
})

const mapDispatchToProps = (dispatch)=>({
    getUserList:()=>{
        dispatch(getUserList())
    },

})

export default connect(mapStateToProps,mapDispatchToProps)(UserSearchComponent)