import { connect } from 'react-redux'
import UserCreateNewPostComponent from '../component/UserCreateNewPostComponent'
import {createPost} from '../UserHomePageAction'

const mapStateToProps =(state)=>({
    isLoading:state.homePageReducer.createPost.isLoading,
    isSuccess:state.homePageReducer.createPost.isSuccess,
    user: state.userReducer.user,
})

const mapDispatchToProps =(dispatch, navigator)=>({
    createPost: (post) => {
        dispatch(createPost(post))
    }
})


export default connect (mapStateToProps,mapDispatchToProps)(UserCreateNewPostComponent)