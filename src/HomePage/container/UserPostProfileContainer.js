import UserPostProfileComponent from '../component/UserPostProfileComponent'
import { connect } from 'react-redux'
import { getUserPost, setLikePost, getOnceUserData, setFollow, checkIsFollowingUser } from '../UserHomePageAction'

const mapStateToProps = (state) => ({
    userPosts: state.homePageReducer.getUserPost.userPosts,
    isLoading: state.homePageReducer.getUserPost.isLoading,
    userData: state.homePageReducer.getOnceUserData.userData,
    user: state.userReducer.user,
    checkIsFollowing:state.homePageReducer.checkFollowing.checkIsFollowing
})

const mapDispatchToProps = (dispatch) => ({
    getUserPost: (user) => {
        dispatch(getUserPost(user))
    },
    setLikePost: (like, postId) => {
        dispatch(setLikePost(like, postId))
    },
    getOnceUserData: (user) => {
        dispatch(getOnceUserData(user))
    },
    setFollow: (userData, user, isFollowing) => {
        dispatch(setFollow(userData, user, isFollowing))
    },
    checkIsFollowingUser: (userData) => {
        dispatch(checkIsFollowingUser(userData))
    }
})

export default connect(mapStateToProps, mapDispatchToProps)(UserPostProfileComponent)