import { connect } from 'react-redux'
import UserPostCommentComponent from '../component/UserPostCommentComponent'
import {getComments,sendComment} from '../UserHomePageAction'

const mapStateToProps=(state)=>({
    commentList: state.homePageReducer.commentList,
    isLoading: state.homePageReducer.isLoading,
    isSended: state.homePageReducer.isSended,
    isSending:state.homePageReducer.isSending,
})

const mapDispatchToProps=(dispatch)=>({
    getComments:(postId)=>{
        dispatch(getComments(postId))
    },
    sendComment:(comment)=>{
        dispatch(sendComment(comment))
    }
})

export default connect(mapStateToProps,mapDispatchToProps)(UserPostCommentComponent)