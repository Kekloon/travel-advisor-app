const initialState = {
    createPost: {
        isLoading: false,
        isSuccess: false,
        isSending: false,
        errorMessage: null,
    },
    getPostListt: {
        posts: [],
        isLoading: false,
        errorMessage: null,
    },
    getUserPost: {
        userPosts: [],
        isLoading: false,
        errorMessage: null,
    },
    getOnceUserData: {
        isLoading: false,
        errorMessage: null,
        userData: {},
    },
    checkFollowing: {
        checkIsFollowing: false,
    },
    getUserList:{
        isLoading:false,
        userList:[],
        errorMessage:null,
    },
    isLoading: false,
    errorMessage: null,
    isSuccess: false,
    isSended: false,
    commentList: null,
    isSending: false,


}

export default function (state = initialState, action) {
    switch (action.type) {
        case 'REQUEST_CREATE_POST':
            return Object.assign({}, state, {
                createPost: {
                    isLoading: true,
                    isSuccess: false,
                    isSending: true,
                },
            })
        case 'SUCCESS_CREATE_POST':
            return Object.assign({}, state, {
                createPost: {
                    isLoading: false,
                    isSuccess: true,
                    isSending: false
                },
            })
        case 'FAILED_CREATE_POST':
            return Object.assign({}, state, {
                createPost: {
                    isLoading: false,
                    isSuccess: false,
                    isSending: false,
                    errorMessage: action.err
                },
            })
        case 'REQUEST_GET_POST_LIST':
            return Object.assign({}, state, {
                getPostListt: {
                    isLoading: true,
                },
            })
        case 'SUCCESS_GET_POST_LISTT':
            return Object.assign({}, state, {
                getPostListt: {
                    isLoading: false,
                    posts: action.payload
                },
            })
        case 'FAILED_GET_POST_LIST':
            return Object.assign({}, state, {
                getPostListt: {
                    isLoading: false,
                    errorMessage: action.err
                },
            })
        case 'REQUEST_SEND_COMMENT':
            return Object.assign({}, state, {
                isLoading: true,
                isSended: false,
                isSending: true,
            })
        case 'SUCCESS_SEND_COMMENT':
            return Object.assign({}, state, {
                isLoading: false,
                isSended: true,
                isSending: false,
            })
        case 'FAILED_SEND_COMMENT':
            return Object.assign({}, state, {
                isLoading: false,
                isSended: false,
                isSending: false,
            })
        case 'REQUEST_GET_COMMENTS':
            return Object.assign({}, state, {
                isLoading: true,
            })
        case 'SUCCESS_GET_COMMENTS':
            return Object.assign({}, state, {
                isLoading: false,
                commentList: action.payload
            })
        case 'FAILED_GET_COMMENTS':
            return Object.assign({}, state, {
                isLoading: false,
                errorMessage: action.err
            })
        case 'REQUEST_GET_USER_POST':
            return Object.assign({}, state, {
                getUserPost: {
                    isLoading: true,
                }
            })
        case 'SUCCESS_GET_USER_POST':
            return Object.assign({}, state, {
                getUserPost: {
                    userPosts: action.payload,
                    isLoading: false,
                }
            })
        case 'FAILED_GET_USER_POST':
            return Object.assign({}, state, {
                getUserPost: {
                    isLoading: false,
                    errorMessage: action.err,
                }
            })
        case 'REQUEST_GET_ONCE_USER_DATA':
            return Object.assign({}, state, {
                getOnceUserData: {
                    isLoading: true,
                }
            })
        case 'SUCCESS_GET_ONCE_USER_DATA':
            return Object.assign({}, state, {
                getOnceUserData: {
                    isLoading: false,
                    userData: action.payload,
                }
            })
        case 'FAILED_GET_ONCE_DATA':
            return Object.assign({}, state, {
                getOnceUserData: {
                    isLoading: false,
                    errorMessage: action.err,
                },
            })
        case 'IS_FOLLOWING':
            return Object.assign({}, state, {
                checkFollowing: {
                    checkIsFollowing: true,
                },
            })
        case 'IS_NOT_FOLLOWING':
            return Object.assign({}, state, {
                checkFollowing: {
                    checkIsFollowing: false,
                },
            })
        case 'REQUEST_GET_USER_LIST':
            return Object.assign({},state,{
                getUserList:{
                    isLoading:true,
                },
            })
        case 'SUCCESS_GET_USER_LIST':
        return Object.assign({},state,{
            getUserList:{
                isLoading:false,
                userList:action.payload
            },
        })
        case 'FAILED_GET_USER_LIST':
        return Object.assign({},state,{
            getUserList:{
                isLoading:false,
                errorMessage:action.err
            },
        })

        default:
            return state
    }
}