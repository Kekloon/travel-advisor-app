import { StackNavigator } from 'react-navigation'
import UserMainPage from '../HomePage/container/UserMainPageContainer'
import UserCreateNewPostContainer from '../HomePage/container/UserCreateNewPostContainer'
import UserPostComment from './container/UserPostCommentContainer'
import UserPostProfile from '../HomePage/container/UserPostProfileContainer'
import UserSearch from '../HomePage/container/UserSearchContainer'
import UserFollowers from '../User/container/UserFollowersContainer'
import UserFollowing from '../User/container/UserFollowingContainer'

export const HomePageNavigator = StackNavigator({
    MainPage: { screen: UserMainPage },
    UserCreatePost: { screen: UserCreateNewPostContainer },
    UserPostComment: { screen: UserPostComment },
    UserPostProfile: { screen: UserPostProfile },
    UserSearch: { screen: UserSearch },
    UserFollowers: { screen: UserFollowers },
    UserFollowing:{screen:UserFollowing},
    
}, {
        initialRouteName: 'MainPage',
        headerMode: 'none'
    })