import firebase from 'react-native-firebase'
import { AsyncStorage, Alert } from 'react-native'
import { uploadFile } from '../config/helper'
import _ from 'lodash'

const firestore = firebase.firestore()

export const createPost = (post) => async (dispatch) => {
    dispatch(_requestCreatePost())
    Alert.alert('Hold On', 'Posting....', [], { cancelable: false }),
        postRef = firestore.collection('post')
    userRef = firestore.collection('user').doc(post.owner.userId)
    let imageUrl = []
    let postId = []
    await Promise.all(post.imageFile.map(async (img) => {
        const uploadImage = await uploadFile(img)
        imageUrl.push(uploadImage)
    })),
        await postRef.add({
            postOwner: post.owner.userId,
            postMessage: post.message,
            postImage: imageUrl,
            postComments: [],
            postLikes: [],
            postCreateDate: new Date(),
        }).then(async (postRef) => {
            await userRef.get().then((val) => {
                postId = val.data().userPost
            })
            postId.push(postRef.id)
            userRef.set({
                userPost: postId
            }, { merge: true })
        }).then(
            Alert.alert('Alert', 'Post has been posted', [{ text: 'OK' }], { cancelable: false }),
            dispatch(_sucessCreatePost()))
            .catch((err) => {
                Alert.alert('Alert', 'Failed to post...', [{ text: 'OK' }]), { cancelable: true },
                    dispatch(_failedCreatePost(err))
            })
}

const _requestCreatePost = () => ({
    type: 'REQUEST_CREATE_POST'
})

const _sucessCreatePost = () => ({
    type: 'SUCCESS_CREATE_POST'
})

const _failedCreatePost = (err) => ({
    type: 'FAILED_CREATE_POST',
    err
})

export const getPostList = () => async (dispatch) => {
    dispatch(_requestGetPostList())
    try {
        let pendingPostList =[]
        let postList=[]
        let user = {}
        const userId = await AsyncStorage.getItem('uid')
        userrRef = firestore.collection('user').doc(userId)
        await userrRef.get().then((val)=>{
            user = val.data()
        })
            await Promise.all(user.userFollowing.map(async(val)=>{
                postRef = firestore.collection('post').where('postOwner','==',val)
                await postRef.get().then(async(querySnapshot)=>{
                    await querySnapshot.forEach(async(doc)=>{
                        let list ={
                            ...doc.data(),
                            postId:doc.id      
                        }
                    pendingPostList.push(list)
                    })  
                })
            }))

            await Promise.all(pendingPostList.map(async(val)=>{
                userRef =firestore.collection('user').doc(val.postOwner)
                await userRef.get().then((user)=>{
                    let owner ={
                        ...user.data()
                    }
                    let post={
                        ...val,
                        owner:owner
                    }
                    postList.push(post)
                })
            }))
            dispatch(_successGetPostListt(postList))
        }
        catch (e) {
            dispatch(_failedGetPostList(e))
        }
    
}

const _requestGetPostList = () => ({
    type: 'REQUEST_GET_POST_LIST'
})

const _successGetPostListt = (payload) => ({
    type: 'SUCCESS_GET_POST_LISTT',
    payload
})

const _failedGetPostList = (err) => ({
    type: 'FAILED_GET_POST_LIST',
    err
})

export const getComments = (postId) => async (dispatch) => {
    dispatch(_requestGetComments())
    try {
        const postRef = firestore.collection('post').doc(postId)
        await postRef.onSnapshot(async (doc) => {
            const commentsList = doc.data().postComments
            const comments = []
            await Promise.all(commentsList.map(async (doc) => {
                let comment = doc
                await comment.commentSender.get().then((user) => {
                    let userInfo = {
                        ...user.data()
                    }
                    comment = {
                        ...comment,
                        sender: userInfo
                    }
                    comments.push(comment)
                })
            }))
            dispatch(_successGetComments(comments))
        })
    }
    catch (e) {
        dispatch(_failedGetComments(e))
    }
}

const _requestGetComments = () => ({
    type: 'REQUEST_GET_COMMENTS'
})

const _successGetComments = (payload) => ({
    type: 'SUCCESS_GET_COMMENTS',
    payload
})

const _failedGetComments = (err) => ({
    type: 'FAILED_GET_COMMENTS',
    err
})

export const sendComment = (comment) => async (dispatch) => {
    try {
        dispatch(_requestSendComment())
        userId = await AsyncStorage.getItem('uid')
        postRef = firestore.collection('post').doc(comment.post.postId)
        userRef = firestore.collection('user').doc(userId)
        let commentsList
        await postRef.get().then((doc) => {
            commentsList = doc.data().postComments
        })

        const newComment = {
            commentDate: new Date(),
            commentMessage: comment.message,
            commentSender: firestore.collection('user').doc(userId),
        }

        commentsList.push(newComment)
        postRef.set({
            postComments: commentsList
        }, { merge: true })
        dispatch(_successSendComment())
    }
    catch (err) {
        dispatch(_failedSendcomment(err))
    }

}

const _requestSendComment = () => ({
    type: 'REQUEST_SEND_COMMENT'
})

const _successSendComment = () => ({
    type: 'SUCCESS_SEND_COMMENT'
})

const _failedSendcomment = (err) => ({
    type: 'FAILED_SEND_COMMENT',
    err
})

export const setLikePost = (like, postId) => async (dispatch) => {
    const userId = await AsyncStorage.getItem('uid')
    const postRef = firestore.collection('post').doc(postId)
    postRef.get().then((post) => {
        const likes = post.data().postLikes
        if (like) {
            likes.push({
                user: userId
            })
            postRef.set({
                postLikes: likes
            }, { merge: true })

        }
        else {
            postRef.set({
                postLikes: _.pullAllBy(likes, [{ 'user': userId }], 'user')
            }, { merge: true })
        }
    })
}

export const getUserPost = (user) => async (dispatch) => {
    if (!_.isEmpty(user)) {
        dispatch(_requestGetUserPost())
        try {
            postRef = firestore.collection('post').where('postOwner', '==', user)
            const postList = []
            await postRef.get().then(async (querySnapshot) => {
                await Promise.all(querySnapshot._docs.map(async (doc) => {
                    let post = doc.data()
                    userRef = firestore.collection('user').doc(post.postOwner)
                    await userRef.get().then((user) => {
                        let owner = {
                            ...user.data()
                        }
                        post = {
                            ...post,
                            postId: doc.id,
                            owner: owner
                        }
                    })
                    postList.push(post)
                }))
                dispatch(_successGetUserPost(postList))
            })
        }
        catch (e) {
            dispatch(_failedGetUserPost(e))
        }
    }
}

const _requestGetUserPost = () => ({
    type: 'REQUEST_GET_USER_POST'
})

const _successGetUserPost = (payload) => ({
    type: 'SUCCESS_GET_USER_POST',
    payload
})

const _failedGetUserPost = (err) => ({
    type: 'FAILED_GET_USER_POST',
    err
})

export const getOnceUserData = (user) => async (dispatch) => {
    dispatch(_requestGetOnceUserData())
    try {
        userRef = firestore.collection('user').doc(user)
        await userRef.get().then((val) => {
            let userData = val.data()
            dispatch(_succcessGetOnceUserData(userData))
        })
    }

    catch (e) {
        dispatch(_failedGetOnceUserData())
    }
}

const _requestGetOnceUserData = () => ({
    type: 'REQUEST_GET_ONCE_USER_DATA'
})

const _succcessGetOnceUserData = (payload) => ({
    type: 'SUCCESS_GET_ONCE_USER_DATA',
    payload
})

const _failedGetOnceUserData = (err) => ({
    type: 'FAILED_GET_ONCE_DATA',
    err
})


export const setFollow = (userData, user, isFollowing) => async (dispatch) => {
    const userId = user.userId
    const userDataId = userData.userId

    userRef = firestore.collection('user').doc(userId)
    userDataRef = firestore.collection('user').doc(userDataId)

    await userRef.get().then(async (userDoc) => {
        const userFollowing = userDoc.data().userFollowing
        await userDataRef.get().then(async (userDataDoc) => {
            const userFollowers = userDataDoc.data().userFollowers
            if (!isFollowing) {
                dispatch(_isFollowing())
                userFollowing.push(userDataId)
                userFollowers.push(userId)
                await userRef.set({
                    userFollowing: userFollowing
                }, { merge: true })
                await userDataRef.set({
                    userFollowers: userFollowers
                }, { merge: true })
            }
            else {
                dispatch(_isNotFollowing())
                await userRef.set({
                    userFollowing: _.pull(userFollowing, userDataId)
                }, { merge: true })
                await userDataRef.set({
                    userFollowers: _.pull(userFollowers, userId)
                }, { merge: true })
            }
        })
    })
}

export const checkIsFollowingUser = (userData) => async (dispatch) => {
    // dispatch(_requestCheckIsFollowing())
    const userFollowing=[]
    const userId = await AsyncStorage.getItem('uid')
    userRef = firestore.collection('user').doc(userId)
    await userRef.get().then(async (data) => {
       await data.data().userFollowing.map((val)=>{
            const user =
            {
             user:val
            }
            userFollowing.push(user)
        })
        if (_.find(userFollowing,{'user':userData})) {
            dispatch(_isFollowing())
        }
        else {
            dispatch(_isNotFollowing())
        }
    })
}

// const _requestCheckIsFollowing = () => ({
//     type: 'REQUEST_CHECK_IS_FOLLOWING'
// })

const _isFollowing = () => ({
    type: 'IS_FOLLOWING'
})

const _isNotFollowing = () => ({
    type: 'IS_NOT_FOLLOWING'
})


export const getUserList = () => async (dispatch) => {
    dispatch(_requestGetUserList())
    userRef = firestore.collection('user')
    userRef.get().then((val) => {
        let userList = []
        val.forEach((doc) => {
            userList.push(doc.data())
        })
        dispatch(_successGetUserList(userList))
    }).catch((e) => dispatch(_failedGetUserList(e)))
}

const _requestGetUserList = () => ({
    type: 'REQUEST_GET_USER_LIST'
})

const _successGetUserList = (payload) => ({
    type: 'SUCCESS_GET_USER_LIST',
    payload
})

const _failedGetUserList = (err) => ({
    type: 'FAILED_GET_USER_LIST',
    err
})