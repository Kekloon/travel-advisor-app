import firebase from 'react-native-firebase'
import { AsyncStorage } from 'react-native'
import _ from 'lodash'

const firestore = firebase.firestore()

export const getDestinationList = () => async (dispatch) => {
    dispatch(_requestGetDestinationList())
    try {
        let destinationPendingList = []
        let destinationList = []
        let list = []
        await firestore.collection('destination').get().then(async (val) => {
            await val.forEach(async (doc) => {
                destinationPendingList.push(doc.data())
            })
        })
        await Promise.all(destinationPendingList.map(async (val) => {
            countryRef = firestore.collection('countryList').doc(val.destinationCountry)
            await countryRef.get().then(async (countryVal) => {
                list = {
                    ...val,
                    destinationCountryName: countryVal.data().name
                }
                destinationList.push(list)
            })
        }))
        dispatch(_sucessGetDestinationList(destinationList))

    } catch (e) {
        dispatch(_failGetDestinationList(e))
    }
}

export const _requestGetDestinationList = () => ({
    type: 'REQUEST_GET_DestinationList'
})

export const _sucessGetDestinationList = (payload) => ({
    type: 'SUCESS_GET_DESTINATIONList',
    payload
})

export const _failGetDestinationList = (err) => ({
    type: 'FAILED_GET_DESTINATIONList',
    err,

})


export const getDestinationComment = (destinationData) => async (dispatch) => {
    dispatch(_requestGetDestinationComment())
    try {
        let destination = null,
            destinationRef = firestore.collection('destination').doc(destinationData.destinationId)
        await destinationRef.get().then(async (val) => {
            destination = val.data()
        })
        const destinationCommentList = []
        await Promise.all(destination.destinationComment.map(async (val) => {
            let commentData = val
            userRef = firestore.collection('user').doc(commentData.user)
            await userRef.get().then((userData) => {
                let commentUserData = {
                    ...commentData,
                    userCommentData: userData.data()
                }
                destinationCommentList.push(commentUserData)
            })
        }))
        dispatch(_successGetDestinationComment(destinationCommentList))
    }
    catch (e) {
        dispatch(_failedGetDestinationComment(e))
    }
}

const _requestGetDestinationComment = () => ({
    type: 'REQUEST_GET_DESTINATION_COMMENT'
})

const _successGetDestinationComment = (payload) => ({
    type: 'SUCCESS_GET_DESTINATION_COMMNET',
    payload
})

const _failedGetDestinationComment = (err) => ({
    type: 'FAILED_GET_DESTINATION_COMMNET',
    err
})


export const getDestinationRate = (destinationData) => async (dispatch) => {
    dispatch(_requestGetDestinationRate())
    try {
        destinationRef = firestore.collection('destination').doc(destinationData.destinationId)
        await destinationRef.onSnapshot(async (doc) => {
            const destinationRateList = doc.data().destinationRate
            dispatch(_successGetDestinationRate(destinationRateList))
        })
    }
    catch (e) {
        dispatch(_failedGetDestinationRate(e))
    }
}

const _requestGetDestinationRate = () => ({
    type: 'REQUEST_GET_DESTINATION_RATE'
})

const _successGetDestinationRate = (payload) => ({
    type: 'SUCCESS_GET_DESTINATION_RATE',
    payload
})

const _failedGetDestinationRate = (err) => ({
    type: 'FAILED_GET_DESTINATION_RATE',
    err
})

export const updateDestinationReviewRate = (userRate, boolean, userId, destinationData) => async (dispatch) => {
    const destinationRef = firestore.collection('destination').doc(destinationData.destinationId)
    destinationRef.get().then(async (destination) => {
        const destinationRateList = destination.data().destinationRate
        if (boolean) {
            const index = _.findIndex(destinationRateList, { user: userId })
            destinationRateList[index].rate = userRate
            destinationRef.set({
                destinationRate: destinationRateList
            }, { merge: true })

        }

        else {
            let newReview = {
                user: userId,
                rate: userRate,
            }
            destinationRateList.push(newReview)

            destinationRef.set({
                destinationRate: destinationRateList
            }, { merge: true })
        }
    })
}

export const getFullDestinationComment = (destinationData) => async (dispatch) => {
    dispatch(_requestGetFullDestinationComment())
    try {
        let destination = null,
            destinationRef = firestore.collection('destination').doc(destinationData.destinationId)
        await destinationRef.onSnapshot(async (val) => {
            destination = val.data()
        const destinationCommentList = []
        await Promise.all(destination.destinationComment.map(async (val) => {
            let commentData = val
            userRef = firestore.collection('user').doc(commentData.user)
            await userRef.get().then((userData) => {
                let commentUserData = {
                    ...commentData,
                    userCommentData: userData.data()
                }
                destinationCommentList.push(commentUserData)
            })
        }))
        dispatch(_successGetFullDestinationComment(destinationCommentList))
    })

    }
    catch (e) {
        dispatch(_failedGetFullDestinationComment(e))
    }
}

const _requestGetFullDestinationComment = () => ({
    type: 'REQUEST_GET_FULL_DESTINATION_COMMENT'
})

const _successGetFullDestinationComment = (payload) => ({
    type: 'SUCCESS_GET_FULL_DESTINATION_COMMNET',
    payload
})

const _failedGetFullDestinationComment = (err) => ({
    type: 'FAILED_GET_FULL_DESTINATION_COMMNET',
    err
})

export const sendDestinationComment = (comment) => async (dispatch) => {
    try {
        // dispatch(_requestSendDestinationComment())
        userId = await AsyncStorage.getItem('uid')
        destinationRef = firestore.collection('destination').doc(comment.destinationData.destinationId)
        userRef = firestore.collection('user').doc(userId)
        let commentsList
        await destinationRef.get().then((doc)=>{
            commentsList=doc.data().destinationComment
        })

        const newComment ={
            commentDate:new Date(),
            comment:comment.message,
            user:userId
        }

        commentsList.push(newComment)
        destinationRef.set({
            destinationComment:commentsList
        },{merge:true})
        // dispatch(_sucessSendDestinationComment())
    }
    catch (e) {
        // dispatch(_failedSendDestinationComment(e))
    }
}

const _requestSendDestinationComment =()=>({
    type:'REQUEST_SEND_DESTINATION_COMMENT'
})

const _sucessSendDestinationComment=()=>({
    type:'SUCCESS_SEND_DESTINATION_COMMENT'
})

const _failedSendDestinationComment=(err)=>({
    type:'FAILED_SEND_DESTINATION_COMMENT',
    err
})


export const saveDestinationList=(boolean,destinationId)=>async(dispatch)=>{
    const userId = await AsyncStorage.getItem('uid')
    userRef = firestore.collection('user').doc(userId)
    userRef.get().then((user)=>{
        const userSaveDestinationListData = user.data().userSaveDestinationList
        if(boolean){
            userSaveDestinationListData.push({
                destination:destinationId
            })
            userRef.set({
                userSaveDestinationList:userSaveDestinationListData
            },{merge:true})

        } 
        else{
            userRef.set({
                userSaveDestinationList:_.pullAllBy(userSaveDestinationListData,[{'destination':destinationId}],'destination')
            },{merge:true})
        }
    })

}

export const purchaseTicket=(destinationData,expireDate,ticketAmount,totalAmount)=>async(dispatch)=>{
    const userId = await AsyncStorage.getItem('uid')
    ticketTransactionRef = firestore.collection('ticketTransaction')
    let ticketTransactionId = {}
    await ticketTransactionRef.add({}).then((doc)=>{
        ticketTransactionId= doc.id
        ticketTransactionRef.doc(doc.id).set({
            ticketTransactionId:doc.id,
            ticketTransactionUser: userId,
            ticketTransactionDestinationId:destinationData.destinationId,
            ticketTransactionExpireDate:expireDate._d,
            ticketTransactionTicketAmount: ticketAmount,
            ticketTransactionTotalAmount: totalAmount,
            ticketTransactionDate: new Date(),
        })
    })
        userRef = firestore.collection('user').doc(userId)
        await userRef.get().then((user)=>{
            const userHistoryPurchaseEntryTicket =user.data().userHistoryPurchaseEntryTicket
            userHistoryPurchaseEntryTicket.push(ticketTransactionId)
            userRef.set({
                userHistoryPurchaseEntryTicket:userHistoryPurchaseEntryTicket
            },{merge:true})
        })
}

