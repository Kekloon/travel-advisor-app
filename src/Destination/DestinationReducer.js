const initialState = {
    getDestinationList: {
        destinationList: null,
        errorMessage: null,
        isLoading: false,
    },
    getDestinationComment:{
        destinationCommentList:null,
        isLoading:false,
        errorMessage:null,
    },
    getDestinationRate:{
        destinationRateList:null,
        isLoading:false,
        errorMessage:null,
    },
    getFullDestinationComment:{
        destinationFullCommentList:null,
        isLoading:false,
        errorMessage:false,
    }
    
}

export default function (state = initialState, action) {
    switch (action.type) {
        case 'REQUEST_GET_DestinationList':
            return Object.assign({}, state, {
                getDestinationList: {
                    isLoading: true,
                }
            })
        case 'SUCESS_GET_DESTINATIONList':
        return Object.assign({},state,{
            getDestinationList: {
                destinationList: action.payload,
                isLoading: false,
            }
        })
        case 'FAILED_GET_DESTINATIONList':
        return Object.assign({},state,{
            getDestinationList: {
                errorMessage: action.err,
                isLoading: false,
            }
        })
        case 'REQUEST_GET_DESTINATION_COMMENT':
        return Object.assign({},state,{
            getDestinationComment:{
                isLoading:true,
            }
            
        })
        case 'SUCCESS_GET_DESTINATION_COMMNET':
        return Object.assign({},state,{
            getDestinationComment:{
                destinationCommentList:action.payload,
                isLoading:false,
            }
        })
        case 'FAILED_GET_DESTINATION_COMMNET':
        return Object.assign({},state,{
            getDestinationComment:{
                isLoading:false,
                errorMessage:action.err,
            }
        })
        case 'REQUEST_GET_DESTINATION_RATE':
        return Object.assign({},state,{
            getDestinationRate:{
                isLoading:true,
            }  
        })
        case 'SUCCESS_GET_DESTINATION_RATE':
        return Object.assign({},state,{
            getDestinationRate:{
                destinationRateList:action.payload,
                isLoading:false,
            }
        })
        case 'FAILED_GET_DESTINATION_RATE':
        return Object.assign({},state,{
            getDestinationRate:{
                isLoading:false,
                errorMessage:action.err,
            }
        })
        case 'REQUEST_GET_FULL_DESTINATION_COMMENT':
        return Object.assign({},state,{
            getFullDestinationComment:{
                isLoading:true,
            }
        })
        case 'SUCCESS_GET_FULL_DESTINATION_COMMNET':
        return Object.assign({},state,{
            getFullDestinationComment:{
                destinationFullCommentList:action.payload,
                isLoading:false,
            }
        })
        case 'FAILED_GET_FULL_DESTINATION_COMMNET':
        return Object.assign({},state,{
            getFullDestinationComment:{
                isLoading:false,
                errorMessage:action.err,
            }
        })
        
        default:
            return state

    }
}