import React, { Component } from 'react'
import { TouchableHighlight, Dimensions, Image, FlatList, StyleSheet, StatusBar, TouchableOpacity, ScrollView, RefreshControl, Animated, Alert } from 'react-native'
import { Container, Item, Text, Icon, View, Toast, Card, ListItem, Body, List, Thumbnail, Input, CardItem, Left, Right, Button, Content, Spinner, Header, Accordion } from 'native-base'
import moment from 'moment'
import _ from 'lodash'
import ImageSlider from 'react-native-image-slider'
import Slideshow from 'react-native-slideshow'
import StarRating from 'react-native-star-rating'
import { Popup } from 'react-native-map-link'
import Modal from 'react-native-modal';
import DestinationCommentListContainer from '../container/DestiantionCommentListContainer'


const { width, height } = Dimensions.get('window')

class DestinationDetailsComponent extends Component {
    constructor(props) {
        super(props)
        this.state = {
            user: {},
            destinationData: {},
            isLoading: false,
            rate: 0,
            myRate: 0,
            isVisible: false,
            isWriteReviewModalVisible: false,
            destinationCommentList: [],
            refreshing: false,
            isLoading: false,
            destinationRateList: [],
            isCommentModalVisible: false,
            passDestinationData: null,
            isPurchaseTicketModalVisible: false,
            ticketAmount: '1',
        }
        this.handleBack = this.handleBack.bind(this)
        this.sumRate = this.sumRate.bind(this)
        this.setPopUpVisible = this.setPopUpVisible.bind(this)
        this.setWriteReviewModalVisible = this.setWriteReviewModalVisible.bind(this)
        this.checkUserRate = this.checkUserRate.bind(this)
        this._onRefresh = this._onRefresh.bind(this)
        this.setCommentModalVisible = this.setCommentModalVisible.bind(this)
        this.saveDestination = this.saveDestination.bind(this)
        this.setPurchaseTicketModalVisible = this.setPurchaseTicketModalVisible.bind(this)
        this.handleBtnPurchaseTicket = this.handleBtnPurchaseTicket.bind(this)
        this.submitPurchaseTicket = this.submitPurchaseTicket.bind(this)
    }

    _onRefresh() {
        this.setState({ refreshing: true });
        this.props.getDestinationComment(this.props.navigation.state.params.passDestination)
        this.props.getDestinationRate(this.props.navigation.state.params.passDestination)
        this.setState({ refreshing: false });
    }

    checkIsSavedDestination(destinationId) {
        if (_.find(this.state.user.userSaveDestinationList, { destination: destinationId })) {
            return true
        }
        else {
            false
        }

    }

    saveDestination() {
        const userData = this.state.user
        if (_.find(userData.userSaveDestinationList, { destination: this.state.destinationData.destinationId })) {
            const savelist = userData.userSaveDestinationList
            const upDateSavedDestinationListArray = _.pullAllBy(savelist, [{ 'destination': this.state.destinationData.destinationId }], 'destination')
            userData.userSaveDestinationList = upDateSavedDestinationListArray
            this.setState({
                user: userData
            })
            this.props.saveDestinationList(false, this.state.destinationData.destinationId)

        }
        else {
            let newSaveDestination = {
                destination: this.state.destinationData.destinationId
            }
            userData.userSaveDestinationList.push(newSaveDestination)

            this.setState({
                user: userData
            })
            this.props.saveDestinationList(true, this.state.destinationData.destinationId)
        }

    }

    setPopUpVisible() {
        this.setState({
            isVisible: true
        })
    }

    onStarRatingPress(rating) {
        this.setState({
            myRate: rating,
        })
    }

    checkUserRate(rateArray) {
        let rate = 0
        if (_.find(rateArray, { user: this.state.user.userId })) {
            rateArray.map((val) => {
                if (val.user == this.state.user.userId) {
                    rate = val.rate
                }
            })
            return rate
        }
        else {
            return 0
        }

    }

    sumRate(rateArray) {
        if (_.isEmpty(rateArray)) {
            return 0
        }
        else {
            let averageRate = 0
            rateArray.map((val, key) => {
                averageRate = averageRate + val.rate
            })
            averageRate = averageRate / rateArray.length
            return averageRate
        }
    }

    handleUpdateUserReview(userRate, rateArray) {
        this.setState({
            isWriteReviewModalVisible: false,
        })
        if (_.find(rateArray, { user: this.state.user.userId })) {
            this.props.updateDestinationReviewRate(userRate, true, this.state.user.userId, this.state.destinationData)
        }
        else {
            this.props.updateDestinationReviewRate(userRate, false, this.state.user.userId, this.state.destinationData)
        }
    }

    ReviewModal() {
        return (
            <View>
                <Modal
                    useNativeDriver={false}
                    isVisible={this.state.isWriteReviewModalVisible}
                    onBackdropPress={() => this.setState({ isWriteReviewModalVisible: false, myRate: 0 })}
                >
                    <Card style={{ width: width - 80, alignSelf: 'center', borderRadius: 20, maxHeight: 150 }}>
                        <View style={{ flex: 1, margin: 15 }}>
                            <Text style={{ fontSize: 20 }}>Give your review : </Text>
                            <Body style={{ marginTop: 5 }}>
                                <StarRating
                                    disabled={false}
                                    maxStars={5}
                                    rating={this.state.myRate}
                                    fullStarColor={'#00a680'}
                                    starSize={40}
                                    selectedStar={(rating) => this.onStarRatingPress(rating)}
                                />
                            </Body>
                            <Button onPress={() => this.handleUpdateUserReview(this.state.myRate, this.state.destinationRateList)} full style={{ height: 30, backgroundColor: '#00a680', borderRadius: 10 }}>
                                {this.state.isLoading ? <Spinner /> : <Text>Update</Text>}
                            </Button>
                        </View>
                    </Card>
                </Modal>
            </View>
        )

    }

    componentWillMount() {
        console.log(this.props.navigation.state.params.passDestination)
        this.setState({
            user: this.props.navigation.state.params.user,
            destinationData: this.props.navigation.state.params.passDestination,
        })
        this.props.getDestinationComment(this.props.navigation.state.params.passDestination)
        this.props.getDestinationRate(this.props.navigation.state.params.passDestination)
    }

    componentDidMount() {
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            destinationCommentList: _.orderBy(nextProps.destinationCommentList, ['commentDate'], ['desc']),
            isLoading: nextProps.isLoading,
            destinationRateList: nextProps.destinationRateList
        })
    }


    setWriteReviewModalVisible() {
        this.setState({
            isWriteReviewModalVisible: true,
        })
    }

    setCommentModalVisible(val) {
        this.setState({
            isCommentModalVisible: true,
            passDestinationData: val
        })
    }

    handleBack() {
        this.setState({
            isCommentModalVisible: false
        })
    }


    setPurchaseTicketModalVisible() {
        this.setState({
            isPurchaseTicketModalVisible: true
        })
    }

    handleBtnPurchaseTicket(expireDate) {

        const ticketAmount = parseInt(this.state.ticketAmount, 10)
        if (ticketAmount !== parseInt(ticketAmount, 10)) {
            Alert.alert('Alert','Please enter Amount', [
                { text: 'OK' },
            ], { cancelable: false })
        }
        else{
            const totalAmount = ticketAmount * this.state.destinationData.destinationTicketPrice
            Alert.alert('Confirmation', 'Total amount: RM' + totalAmount, [
                { text: 'Cancel' },
                { text: 'OK', onPress: () => this.submitPurchaseTicket(expireDate, totalAmount) },
            ], { cancelable: false })
        }
    }

    submitPurchaseTicket(expireDate, totalAmount) {
        this.setState({
            isPurchaseTicketModalVisible: false
        })
        this.props.purchaseTicket(this.state.destinationData, expireDate, this.state.ticketAmount, totalAmount)
        Alert.alert('Alert', 'Successful purchased ticket you can check it in ticket history.', [
            { text: 'OK' },
        ], { cancelable: false })
    }


    render() {
        const defaultProfileImage = require('../../assets/user_avatar.png')
        const sumRate = this.sumRate(this.state.destinationRateList)
        const userRate = this.checkUserRate(this.state.destinationRateList)
        const isSavedDestination = this.checkIsSavedDestination(this.state.destinationData.destinationId)
        const today = moment();
        const expireDate = moment(today).add(30, 'days');
        const dataArray = [
            {
                title: 'Description', content: this.state.destinationData.destinationDescription
            }
        ]
        return (
            <Container style={{ backgroundColor: 'white' }}>
                <Modal
                    style={{ width: width, marginLeft: 0, marginTop: 0, marginBottom: 0 }}
                    animationType="slide"
                    transparent={true}
                    visible={this.state.isCommentModalVisible}
                    onRequestClose={() => { }}>
                    <Header style={{ backgroundColor: 'white' }}>
                        <View style={{ position: 'absolute', left: 1 }}>
                            <Button transparent onPress={this.handleBack}
                            >
                                <Icon name='arrow-back' />
                            </Button>
                        </View>
                        <View>
                            <Text style={{ alignSelf: 'center', fontSize: 25, marginTop: 5 }}> Destination Comments</Text>
                        </View>
                        <View style={{ position: 'absolute', right: 1, marginTop: 7, marginRight: 5 }}>
                        </View>
                    </Header>
                    <DestinationCommentListContainer destinationData={this.state.passDestinationData} user={this.state.user} />
                </Modal>
                <View>
                    <Modal
                        useNativeDriver={false}
                        isVisible={this.state.isPurchaseTicketModalVisible}
                        onBackdropPress={() => this.setState({ isPurchaseTicketModalVisible: false })}
                    >
                        <Card style={{ width: width - 30, alignSelf: 'center', borderRadius: 20, maxHeight: 370 }}>
                            <View style={{ flex: 1, margin: 15 }}>
                                <Text style={{ fontSize: 20, color: '#00a680' }}>Purchase Ticket : </Text>
                                <ListItem>
                                    <Left style={{ maxWidth: 80 }}>
                                        <Text>Ticket:</Text>
                                    </Left>
                                    <Body>
                                        <Text>
                                            {this.state.destinationData.destinationName} One Day Ticket
                                    </Text>
                                    </Body>
                                </ListItem>
                                <ListItem>
                                    <Left style={{ maxWidth: 80 }}>
                                        <Text>Price:</Text>
                                    </Left>
                                    <Body>
                                        <Text>RM{this.state.destinationData.destinationTicketPrice}</Text>
                                    </Body>
                                </ListItem>
                                <ListItem>
                                    <Left style={{ maxWidth: 80 }}>
                                        <Text>Expire Date:</Text>
                                    </Left>
                                    <Body>
                                        <Text>
                                            {moment(expireDate).format("DD MMM YYYY")}
                                        </Text>
                                    </Body>
                                </ListItem>
                                <ListItem>
                                    <Left style={{ maxWidth: 80 }}>
                                        <Text>Ticket Amount:</Text>
                                    </Left>
                                    <Body>
                                        <Input
                                            value={this.state.ticketAmount}
                                            style={{ backgroundColor: '#F5F5F5', maxHeight: 100, borderRadius: 20, maxWidth: 70, marginLeft: 10 }}
                                            placeholder='Amount'
                                            keyboardType='numeric'
                                            onChangeText={(val) => this.setState({ ticketAmount: val })}
                                        />
                                    </Body>
                                </ListItem>
                                <Button onPress={() => this.handleBtnPurchaseTicket(expireDate)} full style={{ height: 30, backgroundColor: '#00a680', borderRadius: 10 }}>
                                    {this.state.isLoading ? <Spinner /> : <Text>Purchase</Text>}
                                </Button>
                            </View>
                        </Card>
                    </Modal>
                </View>
                {this.ReviewModal()}
                <ScrollView
                    refreshControl={
                        <RefreshControl
                            refreshing={this.state.refreshing}
                            onRefresh={this._onRefresh}
                        />
                    }
                >
                    <Slideshow style={{ width: width, height: 300, alignSelf: 'center' }}
                        dataSource={this.state.destinationData.destinationImage}
                    />
                    <List>
                        <ListItem style={{ borderBottomWidth: 1, borderBottomColor: 'lightgray' }}>
                            <Body>
                                <Text style={{ fontWeight: 'bold', fontSize: 20, marginLeft: 0 }}>{this.state.destinationData.destinationName}</Text>
                                <View style={{ flex: 1, flexDirection: 'row' }}>
                                    <StarRating
                                        disabled={true}
                                        maxStars={5}
                                        rating={sumRate}
                                        fullStarColor={'#00a680'}
                                        starSize={20}
                                        selectedStar={(rating) => this.onStarRatingPress(rating)}
                                    />
                                    <Text note>{_.isEmpty(this.state.destinationRateList) ? '  ' + 0 : '  ' + this.state.destinationRateList.length} reviews</Text>
                                </View>
                                <Text style={{ marginLeft: 0 }}>
                                    {
                                        this.state.destinationData.destinationCategory === 'tour' ? <Text>Type: Tour</Text> :
                                            this.state.destinationData.destinationCategory === 'themePark' ? <Text>Type: Theme Park</Text> :
                                                this.state.destinationData.destinationCategory === 'museums' ? <Text>Type: Museums</Text> :
                                                    this.state.destinationData.destinationCategory === 'shopping' ? <Text>Type: Shopping</Text> :
                                                        this.state.destinationData.destinationCategory === 'restaurant' ? <Text>Type: Restaurant</Text> :
                                                            null
                                    }
                                </Text>
                            </Body>
                            <Right style={{ maxWidth: 30 }}>
                                <TouchableOpacity
                                    onPress={() => this.saveDestination()}
                                >
                                    <Icon style={{ color: isSavedDestination ? '#00a680' : 'gray', fontSize: 30 }} name="heart" />
                                </TouchableOpacity>
                            </Right>
                        </ListItem>
                        <ListItem style={{ borderBottomWidth: 1, borderBottomColor: 'lightgray' }}>
                            <Left>
                                <Icon name='clock-o' type='FontAwesome' style={{ marginLeft: 0, fontSize: 25, width: 22, color: '#00a680' }} />
                                <Text>Suggested duration : {this.state.destinationData.destinationDurationTime} {this.state.destinationData.destinationSuggestTimeType}</Text>
                            </Left>
                        </ListItem>
                        <ListItem style={{ borderBottomWidth: 1, borderBottomColor: 'lightgray' }}>
                            <Body>
                                <Text style={{ marginLeft: 0, color: '#00a680', fontSize: 20, fontWeight: 'bold' }}>Description</Text>
                                <Text style={{ marginLeft: 0, marginTop: 10 }}>{this.state.destinationData.destinationDescription}</Text>
                                {/* <Accordion dataArray={dataArray} expanded={0}/> */}
                            </Body>
                        </ListItem>
                        {
                            this.state.destinationData.destinationCategory === 'themePark' ?
                                <ListItem style={{ borderBottomWidth: 1, borderBottomColor: 'lightgray' }}>
                                    <Body>
                                        <Text style={{ marginLeft: 0, color: '#00a680', fontSize: 20, fontWeight: 'bold' }}>
                                            <Icon
                                                style={{ color: '#00a680', fontSize: 20 }}
                                                type='FontAwesome'
                                                name='ticket'
                                            /> {<Text></Text>}
                                            Purchase Ticket
                                        </Text>
                                        <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                                            <View style={{ width: width - 80 }}>
                                                <Text style={{ marginLeft: 0 }}>{this.state.destinationData.destinationName} One Day Ticket</Text>
                                            </View>
                                            <Text style={{}}>RM {this.state.destinationData.destinationTicketPrice}</Text>
                                        </View>
                                        <View style={{ marginRight: -10, marginTop: 5 }}>
                                            <Button full style={{ marginLeft: 0, backgroundColor: '#00a680', borderRadius: 5 }}
                                                onPress={() => this.setPurchaseTicketModalVisible()}
                                            ><Text style={{ fontSize: 18, marginTop: -5 }}>Purchase Now</Text></Button>
                                        </View>
                                    </Body>
                                </ListItem>
                                :
                                null
                        }
                    </List>
                    <ListItem style={{ borderBottomWidth: 1, borderBottomColor: 'lightgray' }}>
                        <Body>
                            <Text style={{ marginLeft: 0, color: '#00a680', fontSize: 20, fontWeight: 'bold' }}>
                                <Icon
                                    style={{ color: '#00a680' }}
                                    type='FontAwesome'
                                    name='map-marker'
                                /> {<Text> </Text>}
                                Location</Text>
                            <TouchableOpacity
                                onPress={this.setPopUpVisible}
                            >
                                <Text style={{ marginLeft: 0, textDecorationLine: 'underline' }}>
                                    {this.state.destinationData.destinationAddress}, {this.state.destinationData.destinationPostalCode}, {this.state.destinationData.destinationCity}, {this.state.destinationData.destinationCountryName}</Text>
                            </TouchableOpacity>
                        </Body>
                    </ListItem>
                    <ListItem style={{ borderBottomWidth: 1, borderBottomColor: 'lightgray' }}>
                        <Body>
                            <Text style={{ marginLeft: 0, color: '#00a680', fontSize: 20, fontWeight: 'bold' }}>
                                Review
                            </Text>
                            <Left>
                                <StarRating
                                    disabled={true}
                                    maxStars={5}
                                    rating={userRate}
                                    fullStarColor={'#00a680'}
                                    starSize={40}
                                />
                            </Left>
                            <Button style={{ backgroundColor: '#00a680', borderRadius: 5, marginTop: 5 }}
                                full
                                onPress={() => this.setWriteReviewModalVisible()}
                            >
                                <Text style={{ fontSize: 18, marginTop: -5 }}>
                                    <Icon style={{ color: 'white', fontSize: 20 }} type='FontAwesome' name='star-o' />
                                    <Text> </Text>
                                    {
                                        userRate == 0 ? <Text style={{ color: 'white', fontSize: 18 }}>Give a review</Text> : <Text style={{ color: 'white', fontSize: 18 }}>Edit a review</Text>
                                    }
                                </Text>
                            </Button>
                        </Body>
                    </ListItem>
                    <ListItem style={{ borderBottomWidth: 1, borderBottomColor: 'white' }}>
                        <Body>
                            <Text style={{ marginLeft: 0, color: '#00a680', fontSize: 20, fontWeight: 'bold' }}>
                                Comment</Text>
                        </Body>
                    </ListItem>
                    {
                        !_.isEmpty(this.state.destinationCommentList) ?
                            <View>
                                <ListItem avatar style={{ borderBottomWidth: 1, borderBottomColor: 'lightgray', marginTop: -10 }}>
                                    <Left>
                                        <TouchableOpacity
                                            onPress={() => this.props.navigation.navigate('UserPostProfile', { passUser: this.state.destinationCommentList[0].userCommentData.userId })}>
                                            <Thumbnail small source={!_.isEmpty(this.state.destinationCommentList[0].userCommentData.userProfilePicture) ? { uri: this.state.destinationCommentList[0].userCommentData.userProfilePicture } : defaultProfileImage} />
                                        </TouchableOpacity>
                                    </Left>
                                    <Body>
                                        <Text style={{ fontWeight: 'bold' }}>{this.state.destinationCommentList[0].userCommentData.userName}</Text>
                                        <Text>{this.state.destinationCommentList[0].comment}</Text>
                                    </Body>
                                    <Right>
                                        <Text note>{moment(this.state.destinationCommentList[0].commentDate, 'day').fromNow()}</Text>
                                    </Right>
                                </ListItem>
                                {
                                    !_.isEmpty(this.state.destinationCommentList[1]) ?
                                        <ListItem avatar style={{ borderBottomWidth: 1, borderBottomColor: 'lightgray', marginTop: -10 }}>
                                            <Left>
                                                <TouchableOpacity
                                                    onPress={() => this.props.navigation.navigate('UserPostProfile', { passUser: this.state.destinationCommentList[1].userCommentData.userId })}>
                                                    <Thumbnail small source={!_.isEmpty(this.state.destinationCommentList[1].userCommentData.userProfilePicture) ? { uri: this.state.destinationCommentList[1].userCommentData.userProfilePicture } : defaultProfileImage} />
                                                </TouchableOpacity>
                                            </Left>
                                            <Body>
                                                <Text style={{ fontWeight: 'bold' }}>{this.state.destinationCommentList[1].userCommentData.userName}</Text>
                                                <Text>{this.state.destinationCommentList[1].comment}</Text>
                                            </Body>
                                            <Right>
                                                <Text note>{moment(this.state.destinationCommentList[1].commentDate, 'day').fromNow()}</Text>
                                            </Right>
                                        </ListItem> :
                                        <ListItem avatar style={{ borderBottomWidth: 1, borderBottomColor: 'lightgray' }}>
                                            <Left>
                                                <Thumbnail small source={defaultProfileImage} />
                                            </Left>
                                            <Body>
                                                <Text style={{ fontWeight: 'bold' }}>No comment</Text>
                                            </Body>
                                        </ListItem>
                                }
                                {
                                    !_.isEmpty(this.state.destinationCommentList[2]) ?
                                        <ListItem avatar style={{ borderBottomWidth: 1, borderBottomColor: 'lightgray', marginTop: -10 }}>
                                            <Left>
                                                <TouchableOpacity
                                                    onPress={() => this.props.navigation.navigate('UserPostProfile', { passUser: this.state.destinationCommentList[2].userCommentData.userId })}>
                                                    <Thumbnail small source={!_.isEmpty(this.state.destinationCommentList[2].userCommentData.userProfilePicture) ? { uri: this.state.destinationCommentList[2].userCommentData.userProfilePicture } : defaultProfileImage} />
                                                </TouchableOpacity>
                                            </Left>
                                            <Body>
                                                <Text style={{ fontWeight: 'bold' }}>{this.state.destinationCommentList[2].userCommentData.userName}</Text>
                                                <Text>{this.state.destinationCommentList[2].comment}</Text>
                                            </Body>
                                            <Right>
                                                <Text note>{moment(this.state.destinationCommentList[2].commentDate, 'day').fromNow()}</Text>
                                            </Right>
                                        </ListItem> :
                                        <ListItem avatar style={{ borderBottomWidth: 1, borderBottomColor: 'lightgray' }}>
                                            <Left>
                                                <Thumbnail small source={defaultProfileImage} />
                                            </Left>
                                            <Body>
                                                <Text style={{ fontWeight: 'bold' }}>No comment</Text>
                                            </Body>
                                        </ListItem>
                                }
                            </View>
                            :
                            <ListItem avatar style={{ borderBottomWidth: 1, borderBottomColor: 'lightgray' }}>
                                <Text style={{ fontWeight: 'bold' }}>No comment</Text>
                            </ListItem>
                    }
                    <ListItem>
                        <TouchableHighlight
                            underlayColor='transparent'
                            onPress={() => this.setCommentModalVisible(this.state.destinationData)}
                        >
                            <View style={{ width: width }}>
                                <Text>
                                    See all comment .....
                        </Text>
                            </View>
                        </TouchableHighlight>
                    </ListItem>
                </ScrollView>
                <Popup
                    isVisible={this.state.isVisible}
                    onCancelPressed={() => this.setState({ isVisible: false })}
                    onAppPressed={() => this.setState({ isVisible: false })}
                    onBackButtonPressed={() => this.setState({ isVisible: false })}
                    modalProps={{
                        animationIn: 'slideInUp'
                    }}
                    appsWhiteList={['google-maps']}
                    options={{
                        latitude: this.state.destinationData.destinationLatitude,
                        longitude: this.state.destinationData.destinationLongitude,
                        title: this.state.destinationData.destinationName,
                    }}
                // style={{}}
                />
            </Container>
        )
    }

}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    scrollViewContainer: {
        flexDirection: 'row',
        backgroundColor: '#222',
    },
    image: {
        width: 200,
        height: '100%',
    },
    buttons: {
        height: 15,
        marginTop: -25,
        marginBottom: 10,
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',

    },
    button: {
        margin: 3,
        width: 8,
        height: 8,
        borderRadius: 8 / 2,
        backgroundColor: 'gray',
        opacity: 0.9,
    },
    buttonSelected: {
        opacity: 1,
        borderRadius: 8 / 2,
        backgroundColor: '#00a680',
        fontSize: 100,
        width: 8,
        height: 8,
    },
});


export default DestinationDetailsComponent