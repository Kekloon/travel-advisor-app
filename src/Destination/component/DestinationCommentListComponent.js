import React, { Component } from 'react'
import { StyleSheet, ScrollView, Image, TouchableHighlight, FlatList, Dimensions } from 'react-native'
import { Container, Content, View, Text, Button, Icon, List, ListItem, Card, CardItem, Item, Input, Body, Left, Right, Thumbnail, Toast, Spinner, Header, Footer } from 'native-base'
import moment from 'moment'
import _ from 'lodash'

class DestiantionCommentListComponent extends Component {
    constructor(props) {
        super(props)
        this.state = {
            isLoading:false,
            commentMessage:{},
            destinationFullCommentList:[],
            user:{},
            isSending:false,
            isSended:false,
            destinationData:null
        }
    }

    componentDidMount(){
        this.setState({
            destinationData:this.props.destinationData,
            user:this.props.user,
        })
        this.props.getFullDestinationComment(this.props.destinationData)
    }

    componentWillReceiveProps(nextProps){
        this.setState({
            destinationFullCommentList:_.orderBy(nextProps.destinationFullCommentList, ['commentDate'], ['asc']),
            isLoading:nextProps.isLoading,
            isSended:nextProps.isSended,
            isSending:nextProps.isSending,
        })
    }

    renderComment(data){
        const defaultProfileImage = require('../../assets/user_avatar.png')
        return(
            <ListItem avatar>
                <Left style={{marginTop:-20}}>
                    {
                    data.userCommentData.userProfilePicture ?
                        <Thumbnail small source={{ uri: data.userCommentData.userProfilePicture }} />
                        :
                        <Thumbnail small source={defaultProfileImage} />
                    }
                </Left>
                <Body style={{marginLeft:5,marginRight:5}}>
                    <Card style={{borderRadius:15,backgroundColor:'#F5F5F5'}}>
                        <Text style={{marginLeft:10, fontWeight:'bold'}}>{data.userCommentData.userName}</Text>
                        <Text style={{marginLeft:10}}>{data.comment}</Text>
                    </Card>
                    <Text style={{marginLeft:10}} note>{moment(data.commentDate, 'day').fromNow()}</Text>
                </Body>
            </ListItem>
        )
    }

    handleBtnSend(){
        if (!_.isEmpty(this.state.commentMessage)) {
            const comment = {
                message: this.state.commentMessage,
                destinationData: this.state.destinationData
            }
            this.props.sendDestinationComment(comment)
            this.setState({
                commentMessage: {},
            })
        }
    }

    render(){
        const defaultProfileImage = require('../../assets/user_avatar.png')
        return (
            <Container style={{ backgroundColor: 'white' }}>
                {
                        <FlatList
                            inverted
                            ref='flatlist'
                            keyExtractor={(item, index) => index}
                            data={_.orderBy(this.state.destinationFullCommentList, ['commentDate'], ['desc'])}
                            renderItem={({ item }) => this.renderComment(item)}
                        />

                }
                <View style={{ position: 'relative', bottom: 1 }}>
                    <Item regular style={{ borderTopWidth: 1, height: 55 }}>
                        <Left style={{marginLeft:5}}>
                            {!_.isEmpty(this.state.user) ?
                         <Thumbnail small source={!_.isEmpty(this.state.user.userProfilePicture) ? { uri: this.state.user.userProfilePicture } : defaultProfileImage} />
                        : null
                            }
                        </Left>
                        <Body>
                            <Input placeholder='   Write a comment...'
                                style={{ backgroundColor: '#F5F5F5', borderRadius: 30, width: 250, margin: 5 }}
                                value={this.state.commentMessage}
                                disabled={this.state.isSending}
                                onChangeText={(val) => this.setState({ commentMessage: val })}
                            />
                        </Body>
                        <Right>
                            {
                                this.state.isSending ?
                                    <Spinner />
                                    :
                                    <Button transparent onPress={() => this.handleBtnSend()}
                                    >
                                        <Icon type='Ionicons' name='md-send' style={{ color: _.isEmpty(this.state.commentMessage) ? 'gray' : '#00a680', margin: 5 }} />
                                    </Button>
                            }
                        </Right>
                    </Item>
                </View>
            </Container>
        )
  
    }
}

export default DestiantionCommentListComponent