import React, { Component } from 'react'
import { TouchableHighlight, Dimensions, Image, FlatList, StyleSheet, StatusBar, TouchableOpacity, ScrollView, Modal, RefreshControl } from 'react-native'
import { Container, Item, Text, Icon, View, Toast, Card, ListItem, Body, List, Thumbnail, Input, CardItem, Left, Right, Button, Content, Spinner, Header, Picker } from 'native-base'
import _ from 'lodash'
import StarRating from 'react-native-star-rating';

const { width } = Dimensions.get('window')
class DestinationListComponent extends Component {
    constructor(props) {
        super(props)
        this.state = {
            destinationList: [],
            isLoading: true,
            user: {},
            refreshing: false,
            searchText: {},
            searchResult: [],
            rate: 0,
            selectedSearchType: 'destination',
        }
        this._onRefresh = this._onRefresh.bind(this)
        this.sumRate = this.sumRate.bind(this)
    }

    componentWillMount() {
        this.setState({
            user: this.props.user
        })
    }

    componentDidMount() {
        this.props.getDestinationList()
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            destinationList: nextProps.destinationList,
            isLoading: nextProps.isLoading,
        })
    }

    _onRefresh() {
        this.setState({
            refreshing:true,
            searchText:{},
            searchResult:[],
            selectedSearchType:'destination'
        })
        this.props.getDestinationList()
        this.setState({refreshing:false})
    }

    onStarRatingPress(rating) {
        this.setState({
            rate: rating,
        })
    }

    sumRate(rateArray) {
        if (_.isEmpty(rateArray)) {
            return 0
        }
        else {
            let averageRate = 0
            rateArray.map((val, key) => {
                averageRate=averageRate+val.rate
            })
            averageRate = averageRate/rateArray.length
            return averageRate
        }

    }

    renderDesitnationList(val, key) {
        const sumRate = this.sumRate(val.destinationRate)
        return (
            <List key={key}>
                <ListItem>
                    <TouchableHighlight underlayColor='transparent'
                        onPress={() => this.props.navigation.navigate('DestinationDetails', { passDestination: val, user: this.state.user })}
                    >
                        <View style={{ width: width - 30, height: 160, flexDirection: 'row' }}>
                            <Left>
                                <Thumbnail style={{ height: 140, width: 150 }} source={{ uri: val.destinationCoverImage }} />
                            </Left>
                            <Body style={{ alignContent: 'flex-start', alignSelf: 'flex-start', alignItems: 'flex-start',marginTop:10 }}>
                                <Text style={{ fontWeight: 'bold' }}>{val.destinationName}</Text>
                                <Text>
                                    {
                                        val.destinationCategory === 'tour' ? <Text>Type: Tour</Text> :
                                            val.destinationCategory === 'themePark' ? <Text>Type: Theme Park</Text> :
                                                val.destinationCategory === 'museums' ? <Text>Type: Museums</Text> :
                                                    val.destinationCategory === 'shopping' ? <Text>Type: Shopping</Text> :
                                                        val.destinationCategory === 'restaurant' ? <Text>Type: Restaurant</Text> :
                                                            null
                                    }
                                    </Text>
                                <Text>
                                    {
                                        !_.isEmpty(val.destinationTicketPrice) ? 'Ticket: RM' + val.destinationTicketPrice : null
                                    }
                                </Text>
                                <Text>
                                    City : {val.destinationCity}
                                </Text>
                                <Text>Country : {val.destinationCountryName}</Text>
                                <StarRating
                                    disabled={true}
                                    maxStars={5}
                                    rating={sumRate}
                                    fullStarColor={'#00a680'}
                                    starSize={20}
                                    selectedStar={(rating) => this.onStarRatingPress(rating)}
                                />
                                <Text note>{_.isEmpty(val.destinationRate) ? 0 : val.destinationRate.length} reviews</Text>
                            </Body>
                        </View>
                    </TouchableHighlight>
                </ListItem>
            </List>
        )
    }

    render() {
        if (!_.isEmpty(this.state.searchText)) {
            const text = this.state.searchText
            if (this.state.selectedSearchType === 'destination') {
                this.state.searchResult = _.filter(this.state.destinationList, function (o) {
                    return o.destinationName.toLowerCase().includes(text.toLowerCase())
                })
            }
            if (this.state.selectedSearchType === 'country') {
                this.state.searchResult = _.filter(this.state.destinationList, function (o) {
                    return o.destinationCountryName.toLowerCase().includes(text.toLowerCase())
                })
            }
            if (this.state.selectedSearchType === 'city') {
                this.state.searchResult = _.filter(this.state.destinationList, function (o) {
                    return o.destinationCity.toLowerCase().includes(text.toLowerCase())
                })
            }
        }
        else {
            this.state.searchResult = this.state.destinationList
        }
        return (
            <Container>
                <Header style={{ backgroundColor: '#00a680' }}>
                    <Left style={{ maxWidth: 70 }}>
                        <Icon style={{ backgroundColor: '#00a680', color: 'white', fontSize: 30 }} name="ios-search" />
                    </Left>
                    <Body marginLeft={-40} marginTop={-10} maxWidth={width} style={{ maxWidth: width, borderBottomWidth: 1, maxHeight: 40, textDecorationLine: 'none', borderBottomColor: 'dimgray' }}>
                        <Item style={{ borderBottomColor: 'transparent' }}>
                            <Input
                                value={this.state.searchText}
                                onChangeText={(text) => this.setState({ searchText: text })}
                                placeholder='Search' style={{ width: width, color: '#E4E2E2', fontSize: 18 }} />
                            <Text style={{ fontSize: 20 }}>|</Text>
                            <Picker
                                mode='dropdown'
                                style={{ width: 145, borderWidth: 20 }}
                                selectedValue={this.state.selectedSearchType}
                                onValueChange={(value) => this.setState({ selectedSearchType: value })}
                            >
                                <Picker.Item label='Destination' value='destination' />
                                <Picker.Item label='Country' value='country' />
                                <Picker.Item label='City' value='city' />
                            </Picker>
                        </Item>
                    </Body>
                </Header>
                <ScrollView
                    refreshControl={
                        <RefreshControl
                        refreshing={this.state.refreshing}
                        onRefresh={this._onRefresh}
                        />
                    }
                >
                    {

                        this.state.isLoading
                            ?
                            <Spinner />
                            :
                            _.isEmpty(this.state.destinationList) ?
                                <Text style={{ color: 'gray', fontSize: 25, alignSelf: 'center', marginTop: 30 }}>No result</Text> :
                                this.state.searchResult.map((val, key) => {
                                    return this.renderDesitnationList(val, key)
                                })
                    }
                </ScrollView>
            </Container>
        )
    }
}

export default DestinationListComponent