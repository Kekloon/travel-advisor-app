import { StackNavigator } from 'react-navigation'
import DestinationList from './container/DestinationListContainer'
import DestinationDetails from './container/DestinationDetailsContainer'


export const DestinationNavigator = StackNavigator({
    DestinationList: { screen: DestinationList },
    DestinationDetails: { screen: DestinationDetails }
}, {
        initialRouteName: 'DestinationList',
        headerMode: 'none'
    })