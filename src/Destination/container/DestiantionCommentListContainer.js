import DestinationCommentListComponent from '../component/DestinationCommentListComponent'
import {connect} from 'react-redux'
import {getFullDestinationComment,sendDestinationComment} from '../DestinationAction'

const mapStateToProps=(state)=>({
    destinationFullCommentList:state.destinationReducer.getFullDestinationComment.destinationFullCommentList,
    isLoading:state.destinationReducer.getFullDestinationComment.isLoading,
    
})

const mapDispatchToProps=(dispatch)=>({
    getFullDestinationComment:(destinationData)=>{
        dispatch(getFullDestinationComment(destinationData))
    },
    sendDestinationComment:(comment)=>{
        dispatch(sendDestinationComment(comment))
    },

})

export default connect(mapStateToProps,mapDispatchToProps)(DestinationCommentListComponent)