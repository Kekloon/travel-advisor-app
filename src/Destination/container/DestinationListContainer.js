import { connect } from 'react-redux'
import DestinationListComponent from '../component/DestinationListComponent'
import { getDestinationList } from '../DestinationAction'

const mapStateToProps = (state) => ({
    destinationList: state.destinationReducer.getDestinationList.destinationList,
    isLoading:state.destinationReducer.getDestinationList.isLoading,
    user:state.userReducer.user
})

const mapDispatchToProp = (dispatch) => ({
    getDestinationList:()=>{
        dispatch(getDestinationList())
    }
})

export default connect(mapStateToProps, mapDispatchToProp)(DestinationListComponent)