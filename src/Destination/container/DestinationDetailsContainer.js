import DestinationDetailsComponent from '../component/DestinationDetailsComponent'
import { connect } from 'react-redux'
import {getDestinationComment,getDestinationRate,updateDestinationReviewRate,saveDestinationList,purchaseTicket} from '../DestinationAction'

const mapStateToProps =(state)=>({
    isLoading:state.destinationReducer.getDestinationComment.isLoading,
    destinationCommentList:state.destinationReducer.getDestinationComment.destinationCommentList,
    destinationRateList:state.destinationReducer.getDestinationRate.destinationRateList
})

const mapDispatchToProps = (dispatch)=>({
    getDestinationComment:(destinationData)=>{
        dispatch(getDestinationComment(destinationData))
    },
    getDestinationRate:(destinationData)=>{
        dispatch(getDestinationRate(destinationData))
    },
    updateDestinationReviewRate:(userRate,boolean,userId,destinationData)=>{
        dispatch(updateDestinationReviewRate(userRate,boolean,userId,destinationData))
    },
    saveDestinationList:(boolean,destinationId)=>{
        dispatch(saveDestinationList(boolean,destinationId))
    },
    purchaseTicket:(destinationData,expireDate,ticketAmount,totalAmount)=>{
        dispatch(purchaseTicket(destinationData,expireDate,ticketAmount,totalAmount))
    }
    
})

export default connect(mapStateToProps,mapDispatchToProps)(DestinationDetailsComponent)