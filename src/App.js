import React, { Component } from "react";
import { connect } from 'react-redux'
import { Root } from "native-base";
import { autoAuthentication } from './Authenication/AuthenticationAction'
// import  PhoneLoginContainer from '../src/Authenication/container/PhoneLoginContainer'
// Component Path
import AutoLogin from '../src/Authenication/LoginNavigation'
import MainLoading from './modules/MainLoading'

// const Main = StackNavigator(
// 	{
// 		App: { screen: App }
// 	},
// 	{
// 		initialRoute: 'App',
// 		headerMode: 'none'
// 	}
// )

class AppComponent extends Component {
	constructor(props) {
		super(props)
		this.state = {
			isLoading: false,
			isAuthenticated: null,
		}
	}

	componentWillMount() {
		this.props.autoAuthentication()
	}

	componentWillReceiveProps(nextProps) {
		this.setState({
			isLoading: nextProps.isLoading,
			isAuthenticated: nextProps.isAuthenticated
		})
	}
	render() {
		return (
			<Root>
				{
					this.state.isLoading  ?
						<MainLoading/>
						:
						<AutoLogin />
				}
			</Root>
		)
	}
}


const mapStateToProps = (state) => ({
	isLoading: state.authenticationReducer.autoAuthenticate.isLoading,
	isAuthenticated: state.authenticationReducer.isAuthenticated,
})

const mapDispatchToProps = (dispatch) => ({
	autoAuthentication: () => {
		dispatch(autoAuthentication())
	},
	// setAppNavRef: (navRef) => {
	// 	dispatch(setAppNavRef(navRef))
	// }
})

export default connect(mapStateToProps, mapDispatchToProps)(AppComponent)

