import { combineReducers } from "redux";
import { reducer as formReducer } from "redux-form";


import authenticationReducer from '../src/Authenication/AuthenticationReducer'
import userReducer from '../src/User/UserReducer'
import homePageReducer from '../src/HomePage/UserHomePageReducer'
import destinationReducer from '../src/Destination/DestinationReducer'
import hotelReducer from '../src/Hotel/HotelReducer'
import saveAndHistoryReducer from '../src/SaveAndHistoryMenu/SaveAndHistoryMenuReducer'
import privateMessageReducer from '../src/privateMessage/privateMessageReducer'
import tripPlanReducer from '../src/tripPlan/tripPlanReducer'

export default combineReducers({
	authenticationReducer,
	userReducer,
	homePageReducer,
	destinationReducer,
	hotelReducer,
	saveAndHistoryReducer,
	privateMessageReducer,
	tripPlanReducer,
	form: formReducer
});
