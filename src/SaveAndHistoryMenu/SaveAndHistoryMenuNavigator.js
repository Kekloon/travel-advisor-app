import { StackNavigator } from 'react-navigation'
import SaveAndHistoryMenu from './container/SaveAndHistoryMenuContainer'
import TicketHistoryDetails from './container/TicketHistoryDetailsContainer'
import BookedHotelHistoryDetails from './container/BookedHotelHistoryDetailsContainer'

export const SaveAndHistoryNavigator = StackNavigator({
    SaveAndHistoryMenu: { screen: SaveAndHistoryMenu},
    TicketHistoryDetails:{screen:TicketHistoryDetails},
    BookedHotelHistoryDetails:{screen:BookedHotelHistoryDetails}
},{
    initialRouteName:'SaveAndHistoryMenu',
    headerMode:'none'
})