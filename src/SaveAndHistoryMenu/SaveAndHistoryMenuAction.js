import firebase from 'react-native-firebase'
import { AsyncStorage } from 'react-native'
import _ from 'lodash'

const firestore = firebase.firestore()

export const getSavedDestinationList = () => async (dispatch) => {
    dispatch(_requestGetSavedDestinationList())
    try {
        let userSavedDestinationList = []
        let destinationPendingList = []
        let destinationList = []
        let list = []
        userId = await AsyncStorage.getItem('uid')
        userRef = firestore.collection('user').doc(userId)
        await userRef.get().then((val) => {
            userSavedDestinationList = val.data().userSaveDestinationList
        })

        await Promise.all(userSavedDestinationList.map(async (val) => {
            destinationRef = firestore.collection('destination').doc(val.destination)
            await destinationRef.get().then(async (destination) => {
                destinationPendingList.push(destination.data())
            })
        }))

        await Promise.all(destinationPendingList.map(async (val) => {
            countryRef = firestore.collection('countryList').doc(val.destinationCountry)
            await countryRef.get().then(async (countryVal) => {
                list = {
                    ...val,
                    destinationCountryName: countryVal.data().name
                }
                destinationList.push(list)
            })
        }))
        dispatch(_successGetSavedDestinationList(destinationList))
    }
    catch (e) {
        dispatch(_failedGetSavedDestinationList())
    }

}


export const _requestGetSavedDestinationList = () => ({
    type: 'REQUEST_GET_SAVED_DESTINATION_LIST'
})

export const _successGetSavedDestinationList = (payload) => ({
    type: 'SUCCESS_GET_SAVED_DESTINATION_LIST',
    payload
})

export const _failedGetSavedDestinationList = (err) => ({
    type: 'FAILED_GET_SAVED_DESTINATION_LIST',
    err
})

export const getSavedHotelList = () => async (dispatch) => {
    dispatch(_requestGetSavedHotelList())
    try {
        let userSavedHotelList = []
        let hotelPendingList = []
        let hotelList = []
        let list = []

        userId = await AsyncStorage.getItem('uid')
        userRef = firestore.collection('user').doc(userId)
        await userRef.get().then((val) => {
            userSavedHotelList = val.data().userSaveHotelList
        })

        await Promise.all(userSavedHotelList.map(async (val) => {
            hotelRef = firestore.collection('hotel').doc(val.hotel)
            await hotelRef.get().then(async (hotel) => {
                hotelPendingList.push(hotel.data())
            })
        }))

        await Promise.all(hotelPendingList.map(async (val) => {
            countryRef = firestore.collection('countryList').doc(val.hotelCountry)
            await countryRef.get().then(async (countryVal) => {
                list = {
                    ...val,
                    hotelCountryName: countryVal.data().name
                }
                hotelList.push(list)
            })
        }))
        dispatch(_successGetSavedHotelList(hotelList))
    }

    catch (e) {
        dispatch(_failedGetSavedHotelList(e))
    }
}

export const _requestGetSavedHotelList = () => ({
    type: 'REQUEST_GET_SAVED_HOTEL_LIST'
})

export const _successGetSavedHotelList = (payload) => ({
    type: 'SUCCESS_GET_SAVED_HOTEL_LIST',
    payload
})

export const _failedGetSavedHotelList = (err) => ({
    type: 'FAILED_GET_SAVED_HOTEL_LIST',
    err
})

export const getTicketHistoryList = () => async (dispatch) => {
    dispatch(_requestGetTicketHistoryList())
    try {
        let userTicketHistoryList = []
        let ticketHistoryPendingList = []
        let list = []
        let TicketHistoryList = []
        userId = await AsyncStorage.getItem('uid')
        userRef = firestore.collection('user').doc(userId)
        await userRef.get().then((val) => {
            userTicketHistoryList = val.data().userHistoryPurchaseEntryTicket
        })

        await Promise.all(userTicketHistoryList.map(async (val) => {
            ticketTransactionRef = firestore.collection('ticketTransaction').doc(val)
            await ticketTransactionRef.get().then(async (ticketTransaction) => {
                ticketHistoryPendingList.push(ticketTransaction.data())
            })
        }))

        await Promise.all(ticketHistoryPendingList.map(async (val) => {
            destinationRef = firestore.collection('destination').doc(val.ticketTransactionDestinationId)
            await destinationRef.get().then(async (destinationData) => {
                list = {
                    ...val,
                    destinationData: destinationData.data()
                }
                TicketHistoryList.push(list)
            })
        }))
        dispatch(_sucessGetTicketHistoryList(TicketHistoryList))

    }
    catch (e) {
        dispatch(_failedGetTicketHistoryList(e))
    }
}

export const _requestGetTicketHistoryList = () => ({
    type: 'REQUEST_GET_TICKET_HISTORY_LIST'
})

export const _sucessGetTicketHistoryList = (payload) => ({
    type: 'SUCCESS_GET_TICKET_HISTORY_LIST',
    payload
})

export const _failedGetTicketHistoryList = (err) => ({
    type: 'FAILED_GET_TICKET_HISTORY_LIST',
    err
})


export const getBookedHotelHistoryList = () => async (dispatch) => {
    dispatch(_requestGetBookedHotelHistoryList())
    try {
        let userBookedHistoryList = []
        let BookedHistoryPendingList = []
        let list = []
        let BookedHistoryList = []
        userId = await AsyncStorage.getItem('uid')
        userRef = firestore.collection('user').doc(userId)
        await userRef.get().then((val) => {
            userBookedHistoryList = val.data().userHistoryOfBookedHotel
        })

        await Promise.all(userBookedHistoryList.map(async (val) => {
            hotelTransactionRef = firestore.collection('hotelTransaction').doc(val)
            await hotelTransactionRef.get().then(async (hotelTransaction) => {
                BookedHistoryPendingList.push(hotelTransaction.data())
            })
        }))

        await Promise.all(BookedHistoryPendingList.map(async (val) => {
            hotelRef = firestore.collection('hotel').doc(val.hotelTransactionHotelId)
            await hotelRef.get().then(async (hotelData) => {
                list = {
                    ...val,
                    hotelData: hotelData.data()
                }
                BookedHistoryList.push(list)
            })
        }))
        console.log(BookedHistoryList)
        dispatch(_successGetBookedHotelHistoryList(BookedHistoryList))

    }
    catch (e) {
        dispatch(_failedGetBookedHotelList(e))
    }
}

export const _requestGetBookedHotelHistoryList = ()=>({
    type:'REQUEST_GET_BOOKED_HOTEL_HISTORY_LIST'
})

export const _successGetBookedHotelHistoryList =(payload)=>({
    type:'SUCCESS_GET_BOOKED_HOTEL_HISTORY_LIST',
    payload
})

export const _failedGetBookedHotelList =(err)=>({
    type:'FAILED_GET_BOOKED_HOTEL_HISTORY_LIST',
    err
})