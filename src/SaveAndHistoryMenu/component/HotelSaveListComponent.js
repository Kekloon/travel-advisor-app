import React, { Component } from 'react'
import { TouchableHighlight, Dimensions, Image, FlatList, StyleSheet, StatusBar, TouchableOpacity, ScrollView, RefreshControl, Animated, Alert } from 'react-native'
import { Container, Item, Text, Icon, View, Toast, Card, ListItem, Body, List, Thumbnail, Input, CardItem, Left, Right, Button, Content, Spinner, Header, Accordion } from 'native-base'
import moment from 'moment'
import StarRating from 'react-native-star-rating'

const { width } = Dimensions.get('window')

class HotelSaveListComponent extends Component {
    constructor(props) {
        super(props)
        this.state = {
            savedHotelList: [],
            isLoading: true,
            user: {},
            refreshing: false,
            rate: 0,
        }
        this._onRefresh = this._onRefresh.bind(this)
        this.sumRate = this.sumRate.bind(this)
    }
    componentWillMount() {
        this.setState({
            user: this.props.user
        })
    }

    componentDidMount() {
        this.props.getSavedHotelList()
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            savedHotelList: nextProps.savedHotelList,
            isLoading: nextProps.isLoading
        })
    }

    _onRefresh() {
        this.setState({
            refreshing: true,
        })
        this.props.getSavedHotelList()
        this.setState({
            refreshing: false
        })
    }

    onStarRatingPress(rating) {
        this.setState({
            rate: rating
        })
    }

    sumRate(rateArray) {
        if (_.isEmpty(rateArray)) {
            return 0
        }

        else {
            let averageRate = 0
            rateArray.map((val, key) => {
                averageRate = averageRate + val.rate
            })
            averageRate = averageRate/rateArray.length
            return averageRate
        }
    }

    renderSavedHotelList(val,key){
        const sumRate = this.sumRate(val.hotelRate)
        return (
            <List key={key}>
                <ListItem>
                    <TouchableHighlight underlayColor='transparent'
                        onPress={() => this.props.navigation.navigate('HotelDetails', { passHotel: val, user: this.state.user })}
                    >
                        <View style={{ width: width - 30, height: 150, flexDirection: 'row' }}>
                            <Left>
                                <Thumbnail style={{ height: 140, width: 150 }} source={{ uri: val.hotelCoverImage }} />
                            </Left>
                            <Body style={{ alignContent: 'flex-start', alignSelf: 'flex-start', alignItems: 'flex-start',marginTop:10 }}>
                                <Text style={{ fontWeight: 'bold' }}>{val.hotelName}</Text>
                                <Text>
                                    City : {val.hotelCity}
                                </Text>
                                <Text>Country : {val.hotelCountryName}</Text>
                                <StarRating
                                    disabled={true}
                                    maxStars={5}
                                    rating={sumRate}
                                    fullStarColor={'#00a680'}
                                    starSize={20}
                                    selectedStar={(rating) => this.onStarRatingPress(rating)}
                                />
                                <Text note>{_.isEmpty(val.hotelRate) ? 0 : val.hotelRate.length} reviews</Text>
                            </Body>
                        </View>
                    </TouchableHighlight>
                </ListItem>
            </List>
        )
    }

    render(){
        return(
            <Container>
                 <ScrollView
                    refreshControl={
                        <RefreshControl
                        refreshing={this.state.refreshing}
                        onRefresh={this._onRefresh}
                        />
                    }
                >
                    {

                        this.state.isLoading
                            ?
                            <Spinner />
                            :
                            _.isEmpty(this.state.savedHotelList) ?
                                <Text style={{ color: 'gray', fontSize: 25, alignSelf: 'center', marginTop: 30 }}>No result</Text> :
                                this.state.savedHotelList.map((val, key) => {
                                    return this.renderSavedHotelList(val, key)
                                })
                    }
                </ScrollView>
            </Container>
        )
    }
}

export default HotelSaveListComponent