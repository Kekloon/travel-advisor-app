import React, { Component } from 'react'
import { TouchableHighlight, Dimensions, Image, FlatList, StyleSheet, StatusBar, TouchableOpacity, ScrollView, RefreshControl, Animated, Alert } from 'react-native'
import { Container, Item, Text, Icon, View, Toast, Card, ListItem, Body, List, Thumbnail, Input, CardItem, Left, Right, Button, Content, Spinner, Header, Accordion } from 'native-base'
import moment from 'moment'
import _ from 'lodash'
import QRCode from 'react-native-qrcode';


const { width } = Dimensions.get('window')
class TicketHistoryDetailsComponent extends Component {
    constructor(props) {
        super(props)
        this.state = {
            TicketData: {},
            user: {},
        }
    }

    componentWillMount() {
        this.setState({
            user: this.props.navigation.state.params.user,
            TicketData: this.props.navigation.state.params.passTicketHistory,
        })
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
        })
    }

    render() {
        return (
            <Container>
            <Header style={{ backgroundColor: '#00a680' }} >
            <Body>
           <Text style={{ color: 'white', alignSelf: 'center', fontSize: 30 }}>Ticket History</Text>
            </Body>
            </Header>
            <ScrollView>
            <List>
                <ListItem>
                <Body style={{alignSelf:'center',alignItems:'center'}}>
                <QRCode 
                value={this.state.TicketData.ticketTransactionId + "\n "
                     + this.state.TicketData.destinationData.destinationName +" \n"
                     + this.state.TicketData.ticketTransactionTicketAmount + "\n"
                     + this.state.TicketData.ticketTransactionTotalAmount + '\n'
                     + moment(this.state.TicketData.ticketTransactionDate).format("DD-MM-YYYY") +'\n'
                     + moment(this.state.TicketData.ticketTransactionExpireDate).format("DD-MM-YYYY")
                         }
                    size={150}
                    bgColor='black'
                    fgColor='white' />
                <Text style={{marginTop:5,color:'#01D801'}}>Your Purchase is confirmed</Text>
                </Body>
                </ListItem>
                <ListItem>
                    <Left style={{maxWidth:130}}>
                        <Text style={{marginLeft:0,color:'#00a680'}}>Confirmation ID:</Text>
                    </Left>
                    <Body>
                <Text style={{marginLeft:0}}>{this.state.TicketData.ticketTransactionId}</Text>
                    </Body>
                </ListItem>
                <ListItem>
                    <Left style={{maxWidth:130}}>
                        <Text style={{marginLeft:0,color:'#00a680'}}>Destination:</Text>
                    </Left>
                    <Body>
                <Text style={{marginLeft:0}}>{this.state.TicketData.destinationData.destinationName}</Text>
                    </Body>
                </ListItem>
                <ListItem>
                    <Left style={{maxWidth:130}}>
                        <Text style={{marginLeft:0,color:'#00a680'}}>Ticket Amount:</Text>
                    </Left>
                    <Body>
                <Text style={{marginLeft:0}}>{this.state.TicketData.ticketTransactionTicketAmount}</Text>
                    </Body>
                </ListItem>
                <ListItem>
                    <Left style={{maxWidth:130}}>
                        <Text style={{marginLeft:0,color:'#00a680'}}>Total price:</Text>
                    </Left>
                    <Body>
                <Text style={{marginLeft:0}}>RM{this.state.TicketData.ticketTransactionTotalAmount}</Text>
                    </Body>
                </ListItem>
                <ListItem>
                    <Left style={{maxWidth:130}}>
                        <Text style={{marginLeft:0,color:'#00a680'}}>Purchase date:</Text>
                    </Left>
                    <Body>
                <Text style={{marginLeft:0}}>{moment(this.state.TicketData.ticketTransactionDate).format("DD-MM-YYYY")}</Text>
                    </Body>
                </ListItem>
                <ListItem>
                    <Left style={{maxWidth:130}}>
                        <Text style={{marginLeft:0,color:'red'}}>Expiry date:</Text>
                    </Left>
                    <Body>
                <Text style={{marginLeft:0,color:'red'}}>{moment(this.state.TicketData.ticketTransactionExpireDate).format("DD-MM-YYYY")}</Text>
                    </Body>
                </ListItem>
            </List>
            </ScrollView>
        </Container>
        )
    }
}

export default TicketHistoryDetailsComponent