import React, { Component } from 'react'
import { TouchableHighlight, Dimensions, Image, FlatList, StyleSheet, StatusBar, TouchableOpacity, ScrollView, RefreshControl, Animated, Alert } from 'react-native'
import { Container, Item, Text, Icon, View, Toast, Card, ListItem, Body, List, Thumbnail, Input, CardItem, Left, Right, Button, Content, Spinner, Header, Accordion } from 'native-base'
import moment from 'moment'
import _ from 'lodash'
import QRCode from 'react-native-qrcode';

class BookedHotelHistoryDetailsComponent extends Component {
    constructor(props) {
        super(props)
        this.state = {
            BookedHotelData: {},
            user: {},
        }
    }

    componentWillMount() {
        this.setState({
            user: this.props.navigation.state.params.user,
            BookedHotelData: this.props.navigation.state.params.passBookedHotelHistory
        })
    }

    render() {
        return (
            <Container>
                <Header style={{ backgroundColor: '#00a680' }} >
                    <Body>
                        <Text style={{ color: 'white', alignSelf: 'center', fontSize: 30 }}>Booked Hotel History</Text>
                    </Body>
                </Header>
                <ScrollView>
                    <List>
                        <ListItem>
                            <Body style={{ alignSelf: 'center', alignItems: 'center' }}>
                                <QRCode
                                    value={this.state.BookedHotelData.hotelTransactionId + "\n "
                                        + this.state.BookedHotelData.hotelData.hotelName + " \n"
                                        + this.state.BookedHotelData.hotelTransactionPackage.hotelPackageName + '\n'
                                        + this.state.BookedHotelData.hotelTransactionRoomAmount + '\n'
                                        + this.state.BookedHotelData.hotelTransactionTotalAmount + '\n'
                                        + moment(this.state.BookedHotelData.hotelTransactionHoteCheckInDate).format("DD-MM-YYYY") + '\n'
                                        + moment(this.state.BookedHotelData.hotelTransactionHoteCheckOutDate).format("DD-MM-YYYY") + ' \n'
                                        + moment(this.state.BookedHotelData.hotelTransactionDate).format("DD-MM-YYYY")
                                    }
                                    size={150}
                                    bgColor='black'
                                    fgColor='white' />
                                <Text style={{ marginTop: 5, color: '#01D801' }}>Your hotel booking is confirmed</Text>
                            </Body>
                        </ListItem>
                        <ListItem>
                            <Left style={{ maxWidth: 130 }}>
                                <Text style={{ marginLeft: 0, color: '#00a680' }}>Confirmation ID:</Text>
                            </Left>
                            <Body>
                                <Text style={{ marginLeft: 0 }}>{this.state.BookedHotelData.hotelTransactionId}</Text>
                            </Body>
                        </ListItem>
                        <ListItem>
                            <Left style={{ maxWidth: 130 }}>
                                <Text style={{ marginLeft: 0, color: '#00a680' }}>Hotel:</Text>
                            </Left>
                            <Body>
                                <Text style={{ marginLeft: 0 }}>{this.state.BookedHotelData.hotelData.hotelName}</Text>
                            </Body>
                        </ListItem>
                        <ListItem>
                            <Left style={{ maxWidth: 130 }}>
                                <Text style={{ marginLeft: 0, color: '#00a680' }}>Hotel Package:</Text>
                            </Left>
                            <Body>
                                <Text style={{ marginLeft: 0 }}>{this.state.BookedHotelData.hotelTransactionPackage.hotelPackageName}</Text>
                            </Body>
                        </ListItem>
                        <ListItem>
                            <Left style={{ maxWidth: 130 }}>
                                <Text style={{ marginLeft: 0, color: '#00a680' }}>Room Amount:</Text>
                            </Left>
                            <Body>
                                <Text style={{ marginLeft: 0 }}>{this.state.BookedHotelData.hotelTransactionRoomAmount}</Text>
                            </Body>
                        </ListItem>
                        <ListItem>
                            <Left style={{ maxWidth: 130 }}>
                                <Text style={{ marginLeft: 0, color: '#00a680' }}>Total Amount:</Text>
                            </Left>
                            <Body>
                                <Text style={{ marginLeft: 0 }}>RM{this.state.BookedHotelData.hotelTransactionTotalAmount}</Text>
                            </Body>
                        </ListItem>
                        <ListItem>
                            <Left style={{ maxWidth: 130 }}>
                                <Text style={{ marginLeft: 0, color: '#00a680' }}>Booking Date:</Text>
                            </Left>
                            <Body>
                                <Text style={{ marginLeft: 0 }}>{moment(this.state.BookedHotelData.hotelTransactionDate).format("DD-MM-YYYY")}</Text>
                            </Body>
                        </ListItem>
                        <ListItem>
                            <Left style={{ maxWidth: 130 }}>
                                <Text style={{ marginLeft: 0, color: '#00a680' }}>Check In Date:</Text>
                            </Left>
                            <Body>
                                <Text style={{ marginLeft: 0 }}>{moment(this.state.BookedHotelData.hotelTransactionHoteCheckInDate).format("DD-MM-YYYY")}</Text>
                            </Body>
                        </ListItem>
                        <ListItem>
                            <Left style={{ maxWidth: 130 }}>
                                <Text style={{ marginLeft: 0, color: 'red' }}>Check Out Date:</Text>
                            </Left>
                            <Body>
                                <Text style={{ marginLeft: 0, color: 'red' }}>{moment(this.state.BookedHotelData.hotelTransactionHoteCheckOutDate).format("DD-MM-YYYY")}</Text>
                            </Body>
                        </ListItem>
                    </List>
                </ScrollView>
            </Container>
        )
    }

}

export default BookedHotelHistoryDetailsComponent