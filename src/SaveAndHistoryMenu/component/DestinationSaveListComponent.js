import React, { Component } from 'react'
import { TouchableHighlight, Dimensions, Image, FlatList, StyleSheet, StatusBar, TouchableOpacity, ScrollView, RefreshControl, Animated, Alert } from 'react-native'
import { Container, Item, Text, Icon, View, Toast, Card, ListItem, Body, List, Thumbnail, Input, CardItem, Left, Right, Button, Content, Spinner, Header, Accordion } from 'native-base'
import moment from 'moment'
import StarRating from 'react-native-star-rating'

const { width } = Dimensions.get('window')
class DestinationSaveListComponent extends Component {
    constructor(props) {
        super(props)
        this.state = {
            savedDestinationList: [],
            isLoading: true,
            user: {},
            refreshing: false,
            rate: 0,
        }
        this._onRefresh = this._onRefresh.bind(this)
        this.sumRate = this.sumRate.bind(this)
    }

    componentWillMount() {
        this.setState({
            user: this.props.user
        })
    }

    componentDidMount() {
        this.props.getSavedDestinationList()
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            savedDestinationList: nextProps.savedDestinationList,
            isLoading: nextProps.isLoading,
        })
    }

    _onRefresh() {
        this.setState({
            refreshing: true,
        })
        this.props.getSavedDestinationList()
        this.setState({
            refreshing: false
        })
    }

    onStarRatingPress(rating) {
        this.setState({
            rate: rating
        })
    }

    sumRate(rateArray) {
        if (_.isEmpty(rateArray)) {
            return 0
        }
        else {
            let averageRate = 0
            rateArray.map((val, key) => {
                averageRate = averageRate + val.rate
            })
            averageRate = averageRate/rateArray.length
            return averageRate
        }
    }

    renderSavedDestinationList(val,key){
        const sumRate = this.sumRate(val.destinationRate)
        return(
            <List key={key}>
                <ListItem>
                    <TouchableHighlight underlayColor='transparent'
                        onPress={() => this.props.navigation.navigate('DestinationDetails', { passDestination: val, user: this.state.user })}
                    >
                        <View style={{ width: width - 30, height: 160, flexDirection: 'row' }}>
                            <Left>
                                <Thumbnail style={{ height: 140, width: 150 }} source={{ uri: val.destinationCoverImage }} />
                            </Left>
                            <Body style={{ alignContent: 'flex-start', alignSelf: 'flex-start', alignItems: 'flex-start',marginTop:10 }}>
                                <Text style={{ fontWeight: 'bold' }}>{val.destinationName}</Text>
                                <Text>
                                    {
                                        val.destinationCategory === 'tour' ? <Text>Type: Tour</Text> :
                                            val.destinationCategory === 'themePark' ? <Text>Type: Theme Park</Text> :
                                                val.destinationCategory === 'museums' ? <Text>Type: Museums</Text> :
                                                    val.destinationCategory === 'shopping' ? <Text>Type: Shopping</Text> :
                                                        val.destinationCategory === 'restaurant' ? <Text>Type: Restaurant</Text> :
                                                            null
                                    }
                                    </Text>
                                <Text>
                                    {
                                        !_.isEmpty(val.destinationTicketPrice) ? 'Ticket: RM' + val.destinationTicketPrice : null
                                    }
                                </Text>
                                <Text>
                                    City : {val.destinationCity}
                                </Text>
                                <Text>Country : {val.destinationCountryName}</Text>
                                <StarRating
                                    disabled={true}
                                    maxStars={5}
                                    rating={sumRate}
                                    fullStarColor={'#00a680'}
                                    starSize={20}
                                    selectedStar={(rating) => this.onStarRatingPress(rating)}
                                />
                                <Text note>{_.isEmpty(val.destinationRate) ? 0 : val.destinationRate.length} reviews</Text>
                            </Body>
                        </View>
                    </TouchableHighlight>
                </ListItem>
            </List>
        )
    }

    render(){
        return(
            <Container>
                 <ScrollView
                    refreshControl={
                        <RefreshControl
                        refreshing={this.state.refreshing}
                        onRefresh={this._onRefresh}
                        />
                    }
                >
                    {

                        this.state.isLoading
                            ?
                            <Spinner />
                            :
                            _.isEmpty(this.state.savedDestinationList) ?
                                <Text style={{ color: 'gray', fontSize: 25, alignSelf: 'center', marginTop: 30 }}>No result</Text> :
                                this.state.savedDestinationList.map((val, key) => {
                                    return this.renderSavedDestinationList(val, key)
                                })
                    }
                </ScrollView>
            </Container>

        )
    }

}

export default DestinationSaveListComponent
