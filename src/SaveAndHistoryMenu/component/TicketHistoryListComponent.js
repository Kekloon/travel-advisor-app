import React, { Component } from 'react'
import { TouchableHighlight, Dimensions, Image, FlatList, StyleSheet, StatusBar, TouchableOpacity, ScrollView, RefreshControl, Animated, Alert } from 'react-native'
import { Container, Item, Text, Icon, View, Toast, Card, ListItem, Body, List, Thumbnail, Input, CardItem, Left, Right, Button, Content, Spinner, Header, Accordion } from 'native-base'
import moment from 'moment'
import _ from 'lodash'

const { width } = Dimensions.get('window')
class TicketHistoryListComponent extends Component {
    constructor(props) {
        super(props)
        this.state = {
            TicketHistoryList: [],
            isLoading: true,
            user: {},
            refreshing: false,
        }
        this._onRefresh = this._onRefresh.bind(this)
    }

    componentWillMount() {
        this.setState({
            user: this.props.user
        })
    }

    componentDidMount() {
        this.props.getTicketHistoryList()
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            TicketHistoryList: _.orderBy(nextProps.TicketHistoryList, ['ticketTransactionDate'], ['desc']),
            isLoading: nextProps.isLoading,
        })
    }

    _onRefresh() {
        this.setState({
            refreshing: true,
        })
        this.props.getTicketHistoryList()
        this.setState({
            refreshing: false,
        })
    }

    renderSavedTicketHistoryList(val, key) {
        return (
            <List key={key}>
                <ListItem>
                    <TouchableHighlight underlayColor='transaparent'
                        onPress={() => this.props.navigation.navigate('TicketHistoryDetails', { passTicketHistory: val, user: this.state.user })}
                    >
                        <View style={{ width: width - 30, flexDirection: 'row' }}>
                            <Left style={{ maxWidth: 50 }}>
                                <Icon style={{ color: '#00a680',fontSize:35 }} type='FontAwesome' name='ticket' />
                            </Left>
                            <Body style={{ alignContent: 'flex-start', alignSelf: 'flex-start', alignItems: 'flex-start' }}>
                                <Text style={{ fontWeight: 'bold' }}>{val.destinationData.destinationName} one day enters ticket</Text>
                                <Text note>Purchase date: {moment(val.ticketTransactionDate).format("DD-MM-YYYY")}</Text>
                                <Text note style={{ color: 'red' }}>Expiry date: {moment(val.ticketTransactionExpireDate).format("DD-MM-YYYY")}</Text>
                            </Body>
                        </View>
                    </TouchableHighlight>
                </ListItem>
            </List>
        )
    }

    render() {
        return (
            <Container>
                <ScrollView
                    refreshControl={
                        <RefreshControl
                            refreshing={this.state.refreshing}
                            onRefresh={this._onRefresh}
                        />
                    }
                >
                    {
                        this.state.isLoading
                            ?
                            <Spinner />
                            :
                            _.isEmpty(this.state.TicketHistoryList) ?
                                <Text style={{ color: 'gray', fontSize: 25, alignSelf: 'center', marginTop: 30 }}>No result</Text> :
                                this.state.TicketHistoryList.map((val, key) => {
                                    return this.renderSavedTicketHistoryList(val, key)
                                })
                    }
                </ScrollView>
            </Container>
        )
    }
}

export default TicketHistoryListComponent