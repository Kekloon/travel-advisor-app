import React, { Component } from 'react'
import { TouchableHighlight, Dimensions, Image, FlatList, StyleSheet, StatusBar, TouchableOpacity, ScrollView, Modal, RefreshControl } from 'react-native'
import { Container, Item, Text, Icon, View, Toast, Card, ListItem, Body, List, Thumbnail, Input, CardItem, Left, Right, Button, Content, Spinner, Header, Picker, Tab, Tabs, TabHeading, ScrollableTab } from 'native-base'
import _ from 'lodash'
import StarRating from 'react-native-star-rating'

import DestinationSaveListContainer from '../container/DestinationSaveListContainer'
import HotelSaveListContainer from '../container/HotelSaveListContainer'
import TicketHistoryListContainer from '../container/TicketHistoryListContainer'
import BookedHotelHistoryContainer from '../container/BookedHotelHistoryContainer'

const { width } = Dimensions.get('window')


class SaveAndHistoryMenuComponent extends Component {
    constructor(props) {
        super(props)
        this.state = {
            user: {},
        }
    }

    componentWillMount() {
        this.setState({
            user: this.props.user
        })
    }

    componentDidMount() {

    }

    render() {
        return (
            <Container>
                <Header style={{ backgroundColor: '#00a680' }} hasTabs>
                    <Left style={{ maxWidth: 30 }}>
                        <Icon type='FontAwesome' name='heart-o' style={{ color: 'white' }} ></Icon>
                    </Left>
                    <Body>
                        <Text style={{ color: 'white', alignSelf: 'center', fontSize: 30 }}>Save & History</Text>
                    </Body>
                    <Right style={{ maxWidth: 30 }}>
                        <Icon type='FontAwesome' name='history' style={{ color: 'white' }} ></Icon>
                    </Right>
                </Header>
                <Tabs renderTabBar={() => <ScrollableTab />}>
                    <Tab heading="Destination Save List" tabStyle={{ backgroundColor: '#00a680' }} textStyle={{ color: '#fff' }} activeTabStyle={{ backgroundColor: '#00a680' }}>
                        <DestinationSaveListContainer user={this.state.user} navigation={this.props.navigation} />
                    </Tab>
                    <Tab heading="Hotel Save List" tabStyle={{ backgroundColor: '#00a680' }} textStyle={{ color: '#fff' }} activeTabStyle={{ backgroundColor: '#00a680' }}>
                        <HotelSaveListContainer user={this.state.user} navigation={this.props.navigation} />
                    </Tab>
                    <Tab heading="Ticket History" tabStyle={{ backgroundColor: '#00a680' }} textStyle={{ color: '#fff' }} activeTabStyle={{ backgroundColor: '#00a680' }}>
                        <TicketHistoryListContainer user={this.state.user} navigation={this.props.navigation} />
                    </Tab>
                    <Tab heading="Hotel History" tabStyle={{ backgroundColor: '#00a680' }} textStyle={{ color: '#fff' }} activeTabStyle={{ backgroundColor: '#00a680' }}>
                    <BookedHotelHistoryContainer user={this.state.user} navigation={this.props.navigation}/>
                    </Tab>
                </Tabs>
            </Container>
        )
    }

}

export default SaveAndHistoryMenuComponent