import React, { Component } from 'react'
import { TouchableHighlight, Dimensions, Image, FlatList, StyleSheet, StatusBar, TouchableOpacity, ScrollView, RefreshControl, Animated, Alert } from 'react-native'
import { Container, Item, Text, Icon, View, Toast, Card, ListItem, Body, List, Thumbnail, Input, CardItem, Left, Right, Button, Content, Spinner, Header, Accordion } from 'native-base'
import moment from 'moment'
import _ from 'lodash'

const { width } = Dimensions.get('window')
class BookedHotelHistoryCoponent extends Component {
    constructor(props){
        super(props)
        this.state={
            BookedHotelHistoryList:[],
            isLoading:true,
            user:{},
            refreshing:false,
        }
        this._onRefresh = this._onRefresh.bind(this)
    }

    componentWillMount(){
        this.setState({
            user:this.props.user
        })
    }

    componentDidMount(){
        this.props.getBookedHotelHistoryList()
    }

    componentWillReceiveProps(nextProps){
        this.setState({
            BookedHotelHistoryList:_.orderBy(nextProps.BookedHotelHistoryList,['hotelTransactionDate'],['desc']),
            isLoading:nextProps.isLoading,
        })
    }

    _onRefresh(){
        this.setState({
            refreshing:true,
        })
        this.props.getBookedHotelHistoryList()
        this.setState({
            refreshing:false,
        })
    }

    renderBookedHotelHistoryList(val,key){
        return(
            <List key={key}>
            <ListItem>
              <TouchableHighlight underlayColor='transaparent'
                        onPress={() => this.props.navigation.navigate('BookedHotelHistoryDetails', { passBookedHotelHistory: val, user: this.state.user })}
                    >
                        <View style={{ width: width - 30, flexDirection: 'row' }}>
                            <Left style={{ maxWidth: 130 }}>
                            <Thumbnail style={{ height: 100, width: 120 }} source={{ uri: val.hotelData.hotelCoverImage }} />
                            </Left>
                            <Body style={{ alignContent: 'flex-start', alignSelf: 'flex-start', alignItems: 'flex-start' }}>
                                <Text style={{ fontWeight: 'bold',marginTop:10 }}>{val.hotelData.hotelName}</Text>
                                <Text note>Book hotel date: {moment(val.hotelTransactionDate).format("DD-MM-YYYY")}</Text>
                                <Text note style={{color:'#00a680'}}>Check in date: {moment(val.hotelTransactionHoteCheckInDate).format("DD-MM-YYYY")}</Text>
                                <Text note style={{ color: 'red' }}>Check out date: {moment(val.hotelTransactionHoteCheckOutDate).format("DD-MM-YYYY")}</Text>
                            </Body>
                        </View>
                    </TouchableHighlight>
                </ListItem>
            </List>
        )
    }
    
    render(){
        return(
            <Container>
            <ScrollView
                refreshControl={
                    <RefreshControl
                        refreshing={this.state.refreshing}
                        onRefresh={this._onRefresh}
                    />
                }
            >
                {
                    this.state.isLoading
                        ?
                        <Spinner />
                        :
                        _.isEmpty(this.state.BookedHotelHistoryList) ?
                            <Text style={{ color: 'gray', fontSize: 25, alignSelf: 'center', marginTop: 30 }}>No result</Text> :
                            this.state.BookedHotelHistoryList.map((val, key) => {
                                return this.renderBookedHotelHistoryList(val, key)
                            })
                }
            </ScrollView>
        </Container>
        )
    }
}

export default BookedHotelHistoryCoponent