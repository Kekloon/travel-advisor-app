
const initialState={
    getSavedDestinationList:{
        savedDestinationList:null,
        errorMessage:null,
        isLoading:false,
    },
    getSavedHotelList:{
        savedHotelList:null,
        errorMessage:null,
        isLoading:false,
    },
    getTicketHistoryList:{
        TicketHistoryList:null,
        errorMessage:null,
        isLoading:false,
    },
    getBookedHotelHistoryList:{
        BookedHotelHistoryList:null,
        errorMessage:null,
        isLoading:false,
    },
}

export default function (state=initialState,action){
    switch(action.type){
        case 'REQUEST_GET_SAVED_DESTINATION_LIST':
        return Object.assign({},state,{
            getSavedDestinationList:{
                isLoading:true,
            }
        })
        case 'SUCCESS_GET_SAVED_DESTINATION_LIST':
        return Object.assign({},state,{
            getSavedDestinationList:{
                savedDestinationList:action.payload,
                isLoading:false,
            },
        })
        case 'FAILED_GET_SAVED_DESTINATION_LIST':
        return Object.assign({},state,{
            getSavedDestinationList:{
                errorMessage:action.err,
                isLoading:false,
            },
        })
        case 'REQUEST_GET_SAVED_HOTEL_LIST':
        return Object.assign({},state,{
            getSavedHotelList:{
                isLoading:true,
            }
        })
        case 'SUCCESS_GET_SAVED_HOTEL_LIST':
        return Object.assign({},state,{
            getSavedHotelList:{
                savedHotelList:action.payload,
                isLoading:false,
            }
        })
        case 'FAILED_GET_SAVED_HOTEL_LIST':
        return Object.assign({},state,{
            getSavedHotelList:{
                isLoading:false,
                errorMessage:action.err,
            }
        })
        case 'REQUEST_GET_TICKET_HISTORY_LIST':
        return Object.assign({},state,{
            getTicketHistoryList:{
                isLoading:true,
            }
        })
        case 'SUCCESS_GET_TICKET_HISTORY_LIST':
        return Object.assign({},state,{
            getTicketHistoryList:{
                TicketHistoryList:action.payload,
                isLoading:false,
            }
        })
        case 'FAILED_GET_TICKET_HISTORY_LIST':
        return Object.assign({},state,{
            getTicketHistoryList:{
                errorMessage:action.err,
                isLoading:false,
            }
        })
        case 'REQUEST_GET_BOOKED_HOTEL_HISTORY_LIST':
        return Object.assign({},state,{
            getBookedHotelHistoryList:{
                isLoading:true,
            }

        })
        case 'SUCCESS_GET_BOOKED_HOTEL_HISTORY_LIST':
        return Object.assign({},state,{
            getBookedHotelHistoryList:{
                BookedHotelHistoryList:action.payload,
                isLoading:false,
            }

        })
        case 'FAILED_GET_BOOKED_HOTEL_HISTORY_LIST':
        return Object.assign({},state,{
            getBookedHotelHistoryList:{
                errorMessage:action.err,
                isLoading:false,
            }
        })
        default:
        return state
    }
}