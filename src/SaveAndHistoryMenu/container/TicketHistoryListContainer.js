import {connect} from 'react-redux'
import TicketHistoryListComponent from '../component/TicketHistoryListComponent'
import {getTicketHistoryList} from '../SaveAndHistoryMenuAction'

const mapStateToProps =(state)=>({
    TicketHistoryList: state.saveAndHistoryReducer.getTicketHistoryList.TicketHistoryList,
    isLoading :state.saveAndHistoryReducer.getTicketHistoryList.isLoading,
})

const mapDispatchToProps =(dispatch)=>({
    getTicketHistoryList:()=>{
        dispatch(getTicketHistoryList())
    }
})

export default connect(mapStateToProps,mapDispatchToProps)(TicketHistoryListComponent)