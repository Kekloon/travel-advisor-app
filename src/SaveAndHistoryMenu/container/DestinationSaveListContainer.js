import { connect } from 'react-redux'
import DestinationSaveListComponent from '../component/DestinationSaveListComponent'
import { getSavedDestinationList } from '../SaveAndHistoryMenuAction'

const mapStateToProps = (state) => ({
    savedDestinationList: state.saveAndHistoryReducer.getSavedDestinationList.savedDestinationList,
    isLoading: state.saveAndHistoryReducer.getSavedDestinationList.isLoading,
})

const mapDispatchToProps = (dispatch) => ({
    getSavedDestinationList: () => {
        dispatch(getSavedDestinationList())
    }
})

export default connect(mapStateToProps, mapDispatchToProps)(DestinationSaveListComponent)