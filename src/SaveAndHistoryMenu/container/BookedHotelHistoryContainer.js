import {connect} from 'react-redux'
import BookedHotelHistoryComponent from '../component/BookedHotelHistoryComponent'
import {getBookedHotelHistoryList} from '../SaveAndHistoryMenuAction'

const mapStateToProps =(state)=>({
    BookedHotelHistoryList: state.saveAndHistoryReducer.getBookedHotelHistoryList.BookedHotelHistoryList,
    isLoading:state.saveAndHistoryReducer.getBookedHotelHistoryList.isLoading
})

const mapDispatchToProps = (dispatch)=>({
    getBookedHotelHistoryList:()=>{
        dispatch(getBookedHotelHistoryList())
    }
})

export default connect(mapStateToProps,mapDispatchToProps)(BookedHotelHistoryComponent)