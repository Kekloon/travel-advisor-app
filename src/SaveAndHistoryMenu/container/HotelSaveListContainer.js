import { connect } from 'react-redux'
import HotelSaveListComponent from '../component/HotelSaveListComponent'
import { getSavedHotelList } from '../SaveAndHistoryMenuAction'

const mapStateToProps = (state) => ({
    savedHotelList: state.saveAndHistoryReducer.getSavedHotelList.savedHotelList,
    isLoading : state.saveAndHistoryReducer.getSavedHotelList.isLoading,
})

const mapDispatchToProps =(dispatch)=>({
    getSavedHotelList:()=>{
        dispatch(getSavedHotelList())
    }
})

export default connect(mapStateToProps,mapDispatchToProps)(HotelSaveListComponent)