import { connect } from 'react-redux'
import SaveAndHistoryMenuComponent from '../component/SaveAndHistoryMenuComponent'

const mapStateToProps=(state)=>({
    user:state.userReducer.user
})

const mapDispatchToProps = (dispatch)=>({

})

export default connect(mapStateToProps,mapDispatchToProps)(SaveAndHistoryMenuComponent)