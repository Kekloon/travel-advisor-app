﻿Welcome to Hopfy!
===================

> **WARNING**: This is a React Native mobile application


Get Started
-------------
Make sure to read the [React Native](https://facebook.github.io/react-native/docs/getting-started.html) docs for environment setup before getting started!

    > git clone https://gitlab.com/hellohopfy/hopfy-app.git
    > yarn or npm i
    > react-native link
    
Install Pods (iOS)

    > brew install cocoapods
    > cd ios && pod install (Takes a while, have some 🍺 )
   
Run Application

    > yarn ios or yarn android
    